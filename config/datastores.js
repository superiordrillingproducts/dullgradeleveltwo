/**
 * Datastores
 * (sails.config.datastores)
 *
 * A set of datastore configurations which tell Sails where to fetch or save
 * data when you execute built-in model methods like `.find()` and `.create()`.
 *
 *  > This file is mainly useful for configuring your development database,
 *  > as well as any additional one-off databases used by individual models.
 *  > Ready to go live?  Head towards `config/env/production.js`.
 *
 * For more information on configuring datastores, check out:
 * https://sailsjs.com/config/datastores
 */

module.exports.datastores = {
  sdpi: {
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/SDPI'
  },
  users:{
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/Users'
  },
  sdptools: {
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/SdpTools'
  },
  thebrain: {
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/TheBrain'
  },
  manu: {
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/manu'
  },
  globalvalues: {
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/GlobalValues'
  },
  repair: {
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/Repair'
  },
  nonsdptools: {
    adapter: require('sails-mysql'),
    url: 'mysql://brain:Sdp112575Sdp%23@67.213.231.201:3307/NonSdpTools'
  },
};

//// Live
//// 162.252.84.66:3306
/// Julie
/// 67.213.231.201:3307