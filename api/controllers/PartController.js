var Q = require('q');

module.exports = {
	getCutterPocketTypes: function(req, res) {
		var promise = CutterService.getCutterPocketTypes();
		promise.then(function(data) {
			res.json(data);
		}).catch(function(err) {
			res.status(500).send(err);
		});
	},
	getCutterPocketSizes: function(req, res) {
		var promise = CutterService.getCutterPocketSizes();
		promise.then(function(sizes) {
			sizes = _.sortBy(sizes, function(size) {
				return size.chrono;
			});
			res.json(sizes);
		}).catch(function(err) {
			res.status(500).send(err);
		});
	},
	getBodyPartsWithCutters: function(req, res) {
		var promise = PartService.getBodyPartsWithCutters();
		promise.then(function(data) {
			res.json(data);
		}).catch(function(err) {
			res.status(err.status).send(err);
		});
	},
	getCutterOrHashStructure: ({body}, res) => {
		let promise;
		console.log(body)

		if(body.hashId) promise = PartService.getHashStructure(body.hashId)
		else if(body.toolRepair && [1,2].indexOf(body.toolFamilyId) !== -1) {
			let toolPartId = body.toolRepair.partIdNewForDNR ? body.toolRepair.partIdNewForDNR : body.toolRepair.partId
			promise = PartService.getPartStructureForNonBitPart(toolPartId, body.toolFamilyId)
		}
		promise.then(data => {
			res.json(data);
		}).catch(err => {
			res.status(err.status).send(err);
		});
	},
	getPocketStructureForPart: function({body}, res) {
		let identifier = [1,2].indexOf(body.toolFamilyId) !== -1 ? body.cutterStructureChronoId : body.partId;
		let promise = PartService.getCutterStructure(identifier, body.toolFamilyId);
		promise.then(function(data) {
			res.json(data);
		}).catch(function(err) {
			res.status(err.status).send(err);
		});
	},
	savePocketStructureForPart: function({body}, res) {
		var promise = PartService.savePocketStructureForPart(body.tool, body.groups);
		promise.then(data => {
			var identifier = body.tool && body.tool.partBakerBitId ? body.tool.partBakerBitId : body.tool.partId;
			console.log('savedPocketStructure')
			// get dullgrades
			var dgPromise = RepairService.getDullGrades(body.tool.repairId);
			dgPromise.then(dullgrades => {
				console.log('========== DULLGRADES ============');
				console.log(dullgrades);
				var promises = [];
				dullgrades.map(dg => {
					console.log('identifier | dg.dullGradeId | body.tool.toolFamilyId');
					console.log(identifier + ' | ' + dg.dullGradeId + ' | ' + body.tool.toolFamilyId)
					promises.push(PartService.alignDullGradesWithMarkup(identifier, dg.dullGradeId, body.tool.toolFamilyId));
				});
				Q.all(promises).then(data => {
					console.log('dullgrades aligned successfully');
					res.json(data);
				}, err => {
					console.log(err);
				})
			}).catch(err => {
				console.log(err);
			});
		}).catch(err => {
			console.log(err);
			res.status(err.status).send(err);
		});
	},
	getCutterPockets: function(req, res) {
		var promise = CutterService.getCutterPockets();
		promise.then(function(data) {
			res.json(data);
		}).catch(function(err) {
			res.status(err.status).send(err);
		});
	},
	getPartsWithAppendix: function(req, res) {
		var promise = PartService.getPartsWithAppendix(req.body.partNumber);
		promise.then((data) => {
			res.json(data);
		}).catch((err) => {
			res.status(err.status).send(err);
		});
	},
	getPartsByFamily: function({body}, res) {
		console.log(body.toolFamilyId);
		if(body.toolFamilyId) {
			var promise = PartService.getPartsByFamily(body.toolFamilyId);
			promise.then(data => res.json(data))
				.catch(err => res.status(err.status).send(err));
		}
	}
};
