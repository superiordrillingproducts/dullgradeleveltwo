module.exports = {
	index: function (req, res) {
		if(req.session && req.session.user) {
			return res.redirect('/dullgradelevelone');
		} else {
			return res.view({
				pagetitle: 'Home',
				layout: 'shared/home-layout'
			});
		}
	},
	carousel: function (req, res) {
		return res.view({
			pagetitle: 'Carousel',
			layout: 'shared/home-layout'
		});
	}
};
