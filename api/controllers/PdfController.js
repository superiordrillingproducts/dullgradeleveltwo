var pdf = require('pdfkit');
var Q = require('q');
var fs = require('fs');
var moment = require('moment');
var nodemailer = require('nodemailer');
// functions
function scaleValues(coords, sizeRatio) {
	_.each(coords, function(coord) {
		if(coord.x) coord.x *= sizeRatio;
		if(coord.y) coord.y *= sizeRatio;
		if(coord.r) coord.r *= sizeRatio;
	});
}
function getStrokeFillInfo(pocket, sizeRatio) {
	var fillStroke = {
		strokeColor: pocket.accent,
		fillColor: pocket.color,
		lineWidth: 0.5
	};
	if(pocket.isFlameSpray) {
		fillStroke.strokeColor = '#000000';
		fillStroke.lineWidth *= 1.5;

	}
	if(sizeRatio) fillStroke.lineWidth *= sizeRatio;
	return fillStroke;
}
function defineStartingXY(coords, currentX, currentY) {
	_.each(coords, function(coord) {
		if(!coord.dontDisplace) {
			coord.x += currentX;
			coord.y += currentY;
		}
	});
}
function createCutter(doc, pocket, sizeRatio, currentX, currentY) {
	var coords = [{x:5, y:5, r:5}];
	scaleValues(coords, sizeRatio);
	var fillStroke = getStrokeFillInfo(pocket, sizeRatio);
	defineStartingXY(coords, currentX, currentY);
	// execute
	var c = coords;
	doc.circle(c[0].x, c[0].y, c[0].r)
		.lineWidth(fillStroke.lineWidth)
		.fillAndStroke(fillStroke.fillColor, fillStroke.strokeColor);
	return {
		startX: currentX,
		startY: currentY,
		endX: c[0].r * 2,
		endY: c[0].r * 2
	};
}
function createDome(doc, pocket, sizeRatio, currentX, currentY) {
	var coords = [{x:0, y:10}, {x:0, y:5}, {x:0, y:0}, {x:5, y:0}, {x:10, y:0}, {x:10, y:5}, {x:10, y:10}];

	scaleValues(coords, sizeRatio);

	var fillStroke = getStrokeFillInfo(pocket, sizeRatio);
	defineStartingXY(coords, currentX, currentY);
	// execute
	var c = coords;
	doc.lineCap('square')
		.moveTo(c[0].x, c[0].y)
		.lineTo(c[1].x, c[1].y)
		.quadraticCurveTo(c[2].x, c[2].y, c[3].x, c[3].y)
		.quadraticCurveTo(c[4].x, c[4].y, c[5].x, c[5].y)
		.lineTo(c[6].x, c[6].y)
		.lineTo(c[0].x, c[0].y)
		.lineWidth(fillStroke.lineWidth)
		.fillAndStroke(fillStroke.fillColor, fillStroke.strokeColor);
	return {
		startX: currentX,
		startY: currentY,
		endX: c[3].x,
		endY: c[1].y
	};
}
function createReplaceableDome(doc, pocket, sizeRatio, currentX, currentY) {
	var coords = [{x:0, y:10}, {x:0, y:5}, {x:0, y:0}, {x:5, y:0}, {x:10, y:0}, {x:10, y:5}, {x:10, y:10}, {x:5, y:8, r:0.25}];
	scaleValues(coords, sizeRatio);
	var fillStroke = getStrokeFillInfo(pocket, sizeRatio);
	defineStartingXY(coords, currentX, currentY);
	// execute
	var c = coords;
	doc.lineCap('square')
		.moveTo(c[0].x, c[0].y)
		.lineTo(c[1].x, c[1].y)
		.quadraticCurveTo(c[2].x, c[2].y, c[3].x, c[3].y)
		.quadraticCurveTo(c[4].x, c[4].y, c[5].x, c[5].y)
		.lineTo(c[6].x, c[6].y)
		.lineTo(c[0].x, c[0].y)
		.lineWidth(fillStroke.lineWidth)
		.fillAndStroke(fillStroke.fillColor, fillStroke.strokeColor);
	doc.circle(c[7].x, c[7].y, c[7].r)
		.lineWidth(1 * sizeRatio)
		.fillAndStroke('#ffffff', '#ffffff');
	return {
		startX: currentX,
		startY: currentY,
		endX: c[3].x,
		endY: c[1].y
	};
}
function createRubbingBlock(doc, pocket, sizeRatio, currentX, currentY) {
	var coords = [{x:0, y:0}, {x:10, y:10, dontDisplace:true}, {r:2}];
	scaleValues(coords, sizeRatio);
	var fillStroke = getStrokeFillInfo(pocket, sizeRatio);
	defineStartingXY(coords, currentX, currentY);
	// execute
	var c = coords;
	console.log('---- rubbing block ----');
	doc.roundedRect(c[0].x, c[0].y, c[1].x, c[1].y, c[2].r)
		.lineWidth(fillStroke.lineWidth)
		.fillAndStroke(fillStroke.fillColor, fillStroke.strokeColor);
	return {
		startX: currentX,
		startY: currentY,
		endX: c[1].x + currentX,
		endY: c[1].y + currentY
	};
}
function createSharktooth(doc, pocket, sizeRatio, currentX, currentY) {
	console.log('make it');
	var coords = [{x:0, y:5}, {x:2.5, y:0}, {x:7.5, y:0}, {x:10, y:5}, {x:10, y:10}, {x:0, y:10}];
	scaleValues(coords, sizeRatio);
	console.log('make it 2');
	var fillStroke = getStrokeFillInfo(pocket, sizeRatio);
	defineStartingXY(coords, currentX, currentY);
	console.log('make it 3');
	// execute
	var c = coords;
	doc.lineCap('square')
		.moveTo(c[0].x, c[0].y)
		.lineTo(c[1].x, c[1].y)
		.lineTo(c[2].x, c[2].y)
		.lineTo(c[3].x, c[3].y)
		.lineTo(c[4].x, c[4].y)
		.lineTo(c[5].x, c[5].y)
		.lineTo(c[0].x, c[0].y)
		.lineWidth(fillStroke.lineWidth)
		.fillAndStroke(fillStroke.fillColor, fillStroke.strokeColor);
	console.log('make it 4');

	return {
		startX: currentX,
		startY: currentY,
		endX: c[4].x,
		endY: c[2].y
	};
}
function createOther(doc, pocket, sizeRatio, currentX, currentY) {
	var coords = [{x:2, y:0}, {x:2, y:2}, {x:0, y:2}, {x:0, y:8}, {x:2, y:8}, {x:2, y:10}, {x:8, y:10}, {x:8, y:8}, {x:10, y:8}, {x:10, y:2}, {x:8, y:2}, {x:8, y:0}];
	scaleValues(coords, sizeRatio);
	var fillStroke = getStrokeFillInfo(pocket, sizeRatio);
	defineStartingXY(coords, currentX, currentY);
	// execute
	var c = coords;
	doc.lineCap('square')
		.moveTo(c[0].x, c[0].y)
		.lineTo(c[1].x, c[1].y)
		.lineTo(c[2].x, c[2].y)
		.lineTo(c[3].x, c[3].y)
		.lineTo(c[4].x, c[4].y)
		.lineTo(c[5].x, c[5].y)
		.lineTo(c[6].x, c[6].y)
		.lineTo(c[7].x, c[7].y)
		.lineTo(c[8].x, c[8].y)
		.lineTo(c[9].x, c[9].y)
		.lineTo(c[10].x, c[10].y)
		.lineTo(c[11].x, c[11].y)
		.lineTo(c[0].x, c[0].y)
		.lineWidth(fillStroke.lineWidth)
		.fillAndStroke(fillStroke.fillColor, fillStroke.strokeColor);
	return {
		startX: currentX,
		startY: currentY,
		endX: c[8].x,
		endY: c[5].y
	};
}

function createNewCutterQtyItem(info, lot, colorClasses, currentX, currentY, ratioSize, left, doc) {
	var circleRadius = 1.75;

	var colorClass = _.find(colorClasses, function(c) {
		return c.class === info.colorClass;
	});
	circleRadius *= ratioSize;
	// create qty badge
	doc.circle(currentX + circleRadius - (0.6*ratioSize), currentY + circleRadius - (0.6*ratioSize), circleRadius)
		.lineWidth(0.1*ratioSize)
		.fillAndStroke(colorClass.color, colorClass.color);
	doc.fillColor('black');
	doc.fontSize(8);
	// create badge text
	doc.text(lot.qty, currentX - (1.4*ratioSize), currentY, {
		width:circleRadius * 3,
		align:'center'
	});

	doc.fillColor('#555555');

	// create label
	var reclaimText = lot.isReclaim ? ' Reclaim' : '';
	doc.text(info.sizeFraction + ' - ' + info.cutterName + reclaimText, currentX + (2*circleRadius), currentY);

}

module.exports = {
	generatePdfCutterPockets: function(req, res) {
		console.log('got it');
		var printConfig = req.body;
		console.log(printConfig);
		if(printConfig) {
			printConfig.toolFamilyId = parseInt(printConfig.toolFamilyId);
			printConfig.partId = parseInt(printConfig.partId);
			printConfig.cutterStructureChronoId = parseInt(printConfig.cutterStructureChronoId);
			printConfig.repairId = parseInt(printConfig.repairId);

			var promises = [];

			var identifier;
	        if (printConfig.toolFamilyId === 4) {
	            // assign partId to identifier
	            identifier = printConfig.partId;
	        } else if (printConfig.toolFamilyId === 1) {
	            // assign cutterStructureChronoId to identifier
	            identifier = printConfig.cutterStructureChronoId;
	        }

			promises.push(RepairService.getDullGradesAndMarkupStructureForTool(identifier, printConfig.toolFamilyId, printConfig.repairId));
			promises.push(RepairService.getCountOfCutterSizeByAction(printConfig.partId, printConfig.repairId));
			promises.push(RepairService.getCountOfFlameSpray(printConfig.partId, printConfig.repairId));
			promises.push(RepairService.getCutterNamesUsedInRepair(printConfig.repairId));
			promises.push(CutterService.getCutters());
			promises.push(RepairService.getAllRepairRecords());

			Q.all(promises).then(function(data) {
				console.log('done with promises');
				var dullgrades = data[0];
				var cutterSizes = data[1];
				var flameSprays = data[2];
				var newCuttersUsed = data[3];
				var cutters = data[4];
				var repairs = data[5];

				var toolRepair = _.find(repairs, function(repair) {
					return repair.repairId === printConfig.repairId;
				});

				var doc = new pdf();

				doc.pipe(res);

				// set size ratio for cutter layout
				ratioSize = 3;
		        var activeColorClasses = [];

		        currentX = 5;
		        currentY = 100;

		        // INIT COLOR AND STYLE
				var colorClasses = [
		            { id: 1, class: 'brightgreen', color: '#00a504', accent: '#007303', available: true }, // brightgreen
		            { id: 2, class: 'red', color: '#db3e4d', accent: '#a7313c', available: true }, // red
		            { id: 3, class: 'orange', color: '#ff977d', accent: '#ba6f5c', available: true }, // orange
		            { id: 4, class: 'purple', color: '#8c3d88', accent: '#6a2f67', available: true }, // purple
		            { id: 5, class: 'realorange', color: '#ff8e1f', accent: '#ce7319', available: true }, // realorange
		            { id: 6, class: 'blue', color: '#3157a7', accent: '#264382', available: true }, // blue
		            { id: 7, class: 'lightpurple', color: '#dc00d1', accent: '#ae00a5', available: true }, // lightpurple
		            { id: 8, class: 'lightblue', color: '#4984ff', accent: '#3967c6', available: true }, // lightblue
		            { id: 9, class: 'brown', color: '#937365', accent: '#654f45', available: true }, // brown
		            { id: 10, class: 'green', color: '#36a692', accent: '#287e6f', available: true }, // green
		            { id: 11, class: 'pink', color: '#e4a9c9', accent: '#a0768d', available: true } // pink
		        ];

				var pocketHeight = 10;
				var pocketWidth = 10;
				var pocketContainerHeight = pocketHeight + 0.5;
				var pocketContainerWidth = pocketWidth + 0.5;

				pocketContainerHeight *= ratioSize;
				pocketContainerWidth *= ratioSize;
				pocketHeight *= ratioSize;
				pocketWidth *= ratioSize;

				var totalPockets = 0;

		        console.log('start looping');
				_.each(dullgrades, function(group) {
					var maxNumberOfPocketsForARow = 0;
					console.log('---group---');
					var bladeCount = 0;
					_.each(group.blades, function(b) {
						console.log('---blade---');
						bladeCount++;
						doc.fillColor('#555555')
							.fontSize(10)
							.text(bladeCount, currentX + ((b.rows.length * pocketContainerWidth)/2) - (1*ratioSize), currentY - (6*ratioSize));
						currentX += (b.rows.length) * pocketContainerWidth;
						var savedY = currentY;
						var rowCount = 0;
						_.each(b.rows, function(r) {
							rowCount++;
							currentX -= pocketContainerWidth;
							doc.fillColor('#555555')
								.fontSize(7)
								.text(rowCount, currentX + (pocketWidth/2) - (0.5*ratioSize), currentY - (3*ratioSize));
							console.log('---row---');
							_.each(r.pockets, function(p) {
								console.log('---pocket---');
								// START: set color classes
						        var colorClass = _.filter(activeColorClasses, function(ac) {
									return p.cutterId === ac.cutterId;
								});
								if (colorClass && colorClass.length > 0) {
									p.colorClass = colorClass[0].class;
									p.color = colorClass[0].color;
									p.accent = colorClass[0].accent;
									colorClass[0].qty++;
								} else {
									// find first availabe color class to use
									var nextClass = _.find(colorClasses, function(cc) {
										return cc.available === true;
									});
									_.each(colorClasses, function(cc) {
										if (cc.id === nextClass.id) {
											cc.available = false;
										}
									});

									p.cutterName = _.find(cutters, function(c) {
										return p.cutterId === c.cutterId;
									}).cutterName;

									var newActiveColorClass = {
										cutterId: p.cutterId,
										class: nextClass.class,
										color: nextClass.color,
										accent: nextClass.accent,
										cutterName: p.cutterName,
										qty: 1
									};
									activeColorClasses.push(newActiveColorClass);
									p.colorClass = newActiveColorClass.class;
									p.color = newActiveColorClass.color;
									p.accent = newActiveColorClass.accent;
								} // END

								console.log('p.cutterPocketTypeId');
								console.log(p.cutterPocketTypeId);
								console.log('x: ' + currentX);
								console.log('y: ' + currentY);

								if(p.cutterPocketTypeId === 1) created = createCutter(doc, p, ratioSize, currentX, currentY);
								if(p.cutterPocketTypeId === 2) created = createDome(doc, p, ratioSize, currentX, currentY);
								if(p.cutterPocketTypeId === 3) created = createReplaceableDome(doc, p, ratioSize, currentX, currentY);
								if(p.cutterPocketTypeId === 4) created = createRubbingBlock(doc, p, ratioSize, currentX, currentY);
								if(p.cutterPocketTypeId === 5) created = createSharktooth(doc, p, ratioSize, currentX, currentY);
								if(p.cutterPocketTypeId === 6) created = createOther(doc, p, ratioSize, currentX, currentY);

								// size background box
								var sizeLabelWidth = 2.5 * ratioSize;
								var sizeLabelHeight = 2 * ratioSize;
								var diff = pocketWidth - sizeLabelWidth;
								doc.lineCap('round')
								   .moveTo(currentX + (diff/2), currentY + (1.5 * ratioSize))
								   .lineTo((currentX + (diff/2)) + sizeLabelWidth, currentY + (1.5 * ratioSize))
								   .lineWidth(2 * ratioSize)
								   .stroke(p.accent);

								// size text
								doc.fillColor('#ffffff')
									.fontSize(7)
									.text(p.sizeFraction, currentX + (diff/2) - (0.5*ratioSize), currentY + (0.8 * ratioSize));
								// dullgrade action text
								doc.fillColor('#ffffff')
									.fontSize(12)
									.text(p.cutterCondition ? p.cutterCondition : '', currentX, currentY + (3.8 * ratioSize));
								// dullgrade condition text
								doc.fillColor('#ffffff')
									.fontSize(12)
									.text(p.cutterAction ? p.cutterAction : '--', currentX + (5.5*ratioSize), currentY + (3.8 * ratioSize));



								totalPockets++;
								console.log('end');

								currentY += pocketContainerHeight;
								//setColorsOnClearoutLegend
							});
							currentY = savedY;
						});
						currentX += (b.rows.length) * pocketContainerWidth + 7;
					});
				});

				console.log('Total Pockets: ' + totalPockets);

				var currentX = 5;
				var currentY = 5;

				// CREATE TOOL REPAIR INFO SECTION
				/////////////////////////////
				var ratioSize = 3;
				// serial block
				doc.lineCap('square')
					.moveTo(currentX, currentY)
					.lineTo(currentX + (40*ratioSize), currentY)
					.lineTo(currentX + (40*ratioSize), currentY + (5*ratioSize))
					.lineTo(currentX, currentY + (5*ratioSize))
					.lineTo(currentX, currentY)
					.fillAndStroke('#555555', '#555555');
				// serial text
				doc.fillColor('#ffffff')
					.fontSize(15)
					.text(toolRepair.serial, currentX, currentY + (0.9*ratioSize), {
						width: currentX + (40*ratioSize),
						align: 'center'
					});

				currentY += (8*ratioSize);
				var xLeft = currentX;
				var xRight = currentX + (9*ratioSize);

				// size
				doc.fillColor('#cccccc')
					.fontSize(8)
					.text('Size', xLeft, currentY);
				doc.fillColor('#555555')
					.fontSize(8)
					.text(toolRepair.cuttingDiameterDecimal, xRight, currentY);
				// style
				doc.fillColor('#cccccc')
					.fontSize(8)
					.text('Style', xLeft, currentY + (3*ratioSize));
				doc.fillColor('#555555')
					.fontSize(8)
					.text(toolRepair.style, xRight, currentY + (3*ratioSize));
				// part
				doc.fillColor('#cccccc')
					.fontSize(8)
					.text('Part', xLeft, currentY + (6*ratioSize));
				doc.fillColor('#555555')
					.fontSize(8)
					.text(toolRepair.partNumber, xRight, currentY + (6*ratioSize));
				// city
				doc.fillColor('#cccccc')
					.fontSize(8)
					.text('City', xLeft, currentY + (9*ratioSize));
				doc.fillColor('#555555')
					.fontSize(8)
					.text(toolRepair.stockPointReceivedFromName, xRight, currentY + (9*ratioSize));
				// order
				doc.fillColor('#cccccc')
					.fontSize(8)
					.text('Order', xLeft, currentY + (12*ratioSize));
				doc.fillColor('#555555')
					.fontSize(8)
					.text('', xLeft, currentY + (12*ratioSize));
				//////////////
				// END SECTION

		        // set color classes on legend
				_.each(newCuttersUsed, function(cn) {
		            _.each(activeColorClasses, function(acc) {
		                if(cn.cutterId === acc.cutterId) {
		                    var thisCutter = _.find(cutters, function(cutter) {
		                        return cutter.cutterId === cn.cutterId;
		                    });
		                    cn.cutterName = acc.cutterName;
		                    cn.colorClass = acc.class;
		                    cn.sizeFraction = thisCutter.sizeFraction;
		                }
		            });
		        });

		        console.log('===================');

		        console.log(newCuttersUsed);

				// CREATE NEW CUTTER SECTION
				////////////////////////////
				currentX += (45*ratioSize);
				currentY = 5;
				// header
				doc.fontSize(9)
					.fillColor('#555555')
					.text('QTY - NEW CUTTERS', currentX, currentY, {
						width: 80*ratioSize,
						align: 'center'
					});
				var left = true;
				xLeft = currentX;
				xRight = currentX + (40*ratioSize);
				_.each(newCuttersUsed, function(info) {
					console.log('hello');
					if(left) {
						currentY += (3*ratioSize);
						currentX = xLeft;
					}
					else currentX = xRight;
					createNewCutterQtyItem(info, colorClasses, currentX, currentY, ratioSize, left, doc);
					if(left) left = false;
					else left = true;
				});
				//////////////
				// END SECTION

				doc.end();
			}, function(err) {
				return res.status(500).send(err);
			});
		}
	},
	generateDNRPdfCutterPockets: function(req, res) {
		var printConfig = req.body;
		if(printConfig) {
			var updateShipToStockPointRepair = {repairId: printConfig.repairId, stockPointShipToId: printConfig.stockPointShipToId}
			var shipStockPointPromise = (RepairService.updateRepair(updateShipToStockPointRepair))
			shipStockPointPromise.then(function(updatedRepair){
				printConfig.toolFamilyId = parseInt(printConfig.toolFamilyId);
				printConfig.partId = parseInt(printConfig.partId);
				printConfig.hashId = parseInt(printConfig.hashId);
				printConfig.repairId = parseInt(printConfig.repairId);

				var newCuttersUsed = printConfig.newCuttersUsed

				var promises = [];

				var identifier = printConfig.partId

				promises.push(RepairService.getDullGradesAndMarkupStructureForTool(identifier, printConfig.toolFamilyId, printConfig.repairId, printConfig.hashId));
				promises.push(CutterService.getCutters());
				promises.push(RepairService.getAllRepairRecords());

				Q.all(promises).then(function(data) {

					var dullgrades = data[0];
					var cutters = data[1];
					var repairs = data[2];


					var toolRepair = _.find(repairs, function(repair) {
						return repair.repairId === printConfig.repairId;
					});

					var doc = new pdf({
						layout: 'landscape',
						margins: {top:5, bottom: 5, left: 10, right: 10}
					});

					doc.pipe(res);

					// set size ratio for cutter layout
					let rowCountRatio = 0
					_.each(dullgrades, function(group){
						_.each(group.blades, function(b){
							_.each(b.rows, function(r){
								rowCountRatio++
							})
						})
					})

					if(rowCountRatio > 14 && rowCountRatio < 17){
						ratioSize = 2.5;
					} else if (rowCountRatio >= 17) {
						ratioSize = 2;
					} else {
						ratioSize = 3;
					}
			        var activeColorClasses = [];

			        currentX = 20;
			        currentY = 80;

			        // INIT COLOR AND STYLE
					var colorClasses = [
			            { id: 1, class: 'brightgreen', color: '#56bf58', accent: '#007303', available: true }, // brightgreen
			            { id: 2, class: 'red', color: '#f5b3b9', accent: '#a7313c', available: true }, // red
			            { id: 3, class: 'orange', color: '#ff977d', accent: '#ba6f5c', available: true }, // orange
			            { id: 4, class: 'purple', color: '#a694a5', accent: '#6a2f67', available: true }, // purple
			            { id: 5, class: 'realorange', color: '#ffc891', accent: '#ce7319', available: true }, // realorange
			            { id: 6, class: 'blue', color: '#8e9dbf', accent: '#264382', available: true }, // blue
			            { id: 7, class: 'lightpurple', color: '#f56eee', accent: '#ae00a5', available: true }, // lightpurple
			            { id: 8, class: 'lightblue', color: '#bdd2ff', accent: '#3967c6', available: true }, // lightblue
			            { id: 9, class: 'brown', color: '#ad968c', accent: '#654f45', available: true }, // brown
			            { id: 10, class: 'green', color: '#95bfb8', accent: '#287e6f', available: true }, // green
			            { id: 11, class: 'pink', color: '#e4a9c9', accent: '#a0768d', available: true } // pink
			        ];

					var pocketHeight = 10;
					var pocketWidth = 10;
					var pocketContainerHeight = pocketHeight + 0.5;
					var pocketContainerWidth = pocketWidth + 0.5;

					pocketContainerHeight *= ratioSize;
					pocketContainerWidth *= ratioSize;
					pocketHeight *= ratioSize;
					pocketWidth *= ratioSize;

					var totalPockets = 0;

					_.each(dullgrades, function(group) {
						var maxNumberOfPocketsForARow = 0;
						var bladeCount = 0;
						_.each(group.blades, function(b) {
							bladeCount++;
							doc.fillColor('#555555')
							doc.font('Helvetica-Bold')
								.fontSize(10)
								.text(bladeCount, currentX + ((pocketContainerWidth)/2) - (10*ratioSize), currentY - (3/ratioSize));
							doc.font('Helvetica')
							currentY += (b.rows.length) * pocketContainerWidth;
							var savedX = currentX;
							var rowCount = 0;
							_.each(b.rows, function(r) {
								rowCount++;
								currentY -= pocketContainerWidth;
								doc.fillColor('#555555')
									.fontSize(7)
									.text(rowCount, currentX + (pocketWidth/4) - (5*ratioSize), currentY + (4*ratioSize));
								_.each(r.pockets, function(p) {
									// START: set color classes
							        var colorClass = _.filter(activeColorClasses, function(ac) {
										return p.cutterId === ac.cutterId;
									});
									if (colorClass && colorClass.length > 0) {
										p.colorClass = colorClass[0].class;
										p.color = colorClass[0].color;
										p.accent = colorClass[0].accent;
										colorClass[0].qty++;
									} else {
										// find first availabe color class to use
										var nextClass = _.find(colorClasses, function(cc) {
											return cc.available === true;
										});
										_.each(colorClasses, function(cc) {
											if (cc.id === nextClass.id) {
												cc.available = false;
											}
										});

										p.sdpPartNumber = _.find(cutters, function(c) {
											return p.cutterId === c.cutterId;
										}).sdpPartNumber;

										var newActiveColorClass = {
											cutterId: p.cutterId,
											class: nextClass.class,
											color: nextClass.color,
											accent: nextClass.accent,
											cutterName: p.sdpPartNumber,
											qty: 1
										};
										activeColorClasses.push(newActiveColorClass);
										p.colorClass = newActiveColorClass.class;
										p.color = newActiveColorClass.color;
										p.accent = newActiveColorClass.accent;
									} // END

									if(p.cutterPocketTypeId === 1) created = createCutter(doc, p, ratioSize, currentX, currentY);
									if(p.cutterPocketTypeId === 2) created = createDome(doc, p, ratioSize, currentX, currentY);
									if(p.cutterPocketTypeId === 3) created = createReplaceableDome(doc, p, ratioSize, currentX, currentY);
									if(p.cutterPocketTypeId === 4) created = createRubbingBlock(doc, p, ratioSize, currentX, currentY);
									if(p.cutterPocketTypeId === 5) created = createSharktooth(doc, p, ratioSize, currentX, currentY);
									if(p.cutterPocketTypeId === 6) created = createOther(doc, p, ratioSize, currentX, currentY);

									// size background box
									var sizeLabelWidth = 2.5 * ratioSize;
									var sizeLabelHeight = 2 * ratioSize;
									var diff = pocketWidth - sizeLabelWidth;
									doc.lineCap('round')
									   .moveTo(currentX + (diff/2), currentY + (1.5 * ratioSize))
									   .lineTo((currentX + (diff/2)) + sizeLabelWidth, currentY + (1.5 * ratioSize))
									   .lineWidth(2 * ratioSize)
									   .stroke(p.accent);

									if(rowCountRatio > 14){
	   									sizeFractionFontSize = 5
										actionConditionFontSize = 8
	   								} else {
										sizeFractionFontSize = 7
										actionConditionFontSize = 12
	   								}

									// size text
									doc.fillColor('#ffffff')
										.fillColor ('black')
										.fontSize(sizeFractionFontSize)
										.text(p.sizeFraction, currentX + (diff/2) - (0.5*ratioSize), currentY + (0.8 * ratioSize));
									// dullgrade action text
									doc.fillColor('#ffffff')
										.fillColor ('black')
										.fontSize(actionConditionFontSize)
										.text(p.cutterCondition ? p.cutterCondition : '', currentX, currentY + (3.8 * ratioSize));
									// dullgrade condition text
									doc.fillColor('#ffffff')
										.fillColor ('black')
										.fontSize(actionConditionFontSize)
										.text(p.cutterAction ? p.cutterAction : '--', currentX + (5.5*ratioSize), currentY + (3.8 * ratioSize));



									totalPockets++;

									currentX += pocketContainerHeight;
									//setColorsOnClearoutLegend
								});
								currentX = savedX;
							});
							currentY += (b.rows.length) * pocketContainerWidth + 7;
						});
					});

					var currentX = 5;
					var currentY = 5;

					// CREATE TOOL REPAIR INFO SECTION
					/////////////////////////////
					ratioSize = 3;
					// serial block
					doc.lineCap('square')
						.moveTo(currentX, currentY)
						.lineTo(currentX + (40*ratioSize), currentY)
						.lineTo(currentX + (40*ratioSize), currentY + (5*ratioSize))
						.lineTo(currentX, currentY + (5*ratioSize))
						.lineTo(currentX, currentY)
						.fillAndStroke('#555555', '#555555');
					// serial text
					doc.fillColor('#ffffff')
						.fontSize(15)
						.text(toolRepair.serial, currentX, currentY + (0.9*ratioSize), {
							width: currentX + (40*ratioSize),
							align: 'center'
						});

					currentY += (8*ratioSize);
					var xLeft = currentX;
					var xRight = currentX + (9*ratioSize);

					// size
					doc.fillColor('#555555')
						.fontSize(8)
						.text('Size', xLeft, currentY);
					doc.fillColor('#555555')
						.fontSize(8)
						.text(toolRepair.cuttingDiameterFraction, xRight + (12*ratioSize), currentY);
					// box thread name
					doc.fillColor('#555555')
						.fontSize(8)
						.text('Box Thread: ', xLeft, currentY + (4*ratioSize));
					doc.fillColor('#555555')
						.fontSize(8)
						.text(printConfig.boxThreadConnection, xRight + (12*ratioSize), currentY + (4*ratioSize));
					// pin thread name
					doc.fillColor('#555555')
						.fontSize(8)
						.text('Pin Thread: ', xLeft, currentY + (8*ratioSize));
					doc.fillColor('#555555')
						.fontSize(8)
						.text(printConfig.pinThreadConnection, xRight + (12*ratioSize), currentY + (8*ratioSize));
					// city
					doc.fillColor('#555555')
						.fontSize(8)
						.text('Received From: ', xLeft, currentY + (12*ratioSize));
					doc.fillColor('#555555')
						.fontSize(8)
						.text(printConfig.receivedFromStockPoint, xRight + (12*ratioSize), currentY + (12*ratioSize));
					//////////////
					// END SECTION

			        // set color classes on legend
					_.each(newCuttersUsed, function(cn) {
			            _.each(activeColorClasses, function(acc) {
			                if(cn.cutterId === acc.cutterId) {
			                    var thisCutter = _.find(cutters, function(cutter) {
			                        return cutter.cutterId === cn.cutterId;
			                    });
			                    cn.cutterName = thisCutter.sdpPartNumber;
			                    cn.colorClass = acc.class;
			                    cn.sizeFraction = thisCutter.sizeFraction;
								cn.count = cn.newQty
			                }
			            });
			        });

					// CREATE NEW CUTTER SECTION
					////////////////////////////
					currentX += (45*ratioSize);
					currentY = 5;
					// header
					doc.fontSize(9)
						.fillColor('#555555')
						.text('QTY - NEW CUTTERS', currentX - (10*ratioSize), currentY, {
							width: 80*ratioSize,
							align: 'center'
						});
					var left = true;
					xLeft = currentX;
					xRight = currentX + (40*ratioSize);
					_.each(newCuttersUsed, function(info) {
						_.each(info.lots, function(lot){
							if (left) {
								currentY += (4 * ratioSize);
								currentX = xLeft;
							} else currentX = xRight;
							createNewCutterQtyItem(info, lot, colorClasses, currentX, currentY, ratioSize, left, doc);
							if (left) left = false;
							else left = true;
						});
					});
					//////////////
					// END SECTION

					currentX = 395
					currentY = 5
					xLeft = currentX;
					xRight = currentX + (32*ratioSize);
					var dateArrived = moment(printConfig.receivedDate).format("MMM D, YYYY")
					var estShipDate = moment(printConfig.extraInfo.estimatedShipDate).format("MMM D, YYYY")
					var costOfRepair = printConfig.extraInfo.costOfRepair
					// date received
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Date Received: ', xLeft, currentY);
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text(dateArrived, xRight, currentY);
					// facility received
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Facility Received: ', xLeft, currentY + (5 * ratioSize));
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text(req.session.facility.facilityName, xRight, currentY + (5 * ratioSize));
					// est ship date
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Estimated Ship Date: ', xLeft, currentY + (10*ratioSize));
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text(estShipDate, xRight, currentY + (10*ratioSize));
					// ship to stock point
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Ship To: ', xLeft, currentY + (15*ratioSize));
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text(printConfig.shipToStockPoint, xRight, currentY + (15*ratioSize));
					// repair cost
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Repair Cost: ', xLeft, currentY + (20*ratioSize));
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text('$' + costOfRepair.toFixed(2), xRight, currentY + (20*ratioSize));
					// reference Number
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Reference #: ', xLeft + (80 * ratioSize), currentY);
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text(printConfig.extraInfo.referenceNum, xRight + (70 * ratioSize), currentY);
					// magnaflux cost
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Magnaflux: ', xLeft + (80 * ratioSize), currentY + (5 * ratioSize));
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text('$80.00', xRight + (70 * ratioSize), currentY + (5 * ratioSize));
					// comments
					doc.fillColor('#555555')
						.fontSize(9)
						.text('Comments: ', xLeft + (80 * ratioSize), currentY + (10 * ratioSize));
					doc.fillColor('#555555')
						.fontSize(9.5)
						.text(printConfig.extraInfo.extraComments, xRight + (70 * ratioSize), currentY + (10 * ratioSize));
					//////////////
					// END SECTION
					doc.moveTo(0, 75)
						.strokeColor('#555555')
	   					.lineTo(828,75)
	   					.stroke()

					doc.moveTo(133, 0)
						.strokeColor('#555555')
						.lineTo(133, 75)
						.stroke()

					doc.moveTo(385, 0)
						.strokeColor('#555555')
						.lineTo(385, 75)
						.stroke()

					var buffers = [];
					doc.on('data', buffers.push.bind(buffers));
					doc.on('end', () => {
	    				let pdfData = Buffer.concat(buffers);
					
						var transporter = nodemailer.createTransport({
							host: 'smtp.gmail.com',
							port: 465,
							secure: true, // secure:true for port 465, secure:false for port 587
							auth: {
								user: 'chuck@teamsdp.com',
								pass: '1vocalp1'
							}
						});
						var maillist = "shawn@teamsdp.com,annette@teamsdp.com";
	    				const mailOptions = {
	        				from: 'chuck@teamsdp.com',
	        				to: maillist,
	        				attachments: [{
	            				filename: 'attachment.pdf',
	            				content: pdfData
	        				}]
	    				};
					
	    				mailOptions.subject = 'Repair PO';
	    				mailOptions.text = 'Repair PO';
	    				return transporter.sendMail(mailOptions).then(() => {
	        				console.log('email sent:');
	    				}).catch(error => {
	        				console.error('There was an error while sending the email:', error);
	    				});
					});

					 doc.end();
				}, function(err) {
					return res.status(500).send(err);
				});
			})
		}
	}
};
