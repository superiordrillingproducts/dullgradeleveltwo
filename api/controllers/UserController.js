var Q = require('q');

module.exports = {
	authenticateToken: function (req, res) {
		if (req.session && req.session.user) {
			var token = req.session.user.token;
			UserService.isTokenAuthentic(token, function (err, response) {
				if (!err) {
					return res.json(response);
				} else {
					return res.status(500).send(err);
				}
			});
		}
	},
	getLoggedInUser: function (req, res) {
		if (req.session && req.session.user) {
			return res.json(req.session.user);
		} else {
			return res.json('{}');
		}
	},
	login: function (req, res) {
		var view = {};
		if (req.body) {
			var credentials = req.body;
			
			// req.session.redirectUrl = req.body && req.body.fragment ? req.session.redirectUrl + req.body.fragment : req.session.redirectUrl;

			if (credentials.username && credentials.password) {
				UserService.authenticateUserAndReturnUser(credentials.username, credentials.password, function (error, user) {
					if (!error && user) {
						delete user.password;

						req.session.user = user;

						if (req.session.redirectUrl) {
							return res.redirect(req.session.redirectUrl);
						} else {
							return res.redirect('/dullgradelevelone');
						}
					} else {
						view.message = 'Invalid Credentials.';
						view.messageType = 'danger';
						req.session.message = 'Invalid login.';
						req.session.messageType = 'danger';
						return res.redirect('/dullgradelevelone');
					}
				});
			} else {
				view.message = 'Username and password required.';
				view.messageType = 'danger';
				return res.view({
					pagetitle: 'Login',
					message: view.message,
					messageType: view.messageType,
					layout: 'shared/dullgradelevelone-layout'
				});
			}
		} else {
			var message = typeof req.session.message !== 'undefined' ? req.session.message : undefined;
			var messageType = typeof req.session.messageType !== 'undefined' ? req.session.messageType : undefined;

			req.session.programId = 2;

			delete req.session.message;
			delete req.session.messageType;

			var facilityId = req.session === 'undefined' ? 'undefined' : req.session.facilityId;
			var promise = UserService.getFacilityManagers(facilityId);
			promise.then(function (managers) {
				var allManagers = [];
				_.each(managers, function (m) {
					allManagers.push(m[0]);
				});
				return res.view({
					message: message,
					messageType: messageType,
					pagetitle: 'Login',
					managers: allManagers
				});
			}).catch(function (err) {
				return res.view({
					pagetitle: 'Login'
				});
			});
		}
	},
	logout: function (req, res) {
		req.session.destroy(function (err) {
			return res.view({
				pagetitle: 'You Logged Out',
				layout: 'shared/dullgradelevelone-layout'
			});
		});
	},
	setfacilityandworkstation: function (req, res) {
		if (!req.body) {
			var promise = RepairService.getFacilities(req.session.programId);
			promise.then(function (facilities) {
				if(req && req.session && req.session.user && req.session.user.groups && req.session.user.groups.length > 0) {
					let facs = []
					for (let group of req.session.user.groups) {
						for (let fac of facilities) {
							if(group.groupId === fac.groupIdForManagers)
								facs.push(fac)
						}
					}
					return res.view({
						pagetitle: 'Set Facility & WorkStation',
						facilities: facs
					});
				} else {
					return res.redirect('/user/login')
				}

			}).catch(function (err) {
				return res.view({
					pageTitle: 'Set Facility & WorkStation',
				});
			});
		} else {
			var promises = [];
			promises.push(RepairService.getFacility(req.body.facility));
			promises.push(RepairService.getWorkStation(req.body.workstation));
			Q.all(promises).then(function (data) {
				data = Global.convert2dArrayTo1dArray(data);
				if (data.length > 1) {
					if (data[0].facilityId) req.session.facility = data[0];
					if (data[1].workStationId) req.session.workStation = data[1];
					if (!req.session.redirectUrl) req.session.redirectUrl = '/dullgradelevelone';
					return res.redirect(req.session.redirectUrl + req.session.workStation.url);
				} else {
					return res.status(500).send({
						error: 'Had trouble getting both facility and workstation based on information provided.'
					});
				}
			}, function (err) {
				return res.status(err.status).send(err);
			});
		};
	},
	getWorkStationsByFacility: function (req, res) {
		if (req.body) {
			var p = RepairService.getAllWorkStationsForFacility(req.body.facilityId);
			p.then(function (workStations) {
				return res.json(workStations);
			}).catch(function (err) {
			});
		} else {
			var facilityId = req.session.facility.facilityId;
			var p = RepairService.getAllWorkStationsForFacility(facilityId);
			p.then(function (workStations) {
				return res.json(workStations);
			}).catch(function (err) {
			});
		}
	},
	managerLogout: function (req, res) {
		var body = req.body;
		if (req.session.user && body.password) {
			UserService.authenticateUserAndReturnUser(req.session.user.email, body.password, function (error, user) {
				if (!error && user) {
					req.session.destroy(function (err) {
						return res.json('success');
					});
				} else {
					return res.status(500).send(error);
				}
			});
		}
	},
	checkManagerCredentials: function (req, res) {
		var body = req.body;
		if (req.session.user && body.password) {
			UserService.authenticateUserAndReturnUser(req.session.user.email, body.password, function (error, user) {
				if (!error && user) {
					var sendUserBack = {
						userId: user.userId,
						nameFirst: user.nameFirst,
						nameLast: user.nameLast,
						phoneCell: user.phoneCell,
						phoneHome: user.phoneHome,
						email: user.email,
						userName: user.userName,
						userAvatarUrl: user.userAvatarUrl
					}
					return res.json(sendUserBack);
				} else {
					return res.status(500).send(error);
				}
			});
		}
	},
	redirectToCorrectUrl: function (req, res) {
		var data = {
			fullUrl: req.session.redirectUrl + req.session.workStation.url,
			workStationUrl: req.session.workStation.url
		}
		return res.json(data);
	},
};
