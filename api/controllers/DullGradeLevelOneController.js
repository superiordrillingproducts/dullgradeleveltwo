const RepairService = require('../services/RepairService');
const ToolService = require ('../services/ToolService');

var path = require('path');
var jimp = require('jimp');
var size = require('image-size');
var json2csv = require('json2csv');
var fs = require('fs');
var Q = require('q');
var nodemailer = require('nodemailer');
var PDFDocument = require('pdfkit');

let dullGradePromiseCount = 0;
function updateDullGradePromise (qty, position, dullGrades, dullGradeId, cb) {
	dullGradePromiseCount = 0;
	let promises = [];
	let loopCount = Math.ceil(dullGrades.length/35);
	for (let i = position; i < qty; i++) {
		promises.push(RepairService.saveUpdateDullgrade(dullGrades[i], dullGradeId))
		position = i
	}
	Q.all(promises).then(function(data) {
		dullGradePromiseCount++
		if(loopCount === dullGradePromiseCount){
			dullGradePromiseCount = 0;
			cb(data);
		} else if (dullGradePromiseCount < loopCount) {
			let newQty = dullGrades.length - position > 35 ? qty + position + 1 : dullGrades.length - position + qty - 1
			updateDullGradePromiseSecond(newQty, qty, dullGrades, dullGradeId, cb)
		} else {
			return res.status(500).send("The loop is not working.")
		}
	}, function(err) {
		return res.status(500).send(err);
	})
}
function updateDullGradePromiseSecond(qty, position, dullGrades, dullGradeId, cb) {
	let promises = [];
	let loopCount = Math.ceil(dullGrades.length / 35);
	for (let i = position; i < qty; i++) {
		promises.push(RepairService.saveUpdateDullgrade(dullGrades[i], dullGradeId))
		position = i
	}
	Q.all(promises).then(function (data) {
		dullGradePromiseCount++
		if (loopCount === dullGradePromiseCount) {
			dullGradePromiseCount = 0;
			cb(data);
		} else if (dullGradePromiseCount < loopCount) {
			let newQty = dullGrades.length - position > 35 ? qty + position + 1 : dullGrades.length - position + qty - 1
			updateDullGradePromiseSecond(newQty, qty, dullGrades, dullGradeId, cb)
		} else {
			return res.status(500).send("The loop is not working.")
		}
	}, function (err) {
		return res.status(500).send(err);
	})
}

module.exports = {
	
	index: function (req, res) {
		return res.view({
			pagetitle: 'Dull Grade Level One Home',
			manager: req.session.user,
			facility: req.session.facility
		});
	},
	workstation: (req, res) => {
		if(req.session.facility) {
			RepairService.getAllWorkStationsForFacility(req.session.facility.facilityId).then(workStations => {
				return res.view({
					pagetitle: 'Choose Workstation',
					manager: req.session.user,
					facility: req.session.facility,
					workStations: workStations,
					currentWorkStation: req.session.workStation,
				})
			})
		} else return res.redirect('/user/setfacilityandworkstation')
	},
	getAllRepairRecords: function (req, res) {
		var promise = RepairService.getAllRepairRecords();
		promise.then(function (repairs) {
			return res.json(repairs);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	getAllOpenRepairRecords: function (req, res) {
		var promise = RepairService.getAllOpenRepairRecords();
		promise.then(function (repairs) {
			return res.json(repairs);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	addRepairRecordAndPutIntoDullGrade: function (req, res) {
		var body = req.body;
		RepairService.addRepairRecordAndPutIntoDullGrade(body.toolObject, function (err, repair) {
			if (!err) return res.json(repair);
			else return res.status(500).send(err);
		});
	},
	changeToolRepairLocationTo: function (req, res) {
		var body = req.body;
		RepairService.changeToolRepairLocationTo(body.repairLocationChangeObject, function (err, repair) {
			if (!err) return res.json(repair);
			else return res.status(500).send(err);
		});

	},
	get8InchFractions: function (req, res) {
		RepairService.get8InchFractions(function (err, c8inches) {
			if (!err) return res.json(c8inches);
			else return res.status(500).send(err);
		});
	},
	getWholeNumbers: function (req, res) {
		RepairService.getWholeNumbers(function (err, wholeNumbers){
			if (!err) return res.json(wholeNumbers);
			else return res.status(500).send(err);
		});
	},
	getArrivingTools: function (req, res) {
		ToolService.getArrivingTools(function (err, tools) {
			if (!err) return res.send(tools);
			else return res.status(500).send(err);
		});
	},
	getReceivingTools: function (req, res) {
		var p = ToolService.getReceivingTools(req);
		p.then(function (tools) {
			return res.json(tools);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	getNewTools: function (req, res) {
		var toolsJustAssembledPromise = ToolService.getToolsJustAssembled();
		toolsJustAssembledPromise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	createWorkOrder: function (req, res) {
		var body = req.body;

		delete body.workOrderType;
		var createWorkOrderPromise = WorkOrderService.createWorkOrder(body);
		createWorkOrderPromise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	createRepair: function (req, res) {
		var body = req.body;
		var createRepairPromise;
		var haveUpdatedCount = 0;
		var firstPromises = [];
		var flowToSendTool;
		// if tool does not have a toolId and is a nonSdpTool, then create it
		if (body.receivingFor === 'evaluation') {
			_.each(body.tool, function (t) {
				if (t.toolId) {
					var repair = {};
					if (req.session.facility.facilityId === 3) {
						flowToSendTool = 76;
					} else if (req.session.facility.facilityId === 4) {
						flowToSendTool = 85;
					} else if (req.session.facility.facilityId === 5) {
						flowToSendTool = 86;
					} else if (req.session.facility.facilityId === 8) {
						flowToSendTool = 124;
					} else if (req.session.facility.facilityId === 9) {
						flowToSendTool = 130;
					} else if (req.session.facility.facilityId === 10) {
						flowToSendTool = 136;
					}

					// create repair for SDP
					repair = {
						toolId: t.toolId,
						partIdNewForDNR: t.partIdNewForDNR,
						hashId: t.hashId,
						stockPointReceivedFromId: t.stockPointId,
						customerId: t.customerId,
						customerOwned: t.customerOwned
					}
					firstPromises.push(RepairService.createRepair(repair, req.session.user, body.ancillaryInformation, req.session.facility, true, flowToSendTool));
				} else {

					// if repairCustomerId does not exist, then assign one; default to RepairCustomer "Baker Hughes"
					body.tool.repairCustomerId = !body.tool.repairCustomerId ? 3 : body.tool.repairCustomerId;

					// create tool and repair for non sdp tool
					body.tool.toolFamilyId = body.ancillaryInformation.toolFamilyId;
					// ToolService.createOrUpdateNonSdpTool will not actually create the tool if body.tool.toolId exists, which means that a new tool does not need to be created.
					// but it will update the tool and create the necessary history records for non sdp tool changes.
					var createOrUpdateNonSdpToolPromise = ToolService.createOrUpdateNonSdpTool(body.tool, body.users);
					createOrUpdateNonSdpToolPromise.then(function (nonSdpTool) {
						createRepairPromise = RepairService.createRepair(nonSdpTool, body.users, body.ancillaryInformation, req.session.facility, false);
						createRepairPromise.then(function (data) {
							sails.sockets.blast('repair', data, req);
							return res.json(data);
						}).catch(function (err) {
							return res.status(err.status).send(err);
						});
					}).catch(function (err) {
						return res.status(err.status).send(err);
					});
				}
			});
			Q.all(firstPromises).then(function (updatedTool) {
				var secondPromise = [];
				var toolUpdate = {};
				var stockPointId;
				var toolStatusId;
				_.each(updatedTool, function (ut) {
					if (req.session.facility.facilityId === 3) {
						toolStatusId = 10;
						stockPointId = 26;
						toolUpdate = {
							toolId: ut.toolId,
							toolStatusId: toolStatusId,
							stockPointId: stockPointId
						}
					} else if (req.session.facility.facilityId === 4) {
						toolStatusId = 10;
						stockPointId = 30;
						toolUpdate = {
							toolId: ut.toolId,
							toolStatusId: toolStatusId,
							stockPointId: stockPointId
						}
					} else if (req.session.facility.facilityId === 5) {
						toolStatusId = 10;
						stockPointId = 28;
						toolUpdate = {
							toolId: ut.toolId,
							toolStatusId: toolStatusId,
							stockPointId: stockPointId
						}
					} else if (req.session.facility.facilityId === 8) {
						toolStatusId = 10;
						stockPointId = 32;
						toolUpdate = {
							toolId: ut.toolId,
							toolStatusId: toolStatusId,
							stockPointId: stockPointId
						}
					} else if (req.session.facility.facilityId === 9) {
						toolStatusId = 10;
						stockPointId = 34;
						toolUpdate = {
							toolId: ut.toolId,
							toolStatusId: toolStatusId,
							stockPointId: stockPointId
						}
					} else if (req.session.facility.facilityId === 10) {
						toolStatusId = 10;
						stockPointId = 35;
						toolUpdate = {
							toolId: ut.toolId,
							toolStatusId: toolStatusId,
							stockPointId: stockPointId
						}
					}
					secondPromise.push(ToolService.updateTool(toolUpdate, req.session.user));
				});
				Q.all(secondPromise).then(function (data) {
					return res.json(updatedTool);
				}).catch(function (err) {
					return res.status(400).send(err);
				});
			}).catch(function (err) {
				return res.status(400).send(err);
			});
		} else {
			var secondPromise = [];
			var toolUpdate = {};
			var stockPointId;
			var toolStatusId = 22;
			_.each(body.tool, function (ut) {
				if (req.session.facility.facilityId === 3) {
					stockPointId = 25;
					toolUpdate = {
						toolId: ut.toolId,
						toolStatusId: toolStatusId,
						stockPointId: stockPointId
					}
				} else if (req.session.facility.facilityId === 4) {
					stockPointId = 29;
					toolUpdate = {
						toolId: ut.toolId,
						toolStatusId: toolStatusId,
						stockPointId: stockPointId
					}
				} else if (req.session.facility.facilityId === 5) {
					stockPointId = 27;
					toolUpdate = {
						toolId: ut.toolId,
						toolStatusId: toolStatusId,
						stockPointId: stockPointId
					}
				} else if (req.session.facility.facilityId === 8) {
					stockPointId = 31;
					toolUpdate = {
						toolId: ut.toolId,
						toolStatusId: toolStatusId,
						stockPointId: stockPointId
					}
				} else if (req.session.facility.facilityId === 9) {
					stockPointId = 33;
					toolUpdate = {
						toolId: ut.toolId,
						toolStatusId: toolStatusId,
						stockPointId: stockPointId
					}
				} else if (req.session.facility.facilityId === 10) {
					stockPointId = 22;
					toolUpdate = {
						toolId: ut.toolId,
						toolStatusId: toolStatusId,
						stockPointId: stockPointId
					}
				}
				secondPromise.push(ToolService.updateTool(toolUpdate, req.session.user));
			});
			Q.all(secondPromise).then(function (data) {
				return res.json(data);
			}).catch(function (err) {
				return res.status(400).send(err);
			});
		}	
	},
	createDnrRepair: function (req, res) {
		var body = req.body;
		var repair = body.repair;
		var users = body.users;

		if (repair.dnrRepairId) {
			var prom = RepairService.updateDnrRepair(repair);
			prom.then(function (repair) {
				return res.json(repair);
			}).catch(function (err) {
				return res.json(repair);
			});
		} else {
			var promiseCreateLaborId = JobService.createLaborId();

			var promises = [];

			promiseCreateLaborId.then(function (data) {
				repair.laborId = data.laborId;
				var prom1 = RepairService.createDnrRepair(repair);
				prom1.then(function (repair) {
					var creationDate = new Date();

					_.each(users, function (user) {
						promises.push(RepairService.createDnrHistory(Enums.dnrHistoryType.repairCreated, repair, creationDate, user.userId, 'Repair record was created.'));
					});

					promises.push(ToolService.changeStatusAndStockPoint(repair.toolId, Enums.toolStatus.repairBeingRepaired, Enums.stockPoint.vernalRepair));

					if (repair.dateArrived) {
						_.each(users, function (user) {
							promises.push(RepairService.createDnrHistory(Enums.dnrHistoryType.toolArrived, repair, repair.dateArrived, user.userId, 'Tool has arrived'));
						});
					}

					Q.all(promises).then(function (dataArray) {
						return res.json(repair);
					}, function (err) {
						return res.status(500).send(err);
					});
				}).catch(function (err) {
					return res.status(500).send(err);
				});
			}).catch(function (err) {
				return res.status(500).send(err);
			});
		}
	},
	createImageRecord: function (req, res) {
		var body;
		if(req.body.imgRecord){
			body = req.body.imgRecord;
		} else {
			body = req.body;
		}
		let user = req.session.user
		if(body.length > 1){
			if(body[0].repairId){
				var repairId = JSON.parse(JSON.stringify(body[0].repairId));
				delete body[0].repairId;
				var createDocumentPromise = DocumentService.createDocument(body[0], user);
				createDocumentPromise.then(function (doc) {
					var addRepairDocumentJunctionPromise = [];
					body[0].repairId = repairId;
					_.each(body, function (b) {
						addRepairDocumentJunctionPromise.push(DocumentService.addRepairDocumentJunctionPromise(b.repairId, doc.documentId));
					})

					Q.all(addRepairDocumentJunctionPromise).then(function (data) {
						return res.json(doc);
					}).catch(function (err) {
						return res.status(500).send(err);
					});
				}).catch(function (err) {
					return res.status(500).send(err);
				});
			} else {
				var toolId = JSON.parse(JSON.stringify(body[0].toolId));
				delete body[0].toolId;
				var createDocumentPromise = DocumentService.createDocument(body[0], user);
				createDocumentPromise.then(function (doc) {
					var addToolDocumentJunctionPromise = [];
					body[0].toolId = toolId;
					_.each(body, function (b) {
						addToolDocumentJunctionPromise.push(DocumentService.addToolDocumentJunctionPromise(b.toolId, doc.documentId));
					})

					Q.all(addToolDocumentJunctionPromise).then(function (data) {
						return res.json(doc);
					}).catch(function (err) {
						return res.status(500).send(err);
					});
				}).catch(function (err) {
					return res.status(500).send(err);
				});
			}
			
		} else {
			if(body.repairId){
				var repairId = JSON.parse(JSON.stringify(body.repairId));
				delete body.repairId;
				var createDocumentPromise = DocumentService.createDocument(body, user);
				createDocumentPromise.then(function (doc) {
					var addRepairDocumentJunctionPromise = DocumentService.addRepairDocumentJunctionPromise(repairId, doc.documentId);
					addRepairDocumentJunctionPromise.then(function (data) {
						return res.json(doc);
					}).catch(function (err) {
						return res.status(500).send(err);
					});
				}).catch(function (err) {
					return res.status(500).send(err);
				});
			} else {
				var toolId = JSON.parse(JSON.stringify(body.toolId));
				delete body.toolId;
				var createDocumentPromise = DocumentService.createDocument(body, user);
				createDocumentPromise.then(function (doc) {
					var addToolDocumentJunctionPromise = DocumentService.addToolDocumentJunctionPromise(toolId, doc.documentId);
					addToolDocumentJunctionPromise.then(function (data) {
						return res.json(doc);
					}).catch(function (err) {
						return res.status(500).send(err);
					});
				}).catch(function (err) {
					return res.status(500).send(err);
				});
			}
		}
	},
	saveImageOnMainServer: function (req, res) {
		var deferred = Q.defer();
		var p = DocumentService.saveImageOnMainServer(req.body);
		p.then(function (err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	saveImage: function (req, res) {
		
		var options = {
			dirname: req.param('dirName'),
			maxBytes: 25000000, // Do not exceed ~25MB
		};
		if (req.param('filename')) {
			options.saveAs = req.param('filename');
		}

		if (req.file('myfile')._files.length !== 0) {
			req.file('myfile').upload(options, function (err, uploadedFiles) {
				if (!err) {
					if (req.param('fileExtension') === 'jpg') {
						var dir = uploadedFiles[0].fd;

						var dimensions = size(dir);

						var h = dimensions.height;
						var w = dimensions.width;
						var newWidth = 1000; // max width. Set this to what you want.
						var newHeight = h; // default
						if (w > newWidth) {
							var newHeight = (h * newWidth) / w;
						} else {
							newWidth = w;
						}

						jimp.read(dir, function (err, imgFile) {
							if (!err) {
								imgFile.resize(newWidth, newHeight)
									.quality(80)
									.write(dir);
							}
						});
						return res.json('success');
					} else {
						return res.json('success');
					}
				} else {
					return res.status(500).send(err);
				}
			});
		} else {
			return res.status(500).send(new Error('No files were received.'));
		}

		//      form.on('file', function (field, file) {
		//          console.log(file);
		//          if (req.param('filename')) {
		//              gm(file.path).size(function (err, size) {
		//                  var h = size.height;
		//                  var w = size.width;
		//                  if(size.width > max) {
		//                      var newHeight = (h * max) / w;
		//                  }
		//
		//                  jimp.read(file.path, function (err, thisFile) {
		//                      if (err) res.status(500).send(err);
		//                      else {
		//                          //
		//                          thisFile.resize(h, w)
		//                              .quality(60)
		//                              .write(path.join(dir, req.param('filename'))); // save
		//                      }
		//                  }).catch(function (err) {
		//                      console.log(err);
		//                  });
		//              });
		//          }
		//      });
		//
		//      form.on('error', function (err) {
		//          console.log(err);
		//          res.status(500).send(err);
		//      });

		//      form.on('end', function (files, fields) {
		//          console.log('end');
		//          res.json('success');
		//      });

		//      form.parse(req, function(err, fields, files) {
		//          console.log(err);
		//          console.log(fields);
		//          console.log(files);
		//      });
	},
	getPicturesForThisRepair: function (req, res) {
		var body = req.body;
		var getDocumentsPromise = DocumentService.getRepairDocuments(body.repairId);

		getDocumentsPromise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	getQtyPicturesRequiredObject: function (req, res) {
		var promise = Global.getProgrammingVariableUsingAPromise(2);
		promise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	generateRepairCsv: function (req, res) {
		var fields = ['RepairID', 'Serial', 'toolId', 'partNumberedObjectId', 'LocationID', 'Part', 'Style', 'FieldComments', 'DateSentFromField', 'DateReceivedAndDullID', 'RepairComments', 'ApprovalRequested', 'ApprovalDenied', 'ApprovalGranted', 'BrazeIDandDate', 'Torqued', 'DateShippedBackToFieldAndID', 'WhoCreatedRepairOrder', 'ShippedFromWhereRECEIVING', 'ShippedToWhereOUTGOING', 'HowShippedRECEIVING', 'HowShippedOUTGOING', 'DateReceivedBackByJim', 'InspectionDateAndID', 'CutoutComments', 'DateApprovalRequested', 'ApprovalIDandDate', 'OverallGrade', 'AllAdditionalWork', 'ExtraBrazeInstructions', 'TotalReplaceCutters', 'TotalReplaceDomes', 'TotalRotates', 'BoxesOnBlades', 'ODTopFace', 'ODConnection', 'ODBottomSeat', 'OverallLength', 'Length1', 'Length2', 'Length3', 'Length4', 'Length5', 'Length6', 'TB1C1', 'TB1C2', 'TB1C3', 'TB1C4', 'TB1C5', 'TB1C6', 'TB1C7', 'TB1C8', 'TB1C9', 'TB1C10', 'TB1C11', 'TB1C12', 'TB1C13', 'TB1C14', 'TB1C15', 'TB2C1', 'TB2C2', 'TB2C3', 'TB2C4', 'TB2C5', 'TB2C6', 'TB2C7', 'TB2C8', 'TB2C9', 'TB2C10', 'TB2C11', 'TB2C12', 'TB2C13', 'TB2C14', 'TB2C15', 'TB3C1', 'TB3C2', 'TB3C3', 'TB3C4', 'TB3C5', 'TB3C6', 'TB3C7', 'TB3C8', 'TB3C9', 'TB3C10', 'TB3C11', 'TB3C12', 'TB3C13', 'TB3C14', 'TB3C15', 'TB4C1', 'TB4C2', 'TB4C3', 'TB4C4', 'TB4C5', 'TB4C6', 'TB4C7', 'TB4C8', 'TB4C9', 'TB4C10', 'TB4C11', 'TB4C12', 'TB4C13', 'TB4C14', 'TB4C15', 'TB5C1', 'TB5C2', 'TB5C3', 'TB5C4', 'TB5C5', 'TB5C6', 'TB5C7', 'TB5C8', 'TB5C9', 'TB5C10', 'TB5C11', 'TB5C12', 'TB5C13', 'TB5C14', 'TB5C15', 'TB6C1', 'TB6C2', 'TB6C3', 'TB6C4', 'TB6C5', 'TB6C6', 'TB6C7', 'TB6C8', 'TB6C9', 'TB6C10', 'TB6C11', 'TB6C12', 'TB6C13', 'TB6C14', 'TB6C15', 'TB7C1', 'TB7C2', 'TB7C3', 'TB7C4', 'TB7C5', 'TB7C6', 'TB7C7', 'TB7C8', 'TB7C9', 'TB7C10', 'TB7C11', 'TB7C12', 'TB7C13', 'TB7C14', 'TB7C15', 'TB8C1', 'TB8C2', 'TB8C3', 'TB8C4', 'TB8C5', 'TB8C6', 'TB8C7', 'TB8C8', 'TB8C9', 'TB8C10', 'TB8C11', 'TB8C12', 'TB8C13', 'TB8C14', 'TB8C15', 'BB1C1', 'BB1C2', 'BB1C3', 'BB1C4', 'BB1C5', 'BB1C6', 'BB1C7', 'BB1C8', 'BB1C9', 'BB1C10', 'BB1C11', 'BB1C12', 'BB1C13', 'BB1C14', 'BB1C15', 'BB2C1', 'BB2C2', 'BB2C3', 'BB2C4', 'BB2C5', 'BB2C6', 'BB2C7', 'BB2C8', 'BB2C9', 'BB2C10', 'BB2C11', 'BB2C12', 'BB2C13', 'BB2C14', 'BB2C15', 'BB3C1', 'BB3C2', 'BB3C3', 'BB3C4', 'BB3C5', 'BB3C6', 'BB3C7', 'BB3C8', 'BB3C9', 'BB3C10', 'BB3C11', 'BB3C12', 'BB3C13', 'BB3C14', 'BB3C15', 'BB4C1', 'BB4C2', 'BB4C3', 'BB4C4', 'BB4C5', 'BB4C6', 'BB4C7', 'BB4C8', 'BB4C9', 'BB4C10', 'BB4C11', 'BB4C12', 'BB4C13', 'BB4C14', 'BB4C15', 'BB5C1', 'BB5C2', 'BB5C3', 'BB5C4', 'BB5C5', 'BB5C6', 'BB5C7', 'BB5C8', 'BB5C9', 'BB5C10', 'BB5C11', 'BB5C12', 'BB5C13', 'BB5C14', 'BB5C15', 'BB6C1', 'BB6C2', 'BB6C3', 'BB6C4', 'BB6C5', 'BB6C6', 'BB6C7', 'BB6C8', 'BB6C9', 'BB6C10', 'BB6C11', 'BB6C12', 'BB6C13', 'BB6C14', 'BB6C15', 'BB7C1', 'BB7C2', 'BB7C3', 'BB7C4', 'BB7C5', 'BB7C6', 'BB7C7', 'BB7C8', 'BB7C9', 'BB7C10', 'BB7C11', 'BB7C12', 'BB7C13', 'BB7C14', 'BB7C15', 'BB8C1', 'BB8C2', 'BB8C3', 'BB8C4', 'BB8C5', 'BB8C6', 'BB8C7', 'BB8C8', 'BB8C9', 'BB8C10', 'BB8C11', 'BB8C12', 'BB8C13', 'BB8C14', 'BB8C15', '180degValue', 'MaxRunOut', 'LOT', 'DateCuttersPulled', 'DateReceived', 'WhoShippedFromField', 'CSI_cert_#', 'ReworkComms', 'drillSizeDecimalInches', 'drillSizeFractionalInches', 'driftSizeDecimalInches', 'driftSizeFractionalInches'];

		SdpiContext.query('call GetAllRepairsWithToolInformation()', function (err, results) {
			if (!err) {
				var data = results[0];
				json2csv({
					data: data,
					fields,
					fields
				}, function (err, csv) {
					var dir = path.resolve(__dirname + '/../../assets/documents/repairs.csv');
					fs.writeFile(dir, csv, function (err) {
						res.download(dir);
					});
				});
			} else {

			}
		});
	},
	getToolsWithCuttersInDullGrade: function (req, res) {
		var promise = ToolService.getToolsWithCuttersInDullGrade();
		promise.then(function (data) {
			res.json(data);
		}).catch(function (err) {
			res.status(err.status).send(err);
		});
	},
	getToolsWithCuttersInPullCutters: function (req, res) {
		var promise = ToolService.getToolsWithCuttersInPullCutters();
		promise.then(function (data) {
			res.json(data);
		}).catch(function (err) {
			res.status(err.status).send(err);
		});
	},
	getToolsWithCuttersInBraze: function (req, res) {
		var promise = ToolService.getToolsWithCuttersInBraze();
		promise.then(function (data) {
			res.json(data);
		}).catch(function (err) {
			res.status(err.status).send(err);
		});
	},
	getDullGradesAndMarkupStructureForTool: function({body}, res) {
		// use partIdNewForDNR as partId if it exists
		if (body.toolRepair.partIdNewForDNR) {
			body.toolRepair.partId = body.toolRepair.partIdNewForDNR;
		}

		var identifier;
		if (body.toolRepair.toolFamilyId === 4) {
			// assign partId to identifier
			identifier = body.toolRepair.partId;
		} else if ([1,2].indexOf(body.toolRepair.toolFamilyId) !== -1) {
			// get part structure from BPCutterStructure and then include dullgrades

			identifier = body.toolRepair.partId;
		}

		console.log('identifier')
		console.log(identifier)

		// get dullgrades (typically only one dullgrade for DNR)
		var getDullGradeObjectsPromise = RepairService.getRepairDullGradesByRepair(body.toolRepair.repairId);
		getDullGradeObjectsPromise.then(function(repairDullGradeRecords) {
			var promises = [];
			_.each(repairDullGradeRecords, function(rdg) {
				promises.push(RepairService.getDullGradesAndMarkupStructureForToolByDullGrade(identifier, body.toolRepair.toolFamilyId, rdg, body.toolRepair.hashId));
			});

			if(!repairDullGradeRecords || repairDullGradeRecords.length === 0) {
				promises.push(RepairService.getEmptyDullGradeStructure(identifier, body.toolRepair.toolFamilyId, body.toolRepair.repairId));
			}

			Q.all(promises).then(function(data) {
				return res.json(data);
			}, function(err) {
			});
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	getDullGradeAndHashStructureForCurrentRepair: function({body}, res) {
		// use partIdNewForDNR as partId if it exists
		if (body.toolRepair.partIdNewForDNR) {
			body.toolRepair.partId = body.toolRepair.partIdNewForDNR;
		}

		var identifier;
		if (body.toolRepair.toolFamilyId === 4) {
			// assign partId to identifier
			identifier = body.toolRepair.partId;
		} else if ([1,2].indexOf(body.toolRepair.toolFamilyId) !== -1) {
			// assign hashId to identifier
			identifier = body.toolRepair.hashId;
		}

		console.log('identifier')
		console.log(identifier)

		var getDullGradeObjectsPromise = RepairService.getRepairDullGradesByRepair(body.toolRepair.repairId);
		getDullGradeObjectsPromise.then(function(repairDullGradeRecords) {
			var promises = [];
			_.each(repairDullGradeRecords, function(rdg) {
				promises.push(RepairService.getDullGradeAndHashStructureForCurrentRepair(identifier, body.toolRepair.toolFamilyId, rdg));
			});

			if(!repairDullGradeRecords || repairDullGradeRecords.length === 0) {
				promises.push(RepairService.getEmptyDullGradeStructure(identifier, body.toolRepair.toolFamilyId, body.toolRepair.repairId));
			}

			Q.all(promises).then(function(data) {

				return res.json(data);
			}, function(err) {
			});
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	getDullGradeStructureForCurrentRepair: function (req, res) {
		var body = req.body;

		// use partIdNewForDNR as partId if it exists
		if (body.toolRepair.partIdNewForDNR) {
			body.toolRepair.partId = body.toolRepair.partIdNewForDNR;
		}

		// 2. Get dullgrades for the current repair
		var promise = RepairService.getDullGradeStructureForCurrentRepair(body.toolRepair.repairId);

		promise.then(function (data) {
			var isNewToolAssembly = false;

			if (data && data.length > 0) {
				// dullgrades exist, use them
				return res.json(data);
			} else {
				// no dullgrades exist, get part pocket structure and create empty dullgrades.

				promise = RepairService.createAndGetDullGradesFromPartPocketStructure(body.toolRepair);
				promise.then(function (data) {
					return res.json(data);
				}).catch(function (err) {
					err.additionalInfo = 'There was either an error or no pocket structure for this part exists.';
					return res.status(500).send(err);
				});
			}
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	getCutterConditions: function (req, res) {
		var promise = GlobalValueService.getCutterConditions();
		promise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	getCutterActions: function (req, res) {
		var promise = GlobalValueService.getCutterActions();
		promise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	getConditionActionCombos: function(req, res) {
		var promise = GlobalValueService.getConditionActionCombos();
		promise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	updateDullGrades: function(req, res) {
		var body = req.body;
		if(body.dullGradeId){
			var dullGradeId = body.dullGradeId;
		}
		var cutterDullGrades = body.dullgrades;
		var repairId = body.repairId;

		// create a dullgrade and get dullGradeId
		var promise = RepairService.createDullGradeRecord(repairId, dullGradeId);
		promise.then(function(d) {
			// var promises = [];
			var qty;
			if(cutterDullGrades.length < 35){
				qty = cutterDullGrades.length
			} else {
				qty = 35
			}
			if(cutterDullGrades.length > 0) {
				updateDullGradePromise(qty, 0, cutterDullGrades, d.dullGradeId, function(){
					return res.json('success')
				})
			} else {
				return res.status(500).send('Did not receive any dullgrades in request.');
			}
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	saveReworkCutterDullGrade: function(req, res) {
		var dullgrades = req.body.dullGrades;
		if(req.body.dullGradeId){
			var dullGradeId = req.body.dullGradeId;
		}
		if(req.body.repairId){
			var repairId = req.body.repairId;
		}
		var promises = [];
		if(repairId){
			var promise = RepairService.createDullGradeRecord(repairId);
			promise.then(function(dullGrade) {
				if(dullgrades.length > 0) {
					_.each(dullgrades, function(dg) {
						dg.dullGradeId = dullGrade.dullGradeId;
						promises.push(RepairService.saveReworkCutterDullGrade(dg));
						Q.all(promises).then(function(data) {
							return res.json('success');
						}, function(err) {
							return res.status(500).send(err);
						});
					});
				} else {
					return res.status(500).send('Did not receive any dullgrades in request.');
				}
			});
		} else {
			if(dullgrades.length > 0) {
				_.each(dullgrades, function(dg) {
					dg.dullGradeId = dullGradeId;
					promises.push(RepairService.saveReworkCutterDullGrade(dg));
					Q.all(promises).then(function(data) {
						return res.json('success');
					}, function(err) {
						return res.status(500).send(err);
					});
				});
			} else {
				return res.status(500).send('Did not receive any dullgrades in request.');
			}
		}
	},
	saveDullGrade: function (req, res) {
		var body = req.body;
		var p1 = RepairService.saveDullGrade(body);

		p1.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	sendToolRepairToDullGrade: function (req, res) {
		var body = req.body;
		var users = body.users;
		var p1 = RepairService.changeRepairStatusTo(body.repair.dnrRepairId, Enums.dnrRepairStatus.dullGrade, users);
		var statusChangeDate = new Date();

		p1.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	completeDullGrade: function (req, res) {
		var body = req.body;
		var users = body.users;
		var p1 = RepairService.changeRepairStatusTo(body.dnrRepairId, Enums.dnrRepairStatus.pullCutters, users);
		p1.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	completePullCutters: function (req, res) {
		var body = req.body;
		var users = body.users;
		var p1 = RepairService.changeRepairStatusTo(body.dnrRepairId, Enums.dnrRepairStatus.braze, users);
		p1.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	completeBraze: function (req, res) {
		var body = req.body;
		var users = body.users;
		var p1 = RepairService.changeRepairStatusTo(body.dnrRepairId, Enums.dnrRepairStatus.inspection, users);
		p1.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	getCutters: function (req, res) {
		var p1 = CutterService.getCutters();
		p1.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	updateCutterDullGrades: function (req, res) {
		var body = req.body;
		var p1 = RepairService.updateCutterDullGrades(body);
		p1.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	getStationUsers: function (req, res) {
		var workStationId = req.session.workStation.workStationId;
		var p1 = UserService.getUsersForWorkStation(workStationId);
		p1.then(function (users) {
			return res.json(users);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	startStopLaborTracking: function (req, res) {
		let body = req.body
		let tools = req.body.tools ? req.body.tools : []
		if (body.users && body.users.length > 0 && body.items && body.items.length > 0) {
			var facilityId = req.session.facility.facilityId;
			var prePromises = [];
			prePromises.push(RepairService.getFlowRecords(facilityId));
			prePromises.push(RepairService.getAllWorkStationsForFacility(facilityId));
			Q.all(prePromises).then(function(flowsAndWorkstations) {
				var promises = [];
				var now = new Date();

				var multipleUsers = false;
				var userId = null;
				_.each(body.items, function(i){
					if(userId === null) {
						userId = i.userId;
					} else {
						if(i.userId != userId){
							multipleUsers = true;
						}
					}
				});


				var partialRateTimeForSharedOrBulkItems;

				if(multipleUsers) partialRateTimeForSharedOrBulkItems = 1;
				else partialRateTimeForSharedOrBulkItems = parseFloat(1 / body.items.length);

				if (body.items[0].laborTimeStampId) {
					var repairs = [];
					_.each(body.items, function (lts) {
						if (repairs.length === 0) {
							repairs.push(lts);
						} else {
							var exist = [];
							var nonExist = [];
							_.each(repairs, function (r) {
								var match = r.repairId === lts.repairId;
								if (match) {
									exist.push(lts);
								} else {
									nonExist.push(lts);
								}
							});
							if (exist.length === 0) {
								repairs.push(nonExist[0]);
							}
						}
						let nextFlow = flowsAndWorkstations[0].filter(flow => flow.flowId === lts.newFlowId)
						lts.nextFlow = nextFlow[0]
						let tool = tools.filter(tool => tool.repairId === lts.repairId)
						lts.tool = tool[0]

						promises.push(RepairService.startStopTimeStampForRepair(lts, partialRateTimeForSharedOrBulkItems, now, body.idleTime));
					});

				} else {
					let newFlowId = 0;
					let repairId;
					let setNewFlowId;

					_.each(body.users, function (user) {
						_.each(body.items, function (lts) {
							if(setNewFlowId) {
								lts.newFlowId = setNewFlowId
							}
							if(lts.newFlowId) {
								setNewFlowId = JSON.parse(JSON.stringify( lts.newFlowId ))
							}
							repairId = lts.repairId;
							lts.repairId = lts.repairId;
							lts.userId = user.userId;
							lts.userRate = !user.rate ? 0 : user.rate;
							lts.workStationId = req.session.workStation.workStationId;
							lts.workStationRate = req.session.workStation.hourlyCost;
							lts.totalRate = lts.userRate + lts.workStationRate;
							// If there is a new flow ID then do the following:
							// (1) set new workStationId and workStationRate on LaborTimeStamp.
							// (2) update this tool's repair with new flow information (add it in the promises array).
							if(lts.newFlowId) {
								newFlowId = lts.newFlowId;
								var newFlow = _.find(flowsAndWorkstations[0], function(fr) {
									return fr.flowId === lts.newFlowId;
								});
								if(newFlow) {
									var workstation = _.find(flowsAndWorkstations[1], function(ws) {
										return ws.workStationId === newFlow.workStationId;
									});
									if(workstation) {
										lts.workStationId = workstation.workStationId;
										lts.workStationRate = workstation.hourlyCost;
										lts.totalRate = lts.userRate + lts.workStationRate;
									}
								}
							}
							delete lts.newFlowId;
							promises.push(RepairService.startStopTimeStampForRepair(lts, partialRateTimeForSharedOrBulkItems, now));
						});
					});
					if(newFlowId !== 0) {
						var repair = {
							repairId: repairId,
							flowId: newFlowId
						};
						promises.push(RepairService.updateRepair(repair));
					}
				}

				Q.all(promises).then(function (data) {
					if (body.items[0].laborTimeStampId) {
						var promises2 = [];
						_.each(repairs, function (r) {
							if(body.closeRepairOnContinue) {
								promises2.push(RepairService.updateRepair({
									repairId: r.repairId,
									lastUserId: r.userId,
									repairStatusId: 1 // repair status of closed
								}));
							} else {
								if (r.notComplete) {
								} else {
									promises2.push(RepairService.updateRepair({
										repairId: r.repairId,
										flowId: r.newFlowId,
										lastUserId: r.userId
									}));
								}
							}
						});
						Q.all(promises2).then(function (data) {
							sails.sockets.blast('repair', data, req);
							return res.json(data);
						}, function (err) {
							return res.status(err.status).send(err);
						});
					} else {
						return res.json(data);
					}
				}, function (err) {
					return res.status(err.status).send(err);
				});
			}, function(err) {
				return res.status(err.status).send(err);
			});

		} else {
			return res.status(500).send('At least one user and at least one item is required.');
		}
	},
	getCostCenter: function (req, res) {
		var body = req.body;
		if (body.costCenterId) {
			var p = CostCenterService.getCostCenter(body.costCenterId);
			p.then(function (data) {
				return res.json(data);
			}).catch(function (err) {
				return res.status(err.status).send(err);
			});
		}
	},
	getLaborTrackingInfo: function (req, res) {
		var workStationId = req.session.workStation.workStationId;
		var p1 = RepairService.getLaborTimeStamps(workStationId, true);
		p1.then(function (laborTimeStamps) {
			var p2 = UserService.getUsersFromLaborTimeStamps(laborTimeStamps);
			p2.then(function (users) {
				var p3 = RepairService.getRepairFromLaborTimeStamps(laborTimeStamps);
				p3.then(function (repairs) {
					var items = [];
					_.each(users, function (user) {
						var laborTimeStampsForUser = _.filter(laborTimeStamps, function (lts) {
							return lts.userId === user.userId;
						});
						items.push(laborTimeStampsForUser);
					});
					// TODO LATER! verify that the items for each user are the same and started at the same time.
					var laborTrackingInfo = {
						users: users,
						items: items,
						repairs: repairs
					};
					return res.json(laborTrackingInfo);
				}).catch(function (err) {
					return res.status(err.status).send(err);
				});
			}).catch(function (err) {
				return res.status(err.status).send(err);
			});

		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	getFlowRecords: function (req, res) {
		var body = req.body;
		var facilityId = req.session.facility.facilityId;
		var promise = RepairService.getFlowRecords(facilityId);
		var flowRecordsObject = {};
		promise.then(function (records) {
			flowRecordsObject.records = records;
			flowRecordsObject.workStation = req.session.workStation;
			return res.json(flowRecordsObject);
		}).catch(function (err) {
			return res.status(500).send(err);
		});
	},
	getFlowsWithToolRepairs: function(req, res) {
		var currentFlow = req.body;
		var proms = [];
		_.each(currentFlow.currentFlowIds, function(id) {
			proms.push(RepairService.getFlowWithToolRepairs(id));
		});
		var facilityId = req.session.facility.facilityId;
		var p = RepairService.getAllWorkStationsForFacility(facilityId);
		p.then(function (workStations) {
			var stations = [];
			Q.all(proms).then(function(data) {
				if(data[0] && data[1]) {
					_.each(data[0], function(d1) {
						_.each(data[1], function(d2) {
							if(d1.workStationId === d2.workStationId) {
								var workStation = _.find(workStations, function(ws) {
									return ws.workStationId === d1.workStationId;
								});
								var station = JSON.parse(JSON.stringify(d1));
								console.log('==========');
								console.log(station.toolRepairs.length + ' | ' + d2.toolRepairs.length);

								_.each(d2.toolRepairs, function(tr) {
									station.toolRepairs.push(tr);
								});
								station.name = workStation.workStationName;
								console.log(station.toolRepairs.length);
								stations.push(station);
							}
						});
					});
					return res.json(stations);
				} else if(data[0]) {
					return res.json(data[0]);
				} else {
					return res.status(500).send({error: 'No flows exist.'});
				}
			}, function(err) {
				console.log(err);
			});
		}).catch(function (err) {
			console.log(err);
		});
	},
	getFlowRelationshipForCurrentFlow: function (req, res) {
		var currentFlowId = req.body;
		var promise = RepairService.getFlowRelationshipForCurrentFlow(currentFlowId);
		promise.then(function (nextFlow) {
			return res.json(nextFlow);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	getToolFamilies: function (req, res) {
		var promise = GlobalValueService.getToolFamilies();
		promise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	saveFlameSprayInfo: function (req, res) {
		var flamesprays = req.body;
		var promise = RepairService.saveFlameSprayInfo(flamesprays);

		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	getToolSections: function (req, res) {
		RepairService.getToolSections(function (err,sections) {
			if (!err) return res.json(sections);
			else return res.status(500).send(err);
		});
	},
	getToolSectionInsepectionIssueJunction: function (req, res) {
		RepairService.getToolSectionInsepectionIssueJunction(function (err,junctions) {
			if (!err) return res.json(junctions);
			else return res.status(500).send(err);
		});
	},
	getInspectionIssues: function (req,res) {
		RepairService.getInspectionIssues(function (err,issues) {
			if (!err) return res.json(issues);
			else return res.status(500).send(err);
		});
	},
	getStockPointsByRepairCustomerId: function (req,res) {
		var body = req.body;
		var promise = RepairService.getStockPointsByRepairCustomerId(body.repairCustomerId);

		promise.then(function(stockPoints){
			return res.json(stockPoints);
		}).catch(function(err) {
			return res.status(err.stauts).send(err);
		});
	},
	getRepairCustomers: function (req,res) {
		RepairService.getRepairCustomers(function (err,repairCustomers) {
			if (!err) return res.json(repairCustomers);
			else return res.status(500).send(err);
		});
	},
	getAncillaryInfo: function (req,res) {
		var body = req.body;
		var promise = RepairService.getAncillaryInfo(body.tableForAncillary, body.repairId);
		promise.then(function(info){
			return res.json(info);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	saveBakerBitAncillaryInfo: function (req,res) {
		var body = req.body;
		var ids = body[0];
		var partInfo = body[1];
		var ancillary = body[2];
		var repairInfo = body[3];
		var promise = RepairService.saveBakerBitAncillaryInfo(ids, partInfo, ancillary, repairInfo);

		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	checkIfPartExist: function (req,res) {
		var body = req.body;
		var promise = RepairService.checkIfPartExist(body.partNumber);
		promise.then(function(part){
			return res.json(part);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	getCountOfFlameSpray: function (req,res) {
    	var body = req.body;
    	var promise;
    	if(body.dullGrade) {
    		// get count of flamespray by dullGradeId
    		promise = RepairService.getCountOfFlameSprayByDullGrade(body.partBakerBitId, body.dullGrade.dullGradeId);
    	} else {
    		// get count of flamespray by repairId
    		promise = RepairService.getCountOfFlameSpray(body.partBakerBitId, body.repairId);
    	}
		promise.then(function(count){
			return res.json(count);
		}).catch(function(err){
			return res.status(err.status).send(err);
		});
	},
	getCutterNamesUsedInRepair: function (req,res) {
		var body = req.body;
		var promise = RepairService.getCutterNamesUsedInRepair(body.repairId, body.dullgradeId);

		promise.then(function(cutterNames){
			return res.json(cutterNames);
		}).catch(function(err){
			return res.status(err.status).send(err);
		});
	},
	getCountOfAllCuttersWithCutterNamesBakerBit: function ({body}, res) {
		console.log(body);
		var promise = RepairService.getCountOfAllCuttersWithCutterNamesBakerBit(body.repairId, body.dullGradeId);

		promise.then(function(cutterNames){
			return res.json(cutterNames);
		}).catch(function(err){
			return res.status(err.status).send(err);
		});
	},
	getCutterNames: function (req,res) {
		var body = req.body;
		var promise = RepairService.getDullGrades (body.repairId);
		promise.then(function(dullGrades){
			var promises =[];
			_.each(dullGrades, function(dg){
				promises.push(RepairService.getCutterNames(dg.dullGradeId));
			});
			Q.all(promises).then(function (cutterNames) {
				return res.json(cutterNames);
			}, function (err) {
				return res.status(err.status).send(err);
			});
		}).catch(function(err){
			return res.status(err.status).send(err);
		});
	},
	getCountOfCutterSizeByAction: function (req, res) {
    	var body = req.body;

    	var promise;
    	if(body.dullGrade) {
    		// get count of cutter size by dullGradeId
    		promise = RepairService.getCountOfCutterSizeByActionByDullGrade(body.partBakerBitId, body.dullGrade.dullGradeId);
    	} else {
    		// get count of cutter size by repairId
    		promise = RepairService.getCountOfCutterSizeByAction(body.partBakerBitId, body.repairId, body.dullGrade);
    	}
		promise.then(function(cutterSizeCount){
			return res.json(cutterSizeCount);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	saveCreate: function (req,res) {
		var body = req.body;
		var ancillaryUpdateObject = {
			ancillaryBakerBitId: body.ancillaryBakerBitId,
			orderNumber: body.orderNumber,
			orderPrice: body.orderPrice,
			totalTimesRepaired: body.totalTimesRepaired,
			lastUserId: body.userId
		};
		if (body.interStationChrono) {
			var repairUpdateObject = {
				repairId: body.repairId,
				interStationChrono: body.interStationChrono + 1
			};
		}

		var promises = [];
		promises.push(RepairService.saveCreate(ancillaryUpdateObject));
		if (body.interStationChrono) {
			promises.push(RepairService.updateRepairPartId(repairUpdateObject));
		}

		Q.all(promises).then(function (data) {
			return res.json(data);
		}, function (err) {
			return res.status(err.status).send(err);
		});
	},
	updateRepair: function(req, res) {
		var body = req.body;
		console.log(body);
		var promise = RepairService.updateRepair(body);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			console.log(err);
			return res.status(err.status).send(err);
		});
	},
	holdRepair: function (req,res) {
		var body = req.body;
		var promise = RepairService.holdRepair(body);

		promise.then(function(hold) {
			return res.json(hold);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	createProgramMoveForward: function (req,res) {
		var body = req.body;
		var promise = RepairService.createProgramMoveForward(body);

		promise.then(function(moved) {
			sails.sockets.blast('repair', moved, req);
			return res.json(moved);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	getAllAncillaryBakerBitInfo: function (req,res) {
		var promise = RepairService.getAllAncillaryBakerBitInfo();

		promise.then(function(ancillaryInfo) {
			return res.json(ancillaryInfo);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	generateEmail: function (req,res) {
		var body = req.body;
		var html = "";
		for(i = 0; i < body.length; i++) {
			var stockPoint = body[i][0].shipToStockPointName;
			var serial = 'Serial';
			var partNumber ='Part Number';
			var size = 'Size';
			var style = 'Style';
			var grade = 'Grade';
			var repair = 'Repair';
			var orderNumber = 'Order #';
			var cost = 'Cost';
			html = html + '<h3>' + stockPoint + '</h3> <table cellspacing="20"> <tr> <th>' + serial + '</th> <th>' + partNumber + '</th> <th>' + size + '</th> <th>' + style + '</th> <th>' + grade + '</th> <th>' + repair + '</th> <th>' + orderNumber + '</th> <th>' + cost + '</th> </tr>';
			for(n = 0; n < body[i].length; n++) {
				var dataSerial = body[i][n].serial;
				var dataPartNumber = body[i][n].partNumber;
				var dataSize = body[i][n].cuttingDiameterDecimal;
				var dataStyle = body[i][n].style;
				var dataGrade = body[i][n].grade;
				var dataRepair = body[i][n].totalTimesRepaired;
				var orderNumber = body[i][n].orderNumber;
				var dataCost = body[i][n].orderPrice;
				html = html + '<tr> <td>' + dataSerial + '</td> <td>' + dataPartNumber + '</td> <td>' + dataSize + '</td> <td>' + dataStyle + '</td> <td>' + dataGrade + '</td> <td>' + dataRepair + '</td> <td>' + orderNumber + '</td> <td>' + dataCost + '</td> </tr>';
			}
			html = html + '</table>';
		}

		// create reusable transporter object using the default SMTP transport
		var transporter = nodemailer.createTransport({
			host: 'smtp.gmail.com',
			port: 465,
			secure: true, // secure:true for port 465, secure:false for port 587
			auth: {
				user: 'taylor@teamsdp.com',
				pass: 'Ss716128'
			}
		});

		// setup email data with unicode symbols
		var mailOptions = {
			from: '"TEST 👻" <taylor@teamsdp.com>', // sender address
			to: 'taylor@teamsdp.com', // list of receivers
			subject: 'Hello ✔', // Subject line
			html: html // html body
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				return res.status(500).send(error);
			} else {
				var promises = [];
				for(i = 0; i < body.length; i++) {
					for(n = 0; n < body[i].length; n++) {
						promises.push(RepairService.createProgramMoveForward(body[i][n]));
					}
				}
				Q.all(promises).then(function (data) {
					sails.sockets.blast('repair', data, req);
					return res.json(data);
				}, function (err) {
					return res.status(err.status).send(err);
				});
			}
		});

	},
	generateCutterOrder: function (req,res) {
		var body = req.body;

		var promise = RepairService.generateCutterOrder(body.flowId);

		promise.then(function(cutterList) {
			var html = "";
			for(i = 0; i < cutterList.length; i++) {
				var cutter = cutterList[i].partNumber;
				var count = cutterList[i].count;
				var replacedCutter = cutterList[i].replacedCutterName;
				if (replacedCutter) {
					if (cutterList[i].isReclaim === 1) {
						html = html + '<h3>' + count + ' - ' + cutter + ' R '  + ' (Replaced By ' + replacedCutter + ')' + '</h3>';
					} else {
						html = html + '<h3>' + count + ' - ' + cutter + ' (Replaced By ' + replacedCutter + ')' + '</h3>';
					}
				} else {
					if (cutterList[i].isReclaim === 1) {
						html = html + '<h3>' + count + ' - ' + cutter + ' R ' + '</h3>';
					} else {
						html = html + '<h3>' + count + ' - ' + cutter + '</h3>';
					}
				}
			}

			// create reusable transporter object using the default SMTP transport
			var transporter = nodemailer.createTransport({
				host: 'smtp.gmail.com',
				port: 465,
				secure: true, // secure:true for port 465, secure:false for port 587
				auth: {
					user: 'taylor@teamsdp.com',
					pass: 'Ss716128'
				}
			});

			// setup email data with unicode symbols
			var mailOptions = {
				from: '"TEST 👻" <taylor@teamsdp.com>', // sender address
				to: 'taylor@teamsdp.com', // list of receivers
				subject: 'Cutters ✔', // Subject line
				html: html // html body
			};

			// send mail with defined transport object
			transporter.sendMail(mailOptions, (error, info) => {
				if (error) {
					return res.status(500).send(error);
				} else {
					var figureProgrammingChronoPromise = RepairService.findProgrammingChronoForCutterOrderList(body.flowId);
					figureProgrammingChronoPromise.then(function(cutterOrderList) {
						for (f = 0; f < cutterOrderList.length; f++) {
							if (cutterOrderList[f].isCutOut === 1) {
								cutterOrderList[f].goToClearOut = true;
							} else {
								cutterOrderList[f].goToClearOut = false;
							}
							if (cutterOrderList[f].addPaintOnly === 1) {
								cutterOrderList[f].goToPaint = true;
							} else {
								cutterOrderList[f].goToPaint = false;
							}
							if (cutterOrderList[f].newCutters > 0) {
								cutterOrderList[f].goToPullCutters = true;
							} else {
								cutterOrderList[f].goToPullCutters = false;
							}
							if (cutterOrderList[f].flameSprays > 0) {
								cutterOrderList[f].goToFlameSpray = true;
							} else {
								cutterOrderList[f].goToFlameSpray = false;
							}
							if (cutterOrderList[f].goToFlameSpray === false && cutterOrderList[f].goToPullCutters === false) {
								cutterOrderList[f].goToBraze = true;
							} else {
								cutterOrderList[f].goToBraze = false;
							}
						}
						for (c = 0; c < cutterOrderList.length; c++) {
							////// send to clear out /////
							if (cutterOrderList[c].goToCutOut === true) {
								cutterOrderList[c].programmingChrono = 1;
								continue;
							}
							////// send to paint /////
							if (cutterOrderList[c].goToPaint === true) {
								cutterOrderList[c].programmingChrono = 2;
								continue;
							}
							/////// send to flame spray //////
							if (cutterOrderList[c].goToFlameSpray === true) {
								cutterOrderList[c].programmingChrono = 3;
								continue;
							}
							/////// send to pull Cutters //////
							if (cutterOrderList[c].goToPullCutters === true) {
								cutterOrderList[c].programmingChrono = 4;
								continue;
							}
							/////// send to braze //////
							if (cutterOrderList[c].goToBraze === true) {
								cutterOrderList[c].programmingChrono = 5;
								continue;
							}
						}
						var currentFlowId = {
							currentFlowId: body.flowId
						};
						var getFlowRelationshipForCurrentFlowPromise = RepairService.getFlowRelationshipForCurrentFlow(currentFlowId);
						getFlowRelationshipForCurrentFlowPromise.then(function(possibleNextFlows) {
							var updateRepairPromise = [];
							for (t = 0; t < cutterOrderList.length; t++) {
								for(pnf = 0; pnf < possibleNextFlows.length; pnf++) {
									if(possibleNextFlows[pnf].programmingChrono === cutterOrderList[t].programmingChrono) {
										cutterOrderList[t].nextFlowId = possibleNextFlows[pnf].nextFlowId;
									}
								}
								updateRepairPromise.push(RepairService.updateRepair({
									repairId: cutterOrderList[t].repairId,
									flowId: cutterOrderList[t].nextFlowId,
									interStationChrono: 1,
									lastUserId: body.user[0].userId
								}));
							}
							Q.all(updateRepairPromise).then(function (data) {
								sails.sockets.blast('repair', data, req);
								return res.json(data);
							}, function (err) {
								return res.status(err.status).send(err);
							});

						}).catch(function(err) {
							return res.status(err.status).send(err);
						});
					}).catch(function(err) {
						return res.status(err.status).send(err);
					});
				}
			});
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	updateCutter: function(req, res) {
		var cutterWithPartialData = req.body;
		var promise = CutterService.updateCutter(cutterWithPartialData);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	saveCutter: function(req, res) {
		var cutter = req.body;
		var promise = CutterService.checkCutterPocketAndSaveCutter(cutter);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	getHistoryByRepairId: function (req,res) {
		var data = req.body;
		var promise = RepairService.getHistoryByRepairId(data.repairId);

		promise.then(function(history) {
			return res.json(history);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	getLegendInfoNewCutters: function(req, res) {
		var body = req.body;

		var promise = RepairService.getCutterNamesUsedInRepair(body.repairId);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(err.status).send(err);
		});
	},
	getLaborTimeStampsForRepair: function(req, res) {
		var body = req.body;
		var promise = RepairService.getLaborTimeStampsForRepair(body.repairId);
		promise.then(function(timestamps) {
			if(timestamps && timestamps.length > 0) {
				var groups = _.groupBy(timestamps, 'workStationId');
				groups = _.map(groups, function(g) {
					return {
						items: g,
						workStationId: g[0].workStationId,
						workStationName: g[0].workStationName
					};
				});
				// figure total times
				var repairTotalTime = 0;
				_.each(groups, function(g) {
					var subTotalTime = 0;
					_.each(g.items, function(i) {
						var diff = Global.getTimeDifferenceBetweenDates(i.stopTime, i.startTime, true);
						repairTotalTime += diff;
						subTotalTime += diff;
					});
					g.subTotalTime = Global.convertMillisecondsToDateFormat(subTotalTime);
				});
				var obj = {
					repairTotalTime: Global.convertMillisecondsToDateFormat(repairTotalTime),
					timeStampData: groups
				};

				return res.json(obj);
			} else {
				return res.json(timestamps);
			}
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	saveLotsForCutterAndRepair: function(req, res) {
		var body = req.body;
		var toolRepair = body.repair;
		var cutter = body.cutter;
		console.log('++++++++++++++++++++++++++++++++++++');
		console.log('+++++++++ activeCutter +++++++++++++');
		console.log(cutter);
		console.log(JSON.stringify(cutter.lots));
		console.log('++++++++++++++++++++++++++++++++++++');
		console.log('++++++++++++++++++++++++++++++++++++');
		var promise = RepairService.saveLotNumbersForDullGrades(cutter, toolRepair.dullGradeId);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send();
		});
	},
	addSaveLot: function(req, res) {
		var body = req.body;
		var cutter = body.cutter;
		var dullGrade = body.dullGrade;
		var lotNumber = body.lotNumber;
		var qty = body.qty;
		var isReclaim = body.isReclaim ? body.isReclaim : false;


		var promise = RepairService.saveLotInfoForCutter(cutter, dullGrade.dullGradeId, lotNumber, qty, isReclaim);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	removeLot: function(req, res) {
		var cutterDullGrades = req.body;
		if(cutterDullGrades.length > 0) {
			var promise = RepairService.removeLotInfoForCutterDullGrades(cutterDullGrades);
			promise.then(function(data) {
				return res.json(data);
			}).catch(function(err) {
				return res.status(500).send(err);
			});
		} else {
			return res.status(500).send();
		}
	},
	getFlowRecordsFromSerial: function (req,res) {
		var body = req.body;
		body.facilityId = req.session.facility.facilityId;
		var promise = RepairService.getFlowRecordsFromSerial(body);
		promise.then(function (data) {
			return res.json(data);
		}).catch(function (err) {
			return res.status(err.status).send(err);
		});
	},
	updateFlowForSerial: function(req, res) {
		var body = req.body;
		var promise;

		promise = RepairService.getCutterNamesUsedInRepair(body.repairId);
		// TODO
		promise = RepairService.updateFlowForSerial(body);
		promise.then(function(data) {
			sails.sockets.blast('repair', data, req);
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	updateRushComments: function(req, res) {
		var body = req.body;
		var promise = RepairService.updateRushComments(body);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	checkboxModel: function(req, res) {
		var body = req.body;
		var promise = RepairService.checkboxModel(body);
		promise.then(function(updated) {
			return res.json(updated);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	getFlags: function(req,res) {
		var data = req.body;
		var promise = RepairService.getFlags(data);
		promise.then(function(flags) {
			return res.json(flags);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	revertReplacementCutterBack: function({body}, res) {
		if(!body.lotNumber) {
			var dg = {
				cutterDullGradeId: body.cutterDullGradeId,
				replaceWithCutterId: null
			}
			RepairService.updateCutterDullGrade(dg)
			 .then(data => res.json(data))
			 .catch(err => res.status(err.statusCode).send(err));
		}
	},
	addAsCutterReplacement: function({body}, res) {
		let currentCutter = body.currentCutter;
		let replacementCutter = body.replacementCutter;
		let cutterDullGrades = body.cutterDullGradesToReplace;
		let dullGrade = body.dullGrade;
		let dullGradeId = dullGrade && dullGrade.dullGradeId ? dullGrade.dullGradeId : undefined;
		let reasonForChange;
		if(body.reasonForChange){
			reasonForChange = body.reasonForChange;
		}

		console.log(dullGradeId);

		let promise = RepairService.setReplacementCuttersOnCutterDullGrades(currentCutter, replacementCutter, cutterDullGrades, reasonForChange, dullGradeId);

		promise.then(function(data) {
			console.log(data);
			return res.json(data);
		}).catch(function(err) {
			console.log(err);
			return res.status(500).send(err);
		});
	},
	getTimeStampsForWorkStation: ({body}, res) => {
    	var repair = body.repair;
    	var promise = RepairService.getLaborTimeStampsForRepair(repair.repairId);
    	promise.then(function(data) {
    		data = _.filter(data, function(d) {
    			return !d.stopTime;
    		});
    		return res.json(data);
    	}).catch(function(err) {
    		console.log(err);
    		return res.status(err.status).send(err);
    	});
	},
	removeRepairFromKiln: (req, res) => {
    	var kilnWithTools = req.body.kiln;
		var laborTimeStamps = req.body.laborTimeStamps
    	var promise = RepairService.removeRepairFromKiln(kilnWithTools, laborTimeStamps);
    	promise.then(function(data) {
    		return res.json(data);
    	}).catch(function(err) {
    		console.log(err);
    		return res.status(err.status).send(err);
    	});
	},
	getOpenTimeStampWithRepairId: function(req, res) {
    	var body = req.body;
		var promise = RepairService.getOpenTimeStampWithRepairId(body);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send(err);
		});
	},
	saveHashStructureAndAssignToToolAndRepair: ({body, session}, res) => {
		// check if hash exists for this hash structure
		let hashValueForStructure = PartService.convertPocketStructureToHashValue(body.groups)

		RepairService.getOrSaveHashByValue(hashValueForStructure, body.groups).then(record => {
			if(record) {
				// assign record hashId to tool and repair
				let hashId = record.hashId
				let promises = [
					ToolService.updateToolSimple({toolId: body.tool.toolId, hashId: hashId}, [session.user]),
					RepairService.updateRepair({repairId: body.tool.repairId, hashId: hashId})
				]
				Q.all(promises).then(data => {
					return res.json({newHashId: data[0][0].hashId})
				}, err => {
					console.log(err)
					return res.status(500).send(err)
				})
			}
		}).catch(err => {
			console.log(err)
			return res.status(500).send(err)
		})
	},
	saveThirdPartyInspection: function(req, res) {
		if(req.body.body){
			var data = req.body.body
		} else  {
			var data = req.body;
		}
		if(data.length){
			var promises = [];
			_.each(data, function (d) {
				promises.push(RepairService.saveThirdPartyInspection(d));
			});
			Q.all(promises).then(updated => {
				return res.json({updated})
			}, err => {
				console.log(err)
				return res.status(500).send(err)
			});
		} else {
			var promise = RepairService.saveThirdPartyInspection(data);
			promise.then(function (updated) {
				return res.json(updated);
			}).catch(function (err) {
				return res.status(500).send(err);
			});

		}
	},
	get32Inches: function(req, res) {
		var data = req.body;
		var promise = RepairService.get32Inches(data);
		promise.then(function(data) {
			return res.json(data);
		}).catch(function(err) {
			return res.status(500).send(err);
		})
	},
	saveFinalInspectionAndPrintStrapSheet: function(req, res) {
		var data = req.body;
		var toolFamilyId = data.ancillaryInfo.toolFamilyId;
		var promise = RepairService.saveThirdPartyInspection(data.ancillaryInfo);
		promise.then(function(updated){
			var promise2 = ToolService.updateToolLengthsAndODs(data.updateToolInfo);
			promise2.then(function(updated2) {
				if(toolFamilyId === 1){
					var strapSheet = new PDFDocument({
						margin: 19,
						layout : 'landscape'
					});
					var strapSheetObject = req.body;

					strapSheet.pipe(res);
					if(strapSheetObject.strapSheetInfo.customerOwned === 1){
						strapSheet.image('assets/images/DTIDNRReamerSheetMakeUpTorqueInspect300DPI.jpg', 0, 0, {
							scale: 0.188
						})
					} else {
						strapSheet.image('assets/images/HRSDNRReamerSheetMUTInspect300DPI.jpg', 0, 0, {
							scale: 0.188
						})
					}
					strapSheet.font('Times-Bold')
					strapSheet.fillColor('white')
					strapSheet.fontSize(20)
					strapSheet.text(strapSheetObject.strapSheetInfo.serial, 55, 9)
					strapSheet.fontSize(30)
					strapSheet.text(strapSheetObject.updateToolInfo.lengthTotal, 350, 225)

					strapSheet.text(strapSheetObject.updateToolInfo.length1, 92, 288)

					strapSheet.text(strapSheetObject.updateToolInfo.length2, 205, 288)

					strapSheet.text(strapSheetObject.updateToolInfo.length3, 360, 288)

					strapSheet.text(strapSheetObject.updateToolInfo.length4, 518, 288)

					strapSheet.text(strapSheetObject.updateToolInfo.length5, 633, 288)
					strapSheet.fillColor('black')
					strapSheet.fontSize(23)
					strapSheet.text(strapSheetObject.updateToolInfo.topOdFractionalInches, 88, 419)

					strapSheet.text(strapSheetObject.updateToolInfo.midOdFractionalInches, 357, 419)

					strapSheet.text(strapSheetObject.updateToolInfo.bottomOdFractionalInches, 632, 419)
					strapSheet.fontSize(28)
					strapSheet.text(strapSheetObject.strapSheetInfo.drillSize, 127, 505)

					strapSheet.text(strapSheetObject.strapSheetInfo.driftSize, 338, 505)

					strapSheet.text(strapSheetObject.strapSheetInfo.innerDiameter, 338, 556)

					if(strapSheetObject.strapSheetInfo.boxConnection.length > 11){
						strapSheet.fontSize(16)
						strapSheet.text(strapSheetObject.strapSheetInfo.boxConnection, 15, 560)

						strapSheet.text(strapSheetObject.strapSheetInfo.pinConnection, 120, 560)
					} else {
						strapSheet.fontSize(21)
						strapSheet.text(strapSheetObject.strapSheetInfo.boxConnection, 15, 560)

						strapSheet.text(strapSheetObject.strapSheetInfo.pinConnection, 120, 560)
					}


					strapSheet.fontSize(14)
					strapSheet.text(strapSheetObject.ancillaryInfo.thirdPartyInspectionNumberTwo, 475, 522)
					strapSheet.fontSize(10)
					strapSheet.text(strapSheetObject.strapSheetInfo.boxTorqueFTLB, 31, 579)

					strapSheet.text(strapSheetObject.strapSheetInfo.pinTorqueFTLB, 137, 579)

					strapSheet.end()
				} else if (toolFamilyId === 2){
					var strapSheet = new PDFDocument({
						margin: 19,
						layout : 'landscape'
					});
					var strapSheetObject = req.body;

					strapSheet.pipe(res);
					if(strapSheetObject.strapSheetInfo.customerOwned === 1){
						strapSheet.image('assets/images/DTIVStreamStrapSheetMakeUpTorqueInspect300DPI.jpg', 0, 0, {
							scale: 0.188
						})
					} else {
						strapSheet.image('assets/images/HRDSVStreamStrapSheetMakeUpTorqueInspect300DPI.jpg', 0, 0, {
							scale: 0.188
						})
					}

					strapSheet.font('Times-Bold')
					strapSheet.fillColor('white')
					strapSheet.fontSize(20)
					strapSheet.text(strapSheetObject.strapSheetInfo.serial, 55, 7)
					strapSheet.fontSize(30)
					strapSheet.text(strapSheetObject.updateToolInfo.lengthTotal, 366, 223)

					strapSheet.text(strapSheetObject.updateToolInfo.length1, 130, 287)

					strapSheet.text(strapSheetObject.updateToolInfo.length2, 375, 287)

					strapSheet.text(strapSheetObject.updateToolInfo.length3, 620, 287)

					strapSheet.fillColor('black')
					strapSheet.fontSize(23)
					strapSheet.text(strapSheetObject.updateToolInfo.topOdFractionalInches, 124, 425)

					strapSheet.text(strapSheetObject.updateToolInfo.midOdFractionalInches, 383, 424)

					strapSheet.text(strapSheetObject.updateToolInfo.bottomOdFractionalInches, 625, 425)
					strapSheet.fontSize(28)
					strapSheet.text(strapSheetObject.strapSheetInfo.drillSize, 258, 503)

					strapSheet.text(strapSheetObject.strapSheetInfo.innerDiameter, 337, 554)

					if(strapSheetObject.strapSheetInfo.boxConnection.length > 11){
						strapSheet.fontSize(16)
						strapSheet.text(strapSheetObject.strapSheetInfo.boxConnection, 15, 558)

						strapSheet.text(strapSheetObject.strapSheetInfo.pinConnection, 120, 558)
					} else {
						strapSheet.fontSize(21)
						strapSheet.text(strapSheetObject.strapSheetInfo.boxConnection, 15, 558)

						strapSheet.text(strapSheetObject.strapSheetInfo.pinConnection, 120, 558)
					}


					strapSheet.fontSize(14)
					strapSheet.text(strapSheetObject.ancillaryInfo.thirdPartyInspectionNumberTwo, 475, 521)
					strapSheet.fontSize(10)
					strapSheet.text(strapSheetObject.strapSheetInfo.boxTorqueFTLB, 32, 578)

					strapSheet.text(strapSheetObject.strapSheetInfo.pinTorqueFTLB, 138, 578)

					strapSheet.end()
				}
			}).catch(function(err) {
				return res.status(500).send(err);
			})
		}).catch(function(err) {
			return res.status(500).send(err);
		})
	},
	getDNRShipToStockPoints: ({body}, res) => {
		var promise = RepairService.getDNRShipToStockPoints();
		promise.then(function(stockPoints){
			return res.json(stockPoints);
		}).catch(function(err) {
			return res.status(500).send(err);
		})
	},
	getUserStockPoints: (req, res) => {
		var user = req.session.user
		var promise = RepairService.getUserStockPoints(user);
		promise.then(function (userStockPoints) {
			return res.json(userStockPoints);
		}).catch(function (err) {
			return res.status(500).send(err);
		})
	},
	saveDNRShippingInfo: function (req, res) {
		var body = req.body;
		var promises = [];
		var promises2 = [];
		if(body.selectedTools[0].repairId){
			_.each(body.selectedTools, function (st) {
				var repairInfo = JSON.parse(JSON.stringify(body.repairInfo));
				repairInfo.repairId = st.repairId
				repairInfo.lastUserId = req.session.user.userId
				promises.push(RepairService.updateRepair(repairInfo))
			})
			Q.all(promises).then(function (data) {
				_.each(body.selectedTools, function (st) {
					var toolInfo = JSON.parse(JSON.stringify(body.toolInfo));
					toolInfo.toolId = st.toolId;
					toolInfo.stockPointId = body.repairInfo.stockPointShipToId;
					promises2.push(ToolService.updateToolSimple(toolInfo))
				})
				Q.all(promises2).then(function (data2) {
					return res.json(data2);
				}, function (err) {
					return res.status(500).send(err);
				});
			}, function (err) {
				return res.status(500).send(err);
			});
		} else {
			_.each(body.selectedTools, function (st) {
				var toolInfo = JSON.parse(JSON.stringify(body.toolInfo));
				toolInfo.toolId = st.toolId;
				toolInfo.stockPointId = body.repairInfo.stockPointShipToId;
				promises2.push(ToolService.updateToolSimple(toolInfo))
			})
			Q.all(promises2).then(function (data2) {
				return res.json(data2);
			}, function (err) {
				return res.status(500).send(err);
			});
		}
	},
	updateAncillaryDnr: function (req, res) {
		var body = req.body;
		var promise = RepairService.updateAncillaryDnr(body);
		promise.then(function(painted){
			return res.json(painted);
		}).catch(function(err){
			return res.status(500).send(err);
		})
	},
	updateAncillaryVstream: function (req, res) {
		var body = req.body;
		var promise = RepairService.updateAncillaryVstream(body);
		promise.then(function (painted) {
			return res.json(painted);
		}).catch(function (err) {
			return res.status(500).send(err);
		})
	},
	closeRepairMoveToStock: function (req, res) {
		var body = req.body;
		var promises = [];
		var promises2 = [];
		var shipToStockPointId = 0;
		_.each(body.selectedTools, function (st) {
			if (st.currentStockPointId === 26) {
				shipToStockPointId = 25
			} else if (st.currentStockPointId === 28) {
				shipToStockPointId = 27
			} else if (st.currentStockPointId === 30) {
				shipToStockPointId = 29
			} else if (st.currentStockPointId === 32) {
				shipToStockPointId = 31
			} else if (st.currentStockPointId === 34) {
				shipToStockPointId = 33
			} else if (st.currentStockPointId === 35) {
				shipToStockPointId = 22
			}
			var repairInfo = JSON.parse(JSON.stringify(body.repairInfo));
			repairInfo.repairId = st.repairId;
			repairInfo.lastUserId = req.session.user.userId;
			repairInfo.stockPointShipToId = shipToStockPointId;
			promises.push(RepairService.updateRepair(repairInfo))
		})
		Q.all(promises).then(function (data) {
			_.each(body.selectedTools, function (st) {
				var toolInfo = JSON.parse(JSON.stringify(body.toolInfo));
				toolInfo.toolId = st.toolId;
				toolInfo.stockPointId = shipToStockPointId;
				promises2.push(ToolService.updateToolSimple(toolInfo))
			})
			Q.all(promises2).then(function (data2) {
				return res.json(data2);
			}, function (err) {
				return res.status(500).send(err);
			});
		}, function (err) {
			return res.status(500).send(err);
		});
	},
	printDNRStrapSheet: function (req, res) {
		var strapSheet = new PDFDocument({
			margin: 19,
			layout: 'landscape'
		});
		var strapSheetObject = req.body;

		strapSheet.pipe(res);
		strapSheet.image('assets/images/HRSDNRReamerSheetMUTPassed300DPI.jpg', 0, 0, {
			scale: 0.188
		})
		strapSheet.font('Times-Bold')
		strapSheet.fillColor('white')
		strapSheet.fontSize(20)
		strapSheet.text(strapSheetObject.serial, 55, 9)
		strapSheet.fontSize(30)
		strapSheet.text(strapSheetObject.lengthTotal, 355, 225)

		strapSheet.text(strapSheetObject.length1, 87, 288)

		strapSheet.text(strapSheetObject.length2, 200, 288)

		strapSheet.text(strapSheetObject.length3, 355, 288)

		strapSheet.text(strapSheetObject.length4, 513, 288)

		strapSheet.text(strapSheetObject.length5, 628, 288)
		strapSheet.fillColor('black')
		strapSheet.fontSize(23)
		strapSheet.text(strapSheetObject.topOdFractionalInches, 88, 419)

		strapSheet.text(strapSheetObject.midOdFractionalInches, 357, 419)

		strapSheet.text(strapSheetObject.bottomOdFractionalInches, 632, 419)
		strapSheet.fontSize(28)
		strapSheet.text(strapSheetObject.cuttingDiameterFraction, 127, 505)

		strapSheet.text(strapSheetObject.passThroughFraction, 338, 505)

		strapSheet.text(strapSheetObject.innerDiameterFraction, 338, 556)

		if (strapSheetObject.boxThreadName.length > 11) {
			strapSheet.fontSize(16)
			strapSheet.text(strapSheetObject.boxThreadName, 15, 560)

			strapSheet.text(strapSheetObject.pinThreadName, 120, 560)
		} else {
			strapSheet.fontSize(21)
			strapSheet.text(strapSheetObject.boxThreadName, 15, 560)

			strapSheet.text(strapSheetObject.pinThreadName, 120, 560)
		}

		strapSheet.fontSize(10)
		strapSheet.text(strapSheetObject.boxTorqueFTLB, 31, 579)

		strapSheet.text(strapSheetObject.pinTorqueFTLB, 137, 579)

		strapSheet.end()
	},
	getHeatCert: function (req, res) {
		var body = req.body;
		body.token = req.session.user.token;
		ToolService.getHeatCert(body, function (err, heatCert) {
			if (!err) {
				fs.readFile(body.documentId + ".pdf", function (err, data) {
					if (err) throw err;
					var pdf = new Buffer(data, 'base64');
					res.writeHead(200, {
						'Content-Type': 'application/pdf',
						'Content-Length': pdf.length
					});
					fs.unlink(body.documentId + ".pdf", function (err) {
						if (err) return console.log(err);
						res.end(pdf)
					});
				});
			} else {
				return res.status(500).send(err);
			}
		});
	},
	getVTI: function (req, res) {
		var body = req.body;
		fs.readFile(body.documentLocation + '/' + body.documentName, function (err, data) {
			if (err) return res.status(500).send(err)
			else {
				var pdf = new Buffer(data, 'base64');
				res.writeHead(200, {
					'Content-Type': 'application/pdf',
					'Content-Length': pdf.length
				});
				res.end(data)
			}
		});
	}
};
