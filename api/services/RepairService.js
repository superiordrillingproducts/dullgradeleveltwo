let Q = require('q');
let moment = require('moment');
let PartService = require('./PartService')

let createHashStructureSplitCount = 0;
let updateCutterDullGradeSplitCount = 0;
function createHashStructureSplit (qty, position, pockets, hashId, cb) {
	createHashStructureSplitCount = 0;
	let promises = []
	let loopCount = Math.ceil(pockets.length/35)
	for(let i = position; i < qty; i++) {
		promises.push(RepairService.createHashStructure(hashId, pockets[i]))
		position = i
	}
	Q.all(promises).then(function(data) {
		createHashStructureSplitCount++
		if(loopCount === createHashStructureSplitCount){
			createHashStructureSplitCount = 0
			cb(data);
		} else if(createHashStructureSplitCount < loopCount) {
			let newQty = pockets.length - position > 35 ? qty + position + 1 : pockets.length - position + qty - 1
			createHashStructureSplitSecond(newQty, qty, pockets, hashId, cb)
		} else {
			return res.status(500).send("The loop is not working.")
		}
	}, function(err) {
		return res.status(500).send(err);
	})
}
function createHashStructureSplitSecond(qty, position, pockets, hashId, cb) {
	let promises = [];
	let loopCount = Math.ceil(pockets.length / 35);
	for (let i = position; i < qty; i++) {
		promises.push(RepairService.createHashStructure(hashId, pockets[i]));
		position = i
	}
	Q.all(promises).then(function (data) {
		createHashStructureSplitCount++;
		if (loopCount === createHashStructureSplitCount) {
			createHashStructureSplitCount = 0;
			cb(data);
		} else if (createHashStructureSplitCount < loopCount) {
			let newQty = pockets.length - position > 35 ? qty + position + 1 : pockets.length - position + qty - 1;
			createHashStructureSplitSecond(newQty, qty, pockets, hashId, cb);
		} else {
			return res.status(500).send("The loop is not working.");
		}
	}, function (err) {
		return res.status(500).send(err);
	})
}
function updateCutterDullGradeSplit (qty, position, array, cb) {
	updateCutterDullGradeSplitCount = 0
	let promises = []
	let loopCount = Math.ceil(array.length/35)
	for(let i = position; i < qty; i++) {
		promises.push(RepairService.updateCutterDullGrade({cutterDullGradeId: array[i].cutterDullGradeId, cutterId: array[i].cutterId}))
		position = i
	}
	Q.all(promises).then(function(data) {
		updateCutterDullGradeSplitCount++
		if(loopCount === updateCutterDullGradeSplitCount){
			updateCutterDullGradeSplitCount = 0
			cb(data);
		} else if (updateCutterDullGradeSplitCount < loopCount) {
			let newQty = array.length - position > 35 ? qty + position + 1 : array.length - position + qty - 1
			updateCutterDullGradeSplitSecond(newQty, qty, array, cb)
		} else {
			return res.status(500).send("The loop is not working.")
		}
	}, function(err) {
		return res.status(500).send(err);
	})
}
function updateCutterDullGradeSplitSecond(qty, position, array, cb) {
	let promises = []
	let loopCount = Math.ceil(array.length / 35)
	for (let i = position; i < qty; i++) {
		promises.push(RepairService.updateCutterDullGrade({
			cutterDullGradeId: array[i].cutterDullGradeId,
			cutterId: array[i].cutterId
		}))
		position = i
	}
	Q.all(promises).then(function (data) {
		updateCutterDullGradeSplitCount++
		if (loopCount === updateCutterDullGradeSplitCount) {
			updateCutterDullGradeSplitCount = 0
			cb(data);
		} else if (updateCutterDullGradeSplitCount < loopCount) {
			let newQty = array.length - position > 35 ? qty + position + 1 : array.length - position + qty - 1
			updateCutterDullGradeSplitSecond(newQty, qty, array, cb)
		} else {
			return res.status(500).send("The loop is not working.")
		}
	}, function (err) {
		return res.status(500).send(err);
	})
}
module.exports = {
	createDnrRepair: function(repair) {
		var deferred = Q.defer();
		if (!repair.dateRecordCreated) {
			repair.dateRecordCreated = new Date();
		}
		Sdpi_DnrRepair.create(repair).exec(function(err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getRepairDullGradesByRepair: function(repairId) {
		var deferred = Q.defer();
		Repair_Repair.getDatastore().sendNativeQuery('CALL GetDullGrades($1)', [repairId], function(err, dgr) {
			if(!err) deferred.resolve(dgr.rows[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	mergeDullGradeCutterDullGrades: function(pocketStructure) {
		// assuming dullgrades are in ascending order: most recent is last
		_.each(pocketStructure, function(groups) {
			_.each(groups.blades, function(b) {
				_.each(b.rows, function(r) {
					_.each(r.pockets, function(p) {
						_.each(dullGradesAsc, function(dg) {
							_.each(dg.cutterDullGrades, function(cdg) {
								// implement logic to determine merging

								// precedence: (1) new, (2) rotate
								if(cdg.group === p.group && cdg.blade === p.blade && cdg.row === p.row && cdg.pocket === p.pocket) {
									// push cutterDullGrade to array inside the pocket, just to have the info at runtime
									if(!p.cutterDullGrades) p.cutterDullGrades = [];
									p.cutterDullGrades.push(cdg);

									// chance to override
									if(dg.cutterActionId === 1) {
										p.cutterDullGradeId = cdg.cutterDullGradeId;
										p.cutterConditionId = cdg.cutterConditionId;
										p.dullGradeId = cdg.dullGradeId;
										p.isFlameSpray = cdg.isFlameSpray;
										p.replaceWithCutterId = cdg.replaceWithCutterId;
										p.isReclaim = cdg.isReclaim;
										p.lotNumber = cdg.lotNumber;
										p.isAddOnRecord = cdg.isAddOnRecord;
										p.reasonForChange = cdg.reasonForChange;
									}
								}
							});
						});
					});
				});
			});
		});
	},
	getEmptyDullGradeStructure: function(identifier, toolFamilyId, repairId) {
		var deferred = Q.defer();

		var promise = PartService.getCutterStructure(identifier, toolFamilyId);
		promise.then(function(structure) {
			var dullGrade = {
				repairId: repairId,
				groups: structure
			};
			deferred.resolve(dullGrade);
		}).catch(function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	getDullGradesAndMarkupStructureForToolByDullGrade: function(identifier, toolFamilyId, dullGrade, hashId) {
		var deferred = Q.defer();
		var promise = PartService.getCutterStructure(identifier, toolFamilyId, hashId);
		var dullGradeId = dullGrade.dullGradeId;
		promise.then(function(markup) {
			var promise2 = RepairService.getDullGradesForRepairByDullGrade(dullGradeId);
			promise2.then(function(records) {
				// let promises = []
				let updateCutterDullGradeArray = []
				_.each(markup, function(group) {
					_.each(group.blades, function(b) {
						_.each(b.rows, function(r) {
							_.each(r.pockets, function(p) {
								_.each(records, function(rec) {
									if(rec.group === p.group && rec.blade === p.blade && rec.row === p.row && rec.pocket === p.pocket) {

										if(p.cutterId){
											if(p.cutterId !== rec.cutterId) {
												updateCutterDullGradeArray.push({cutterDullGradeId: rec.cutterDullGradeId, cutterId: p.cutterId})
												// promises.push(RepairService.updateCutterDullGrade({cutterDullGradeId: rec.cutterDullGradeId, cutterId: p.cutterId}))
											}
										}

										if(p.cutterDullGradeId){
											rec.oldPocketInfo = JSON.parse(JSON.stringify(p));
										}
										p.cutterDullGradeId = rec.cutterDullGradeId;
										p.dnrRepairId = rec.dnrRepairId;
										p.cutterConditionId = rec.cutterConditionId;
										p.cutterActionId = rec.cutterActionId;
										p.isFlameSpray = rec.isFlameSpray;
										p.cutterAction = rec.cutterAction;
										p.cutterCondition = rec.cutterCondition;
										p.cutterActionIcon = rec.cutterActionIcon;
										p.cutterName = rec.cutterName;
										p.replaceWithCutterName = rec.replaceWithCutterName;
										p.replaceWithCutterId = rec.replaceWithCutterId;
										p.isReclaim = rec.isReclaim;
										p.dullGradeId = rec.dullGradeId;
										p.lotNumber = rec.lotNumber;
										p.isAddOnRecord = rec.isAddOnRecord;
										if(rec.oldPocketInfo){
											if(p.oldPocketInfo && p.oldPocketInfo.length > 0){
												p.oldPocketInfo.push(rec.oldPocketInfo);
											} else {
												p.oldPocketInfo = [rec.oldPocketInfo];
											}
										}
									}
								});
							});
						});
					});
				});
				var dg = JSON.parse(JSON.stringify(dullGrade));
				dg.groups = markup;
				if(updateCutterDullGradeArray.length > 0){
					if(updateCutterDullGradeArray.length < 35){
						qty = updateCutterDullGradeArray.length
					} else {
						qty = 35
					}
					updateCutterDullGradeSplit(qty, 0, updateCutterDullGradeArray, function(data) {
						deferred.resolve(dg)
					})
				} else {
					deferred.resolve(dg)
				}
				// if(promises && promises.length > 0) {
				// 	Q.all(promises).then(data => {
				// 		deferred.resolve(dg)
				// 	}, err => {
				// 		deferred.reject(err)
				// 	})
				// } else {
				// 	deferred.resolve(dg);
				// }
			}).catch(function(err) {
				deferred.reject(err);
			});
		}).catch(function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	getDullGradeAndHashStructureForCurrentRepair: function(identifier, toolFamilyId, dullGrade) {
		console.log('made it')
		var deferred = Q.defer();
		var promise = PartService.getHashStructure(identifier);
		var dullGradeId = dullGrade.dullGradeId;
		promise.then(function(markup) {
			console.log('got markup');
			// get CutterDullGrades
			var promise2 = RepairService.getDullGradesForRepairByDullGrade(dullGradeId);
			promise2.then(function(records) {
				console.log('got CutterDullGrades');
				_.each(markup, function(group) {
					_.each(group.blades, function(b) {
						_.each(b.rows, function(r) {
							_.each(r.pockets, function(p) {
								_.each(records, function(rec) {
									if(rec.group === p.group && rec.blade === p.blade && rec.row === p.row && rec.pocket === p.pocket) {
										if(p.cutterDullGradeId){
											rec.oldPocketInfo = JSON.parse(JSON.stringify(p));
										}
										p.cutterDullGradeId = rec.cutterDullGradeId;
										p.dnrRepairId = rec.dnrRepairId;
										p.cutterConditionId = rec.cutterConditionId;
										p.cutterActionId = rec.cutterActionId;
										p.isFlameSpray = rec.isFlameSpray;
										p.cutterAction = rec.cutterAction;
										p.cutterCondition = rec.cutterCondition;
										p.cutterActionIcon = rec.cutterActionIcon;
										p.cutterName = rec.cutterName;
										p.replaceWithCutterName = rec.replaceWithCutterName;
										p.replaceWithCutterId = rec.replaceWithCutterId;
										p.isReclaim = rec.isReclaim;
										p.dullGradeId = rec.dullGradeId;
										p.lotNumber = rec.lotNumber;
										p.isAddOnRecord = rec.isAddOnRecord;
										if(rec.oldPocketInfo){
											if(p.oldPocketInfo && p.oldPocketInfo.length > 0){
												p.oldPocketInfo.push(rec.oldPocketInfo);
											} else {
												p.oldPocketInfo = [rec.oldPocketInfo];
											}
										}
									}
								});
							});
						});
					});
				});
				var dg = JSON.parse(JSON.stringify(dullGrade));
				dg.groups = markup;
				deferred.resolve(dg);
			}).catch(function(err) {
				deferred.reject(err);
			});
		}).catch(function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	getDullGradesAndMarkupStructureForTool: function(identifier, toolFamilyId, repairId, hashId) {
		var deferred = Q.defer();
		var promise = PartService.getCutterStructure(identifier, toolFamilyId, hashId);
		promise.then(function(markup) {
			var promise2 = RepairService.getDullGradesForRepair(repairId);
			promise2.then(function(records) {
				_.each(markup, function(group) {
					_.each(group.blades, function(b) {
						_.each(b.rows, function(r) {
							_.each(r.pockets, function(p) {
								_.each(records, function(rec) {
									if(rec.group === p.group && rec.blade === p.blade && rec.row === p.row && rec.pocket === p.pocket) {
										p.cutterDullGradeId = rec.cutterDullGradeId;
										p.dnrRepairId = rec.dnrRepairId;
										p.cutterConditionId = rec.cutterConditionId;
										p.cutterActionId = rec.cutterActionId;
										p.isFlameSpray = rec.isFlameSpray;
										p.cutterAction = rec.cutterAction;
										p.cutterCondition = rec.cutterCondition;
										p.cutterActionIcon = rec.cutterActionIcon;
										p.cutterName = rec.cutterName;
										p.replaceWithCutterId = rec.replaceWithCutterId;
										p.isReclaim = rec.isReclaim;
										p.dullGradeId = rec.dullGradeId;
										p.lotNumber = rec.lotNumber;
									}
								});
							});
						});
					});
				});
				deferred.resolve(markup);
			}).catch(function(err) {
				deferred.reject(err);
			});
		}).catch(function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	updateDnrRepair: function(repair) {
		var deferred = Q.defer();

		Sdpi_DnrRepair.update({
			dnrRepairId: repair.dnrRepairId
		}, {
			statusId: repair.statusId,
			toolId: repair.toolId,
			repairTypeId: repair.repairTypeId,
			threadInspectionNumber: repair.threadInspectionNumber,
			estimatedFinishDate: repair.estimatedFinishDate,
			dueDate: repair.dueDate,
			completionDate: repair.completionDate,
			dateRecordCreated: repair.dateRecordCreated,
		}).exec(function(err, updated) {
			if (!err) deferred.resolve(updated[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	changeRepairStatusTo: function(dnrRepairId, statusId, users) {
		var deferred = Q.defer();

		Sdpi_DnrRepair.update({
			dnrRepairId: dnrRepairId
		}, {
			statusId: statusId
		}, function(err, updatedRepair) {
			if (!err) {
				var repair = {
					dnrRepairId: dnrRepairId
				};
				var date = new Date();

				var promises = [];
				_.each(users, function(user) {
					promises.push(RepairService.createDnrHistory(Enums.dnrHistoryType.statusChanged, repair, date, user.userId, 'Repair status changed to ' + statusId));
				});
				Q.all(promises, function(data) {
					deferred.resolve(updatedRepair);
				}, function(err) {
					deferred.reject(err);
				});
			} else deferred.reject(err);
		});

		return deferred.promise;
	},
	createDnrHistory: function(dnrHistoryTypeId, repair, date, userId, comments) {
		var deferred = Q.defer();

		var history = {
			dnrRepairId: repair.dnrRepairId,
			dnrHistoryTypeId: dnrHistoryTypeId,
			userId: userId,
			date: date,
			comments: comments
		};

		Sdpi_DnrHistory.create(history).exec(function(err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	getDullGradesForRepair: function(repairId) {
		var deferred = Q.defer();
		SdpiContext.query('CALL GetDullGradeStructureForRepair(?)', [repairId], function(err, results) {
			if (!err) {
				deferred.resolve(results[0]);
			} else {
				deferred.reject(err);
			}
		});

		return deferred.promise;
	},
	getDullGradesForRepairByDullGrade: function(dullGradeId) {
		var deferred = Q.defer();
		Repair_Repair.getDatastore().sendNativeQuery('CALL GetDullGradeStructureForRepairByDullGrade($1)', [dullGradeId], function(err, results) {
			if (!err) {
				deferred.resolve(results.rows[0]);
			} else {
				deferred.reject(err);
			}
		});

		return deferred.promise;
	},
	getDullGradeStructureForCurrentRepair: function(dnrRepairId) {
		var deferred = Q.defer();

		var promise = RepairService.getDullGradesForRepair(dnrRepairId);

		promise.then(function(records) {
			var groups = [];
			if (records && records.length > 0) {
				var cs = records;
				groups = [{
					blades: [{
						order: 1,
						rows: [{
							order: 1,
							pockets: []
						}]
					}]
				}];
				_.each(cs, function(record) {
					record.type = record.key;
					PartService.findPlaceInCutterStructure(record, groups);
				});
			}

			deferred.resolve(groups);
		}).catch(function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	findPlaceInCutterStructure: function(record, groups) {
		var gFound = false;
		for (var g = 0; g < groups.length; g++) {
			if (record.group === g + 1) {
				gFound = true;
				var bFound = false;
				for (var b = 0; b < groups[g].blades.length; b++) {
					if (record.blade === b + 1) {
						bFound = true;
						var rFound = false;
						for (var r = 0; r < groups[g].blades[b].rows.length; r++) {
							if (record.row === r + 1) {
								rFound = true;
								groups[g].blades[b].rows[r].pockets.push(record);
							}
						}
						if (!rFound) {
							groups[g].blades[b].rows.push({
								order: groups[g].blades[b].rows.length + 1,
								pockets: []
							});
							PartService.findPlaceInCutterStructure(record, groups);
						}
					}
				}
				if (!bFound) {
					groups[g].blades.push({
						order: groups[g].blades.length + 1,
						rows: []
					});
					PartService.findPlaceInCutterStructure(record, groups);
				}
			}
		}
		if (!gFound) {
			groups.push({
				order: groups.length + 1,
				blades: []
			});
			PartService.findPlaceInCutterStructure(record, groups);
		}
	},
	createAndGetDullGradesFromPartPocketStructure: function(toolRepair) {
		var deferred = Q.defer();

		var parentPartId = toolRepair.partId;
		var cutterStructureChronoId = toolRepair.cutterStructureChronoId;
		var toolFamilyId = toolRepair.toolFamilyId;
		var identifier;

		if (toolFamilyId === 4) identifier = parentPartId;
		else identifier = cutterStructureChronoId;

		var promise = PartService.getCutterStructureRecords(identifier, toolFamilyId);

		promise.then(function(records) {
			// loop through pocket structure records and add a dullgrade record for each loop.
			var dullgrades = [];

			_.each(records, function(r) {
				var dg;
				if (toolFamilyId === 4) {
					dg = {
						group: r.group,
						blade: r.blade,
						row: r.row,
						pocket: r.pocket,
						cutterPocketId: r.cutterPocketId,
						cutterId: null,
						cutterConditionId: null,
						cutterActionId: null,
						dnrRepairId: toolRepair.repairId
					};
				} else {
					dg = {
						group: r.group,
						blade: r.blade,
						row: r.row,
						pocket: r.pocket,
						cutterPocketId: r.cutterPocketId,
						cutterId: null,
						cutterConditionId: null,
						cutterActionId: null,
						dnrRepairId: toolRepair.repairId
					};
				}
				dullgrades.push(dg);
			});

			promise = RepairService.saveDullGrades(dullgrades);

			promise.then(function(data) {
				promise = RepairService.getDullGradeStructureForCurrentRepair(toolRepair.repairId);
				promise.then(function(data) {
					deferred.resolve(data);
				}).catch(function(err) {
					deferred.reject(err);
				});
			}).catch(function(err) {
				deferred.reject(err);
			});

		}).catch(function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	saveDullGrades: function(dullgrades) {
		var deferred = Q.defer();
		if (dullgrades && dullgrades.length > 0) {
			Repair_CutterDullGrade.create(dullgrades).exec(function(err, data) {
				if (!err) deferred.resolve(data);
				else deferred.reject(err);
			});
		}
		return deferred.promise;
	},
	updateCutterDullGrade: function(dullgrade) {
		var deferred = Q.defer();
		Repair_CutterDullGrade.update({cutterDullGradeId: dullgrade.cutterDullGradeId}, dullgrade).exec((err, data) => {
			if(!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	updateCutterDullGrades: function(dullgrades) {
		var deferred = Q.defer();
		var promises = [];
		_.each(dullgrades, function(dg) {
			dg = {
				cutterDullGradeId: dg.cutterDullGradeId,
				group: dg.group,
				blade: dg.blade,
				row: dg.row,
				pocket: dg.pocket,
				cutterPocketId: dg.cutterPocketId,
				cutterId: dg.cutterId,
				cutterConditionId: dg.cutterConditionId,
				cutterActionId: dg.cutterActionId,
				dnrRepairId: dg.dnrRepairId
			};

			promises.push(RepairService.updateDullGrade(dg));
		});

		Q.all(promises).then(function(data) {
			deferred.resolve(data);
		}, function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	updateDullGrade: function(dullgrade) {
		var deferred = Q.defer();
		var cutterDullGradeId = dullgrade.cutterDullGradeId;
		delete dullgrade.cutterDullGradeId;

		Repair_CutterDullGrade.update({
			cutterDullGradeId: cutterDullGradeId
		}, dullgrade).exec(function(err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	saveUpdateDullgrade: function(dullgrade, dullGradeId) {
		var deferred = Q.defer();
		var dg = {
			cutterDullGradeId: dullgrade.cutterDullGradeId,
			group: dullgrade.group,
			blade: dullgrade.blade,
			row: dullgrade.row,
			pocket: dullgrade.pocket,
			cutterPocketId: dullgrade.cutterPocketId,
			cutterId: dullgrade.cutterId,
			cutterConditionId: dullgrade.cutterConditionId,
			cutterActionId: dullgrade.cutterActionId,
			dullGradeId: dullGradeId,
			replaceWithCutterId: dullgrade.replaceWithCutterId,
			lotNumber: dullgrade.lotNumber
		};
		if(dullgrade.isFlameSpray) dg.isFlameSpray = dullgrade.isFlameSpray;
		else dg.isFlameSpray = 0;

		if(dullgrade.isReclaim) dg.isReclaim = dullgrade.isReclaim;
		else dg.isReclaim = 0;

		// try update it
		if (dullgrade.cutterDullGradeId) {
			if(dg.cutterConditionId === undefined){
				dg.cutterConditionId = null;
			}
			Repair_CutterDullGrade.update({
				cutterDullGradeId: dullgrade.cutterDullGradeId
			}, dg).exec(function (err, updated) {
				if (!err) {
					deferred.resolve(updated);
				} else {
					deferred.reject(err);
				}
			});
		} else {
			Repair_CutterDullGrade.create(dg).exec(function(err, created) {
				if (!err) {
					deferred.resolve(created);
				} else {
					deferred.reject(err);
				}
			});
		}
		return deferred.promise;
	},
	saveReworkCutterDullGrade: function(dullgrade) {
		var deferred = Q.defer();
		if(!dullgrade.isAddOnRecord) dullgrade.isAddOnRecord = 0;
		var dg = {
			//cutterDullGradeId: dullgrade.cutterDullGradeId, 
			group: dullgrade.group,
			blade: dullgrade.blade,
			row: dullgrade.row,
			pocket: dullgrade.pocket,
			cutterPocketId: dullgrade.cutterPocketId,
			cutterId: dullgrade.cutterId,
			cutterConditionId: dullgrade.cutterConditionId,
			cutterActionId: dullgrade.cutterActionId,
			dullGradeId: dullgrade.dullGradeId,
			isAddOnRecord: dullgrade.isAddOnRecord
		};
		if(dullgrade.isFlameSpray) dg.isFlameSpray = dullgrade.isFlameSpray;
		else dg.isFlameSpray = 0;

		Repair_CutterDullGrade.create(dg).exec(function(err, created) {
			if (!err) {
				deferred.resolve(created);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	createDullGradeRecord: function(repairId, getExistingWithDullGradeId) {
		var deferred = Q.defer();

		if (getExistingWithDullGradeId) {
			Repair_DullGrade.findOne({
				dullGradeId: getExistingWithDullGradeId
			}).exec(function (err, dullGrade) {
				if (!err) deferred.resolve(dullGrade);
				else deferred.reject(err);
			});
		} else {
			Repair_DullGrade.create({
				repairId: repairId
			}).exec(function (err, dullGrade) {
				if (!err) deferred.resolve(dullGrade);
				else deferred.reject(err);
			});
		}
		return deferred.promise;
	},
	saveDullGrade: function(dullgrade) {
		var deferred = Q.defer();
		if (dullgrade) {
			var dg = {
				group: dullgrade.group,
				blade: dullgrade.blade,
				row: dullgrade.row,
				pocket: dullgrade.pocket,
				cutterPocketId: dullgrade.cutterPocketId,
				cutterId: dullgrade.cutterId,
				cutterConditionId: dullgrade.cutterConditionId,
				cutterActionId: dullgrade.cutterActionId
			};
			Repair_CutterDullGrade.update({
				cutterDullGradeId: dullgrade.cutterDullGradeId
			}, dg).exec(function(err, data) {
				if (!err) {
					deferred.resolve(data);
				} else {
					deferred.reject(err);
				}
			});
		}
		return deferred.promise;
	},
	getFlowRecord: function(flowId) {
		return new Promise((resolve, reject) => {
			Repair_Flow.findOne({flowId: flowId}).exec((err, flow) => {
				if(!err) resolve(flow)
				else reject(new Error('Now found'))
			})

		})
	},
	startStopTimeStampForRepair: function(laborTimeStamp, partialRateTimeForSharedOrBulkItems, now, idleTime) {
		var deferred = Q.defer();

		if (!now) {
			now = new Date();
		}
		if (laborTimeStamp.laborTimeStampId) {
			// update start/stop
			if (laborTimeStamp.startTime) {
				// close it
				if (idleTime) {
					var nowMoment = moment(now);
					var updatedStopTime = nowMoment.subtract(idleTime, 's');
					laborTimeStamp.stopTime = updatedStopTime.toDate();
				} else {
					laborTimeStamp.stopTime = now;
				}
				var diff = moment.duration(moment(now).diff(moment(laborTimeStamp.startTime)));
				var elapsedTime;
				var elapsedTimeInDecimal;
				if (partialRateTimeForSharedOrBulkItems && partialRateTimeForSharedOrBulkItems < 1) {
					// because several items were done at the same time, the time spent needs to be shared.
					// take the partial time of this item based on partialRateTimeForSharedOrBulkItems value.
					var partialMilliseconds = diff * partialRateTimeForSharedOrBulkItems;
					var millsecondsStartTime = moment(laborTimeStamp.startTime).valueOf();
					var adjustedStopTime = moment(millsecondsStartTime + partialMilliseconds);
					/// figure total elapsed time for multi tool
					elapsedTime = Global.getTimeDifferenceBetweenDates(laborTimeStamp.stopTime, laborTimeStamp.startTime);
					var timeToMultiByTotalRate = Global.getTimeDifferenceBetweenDates(adjustedStopTime, laborTimeStamp.startTime);
					laborTimeStamp.totalElapsedTime = elapsedTime;
					laborTimeStamp.timeToMultiplyByTotalRate = timeToMultiByTotalRate;
					elapsedTimeInDecimal = moment.duration(timeToMultiByTotalRate).asHours();
					laborTimeStamp.totalValueOfRecord = elapsedTimeInDecimal * laborTimeStamp.totalRate;
				} else {
					// figure total elapsed time for single tool
					elapsedTime = Global.getTimeDifferenceBetweenDates(laborTimeStamp.stopTime, laborTimeStamp.startTime);
					laborTimeStamp.totalElapsedTime = elapsedTime;
					laborTimeStamp.timeToMultiplyByTotalRate = elapsedTime;
					elapsedTimeInDecimal = moment.duration(elapsedTime).asHours();
					laborTimeStamp.totalValueOfRecord = elapsedTimeInDecimal * laborTimeStamp.totalRate;
				}

				let promises = []

				promises.push(new Promise((resolve, reject) => {
					// now update it
					Repair_LaborTimeStamp.update({
						laborTimeStampId: laborTimeStamp.laborTimeStampId
					}, {
						stopTime: laborTimeStamp.stopTime,
						totalElapsedTime: laborTimeStamp.totalElapsedTime,
						timeToMultiplyByTotalRate: laborTimeStamp.timeToMultiplyByTotalRate,
						totalValueOfRecord: laborTimeStamp.totalValueOfRecord
					}).exec(function(err, data) {
						if (!err) {
							resolve(data);
						} else {
							reject(err);
						}
					});
				}))

				// if new flow is a "Complete" flow then also change this repair and tool
				if(laborTimeStamp.nextFlow && laborTimeStamp.nextFlow.name === 'Complete') {
					// update repair promise
					promises.push(new Promise((resolve, reject) => {
						Repair_Repair.update({
							repairId: laborTimeStamp.repairId
						}, {
							repairStatusId: 1
						}).exec((err, data) => {
							if(!err) resolve(data)
							else reject(err)
						})
					}))

					// update tool promise
					promises.push(new Promise((resolve, reject) => {
						SdpTools_Tools.update({
							toolId: laborTimeStamp.tool.toolId
						}, {
							toolStatusId: 22
						}).exec((err, data) => {
							if(!err) resolve(data)
							else reject(err)
						})
					}))
				}

				Promise.all(promises).then(data => {
					deferred.resolve(data[0])
				}).catch(err => {
					deferred.reject(err)
				})

			} else {
				// start it
				laborTimeStamp.startTime = now;
				Repair_LaborTimeStamp.update({
					laborTimeStampsId: laborTimeStamp.laborTimeStampsId
				}, laborTimeStamp).exec(function(err, data) {
					if (!err) deferred.resolve(data);
					else deferred.reject(err);
				});
			}
		} else {
			// create new laborTimeStamp and start it
			laborTimeStamp.startTime = now;
			Repair_LaborTimeStamp.create(laborTimeStamp).exec(function(err, data) {
				if (!err) deferred.resolve(data);
				else deferred.reject(err);
			});
		}
		return deferred.promise;
	},
	getLaborTimeStamps: function(workStationId, noStopDate) {
		var deferred = Q.defer();

		var query = {
			workStationId: workStationId,
		};
		if (noStopDate) {
			query.stopTime = null;
		}

		Repair_LaborTimeStamp.find(query).exec(function(err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getAllRepairRecords: function() {
		var deferred = Q.defer();
		RepairContext.query('CALL GetAllRepairRecords', function(err, repairs) {
			if (!err) {
				deferred.resolve(repairs[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getAllOpenRepairRecords: function() {
		var deferred = Q.defer();
		Repair_Repair.getDatastore().sendNativeQuery('CALL GetAllOpenRepairRecords', function(err, repairs) {
			if (!err) {
				deferred.resolve(repairs.rows[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getFlowRecords: function(facilityId) {
		var deferred = Q.defer();
		Repair_Flow.getDatastore().sendNativeQuery('SELECT * FROM Flow where facilityId=$1', [facilityId], function(err, records) {
			if (!err) {
				deferred.resolve(records.rows);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getFlowRelationshipForCurrentFlow: function (currentFlow) {
		var deferred = Q.defer();
		Repair_FlowRelationship.getDatastore().sendNativeQuery('SELECT fr.currentFlowId, fr.nextFlowId, fr.utilizesProgrammingChrono, fr.programmingChrono, f.familyId, f.workStationId, f.name, f.description FROM Repair.FlowRelationship fr LEFT JOIN Repair.Flow f on fr.nextFlowId = f.flowId where currentFlowId = $1 ORDER BY fr.nextFlowId', [currentFlow.currentFlowId], function (err, nextFlow) {
			if (!err) deferred.resolve(nextFlow.rows);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getFlowWithToolRepair: function(flow, repairs) {
		var deferred = Q.defer();
		// get tool repairs for this flow
		var toolRepairs = [];
		_.each(repairs, function(repair) {
			if(repair.flowId === flow.nextFlowId) {
        		repair.prevFlowId = flow.currentFlowId;
        		toolRepairs.push(repair);
        	}
		});
		flow.toolRepairs = toolRepairs;

		deferred.resolve(flow);

		return deferred.promise;
	},
	getRepairFromLaborTimeStamps: function(laborTimeStamps) {
		var deferred = Q.defer();
		var promises = [];
		var repairIds = [];
		_.each(laborTimeStamps, function(lts) {
			var repairIdExists = _.find(repairIds, function(rid) {
				return lts.repairId === rid;
			});
			if (!repairIdExists) {
				repairIds.push(lts.repairId);
				promises.push(RepairService.getRepairByRepairId(lts.repairId));
			}
		});

		Q.all(promises).then(function(data) {
			data = Global.convert2dArrayTo1dArray(data);
			deferred.resolve(data);
		}, function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	getRepairByRepairId: function(repairId) {
		var deferred = Q.defer();
		Repair_Repair.find({
			repairId: repairId
		}).exec(function(err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getFacility: function(facilityId) {
		var deferred = Q.defer();
		Repair_Facility.find({
			facilityId: facilityId
		}).exec(function(err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getWorkStation: function(workStationId) {
		var deferred = Q.defer();
		Repair_WorkStation.find({
			workStationId: workStationId
		}).exec(function(err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getFacilities: function(programId) {
		var deferred = Q.defer();
		if (programId) {
			Repair_Facility.find({
				where: {
					programId: programId
				},
			}).exec(function (err, data) {
				if (!err) deferred.resolve(data);
				else deferred.reject(err);
			});
		} else {
			Repair_Facility.find({}).exec(function (err, data) {
				if (!err) deferred.resolve(data);
				else deferred.reject(err);
			});

		}
		return deferred.promise;
	},
	createRepair: function(tool, users, ancillaryInfo, facility, isSdpTool, flowToSendTool) {
		var deferred = Q.defer();
		var repair = {
			toolFamilyId: ancillaryInfo.toolFamilyId,
			flowId: flowToSendTool,
			repairStatusId: 2,
			dateToolArrived: ancillaryInfo.dateToolArrived,
			lastUserId: users.userId
		};

		// Check argument 'isSdpTool' to determine if this repair is for a sdp owned tool or non-sdp owned tool.
		if (!isSdpTool) {
			repair.nonSdpToolId = tool.toolId;
			repair.toolId = null;
			repair.repairCustomerId = tool.repairCustomerId;
		} else {
			repair.toolId = tool.toolId;
			repair.partIdForThisRepair = tool.partIdNewForDNR;
			repair.hashId = tool.hashId;
			repair.stockPointReceivedFromId = tool.stockPointReceivedFromId;
			repair.repairCustomerId = tool.customerId;
			repair.customerOwned = tool.customerOwned;
		}
		Repair_Repair.create(repair).fetch().exec(function(err, createdRepair) {
			if (!err) {
				deferred.resolve(createdRepair);
			} else deferred.reject(err);
		});

		return deferred.promise;
	},
	getFirstFlow: function(facilityId, familyId) {
		var deferred = Q.defer();
		var currentFlowId;
		if(facilityId === 1 && familyId === 4) {
			currentFlowId = 48; // receiving flow for baker bit
		} else if (facilityId === 6 && familyId === 1) {
			currentFlowId = 105; // receiving flow for dnr texas
		} else if (facilityId === 6 && familyId === 2) {
			currentFlowId = 106; // receiving flow for vstream texas
		} else if (facilityId === 2 && familyId === 1) {
			currentFlowId = 56; // receiving flow for dnr
		} else if (facilityId === 7 && familyId === 1) {
			currentFlowId = 116; // receiving flow for dnr SIGS Dubai
		} else if(facilityId === 3) {
			currentFlowId = 80
		} else if(facilityId === 4) {
			currentFlowId = 81
		} else if(facilityId === 5) {
			currentFlowId = 82
		} else {
			currentFlowId = 57; // receiving flow for vstream
		}

		Repair_FlowRelationship.findOne({
			currentFlowId: currentFlowId
		}).exec(function (err, flowRel) {
			if (!err) {
				Repair_Flow.findOne({
					flowId: flowRel.nextFlowId
				}).exec(function (err, flow) {
					if (!err) deferred.resolve(flow);
					else deferred.reject(err);
				});
			}
		});

		return deferred.promise;
	},
	getFlowWithToolRepairs: function(flowId) {
		var deferred = Q.defer();
		var currentFlow = {currentFlowId: flowId};
		var proms = [];
		proms.push(RepairService.getFlowRelationshipForCurrentFlow(currentFlow));
		proms.push(RepairService.getAllRepairRecords());
		Q.all(proms).then(function(data) {
			var promises = [];
			var flows = data[0];
			var repairs = data[1];
			_.each(flows, function(flow) {
				promises.push(RepairService.getFlowWithToolRepair(flow, repairs));
			});
			Q.all(promises).then(function(data) {
				deferred.resolve(data);
			}, function(err) {
				deferred.reject(err);
			});
		}, function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	updateRepair: function(repairToUpdate) {
		var deferred = Q.defer();
		var repairId = repairToUpdate.repairId;
		delete repairToUpdate.repairId;
		Repair_Repair.update({
			repairId: repairId
		}, repairToUpdate).exec(function (err, updatedRepair) {
			if (!err) deferred.resolve(updatedRepair);
			else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getAllWorkStationsForFacility: function(facilityId) {
		var deferred = Q.defer();
		var query = 'SELECT * FROM WorkStation where status = 1 and facilityId = $1 order by chrono';
		var rawResult = Repair_WorkStation.getDatastore().sendNativeQuery(query, [facilityId],function(err,workStations){
			if (!err) deferred.resolve(workStations.rows);
			else deferred.reject(err);
		});
		// Repair_WorkStation.query('SELECT * FROM WorkStation where status = 1 and facilityId = (?) order by chrono', [facilityId], function(err, workStations) {
		// 	if (!err) deferred.resolve(workStations);
		// 	else deferred.reject(err);
		// });
		// Repair_WorkStation.find({
		//     facilityId: facilityId
		// }).exec(function(err, workStations) {
		//     if (!err) deferred.resolve(workStations);
		//     else deferred.reject(err);
		// });
		return deferred.promise;
	},
	saveFlameSprayInfo: function(flamesprays) {
		var deferred = Q.defer();
		var promises = [];
		_.each(flamesprays, function(fs) {
			promises.push(RepairService.saveFlameSpray(fs));
		});

		Q.all(promises).then(function(data) {
			deferred.resolve(data);
		}, function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	saveFlameSpray: function(flamespray) {
		var deferred = Q.defer();

		Repair_CutterDullGrade.update({
			cutterDullGradeId: flamespray.cutterDullGradeId
		}, {
			isFlameSpray: flamespray.isFlameSpray,
			cutterActionId: flamespray.cutterActionId
		}).exec(function (err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	get8InchFractions: function(cb) {
    	GlobalValues_8Inches.query('SELECT `8InchesId` as Id, `8inchesfraction` as fraction, `8inchesdecimal` as `decimal` FROM `8Inches`', function(err, fractions) {
			if (!err) return cb(err, fractions);
			else return cb(err, null);
		});
	},
	getWholeNumbers: function(cb) {
		GlobalValues_WholeNumbers.query('SELECT * FROM wholeNumbers', function(err, wholeNumbers) {
			if (!err) return cb(err, wholeNumbers);
			else return cb(err, null);
		});
	},
	getToolSections: function(cb) {
		Repair_ToolSection.query('SELECT * FROM toolSection', function(err, sections) {
			if (!err) return cb(err, sections);
			else return cb(err, null);
		});
	},
	getToolSectionInsepectionIssueJunction: function(cb) {
		Repair_ToolSection_InspectionIssue.query('SELECT * FROM ToolSection_InspectionIssue', function(err, junctions) {
			if (!err) return cb(err, junctions);
			else return cb(err, null);
		});
	},
	getInspectionIssues: function(cb) {
		Repair_InspectionIssue.query('SELECT * FROM inspectionIssue', function(err, issues) {
			if (!err) return cb(err, issues);
			else return cb(err, null);
		});
	},
	getStockPointsByRepairCustomerId: function(repairCustomerId) {
		var deferred = Q.defer();

		Repair_RepairStockPoints.getDatastore().sendNativeQuery('SELECT * FROM RepairStockPoint WHERE repairCustomerId = $1', [repairCustomerId], function(err, stockPoints) {
			if (stockPoints) deferred.resolve(stockPoints.rows[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getRepairCustomers: function(cb) {
		Repair_RepairCustomer.query('SELECT * FROM RepairCustomer', function(err, customers) {
			if (!err) return cb(err, customers);
			else return cb(err, null);
		});
	},
	getAncillaryInfo: function(tableNameOfAncillaryInfo, repairId) {
		var deferred = Q.defer();

		if (tableNameOfAncillaryInfo && repairId) {
			Repair_Repair.getDatastore().sendNativeQuery('SELECT * FROM ' + tableNameOfAncillaryInfo + ' WHERE repairId = $1', [repairId], function(err, data) {
				if (data) deferred.resolve(data.rows[0]);
				else deferred.reject(err);
			});
		}
		return deferred.promise;
	},
	saveBakerBitAncillaryInfo: function(ids, partInfo, ancillary, repairInfo) {
		var deferred = Q.defer();
		var promises = [];
		if (partInfo.partId) {
			promises.push(RepairService.updateNonSdpToolPartId({
				partId: partInfo.partId,
				toolId: ids.nonSdpToolId,
				repairCustomerId: repairInfo.repairCustomerId
			}));
			repairInfo.repairId = ids.repairId;
			repairInfo.partIdForThisRepair = partInfo.partId;
			promises.push(RepairService.updateRepairPartId(
				repairInfo
			));
		} else {
			NonSdpTools_PartBakerBit.create(partInfo).exec(function(err, created) {
				if (!err) {
					promises.push(RepairService.updateNonSdpToolPartId({
						partId: created.partBakerBitId,
						toolId: ids.nonSdpToolId,
						repairCustomerId: repairInfo.repairCustomerId
					}));
					repairInfo.repairId = ids.repairId;
					repairInfo.partIdForThisRepair = created.partBakerBitId;
					promises.push(RepairService.updateRepairPartId(
						repairInfo
					));
				} else {
					deferred.reject(err);
				}
			});
		};

		Q.all(promises).then(function(data) {
			ancillary.repairId = ids.repairId;
			if (ids.ancillaryBakerBitId) {
				Repair_AncillaryBakerBit.update({
					ancillaryBakerBitId: ids.ancillaryBakerBitId
				}, ancillary).exec(function (err, updated) {
					if (!err) deferred.resolve(updated);
					else deferred.rejected(err);
				});
			} else {
				Repair_AncillaryBakerBit.create(ancillary).exec(function(err, created) {
					if (!err) {
						deferred.resolve(created);
					} else {
						deferred.reject(err);
					}
				});
			}
		}, function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	updateNonSdpToolPartId: function(partObject) {
		var deferred = Q.defer();
		NonSdpTools_Tools.update({
			toolId: partObject.toolId
		}, {
			partId: partObject.partId,
			repairCustomerId: partObject.repairCustomerId
		}).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.rejected(err);
		});
		return deferred.promise;
	},
	updateRepairPartId: function(repairObject) {
		var deferred = Q.defer();
		var repairId = repairObject.repairId;
		delete repairObject.repairId;
		Repair_Repair.update({
			repairId: repairId
		}, repairObject).exec(function(err, repairUpdated) {
			if (!err) deferred.resolve(repairUpdated);
			else deferred.reject(err);
		});
	},
	checkIfPartExist: function(partNumber) {
		var deferred = Q.defer();
		NonSdpTools_PartBakerBit.findOne({
			partNumber: partNumber
		}).exec(function(err, part) {
			if (!err) deferred.resolve(part);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCountOfFlameSprayByDullGrade: function(partBakerBitId, dullGradeId) {
		var deferred = Q.defer();
		RepairContext.query('CALL GetCountOfFlameSpraysForBakerBitRepairByDullGrade(?,?)', [partBakerBitId, dullGradeId], function(err, count) {
			if (!err) {
				deferred.resolve(count[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getCountOfFlameSpray: function(partBakerBitId, repairId) {
		var deferred = Q.defer();
		RepairContext.query('CALL GetCountOfFlameSpraysForBakerBitRepair(?,?)', [partBakerBitId, repairId], function(err, count) {
			if (!err) {
				deferred.resolve(count[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getCutterNamesUsedInRepair: function(repairId, dullgradeId) {
		var deferred = Q.defer();
		var promises = [];
		promises.push(RepairService.getCutterNames(dullgradeId));
		promises.push(RepairService.getCutterDullGrades(dullgradeId));
		Q.all(promises).then(function(data) {
			var cutterNames = data[0];
			var cutterDullGrades = data[1];
			var cuttersWithLotNumberAndNew = _.filter(cutterDullGrades, function(dg) {
				return dg.lotNumber && dg.cutterActionId === 1;
			});
			cutterNames = _.filter(cutterNames, function(cn) {
				return cn.count !== 0;
			});

			_.each(cutterNames, function(cn) {

				if(cn.isReclaim && cn.replacedCutterId) {
					cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
						return c.isReclaim && c.replaceWithCutterId && cn.cutterId === c.replaceWithCutterId;
					});
				} else if(!cn.isReclaim && !cn.replacedCutterId) {
					cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
						return !c.isReclaim && !c.replaceWithCutterId && cn.cutterId === c.cutterId;
					});
				} else if(!cn.isReclaim && cn.replacedCutterId) {
					cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
						return !c.isReclaim && c.replaceWithCutterId && cn.cutterId === c.replaceWithCutterId;
					});
				} else if(cn.isReclaim && !cn.replacedCutterId) {
					cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
						return c.isReclaim && !c.replaceWithCutterId && cn.cutterId === c.cutterId;
					});
				}


				if(cuttersWithLotForThisCutterName && cuttersWithLotForThisCutterName.length > 0) {
					// group them
					var groups = _.groupBy(cuttersWithLotForThisCutterName, 'lotNumber');
					groups = _.map(groups, function(g) {
						return {
							items: g,
							lotNumber: g[0].lotNumber
						};
					});
					cn.lots = groups;
				}

				cn.originalOrReplacedCutterId = cn.replaceWithCutterId ? cn.replaceWithCutterId : cn.cutterId;
				cn.dullGradeId = dullgradeId;
			});
			// adjust cutterNames to reflect what CutterDullGrade lots are in each CutterName
			// _.each(cutterNames, function(cn) {
			//     var pushItemsToOther = [];
			//     if(cn.isReclaim) {
			//         _.each(cn.lots, function(lot) {
			//             _.each(lot.items, function(item) {
			//                 if(!item.lotNumber) {
			//                     pushItemsToOther.push(item);
			//                     lot.items = _.filter(lot.items, function(i) {
			//                         return i.cutterDullGradeId !== item.cutterDullGradeId;
			//                     });

			//                 }
			//             });
			//         });
			//     }
			// });

			deferred.resolve(cutterNames);
		}, function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	getCountOfAllCuttersWithCutterNamesBakerBit: function (repairId, dullGradeId) {
		var deferred = Q.defer();
		var promises = [];

		var query = 'CALL GetCountOfAllCuttersWithCutterNamesBakerBit(?)';
		var params = [repairId];

		if(dullGradeId) {
			query = 'CALL GetCountOfAllCuttersWithCutterNamesBakerBitByDullGrade(?)';
			params = [dullGradeId];
		}

		RepairContext.query(query, params, function(err, data) {
			console.log(err);
			console.log(data);
			if(!err) {

				var cuttersWithLotNumberAndNew = _.filter(data, function(dg) {
					return dg.lotNumber && dg.cutterActionId === 1;
				});
				var cutterNames = _.filter(data, function(cn) {
					return cn.count !== 0;
				});

				_.each(cutterNames, function(cn) {

					if(cn.isReclaim && cn.replacedCutterId) {
						cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
							return c.isReclaim && c.replaceWithCutterId && cn.cutterId === c.replaceWithCutterId;
						});
					} else if(!cn.isReclaim && !cn.replacedCutterId) {
						cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
							return !c.isReclaim && !c.replaceWithCutterId && cn.cutterId === c.cutterId;
						});
					} else if(!cn.isReclaim && cn.replacedCutterId) {
						cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
							return !c.isReclaim && c.replaceWithCutterId && cn.cutterId === c.replaceWithCutterId;
						});
					} else if(cn.isReclaim && !cn.replacedCutterId) {
						cuttersWithLotForThisCutterName = _.filter(cuttersWithLotNumberAndNew, function(c) {
							return c.isReclaim && !c.replaceWithCutterId && cn.cutterId === c.cutterId;
						});
					}


					if(cuttersWithLotForThisCutterName && cuttersWithLotForThisCutterName.length > 0) {
						// group them
						var groups = _.groupBy(cuttersWithLotForThisCutterName, 'lotNumber');
						groups = _.map(groups, function(g) {
							return {
								items: g,
								lotNumber: g[0].lotNumber
							};
						});
						cn.lots = groups;
					}

					cn.originalOrReplacedCutterId = cn.replaceWithCutterId ? cn.replaceWithCutterId : cn.cutterId;
				});
				deferred.resolve(cutterNames);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	removeDullGrade: function(cutterDullGrade) {
		let deferred = Q.defer();
		if(cutterDullGrade.cutterDullGradeId) {
			Repair_CutterDullGrade.destroy({cutterDullGradeId: cutterDullGrade.cutterDullGradeId}).exec((err, data) => {
				if(!err) deferred.resolve(data);
				else deferred.reject(err);
			})
		} else {
			deferred.reject({message: 'not a valid dullgrade to remove.'})
		}
	},
	getCutterNames: function(dullgradeId) {
		var deferred = Q.defer();
    	RepairContext.query('CALL GetAllCutterNamesUsedInBakerBitRepair(?)', [dullgradeId], function(err, cutterNames) {
			if (!err) deferred.resolve(cutterNames[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getDullGrades: function (repairId) {
		var deferred = Q.defer();
		Repair_DullGrade.find({
			repairId: repairId
		}).exec(function (err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCutterDullGrades: function (dullGradeId) {
		var deferred = Q.defer();
		Repair_CutterDullGrade.find({
			dullGradeId: dullGradeId
		}).exec(function (err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCountOfCutterSizeByAction: function(partBakerBitId, repairId) {
		var deferred = Q.defer();
		RepairContext.query('CALL GetCountOfAllCutterSizeByActionForBakerBitRepair(?,?)', [partBakerBitId, repairId], function(err, cutterSizeCount) {
			if (!err) {
				deferred.resolve(cutterSizeCount[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getCountOfCutterSizeByActionByDullGrade: function(partBakerBitId, dullGradeId) {
		var deferred = Q.defer();
		RepairContext.query('CALL GetCountOfAllCutterSizeByActionForBakerBitRepairByDullGrade(?,?)', [partBakerBitId, dullGradeId], function(err, cutterSizeCount) {
			if (!err) {
				deferred.resolve(cutterSizeCount[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	saveCreate: function(data) {
		var deferred = Q.defer();
		var updateObject = {
			orderNumber: data.orderNumber,
			orderPrice: data.orderPrice,
			totalTimesRepaired: data.totalTimesRepaired,
			lastUserId: data.lastUserId
		};

		Repair_AncillaryBakerBit.update({
			ancillaryBakerBitId: data.ancillaryBakerBitId
		}, updateObject).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	holdRepair: function (data) {
		var deferred = Q.defer();
		var object = {
			onHold: data.onHold,
			holdComments: data.holdComments,
			lastUserId: data.userId
		};

		Repair_Repair.update({
			repairId: data.repairId
		}, object).exec(function (err, hold) {
			if (!err) deferred.resolve(hold);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	createProgramMoveForward: function (data) {
		var newChrono = data.interStationChrono + 1;
		var deferred = Q.defer();
		Repair_Repair.update({
			repairId: data.repairId
		}, {
			interStationChrono: newChrono
		}).exec(function (err, moved) {
			if (!err) deferred.resolve(moved);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getAllAncillaryBakerBitInfo: function () {
		var deferred = Q.defer();
		Repair_AncillaryBakerBit.query('SELECT * FROM AncillaryBakerBit', function(err, ancillaryInfo) {
			if (!err) deferred.resolve(ancillaryInfo);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	generateCutterOrder: function (flowId) {
		var deferred = Q.defer();
		RepairContext.query('CALL GetAllCuttersToOrder(?)', [flowId], function(err, cutterList) {
			if (!err) {
				deferred.resolve(cutterList[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getHistoryByRepairId: function (repairId) {
		var deferred = Q.defer();

		Repair_RepairHistory.query('SELECT rh.*, u.nameFirst, u.nameLast FROM Repair.RepairHistory rh LEFT JOIN Users.users u on rh.userId = u.userId  where repairId = ?', [repairId], function (err, history) {
			if(!err) {
				// for (let h = 0; h < history.length; h++) {
				return history.reduce(function(loop, h) {
					if (h.firstQueryFromSchema != null) {
						if (h.previousValue != null) {
							return loop.then(function() {
								var previousValueObject = {
									selectedField: h.firstQuerySelectField,
									fromSchema: h.firstQueryFromSchema,
									fromTable: h.firstQueryFromTable,
									whereField: h.firstQueryWhereField,
									whereValue: h.previousValue
								};
								return RepairService.historyAdditionalQuery(previousValueObject);
							}).then(function(newValue) {
								h.actualPreviousValue = newValue;
								var updatedValueObject = {
									selectedField: h.firstQuerySelectField,
									fromSchema: h.firstQueryFromSchema,
									fromTable: h.firstQueryFromTable,
									whereField: h.firstQueryWhereField,
									whereValue: h.updatedValue
								};
								return RepairService.historyAdditionalQuery(updatedValueObject);
							}).then(function(newValue2) {
								h.actualUpdatedValue = newValue2;
								var previousValueObject2 = {
									selectedField: h.secondQuerySelectField,
									fromSchema: h.secondQueryFromSchema,
									fromTable: h.secondQueryFromTable,
									whereField: h.secondQueryWhereField,
									whereValue: h.actualPreviousValue
								};
								return RepairService.historyAdditionalQuery(previousValueObject2);
							}).then(function(newValue3) {
								h.actualPreviousValue = newValue3;
								var updatedValueObject2 = {
									selectedField: h.secondQuerySelectField,
									fromSchema: h.secondQueryFromSchema,
									fromTable: h.secondQueryFromTable,
									whereField: h.secondQueryWhereField,
									whereValue: h.actualUpdatedValue
								};
								return RepairService.historyAdditionalQuery(updatedValueObject2);
							}).then(function(newValue4) {
								h.actualUpdatedValue = newValue4;
								return h;
							});
						} else {
							return loop.then(function() {
								var updatedValueObject3 = {
									selectedField: h.firstQuerySelectField,
									fromSchema: h.firstQueryFromSchema,
									fromTable: h.firstQueryFromTable,
									whereField: h.firstQueryWhereField,
									whereValue: h.updatedValue
								};
								return RepairService.historyAdditionalQuery(updatedValueObject3);
							}).then(function(newValue5) {
								h.actualUpdatedValue = newValue5;
								var updatedValueObject4 = {
									selectedField: h.secondQuerySelectField,
									fromSchema: h.secondQueryFromSchema,
									fromTable: h.secondQueryFromTable,
									whereField: h.secondQueryWhereField,
									whereValue: h.actualUpdatedValue
								};
								return RepairService.historyAdditionalQuery(updatedValueObject4);
							}).then(function(newValue6) {
								h.actualUpdatedValue = newValue6;
								return h;
							});
						}
					} else {
						return loop.then(function() {
							return h;
						});
					}
				}, Q.resolve(null)).then(function() {
					deferred.resolve(history);
				});
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	historyAdditionalQuery: function (queryObject) {
		var deferred = Q.defer();
		RepairContext.query('SELECT ' + queryObject.selectedField + ' FROM ' + queryObject.fromSchema + '.' + queryObject.fromTable + ' WHERE ' + queryObject.whereField + ' = ' + queryObject.whereValue, function (err, queryReturn) {
			if (!err) {
				deferred.resolve(queryReturn[0][queryObject.selectedField]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	findProgrammingChronoForCutterOrderList: function (flowId) {
		var deferred = Q.defer();
		RepairContext.query('SELECT r.repairId, r.flowId, r.isCutOut, abb.addPaintOnly, SUM(IF(cdg.cutterActionId = 1, 1, 0)) AS newCutters, SUM(IF(cdg.isFlameSpray = 1, 1, 0)) AS flameSprays FROM Repair.Repair r LEFT JOIN Repair.DullGrade dg on dg.repairId = r.repairId LEFT JOIN Repair.CutterDullGrade cdg ON cdg.dullGradeId = dg.dullGradeId LEFT JOIN Repair.AncillaryBakerBit abb ON abb.repairId = r.repairId WHERE r.flowId = ? AND r.interStationChrono = 4 AND r.onHold = 0 GROUP BY r.repairId', [flowId], function(err, cutterOrderList) {
			if (!err) deferred.resolve(cutterOrderList);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getLaborTimeStampsForRepair: function(repairId) {
		var deferred = Q.defer();
		RepairContext.query('CALL GetTimeStampsForRepair(?)', [repairId], function(err, results) {
			if (!err) {
				deferred.resolve(results[0]);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getFlowRecordsFromSerial: function (serialFlow) {
		var deferred = Q.defer();
		RepairContext.query('SELECT fl.*, ws.status FROM Repair.Flow fl JOIN Repair.WorkStation ws ON fl.workStationId = ws.workStationId WHERE fl.familyId = ? AND fl.facilityId = ? AND ws.status = 1', [serialFlow.familyId, serialFlow.facilityId], function(err, records) {
			if (!err) {
				deferred.resolve(records);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	updateFlowForSerial: function (updateObject) {
		var deferred = Q.defer();
		Repair_Repair.update({
			repairId: updateObject.repairId
		}, {
			flowId: updateObject.flowId
		}).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.rejected(err);
		});
		return deferred.promise;
	},
	updateRushComments: function (updateCommentsForRush) {
		var deferred = Q.defer();
		Repair_Repair.update({
			repairId: updateCommentsForRush.repairId
		}, {
			rushComments: updateCommentsForRush.rushComments
		}).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	checkboxModel: function (rushObject) {
		var deferred = Q.defer();
		Repair_Repair.update({
			repairId: rushObject.repairId
		}, {
			rush: rushObject.rush
		}).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	saveLotNumbersForDullGrades: function (cutter, dullGradeId) {
		var deferred = Q.defer();
		Repair_CutterDullGrade.update({
			dullGradeId: dullGradeId,
			cutterId: cutter.cutterId,
			cutterActionId: 1
		}, {
			lotNumber: null
		}).exec(function (err, updated) {
			if (!err) {
				// loop through cutterDullGrades for these cutter lots and update with new lotNumber
				var promises = [];
				_.each(cutter.lots, function(lot) {
					_.each(lot.items, function(item) {
						promises.push(RepairService.saveLotNumberForDullGrade(item));
					});
				});
				Q.all(promises).then(function(data) {
					deferred.resolve(data);
				}, function(err) {
					deferred.reject(err);
				});
			} else deferred.reject(err);
		});


		return deferred.promise;
	},
	saveLotNumberForDullGrade: function (dg) {
		var deferred = Q.defer();
		Repair_CutterDullGrade.update({
			cutterDullGradeId: dg.cutterDullGradeId
		}, {
			lotNumber: dg.lotNumber,
			isReclaim: dg.isReclaim
		}).exec(function (err, data) {
			console.log(err);
			console.log(data);
			if(!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getFlags: function (data) {
		var deferred = Q.defer();
		Repair_ChangeRequest.query('SELECT * FROM ChangeRequest Where statusId = 1 AND (number = ? OR number = ?)', [data.serial, data.partNumber], function(err, flags) {
			if (!err) {
				deferred.resolve(flags);
			} else (
				deferred.reject(err)
			);
		});
		return deferred.promise;
	},
	addCutterDullGrade: function(cutterDullGrade) {
		return new Promise((resolve, reject) => {
			Repair_CutterDullGrade.create(cutterDullGrade).exec((err, created) => {
				if(!err) resolve(created)
				else reject(err)
			})
		})
	},
	setReplacementCuttersOnCutterDullGrades: function(currentCutter, replacementCutter, cutterDullGrades, reasonForChange, dullGradeId) {
		var deferred = Q.defer();

		console.log('dullGradeId');
		console.log(dullGradeId);

		// any cutterdullgrades that were included that do not have a cutterDullGradeId (not actually a CutterDullGrade),
		// then create a CutterDullGrade for this pocket record, forcing the dullgrade action to "NEW"
		let promisesCreateCutterDullGrades = [];
		for(let cdg of cutterDullGrades) {
			if(!cdg.cutterDullGradeId){
				cdg = {
					group: cdg.group,
					blade: cdg.blade,
					row: cdg.row,
					pocket: cdg.pocket,
					cutterPocketId: cdg.cutterPocketId,
					cutterId: cdg.cutterId,
					cutterActionId: 1,
					dullGradeId: dullGradeId
				}
				promisesCreateCutterDullGrades.push(RepairService.addCutterDullGrade(cdg));
			}
		}
		Promise.all(promisesCreateCutterDullGrades).then(newCutterDullGrades => {
			// replace non-cutterdullgrade pockets with new cutterdullgrades, if there are any
			if(newCutterDullGrades && newCutterDullGrades.length > 0) {
				cutterDullGrades = cutterDullGrades.map(cdg => {
					let match = newCutterDullGrades.filter(ncdg => {
						return ncdg.group === cdg.group
							&& ncdg.blade === cdg.blade
							&& ncdg.row === cdg.row
							&& ncdg.cutterPocketId === cdg.cutterPocketId
							&& ncdg.cutterId === cdg.cutterId
					})
					if(match && match[0]) return match[0]
					else return cdg
				})
			}

			// continue as usual
			let promises = [];

			let lotnumbers = 0;
			_.each(cutterDullGrades, function(cdg) {
				if(!cdg.lotNumber) {
					if(currentCutter.replacedCutterId) {
						if(currentCutter.replacedCutterId === replacementCutter.cutterId) {
							promises.push(RepairService.removeReplacementCutter(cdg.cutterDullGradeId));
						} else {
							promises.push(RepairService.setReplacementCutter(cdg.cutterDullGradeId, replacementCutter.cutterId, reasonForChange));
						}
					} else {
						if(currentCutter.cutterId === replacementCutter.cutterId) {
							promises.push(RepairService.removeReplacementCutter(cdg.cutterDullGradeId));
						} else {
							promises.push(RepairService.setReplacementCutter(cdg.cutterDullGradeId, replacementCutter.cutterId, reasonForChange));
						}
					}
				} else {
					console.log('Cannot Replace This. Has Lot Number');
					lotnumbers++;
				}
			});
			if(lotnumbers === 0) {
				Q.all(promises).then(function(data) {
					deferred.resolve(data);
				}, function(err) {
					deferred.reject(err);
				});
			} else {
				let lotNumberErrorMessage = lotnumbers.length > 1 ? 'Lot Numbers have been assigned to some of these cutters. Please try again.' : 'A Lot Number has been assigned to this cutter and cannot be replaced.';
				deferred.reject({message: lotNumberErrorMessage})
			}
		}).catch(err => {
			deferred.reject(err)
		})
		return deferred.promise;
	},
	setReplacementCutter: function (currentCutterDullGradeId, replacementCutterId, reasonForChange) {
		var deferred = Q.defer();
		Repair_CutterDullGrade.update({
			cutterDullGradeId: currentCutterDullGradeId,
			lotNumber: null
		}, {
			replaceWithCutterId: replacementCutterId,
			reasonForChange: reasonForChange
		}).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	removeReplacementCutter: function (currentCutterDullGradeId) {
		var deferred = Q.defer();
		Repair_CutterDullGrade.update({
			cutterDullGradeId: currentCutterDullGradeId,
			lotNumber: null
		}, {
			replaceWithCutterId: null,
			reasonForChange: null
		}).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	saveLotInfoForCutter: function(cutter, dullGradeId, lotNumber, qty, isReclaim) {
		var deferred = Q.defer();

		var cutterId = cutter.replacedCutterId ? cutter.replacedCutterId : cutter.cutterId;

		console.log('dullGradeId / cutterId')
		console.log(dullGradeId + " / " + cutterId)
		Repair_CutterDullGrade.find({
			dullGradeId: dullGradeId,
			cutterId: cutterId
		}).exec(function (err, cutterDullGrades) {
			var validCutterDullGradesToBeUpdated = _.filter(cutterDullGrades, function (cdg) {
				return cdg.cutterActionId === 1 && !cdg.lotNumber;
			});

			console.log('========================VALID=============================');
			console.log(validCutterDullGradesToBeUpdated);

			var promises = [];
			console.log(qty);
			for(var i = 0; i < validCutterDullGradesToBeUpdated.length && i < qty; i++) {
				validCutterDullGradesToBeUpdated[i].lotNumber = lotNumber;
				validCutterDullGradesToBeUpdated[i].isReclaim = isReclaim;
				promises.push(RepairService.saveLotNumberForDullGrade(validCutterDullGradesToBeUpdated[i]));
			}

			console.log(promises.length);

			Q.all(promises).then(function(data) {
				console.log(data);
				deferred.resolve(data);
			}, function(err) {
				console.log(err);
				deferred.reject(err);
			});
		});
		return deferred.promise;
	},
	removeLotInfoForCutterDullGrades: function(dullGrades) {
		var deferred = Q.defer();
		var promises = [];
		_.each(dullGrades, function(dg) {
			promises.push(RepairService.removeLotForCutterDullGrade(dg));
			Q.all(promises).then(function(data) {
				deferred.resolve(data);
			}, function(err) {
				deferred.reject(err);
			});
		});
		return deferred.promise;
	},
	removeLotForCutterDullGrade: function(dg) {
		var deferred = Q.defer();

		Repair_CutterDullGrade.update({
			cutterDullGradeId: dg.cutterDullGradeId
		}, {
			lotNumber: null,
			isReclaim: false
		}).exec(function (err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getOpenTimeStampWithRepairId: function(openTimeStamp) {
		var deferred = Q.defer();
		Repair_LaborTimeStamp.query('SELECT * FROM Repair.LaborTimeStamp WHERE repairId=(?) && stopTime IS NULL', [openTimeStamp.repairId], function(err, results) {
			if (!err) {
				deferred.resolve(results);
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	removeRepairFromKiln: function (kilnWithTools, laborTimeStamps) {
		var deferred = Q.defer();
		// make sure it has tools
		if (kilnWithTools.toolRepairs && kilnWithTools.toolRepairs.length > 0) {
			var promises = [];
			if (kilnWithTools.hasLaborTimeStamps) {
				_.each(laborTimeStamps, function(lts) {
					promises.push(RepairService.startStopTimeStampForRepair(lts))
				});
			}
			promises.push(RepairService.updateThisRepair({
				repairId: kilnWithTools.toolRepairs[0].repairId
			}, {
				flowId: kilnWithTools.toolRepairs[0].prevFlowId
			}));

			Q.all(promises).then(function (data) {
				deferred.resolve(data);
			}, function (err) {
				console.log(err);
				deferred.reject(err);
			});
		} else {
			deferred.reject({
				error: 'This Kiln does not have any tools in it.'
			});
		}

		return deferred.promise;
	},
	updateLaborTimeStamp: function (query, updatedObject) {
		var deferred = Q.defer();
		console.log(query);
		console.log(updatedObject);
		Repair_LaborTimeStamp.update(query, updatedObject).exec(function (err, updated) {
			console.log(updated);
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	updateThisRepair: function (query, updatedObject) {
		var deferred = Q.defer();

		Repair_Repair.update(query, updatedObject).exec(function (err, updated) {

			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getOrSaveHashByValue: (hashValue, groups) => {
		return new Promise((resolve, reject) => {
			Repair_Hash.findOne({hash: hashValue}).exec((err, record) => {
				if (!err) {
					if(record) {
						console.log('found one')
						resolve(record)
					}
					else {
						console.log('did not find one')
						// save new hash and resolve
						Repair_Hash.create({hash: hashValue}).exec((err, record) => {
							console.log(record)
							if(!err) {
								RepairService.saveHashStructureRecords(groups, record.hashId).then(data => {
									console.log('length of saved records')
									console.log(data.length)
									resolve(record)
								}).catch(err => {
									console.log(err)
									reject(err)
								})
							} else {
								console.log(err)
								reject(err)
							}
						})
					}
				} else {
					console.log(err)
					reject(err)
				}
			})
		})
	},
	saveHashStructureRecords: (groups, hashId) => {
		console.log('groups & hashId')
		console.log(groups)
		console.log(hashId)
		return new Promise((resolve, reject) => {
			// let promises = []
			let pocketArray = []
			for (let g of groups)
				for (let b of g.blades)
					for (let r of b.rows)
						for (let p of r.pockets)
							pocketArray.push(p)
			// 				promises.push(RepairService.createHashStructure(hashId, p))
			// Promise.all(promises).then(data => {
			// 	resolve(data)
			// }).catch(err => {
			// 	reject(err)
			// })
			if(pocketArray.length < 35){
				qty = pocketArray.length
			} else {
				qty = 35
			}
			createHashStructureSplit(qty, 0, pocketArray, hashId, function(data){
				resolve(data)
			})
		})
	},
	createHashStructure: (hashId, pocket) => {
		return new Promise((resolve, reject) => {
			Repair_HashStructure.create({
				hashId: hashId,
				group: pocket.group,
				blade: pocket.blade,
				row: pocket.row,
				pocket: pocket.pocket,
				cutterPocketId: pocket.cutterPocketId,
				cutterId: pocket.cutterId
			}).exec((err, hashStructure) => {
				if(!err) resolve(hashStructure)
				else reject(err)
			})
		})
	},
	saveThirdPartyInspection: function (data) {
		var deferred = Q.defer();
		if (data.toolFamilyId === 1) {
			delete data.toolFamilyId
			delete data._csrf
			if (data.ancillaryDnrId) {
				Repair_AncillaryDnr.update({
					ancillaryDnrId: data.ancillaryDnrId
				}, data).exec(function (err, updated) {
					if (!err) deferred.resolve(updated);
					else deferred.reject(err);
				});
			} else {
				Repair_AncillaryDnr.create(data).fetch().exec(function(err, created) {
					if (!err) {
						deferred.resolve(created);
					} else {
						deferred.reject(err);
					}
				});
			}
		} else if (data.toolFamilyId === 2) {
			delete data.toolFamilyId
			if (data.ancillaryVStreamId) {
				Repair_AncillaryVStream.update({
					ancillaryVStreamId: data.ancillaryVStreamId
				}, data).exec(function (err, updated) {
					if (!err) deferred.resolve(updated);
					else deferred.reject(err);
				});
			} else {
				Repair_AncillaryVStream.create(data).exec(function(err, created) {
					if (!err) {
						deferred.resolve(created);
					} else {
						deferred.reject(err);
					}
				});
			}
		}

		return deferred.promise;
	},
	get32Inches: function () {
		var deferred = Q.defer();
		GlobalValues_32Inches.query('SELECT `32inchesId` as id, `32fraction` as fraction, `32decimal` as `decimal` FROM `32Inches`', function(err, data){
			if(!err) deferred.resolve(data.rows)
			else deferred.reject(err)
		});
		return deferred.promise;
	},
	getDNRShipToStockPoints: () => {
		var deferred = Q.defer();
		SdpTools_StockPoints.getDatastore().sendNativeQuery('SELECT * FROM StockPoints', function(err, stockPoints) {
		    if (!err) {
		        deferred.resolve(stockPoints.rows);
		    } else (
		        deferred.reject(err)
		    );
		});
		return deferred.promise;
	},
	getUserStockPoints: (user) => {
		var deferred = Q.defer();
		TheBrain_UserStockPointJunction.getDatastore().sendNativeQuery('SELECT * FROM UserStockPointJunction where userId = $1',[user.userId], function (err, userStockPoints) {
			if (!err) {
				deferred.resolve(userStockPoints.rows);
			} else(
				deferred.reject(err)
			);
		});
		return deferred.promise;
	},
	updateAncillaryDnr: function (body) {
		var deferred = Q.defer();
		Repair_AncillaryDnr.update(body.ancillaryDnrId, body).exec(function(err, updated) {
			if(!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	updateAncillaryVstream: function (body) {
		var deferred = Q.defer();
		Repair_AncillaryVStream.update(body.ancillaryVStreamId, body).exec(function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	}
}
