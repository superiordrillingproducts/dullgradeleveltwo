module.exports = {
	getCompany: function(user, cb) {
		if(user) {
			TheBrainContext.query('SELECT * FROM User_Customer Where userId = ' + user.userId, function(err, records) {
				if(!err) {
					if(records[0]) {
						var firstUserCustomer = records[0];
						// get customer
						TheBrainContext.query('SELECT * FROM Customer Where customerId = ' + firstUserCustomer.customerId, function(err, records) {
							if(!err) {
								var firstCustomer = records[0];
								return cb(err, firstCustomer);
							} else {
								return cb(err, null);
							}
						});
					} else {
						return cb(null, 'nousercustomer');
					}
				} else {
					return cb(err, null);
				}
			});
		}
	},
    
    getCustomers: function(cb){
        TheBrainContext.query('SELECT * FROM TheBrain.Customer ORDER BY customerName', function(err, customer){
            if(!err && customer){
                return cb(err,customer);
            } else {
                return cb({
                    'message': 'There is a problem getting customers.'
                }, null);
            }
        })
    }
}