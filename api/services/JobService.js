var moment = require('moment');
var Q = require('q');
module.exports = {
	data: {},

	getOpenJobsByCostCenterId: function (costCenterId, userId, cb) {
		Job.query('call GetOpenJobsByCostCenterId(' + costCenterId + ',' + userId + ')', function (err, records) {
			cb(err, records);
		});
	},

	startStopTimeStamp: function (job, laborTimeStampTypeId, userId, userRate, cb) {
		var Now = new Date();

		if (laborTimeStampTypeId === 1 || laborTimeStampTypeId === 3) {
			var updateStartObject = {
				'laborId': job.laborId,
				'costCenterId': job.costCenterId,
				'rate': job.currentHourlyLaborRate,
				'employeeId': userId,
				'employeeRate': userRate,
				'totalRate': parseFloat(job.currentHourlyLaborRate) + parseFloat(userRate),
				'startTime': Now,
				'laborTimeStampTypeId': laborTimeStampTypeId
			};

			JobService.startStopFunction(job, updateStartObject, function (err, record) {
				cb(err, record);
			});
		} else if (laborTimeStampTypeId === 2) {
			CostCenters.query('SELECT * FROM CostCenters WHERE costCenterId =' + job.costCenterId + '+ 1', function (err, setup) {
				if (!err) {
					var updateStartObject = {
						'laborId': job.laborId,
						'costCenterId': setup[0].costCenterId,
						'rate': setup[0].currentHourlyLaborRate,
						'employeeId': userId,
						'totalRate': parseFloat(setup[0].currentHourlyLaborRate),
						'startTime': Now,
						'laborTimeStampTypeId': laborTimeStampTypeId
					};

					JobService.startStopFunction(job, updateStartObject, function (err, record) {
						cb(err, record);
					});
				}
			});
		} else if (laborTimeStampTypeId === 4 || laborTimeStampTypeId === 5) {
			var updateStartObject = {
				'laborId': job.laborId,
				'costCenterId': job.costCenterId,
				'rate': job.currentHourlyLaborRate,
				'employeeId': userId,
				'totalRate': parseFloat(job.currentHourlyLaborRate),
				'startTime': Now,
				'laborTimeStampTypeId': laborTimeStampTypeId
			};
			JobService.startStopFunction(job, updateStartObject, function (err, record) {
				cb(err, record);
			});
		} else if (laborTimeStampTypeId === 0) {
			JobService.startStopFunction(job, updateStartObject, function (err, record) {
				record[0].hasStopTime = true;
				console.log(record);
				cb(err, record);
			})
		}

	},

	startStopFunction: function (job, updateStartObject, cb) {
		var Now = new Date();
		var start = moment(job.startTime);
		var stop = moment(job.selectedStopTime);
		var diff = moment.duration(stop.diff(start));
		if (diff._data.days > 0) {
			var daysToHours = diff._data.days * 24;
			var formated = daysToHours + diff._data.hours + ":" + diff._data.minutes + ":" + diff._data.seconds;
		} else {
			var formated = diff._data.hours + ":" + diff._data.minutes + ":" + diff._data.seconds;
		}

		var updateStopObject = {
			'stopTime': stop._i,
			'totalElapsedTime': formated,
		}

		if (!job.laborTimeStampsId) {
			LaborTimeStamps.create(updateStartObject, function (err, laborTimeStamp) {
				if (!err && laborTimeStamp) {
					return cb(err, laborTimeStamp)
				} else {
					return cb({
						'message': 'There was a problem starting the job.'
					}, null);
				}
			})
		} else {
			LaborTimeStamps.update({
				'laborTimeStampsId': job.laborTimeStampsId
			}, updateStopObject, function (err, record) {
				cb(err, record);
			})
		}
	},

	getLastLaborTimeStampForJobAndUser: function (jobId, userId, cb) {
		// get labor time stamps for this user and job and that also
		var laborTimeStamps = {};
		Job.findOne({
			jobId: jobId
		}).exec(function (err, job) {
			if (!err) {
				if (job) {
					LaborTimeStamps.find({
						laborId: job.laborId,
						employeeId: userId
					}).exec(function (err, stamps) {
						if (!err) {
							var stampsWhereDrawingPrinted = _.filter(stamps, function (s) {
								return s.didPrintDrawing;
							});
							var lastPrintedStamp = _.last(stampsWhereDrawingPrinted);
							var lastStamp = _.last(stamps);

							laborTimeStamps.notPrintedTimeStamp = lastStamp;
							laborTimeStamps.printedTimeStamp = lastPrintedStamp;


							if (laborTimeStamps) {
								cb(null, laborTimeStamps);
							} else {
								cb(null, null);
							}
						} else {
							cb(err, null);
						}
					});
				} else {
					cb(null, null);
				}
			} else {
				cb(err, null);
			}
		});
	},

	setInfoForPrintDrawing: function (jobId, laborTimeStampsId) {
		Job.findOne({
			jobId: jobId
		}).exec(function (err, job) {
			if (!err) {
				if (job) {
					Job.update({
						jobId: jobId
					}, {
						qtyDrawingPrinted: job.qtyDrawingPrinted + 1
					}).exec(function (err, updatedJob) {
						if (!err) {
							console.log(updatedJob);
						} else {
							console.log(err);
						}
					})
				} else {
					console.log('no job found');
				}
			} else {
				console.log(err);
			}
		});

		LaborTimeStamps.update({
			laborTimeStampsId: laborTimeStampsId
		}, {
			didPrintDrawing: true
		}).exec(function (err, updatedStamp) {
			if (!err) {
				console.log(updatedStamp);
			} else {
				console.log(err);
			}
		});
	},

	getWhiteboardComments: function (partId, cb) {
		Whiteboard.query('SELECT  wb.*, u.nameFirst, u.nameLast, cc.costCenterName FROM manu.Whiteboard wb LEFT JOIN Users.Users u ON wb.userId = u.userId Left join SDPI.CostCenters cc on wb.costCenterId = cc.costCenterId WHERE partId =' + partId + ' ORDER BY wb.dateCreated DESC', function (err, comments) {
			if (!err) {
				cb(err, comments);
			} else {
				cb(err, null);
			}
		})
	},

	addNewComment: function (partId, jobId, costCenterId, userId, newComment, cb) {
		var createObject = {
			'partId': partId,
			'userId': userId,
			'jobId': jobId,
			'costCenterId': costCenterId,
			'dateCreated': new Date(),
			'comments': newComment
		}
		Whiteboard.create(createObject, function (err, comment) {
			if (!err && comment) {
				return cb(err, comment);
			} else {
				return cb({
					'message': 'There was problem saving your commnet.'
				}, null);
			}
		})
	},

	getAllPartsWithSamePno: function (partId, cb) {
		Part.query('SELECT * FROM SDPI.Part Where partId =' + partId, function (err, part) {
			if (!err) {
				if (part) {
					Part.query('SELECT p.partId, d.originalDocumentName FROM SDPI.Part p LEFT JOIN SDPI.BPDrawingNumbers bdn ON p.partId = bdn.partId LEFT JOIN SDPI.Documents d ON bdn.documentId = d.documentId WHERE p.partNumberedObjectId =' + part[0].partNumberedObjectId, function (err, parts) {
						if (!err) {
							cb(err, parts);
						} else {
							return cb({
								'message': 'There was a problem getting all parts with same pno.'
							}, null);
						}
					})
				}
			} else {
				return cb({
					'message': 'There was a problem finding pno.'
				}, null);
			}
		})
	},

	getJobTypes: function (cb) {
		JobType.query('SELECT * FROM JobType', function (err, types) {
			if (!err && types) {
				return cb(err, types)
			} else {
				return cb({
					'message': 'There was a problem getting job types.'
				}, null);
			}
		})
	},

	getAllOpenJobs: function (cb) {
		Job.query('CALL GetAllOpenManuJobs', function (err, openJobs) {
			if (!err && openJobs) {
				return cb(err, openJobs[0])
			} else {
				return cb({
					'message': 'There was a problem getting open jobs.'
				}, null);
			}
		})
	},

	getMachines: function (cb) {
		CostCenters.query('SELECT costCenterId, costCenterName, currentHourlyLaborRate, laborTrackingTableName FROM CostCenters WHERE costCenterId != 16 AND costCenterId != 18 AND costCenterId != 20 AND costCenterId > 2 AND (costCenterId % 2) = 0 OR costCenterId = 17 OR costCenterId = 19 OR costCenterId = 23', function (err, machines) {
			if (!err && machines) {
				return cb(err, machines)
			} else {
				return cb({
					'message': 'There was a problem getting machines.'
				}, null);
			}
		})
	},

	getPartsLastJobCostCenters: function (partId, cb) {
		SdpiContext.query('CALL GetLastJobTotalTimeForPartId(?)', [partId], function (err, lastJob) {
			if (!err && lastJob) {
				if (lastJob[0].length > 0) {
					SdpiContext.query('CALL GetTotalCostCenterTimePerPartByLaborId(?)', [lastJob[0][0].laborId], function (err, costCenterTimes) {
						return cb(err, {
							costCenterTimes: costCenterTimes[0],
							lastJob: lastJob[0]
						})
					})
				} else {
					return cb(err, {
						'message': 'No Previous Job For This Part.'
					})
				}
			} else {
				return cb({
					'message': 'There was a problem getting parts last job cost centers.'
				})
			}
		})
	},

	getThirdPartyCustomers: function (cb) {
		ThirdPartyCustomer.query('SELECT * FROM ThirdPartyCustomer', function (err, thirdPartyCustomers) {
			if (!err && thirdPartyCustomers) {
				return cb(err, thirdPartyCustomers)
			} else {
				return cb({
					'message': 'There was a problem getting third party customers.'
				}, null);
			}
		})
	},

	addNewThirdPartyCustomer: function (customerName, cb) {
		ThirdPartyCustomer.create({
			customerName: customerName
		}, function (err, newThirdPartyCustomer) {
			if (!err && newThirdPartyCustomer) {
				return cb(err, newThirdPartyCustomer);
			} else {
				return cb({
					'message': 'There was a problem saving new customer.'
				}, null);
			}
		})
	},

	createThirdPartyFileUpload: function (fileRecord, cb, user) {
		var userId = 0;
		if (user) userId = user.userId;

		var fileObject = {
			'documentTypeId': fileRecord.documentTypeId,
			'documentLocation': fileRecord.documentLocation,
			'name': fileRecord.name,
			'uploadedByUserId': userId,
			'dateUploaded': new Date(),
			'originalDocumentName': fileRecord.originalDocumentName,
			'fileExtension': fileRecord.fileExtension
		}

		Sdpi_Documents.create(fileObject, function (err, file) {
			if (!err && file) {
				return cb(err, file);
			} else {
				return cb({
					'message': 'There was a problem saving file.'
				}, null);
			}
		});
	},
	createLaborId: function() {
		var deferred = Q.defer();
		LaborId.create({costCenterId: 1}).exec(function(err, data) {
			if(!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	createJob: function (jobCreationObject, user) {
		var deferred = Q.defer();

		var userId = 0;
		if (user) userId = user.userId;

		var jobObject = {
			'jobTypeId': jobCreationObject.jobTypeId,
			'jobStatusId': 1,
			'partId': jobCreationObject.partId,
			'qtyToManufacture': jobCreationObject.quantityToManufacture,
			'dateCreated': jobCreationObject.dateCreated,
			'dateDue': jobCreationObject.dateDue,
			'createdByEmpId': userId,
			'steelTypeId': jobCreationObject.steelTypeId,
		}

		if (jobCreationObject.po) {
			jobObject.po = jobCreationObject.po;
		}

		if (jobCreationObject.poQuotedPrice) {
			jobObject.poQuotedPrice = jobCreationObject.poQuotedPrice;
		}
		///////// Create LaborId Record ////////////
		LaborId.create({
			costCenterId: 1
		}, function (err, laborIdRecord) {
			if (!err && laborIdRecord) {
				jobObject.laborId = laborIdRecord.laborId;
				///////////// Create Job Record //////////////////
				Job.create(jobObject, function (err, job) {
					if (!err && job) {
						//////////////// Update Job Record with jobNumber //////////////////
						Job.update({
							'jobId': job.jobId
						}, {
							'jobNumber': job.jobId
						}, function (err, job2) {
							if (!err && job2) {
								SdpiContext.query('CALL GetLastJobTotalTimeForPartId(?)', [job.partId], function (err, lastJob) {
									if (!err && lastJob) {
										if (lastJob[0].length > 0) {
											SdpiContext.query('CALL GetTotalCostCenterTimePerPartByLaborId(?)', [lastJob[0][0].laborId], function (err, costCenterTimes) {
												if (!err && costCenterTimes) {
													var dateFormated = moment(jobCreationObject.dateCreated).format("YYYY-MM-DD HH:mm:ss")

													var jobHistoryCreatedObject = {
														'jobId': job.jobId,
														'date': jobCreationObject.dateCreated,
														'employeeId': userId,
														'jobHistoryStatusId': 1,
														'dataChangedTo': dateFormated
													};
													var jobHistoryStatusChangedObject = {
														'jobId': job.jobId,
														'date': jobCreationObject.dateCreated,
														'employeeId': userId,
														'jobHistoryStatusId': 2,
														'dataChangedTo': 1
													};
													var jobHistoryJobNumberChangedObject = {
														'jobId': job.jobId,
														'date': jobCreationObject.dateCreated,
														'employeeId': userId,
														'jobHistoryStatusId': 3,
														'dataChangedTo': job.jobId
													};
													var jobHistoryPartAssignedObject = {
														'jobId': job.jobId,
														'date': jobCreationObject.dateCreated,
														'employeeId': userId,
														'jobHistoryStatusId': 4,
														'dataChangedTo': jobObject.partId
													};

													var jobHistoryPromises = [];
													//////////////// Create Job History Created Record /////////////
													jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryCreatedObject));
													//////////////// Create Job History Status Changed Record /////////////////
													jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryStatusChangedObject));
													//////////////// Create Job History Job Number Changed Record /////////////////
													jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryJobNumberChangedObject));
													//////////////// Create Job History Part Assigned Record /////////////////
													jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryPartAssignedObject));

													Q.all(jobHistoryPromises).then(function (history) {
														var costCenterSteelSaw = {
															'jobId': job.jobId,
															'costCenterId': 16
														};

														var costCenterQA = {
															'jobId': job.jobId,
															'costCenterId': 21
														};

														var jobCostCenterPromises = [];
														//////////////// Create Job_CostCenter Record for steel saw //////////
														jobCostCenterPromises.push(JobService.addJobCostCenterObject(costCenterSteelSaw));
														//////////////// Create Job_CostCenter Record for QA //////////
														jobCostCenterPromises.push(JobService.addJobCostCenterObject(costCenterQA));
														//////////////// Create Job_CostCenterRecord for each selected machine ///////////

														for (i = 0; i < jobCreationObject.machines.length; i++) {
															var costCenters = {
																'jobId': job.jobId,
																'costCenterId': jobCreationObject.machines[i].costCenterId
															};
															jobCostCenterPromises.push(JobService.addJobCostCenterObject(costCenters));
														}
														Q.all(jobCostCenterPromises).then(function (costCenters) {;
															if (jobCreationObject.partId === 461) {
																var jobThirdPartyAdditional = {
																	'jobId': job.jobId,
																	'thirdPartyCustomerId': jobCreationObject.thirdPartyCustomerId,
																	'originalDocumentName': jobCreationObject.fileUploaded.originalDocumentName,
																	'documentId': jobCreationObject.fileUploaded.documentId
																};
																Job_ThirdPartyAdditional.create(jobThirdPartyAdditional, function (err, jobThirdParty) {
																	if (!err && jobThirdParty) {
																		deferred.resolve({
																			job: job
																		});
																	} else {
																		deferred.reject(err);
																	}
																});
															} else {
																deferred.resolve({
																	job: job,
																	costCenterTimes: costCenterTimes[0],
																	lastJob: lastJob[0]
																});
															}
														}, function (error) {
															deferred.reject(err);
														});

													}, function (error) {
														deferred.reject(err);
													});
												} else {
													deferred.reject(err);
												}
											});
										} else {
											var dateFormated = moment(jobCreationObject.dateCreated).format("YYYY-MM-DD HH:mm:ss")

											var jobHistoryCreatedObject = {
												'jobId': job.jobId,
												'date': jobCreationObject.dateCreated,
												'employeeId': userId,
												'jobHistoryStatusId': 1,
												'dataChangedTo': dateFormated
											};
											var jobHistoryStatusChangedObject = {
												'jobId': job.jobId,
												'date': jobCreationObject.dateCreated,
												'employeeId': userId,
												'jobHistoryStatusId': 2,
												'dataChangedTo': 1
											};
											var jobHistoryJobNumberChangedObject = {
												'jobId': job.jobId,
												'date': jobCreationObject.dateCreated,
												'employeeId': userId,
												'jobHistoryStatusId': 3,
												'dataChangedTo': job.jobId
											};
											var jobHistoryPartAssignedObject = {
												'jobId': job.jobId,
												'date': jobCreationObject.dateCreated,
												'employeeId': userId,
												'jobHistoryStatusId': 4,
												'dataChangedTo': jobObject.partId
											};

											var jobHistoryPromises = [];
											//////////////// Create Job History Created Record /////////////
											jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryCreatedObject));
											//////////////// Create Job History Status Changed Record /////////////////
											jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryStatusChangedObject));
											//////////////// Create Job History Job Number Changed Record /////////////////
											jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryJobNumberChangedObject));
											//////////////// Create Job History Part Assigned Record /////////////////
											jobHistoryPromises.push(JobService.addJobHistoryObject(jobHistoryPartAssignedObject));

											Q.all(jobHistoryPromises).then(function (history) {
												var costCenterSteelSaw = {
													'jobId': job.jobId,
													'costCenterId': 16
												};

												var costCenterQA = {
													'jobId': job.jobId,
													'costCenterId': 21
												};

												var jobCostCenterPromises = [];
												//////////////// Create Job_CostCenter Record for steel saw //////////
												jobCostCenterPromises.push(JobService.addJobCostCenterObject(costCenterSteelSaw));
												//////////////// Create Job_CostCenter Record for QA //////////
												jobCostCenterPromises.push(JobService.addJobCostCenterObject(costCenterQA));
												//////////////// Create Job_CostCenterRecord for each selected machine ///////////

												for (i = 0; i < jobCreationObject.machines.length; i++) {
													var costCenters = {
														'jobId': job.jobId,
														'costCenterId': jobCreationObject.machines[i].costCenterId
													};
													jobCostCenterPromises.push(JobService.addJobCostCenterObject(costCenters));
												}
												Q.all(jobCostCenterPromises).then(function (costCenters) {;
													if (jobCreationObject.partId === 461) {
														var jobThirdPartyAdditional = {
															'jobId': job.jobId,
															'thirdPartyCustomerId': jobCreationObject.thirdPartyCustomerId,
															'originalDocumentName': jobCreationObject.fileUploaded.originalDocumentName,
															'documentId': jobCreationObject.fileUploaded.documentId
														}
														Job_ThirdPartyAdditional.create(jobThirdPartyAdditional, function (err, jobThirdParty) {
															if (!err && jobThirdParty) {
																deferred.resolve({
																	job: job
																});
															} else {
																deferred.reject(err);
															}
														})
													} else {
														deferred.resolve({
															job: job
														});
													}
												}, function (error) {
													deferred.reject(err);
												})

											}, function (error) {
												deferred.reject(err);
											})
										}
									} else {
										deferred.reject(err);
									}
								})
							} else {
								deferred.reject(err);
							}
						})
					} else {
						deferred.reject(err);
					}
				})
			} else {
				deferred.reject(err);
			}
		})
		return deferred.promise
	},

	getOpenJobsCostCenters: function (jobId, cb) {
		Job_CostCenter.query('SELECT jcc.*, cc.costCenterName FROM Job_CostCenter jcc LEFT JOIN SDPI.CostCenters cc on jcc.costCenterId = cc.costCenterId  Where jobId = ' + jobId + ' AND jcc.costCenterId != 16 AND jcc.CostCenterId != 21', function (err, costCenters) {
			if (!err && costCenters) {
				return cb(err, costCenters)
			} else {
				return cb({
					'message': 'There was a problem getting open job cost centers.'
				})
			}
		})
	},

	addJobHistoryObject: function (jobHistoyObject) {
		var deferred = Q.defer();

		JobHistory.create(jobHistoyObject, function (err, jobHistory) {
			if (!err && jobHistory) {
				deferred.resolve(jobHistory)
			} else {
				deferred.reject(err);
			}
		})
		return deferred.promise
	},

	addJobCostCenterObject: function (jobCostCenterObject) {
		var deferred = Q.defer();

		Job_CostCenter.create(jobCostCenterObject, function (err, jobCostCenter) {
			if (!err && jobCostCenter) {
				deferred.resolve(jobCostCenter)
			} else {
				deferred.reject(err);
			}
		})
		return deferred.promise
	},

	editJob: function(selectedJobObject){

	},

}
