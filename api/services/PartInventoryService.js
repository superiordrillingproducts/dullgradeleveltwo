var Q = require('q');

module.exports = {
	getPartInventoryObject: function (partInventoryId, cb) {
		SdpiContext.query({
			text: 'SELECT * FROM PartInventory WHERE partInventoryId = $1',
			values: [partInventoryId]
		}, function (err, records) {
			cb(err, records);
		});
	},
	getPartInventoryById: function(partInventoryId) {
		var deferred = Q.defer();
		PartInventory.find({partInventoryId: partInventoryId}).exec(function(err, records) {
			if(!err) {
				if(records.length > 0) deferred.resolve(records[0]);
				else deferred.reject(new Error('This part inventory object does not exist.'));
			} else deferred.reject(err);
		});
		return deferred.promise;
	},
	updateQty: function (partInventoryObjectId, newQty, cb) {
		if (newQty === -1) {
			cb({
				'error': 'Qty cannot be a negative number'
			}, null);
		} else {
			var updates = {
				'quantityAvailable': newQty
			}

			if (newQty === 0) {
				updates.partInventoryStatus = 6 // goes to 6 if new qty is 0
			}

			PartInventory.update({
				'partInventoryId': partInventoryObjectId
			}, updates, function (err, records) {
				cb(err, records);
			})
		}
	},
	updatePartInventoryPart: function (partInventoryId, partId, status, qtyAvailable, cb) {
		PartInventory.update({
			'partInventoryId': partInventoryId
		}, {
			'partId': partId,
			'partInventoryStatus': status,
			'quantityAvailable': qtyAvailable
		}, function (err, updated) {
			return cb(err, updated);
		});
	},
	getSubPartInventoryObjectsForDnrWorkOrder: function (partId) {
		var deferred = Q.defer();
		SdpiContext.query('call GetSubPartInventorySteelInventoryOfParentPart(' + partId + ')', function (err, results) {
			if (!err) deferred.resolve(results[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	deductPartInventoryObjectsQty: function (qty, partInventoryIds, partId) {
		var deferred = Q.defer();
		var partInventoryIdsString = partInventoryIds.join();
		var partInventoryUsed = [];
		SdpiContext.query({
			query: 'SELECT PartInventory WHERE partInventoryStatus != 6 AND partInventoryId IN ($1)',
			values: [partInventoryIdsString]
		}, function (err, data) {
			if (!err) {
				if (data[0] && data[0].length > 0) {
					var qtyDeductionPromises = [];
					var qtyLeft = qty;
					_.each(data[0], function (pi) {
						if (qtyLeft !== 0) {
							// keep going
							if (qtyLeft >= pi.quantityAvailable) {
								qtyLeft -= pi.quantityAvailable;
								qtyDeductionPromises.push(PartInventoryService.setQtyFromPartInventory(pi.quantityAvailable, 6, pi.partInventoryId));
								partInventoryUsed.push(pi);
							} else {
								var newQty = pi.quantityAvailable - qtyLeft;
								qtyDeductionPromises.push(PartInventoryService.setQtyFromPartInventory(newQty, pi.partInventoryStatus, pi.partInventoryId));
								partInventoryUsed.push(pi);
							}
						}
					});
					if(qtyLeft === 0 && qtyDeductionPromises.length > 0) {
						Q.all(qtyDeductionPromises).then(function(data) {
							deferred.resolve(partInventoryUsed);
						}, function(err) {
							deferred.reject(err);
						});
					} else deferred.reject(new Error('There were not enough parts to meet qty'))
				} else {
					deferred.resolve([]);
				}
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	setQtyFromPartInventory: function (qty, partInventoryStatusId, partInventoryId) {
		var deferred = Q.defer();
		PartInventory.update({
			'partInventoryId': partInventoryId
		}, {
			'partInventoryStatus': partInventoryStatusId,
			'quantityAvailable': qty
		}).exec(function (err, updated) {
			if(!err) {
				deferred.resolve(updated);
			} else deferred.reject(err);
		});
		return deferred.promise;
	}
}