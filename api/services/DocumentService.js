var request = require('request-promise');
var Q = require('q');
var uuid = require('uuid');
var moment = require('moment');

module.exports = {
	createDocument: function(doc, user) {
		var deferred = Q.defer();
		var userId = 0;
		if(user) userId = user.userId;
		var date = new Date();
		if (!doc.dateUploaded) doc.dateUploaded = moment(date).format("YYYY-MM-DD HH:mm:ss");
		if(!doc.uploadedByUserId) doc.uploadedByUserId = userId;
		delete doc._csrf;

		doc.encryptedName = uuid.v1();
		Sdpi_Documents.create(doc).fetch().exec(function(err, data) {
			if(!err) {
				deferred.resolve(data);
			}
			else {
				deferred.reject(err);
			}
		});

		return deferred.promise;
	},
	addRepairDocumentJunctionPromise: function(repairId, documentId) {
		var deferred = Q.defer();
		Repair_Repair_Documents.create({repairId:repairId, documentId:documentId}).exec(function(err, data) {
			if(!err) deferred.resolve(data);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	addToolDocumentJunctionPromise: function (toolId, documentId) {
		var deferred = Q.defer();
		Sdpi_Tool_Document.create({
			toolId: toolId,
			documentId: documentId
		}).exec(function (err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	getRepairDocuments: function(repairId) {
		var deferred = Q.defer();
		Repair_Repair.getDatastore().sendNativeQuery('CALL GetRepairDocuments($1)', [repairId], function(err, data) {
			if(!err) deferred.resolve(data.rows[0]);
			else deferred.reject(err);
		});

		return deferred.promise;
	}
};
