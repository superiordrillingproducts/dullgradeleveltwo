var Q = require('q');

module.exports = {
	getCutterPocketTypes: function() {
		var deferred = Q.defer();
		GlobalValues_CutterPocketType.find({}).exec(function(err, types) {
			if (!err) deferred.resolve(types);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCutterPocketSizes: function() {
		var deferred = Q.defer();
		GlobalValues_CutterPocketSize.find({}).exec(function(err, sizes) {
			if (!err) deferred.resolve(sizes);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCutterPockets: function() {
		var deferred = Q.defer();
		GlobalValues_CutterPocket.find({}).exec(function(err, pockets) {
			if (!err) deferred.resolve(pockets);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCutterInformationByTool: function(serial, cb) {
		var deferred = Q.defer();
		/////////////////////////////////////////////////////////////
		ToolService.getTool(serial, function(err, tool) {
			if (!err && tool) {
				var promises = [];
				promises.push(CutterService.getGlobalCutters());
				promises.push(CutterService.getCutterTypes());
				promises.push(CutterService.getCutterLocations(tool.toolId));

				Q.all(promises).then(function(data) {
					var globalCutters = data[0];
					var cutterTypes = data[1];
					var cutterLocations = data[2];

					// PROCEED
					var cutterRepairObjects = [];
					for (var i = 0; i < cutterLocations.length; i++) {
						var cl = cutterLocations[i];
						var globCutter = _.find(globalCutters, 'cutterId', cl.cutterId);
						var cutterType = _.find(cutterTypes, 'cutterTypeId', globCutter.cutterTypeId);

						var cutterRepairObject = {
							DnrCutterLocation: cl,
							Cutter: globCutter,
							CutterType: cutterType
						};
						cutterRepairObjects.push(cutterRepairObject);
					}

					var maxCutterObject = CutterService.getMaxCutterObject(cutterLocations);
					var cutterRepairViewObject = {
						Cutters: cutterRepairObjects,
						MaxCutterObject: maxCutterObject
					};
					deferred.resolve(cutterRepairViewObject);
				}, function(err) {
					deferred.reject(err);
				});
			} else {
				// resolve err
				deferred.reject(err);
			}
		});
		//////////////////////////////////////////////////////////////

		return deferred.promise;
	},
	getGlobalCutters: function() {
		var deferred = Q.defer();
		GlobalValues_Cutters.find({}).exec(function(err, globalCutters) {
			if (!err) deferred.resolve(globalCutters);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCutterTypes: function() {
		var deferred = Q.defer();
		GlobalValues_CutterTypes.find({}).exec(function(err, cutterTypes) {
			if (!err) deferred.resolve(cutterTypes);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getCutterLocations: function(toolId) {
		var deferred = Q.defer();
		Sdpi_CutterLocation.find({ toolId: toolId }).exec(function(err, cutterLocations) {
			if (!err) deferred.resolve(cutterLocations);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getMaxCutterObject: function(cutterLocations) {
		var maxObject = {
			MaxBlade: 0,
			MaxBladeGroup: 0,
			MaxCutter: 0
		};

		for (var i = 0; i < cutterLocations.length; i++) {
			var c = cutterLocations[i];
			if (c.bladeGroupNum > maxObject.MaxBladeGroup) {
				maxObject.MaxBladeGroup = parseInt(c.bladeGroupNum);
			}
			if (c.bladeNum > maxObject.MaxBlade) {
				maxObject.MaxBlade = parseInt(c.bladeNum);
			}
			if (c.cutterNum > maxObject.MaxCutter) {
				maxObject.MaxCutter = parseInt(c.cutterNum);
			}
		}
		return maxObject;
	},
	getCutterPositionsWithDullGradesForLatestRepairByTool: function(serial) {
		var deferred = Q.defer();
		/////////////////////////////////////////////////////////////
		var getToolPromise = ToolService.getTool(serial);

		getToolPromise.then(function(tool) {
			if (tool) {
				var promises = [];
				promises.push(CutterService.getGlobalCutters());
				promises.push(CutterService.getCutterTypes());
				promises.push(ToolService.getToolsInRepair(Enums.dnrRepairLocation.dullGrade, serial));
				promises.push(CutterService.getCutterLocations(tool.toolId));

				Q.all(promises).then(function(data) {
					var globalCutters = data[0];
					var cutterTypes = data[1];
					var toolsInRepair = data[2];
					var dnrCutterLocations = data[3];

					var cutterDullGradeObjects = [];

					if (toolsInRepair.length > 0) {
						var repairForThisTool = toolsInRepair[0];
						var repairId = repairForThisTool.RepairID;
						CutterService.getDullGradedCutters(repairId, function(err, dullGradedCutters) {
							if (!err) {
								if (dullGradedCutters.length > 0) {
									// if dull graded cutters do exist
									_.each(dnrCutterLocations, function(cl) {
										var globalCutter = _.find(globalCutters, function(gc) {
											return gc.cutterId === cl.cutterId;
										});
										var cutterType = _.find(cutterTypes, function(ct) {
											return ct.cutterTypeId === globalCutter.cutterTypeId;
										});
										var cutterRepairObject = {
											DnrCutterLocation: cl,
											Cutter: globalCutter,
											CutterType: cutterType
										};
										var cutterDullGrade = _.find(dullGradedCutters, function(dgc) {
											return dgc.bladeGroupNum === cl.bladeGroupNum && dgc.bladeNum === cl.bladeNum && dgc.cutterNum === cl.cutterNum;
										});
										if (cutterDullGrade) {
											cutterRepairObject.RepairDullGrade = cutterDullGrade;
										}
										cutterDullGradeObjects.push(cutterRepairObject);
									});
								} else {
									// if dull graded cutters do not exist yet
									_.each(dnrCutterLocations, function(cl) {
										var cutterRepairObject = {
											DnrCutterLocation: cl
										};
										cutterDullGradeObjects.push(cutterRepairObject);
									});
								}
								var maxCutterObject = CutterService.getMaxCutterObject(dnrCutterLocations);
								var cutterRepairViewObject = {
									Cutters: cutterDullGradeObjects,
									MaxCutterObject: maxCutterObject,
									RepairId: repairId
								};
								deferred.resolve(cutterRepairViewObject);

							} else deferred.reject(err);
						});

					} else {
						deferred.reject('Tool/Repair was not found for this serial number.');
					}
				}, function(err) {
					deferred.reject(err);
				});
			} else {
				// resolve err
				deferred.reject(err);
			}
		}, function(err) {
			deferred.reject(err);
		});

		ToolService.getTool(serial, function(err, tool) {

		});
		//////////////////////////////////////////////////////////////

		return deferred.promise;
	},
	getDullGradedCutters: function(repairId, cb) {
		SdpiToolsContext.query({
			text: 'SELECT RepairDullGrade WHERE repairId = $1',
			values: [repairId]
		}, function(err, results) {
			if (!err && results) {
				if (results[0] && results[0].length > 0)
					return cb(null, results[0]);
				else return cb(null, []);
			} else {
				return cb(err, null);
			}
		});
	},
	getCutters: function() {
		var deferred = Q.defer();
		GlobalValuesContext.query('CALL GetCutters()', function(err, data) {
			if (!err) deferred.resolve(data[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	updateCutter: function(cutter) {
		var deferred = Q.defer();
		GlobalValues_Cutters.update({ cutterId: cutter.cutterId }, cutter).exec(function(err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	checkCutterPocketAndSaveCutter: function(cutter) {
		var deferred = Q.defer();
		var promise;
		if (!cutter.cutterPocketId) {
			// create cutterPocket and assign it
			console.log('hit it');
			var saveCutterPocketPromise = CutterService.saveCutterPocket(cutter.cutterPocketTypeId, cutter.cutterPocketSizeId);
			saveCutterPocketPromise.then(function(cutterPocket) {
				cutter.cutterPocketId = cutterPocket.cutterPocketId;
				promise = CutterService.saveCutter(cutter);
				promise.then(function(c) {
                	console.log(c);
					deferred.resolve(c);
				}).catch(function(err) {
                	console.log(err);
					deferred.reject(err);
				});
			}).catch(function(err) {
            	deferred.reject(err);
			});
		} else {
        	console.log('hit it 2');
			promise = CutterService.saveCutter(cutter);
			promise.then(function(cutter) {
				deferred.resolve(cutter);
			}).catch(function(err) {
				deferred.reject(err);
			});
		}

		return deferred.promise;
	},
	saveCutter: function(cutter) {
		var deferred = Q.defer();
		cutter = {
        	cutterName: cutter.cutterName,
        	partNumber: cutter.partNumber,
        	cutterPocketId: cutter.cutterPocketId,
        	manufacturer: cutter.manufacturer,
        	sdpPartNumber: cutter.sdpPartNumber
		};
		GlobalValues_Cutters.create(cutter).exec(function(err, created) {
			if (!err) deferred.resolve(created);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	saveCutterPocket: function(cutterPocketTypeId, cutterPocketSizeId) {
		var deferred = Q.defer();
		GlobalValues_CutterPocket.create({ cutterPocketTypeId: cutterPocketTypeId, cutterPocketSizeId: cutterPocketSizeId }).exec(function(err, cutterPocket) {
			if (!err) deferred.resolve(cutterPocket);
			else deferred.reject(err);
		});
		return deferred.promise;
	}
};
