var Q = require('q');
var moment = require('moment');



module.exports = {
	baseUrl: sails.config.baseUrl,
	brainBaseUrl: sails.config.brainBaseUrl,

	getProgrammingVariable: function (progVarId, cb) {
		if (progVarId) {
			// get one specific programming variable
			ProgrammingVariable.findOne({
				progVarId: progVarId
			}).exec(function (err, progVar) {
				if (err) {
					err += ' : Global.js line 11 -> Problem getting programming variable.';
				}
				cb(err, progVar);
			});
		}
	},
	getProgrammingVariableUsingAPromise: function (progVarId) {
		var deferred = Q.defer();
		// get one specific programming variable
		ProgrammingVariable.findOne({
			progVarId: progVarId
		}).exec(function (err, progVar) {
			if (!err) deferred.resolve(progVar);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	validateUser: function (req, cb) {
		if (req.session && req.session.user) {
			return cb(null, req.session.user);
		} else {
			return cb('User not authenticated', null);
		}
	},
	getTimeDifferenceBetweenDates: function (endDate, startDate, returnInMilliSeconds) {
		var diff = moment.duration(moment(endDate).diff(moment(startDate)));
		if (returnInMilliSeconds) return diff._milliseconds;
		else {
			var formatted;
			if (diff._data.days > 0) {
				var daysToHours = diff._data.days * 24;
				formatted = daysToHours + diff._data.hours + ":" + diff._data.minutes + ":" + diff._data.seconds;
			} else {
				formatted = diff._data.hours + ":" + diff._data.minutes + ":" + diff._data.seconds;
			}
			return formatted;
		}
	},
	convertMillisecondsToDateFormat: function (ms) {
		ms = moment.duration(ms);
		var formatted;
		if (ms._data.days > 0) {
			var daysToHours = ms._data.days * 24;
			formatted = (ms._data.hours ? daysToHours + ms._data.hours + 'h ' : '') + (ms._data.minutes ? ms._data.minutes + 'm ' : '') + (ms._data.seconds ? ms._data.seconds + 's ' : '');
		} else {
			formatted = (ms._data.hours ? ms._data.hours + 'h ' : '') + (ms._data.minutes ? ms._data.minutes + 'm ' : '') + (ms._data.seconds ? ms._data.seconds + 's ' : '');
		}
		return formatted;
	},
	convert2dArrayTo1dArray: function (array) {
		var arr = [];
		_.each(array, function (a) {
			if (Global.isArray(a) && a.length === 1) {
				arr.push(a[0]);
			}
		});
		return arr;
	},
	isArray: function (o) {
		return Object.prototype.toString.call(o) === '[object Array]';
	},
	getTodayDateInJsStringForm: function () {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1;
		var yyyy = today.getFullYear();
		if (dd < 10) dd = '0' + dd;
		if (mm < 10) mm = '0' + mm;
		today = yyyy + '-' + mm + '-' + dd;

		return today;
	}
};
