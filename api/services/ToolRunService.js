var Q = require('q');

module.exports = {
    saveToolRunData: function (toolRunData, cb) {
        ToolRun.create(toolRunData, function (err, toolRun) {
            if (!err && toolRun) {
                return cb(err, toolRun);
            } else {
                return cb({
                    'message': 'There was a problem saving tool run data.'
                }, null);
            }
        })
    },

    updateToolRunData: function (updateRunData, cb) {
        console.log(updateRunData);
        ToolRun.update({
            toolRunId: updateRunData.toolRunId
        }, {
            customerWeAreBillingId: updateRunData.customerWeAreBillingId,
            customerThatRanToolId: updateRunData.customerThatRanToolId,
            rig: updateRunData.rig,
            well: updateRunData.well,
            county: updateRunData.county,
            stateId: updateRunData.stateId,
            drillingForeman: updateRunData.drillingForeman,
            runDateStart: updateRunData.runDateStart,
            runDateStop: updateRunData.runDateStop,
            quickBooksInvoiceNum: updateRunData.quickBooksInvoiceNum,
            sdpFieldTicketNum: updateRunData.sdpFieldTicket,
            customerInvoiceNum: updateRunData.customerInvoiceNum,
            grossRevenue: updateRunData.grossRevenue,
            sdpRoyaltyPercentage: updateRunData.sdpRoyaltyPercentage,
            sdpRevenue: updateRunData.sdpRevenue,
            customerRevenue: updateRunData.customerRevenue,
            footage: updateRunData.footage,
            salesRep: updateRunData.salesRep,
            basinId: updateRunData.basinId,
            invoiceDate: updateRunData.invoiceDate
        }, function (err, updated) {
            console.log("ERROR");
            console.log(err);
            if (!err && updated) {
                return cb(err, updated);
            } else {
                return cb({
                    'message': 'There was a problem updating the tool run.'
                }, null);
            }
        })
    },

    getToolRunStatuses: function (cb) {
        SdpiContext.query('SELECT * FROM SDPI.ToolRunStatus', function (err, statuses) {
            if (!err && statuses) {
                return cb(err, statuses)
            } else {
                return cb({
                    'message': 'There was a problem getting tool run statuses.'
                }, null);
            }
        })
    },

    getBasins: function (cb) {
        SdpiContext.query('SELECT * FROM SDPI.Basins', function (err, basins) {
            if (!err && basins) {
                return cb(err, basins)
            } else {
                return cb({
                    'message': 'There was a problem getting basins.'
                }, null);
            }
        })
    },

    getToolRuns: function (cb) {
        SdpiContext.query('CALL ToolRuns()', function (err, toolRuns) {
            if (!err && toolRuns) {
                return cb(err, toolRuns[0])
            } else {
                return cb({
                    'message': 'There was a problem getting tool runs for this month.'
                }, null);
            }
        })
    },
}