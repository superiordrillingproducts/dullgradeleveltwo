var Q = require('q');
var moment = require('moment');

module.exports = {
    getCostCenter: function(costCenterId) {
        var deferred = Q.defer();
        CostCenters.findOne({costCenterId: costCenterId}).exec(function(err, data) {
            if(!err) deferred.resolve(data);
            else deferred.reject(err);
        });
        return deferred.promise;
    }
};
