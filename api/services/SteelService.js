var moment = require('moment');
var Q = require('q');

module.exports = {
    getAllAvailableSteelWithMatchingSteelType: function (job, cb) {
        SteelInventory.query('SELECT * FROM manu.SteelInventory WHERE  steelInventoryStatusId = 1 and steelTypeId = ' + job.steelTypeId, function (err, records) {
            cb(err, records);
        });
    },

    getSteelInventoryBySteelInventoryId: function (steelInventoryId, cb) {
        SteelInventory.query('SELECT * FROM manu.SteelInventory where steelInventoryId = ' + steelInventoryId, function (err, record) {
            cb(err, record);
        })
    },

    getQtyLeftToBeManufacturedByJobId: function (job, cb) {
        SteelInventory.query('call GetQtyLeftToBeManufacturedByJobId(' + job.jobId + ')', function (err, qtyLeft) {
            cb(err, qtyLeft);
        });
    },

    cutSteelAndGetNewSerial: function (job, parentSteelObject, userId, lengthRemoving, qtyCutting, cb) {
        ////// Get a new serial number ///////
        InternalSerialGenerator.create({
            createdById: userId
        }, function (err, newGeneratedSerial) {
            if (!err && newGeneratedSerial) {
                var hex = newGeneratedSerial.internalSerialGeneratorId.toString(36);
                var hexSerial = hex.toUpperCase();
                ////// adjust parent steel current length //////
                SteelInventory.update({
                    steelInventoryId: parentSteelObject.steelInventoryId
                }, {
                    currentLengthInches: parentSteelObject.currentLengthInches - lengthRemoving,
                    steelLocationId: 30,
                }, function (err, steelRecord) {
                    if (!err && steelRecord) {
                        /////// create new steel inventory record from cut ////////
                        var newSteelObject = {
                            'serialNumber': hexSerial,
                            'steelInventoryStatusId': 2,
                            'steelLocationId': 27,
                            'parentSteelInventoryId': parentSteelObject.steelInventoryId,
                            'documentId': parentSteelObject.documentId,
                            'steelTypeId': parentSteelObject.steelTypeId,
                            'diameterInches': parentSteelObject.diameterInches,
                            'diameter8InchFraction': parentSteelObject.diameter8InchFraction,
                            'innerDiameter': parentSteelObject.innerDiameter,
                            'innerDiameter8InchFraction': parentSteelObject.innerDiameter8InchFraction,
                            'beginningLengthInches': lengthRemoving,
                            'currentLengthInches': lengthRemoving,
                            'dateCreated': new Date(),
                            'costPerInch': parentSteelObject.costPerInch,
                            'createdByEmpId': userId,
                            'jobId': job.jobId,
                            'qtyToManufacture': qtyCutting
                        };
                        SteelInventory.create(newSteelObject, function (err, newSteelInventory) {
                            if (!err && newSteelInventory) {
                                /////// create history that parent length was changed ////////
                                var parentLengthHistoryObject = {
                                    'steelInventoryId': parentSteelObject.steelInventoryId,
                                    'date': new Date(),
                                    'employeeId': userId,
                                    'steelHistoryStatusId': 2,
                                    'dataChangedTo': parentSteelObject.currentLengthInches - lengthRemoving
                                }
                                SteelHistory.create(parentLengthHistoryObject, function (err, parentLengthHistory) {
                                    if (!err && parentLengthHistory) {
                                        ////// create history that parent location is at saw ///////
                                        var parentLocationHistoryObject = {
                                            'steelInventoryId': parentSteelObject.steelInventoryId,
                                            'date': new Date(),
                                            'employeeId': userId,
                                            'steelHistoryStatusId': 6,
                                            'dataChangedTo': 30
                                        }
                                        SteelHistory.create(parentLocationHistoryObject, function (err, parentLocationHistory) {
                                            if (!err && parentLocationHistory) {
                                                ///// create history that parent cut off piece created new steel inventory record ///////
                                                var parentCreatedNewSteelHistoryObject = {
                                                    'steelInventoryId': parentSteelObject.steelInventoryId,
                                                    'date': new Date(),
                                                    'employeeId': userId,
                                                    'steelHistoryStatusId': 10,
                                                    'dataChangedTo': newSteelInventory.steelInventoryId
                                                }
                                                SteelHistory.create(parentCreatedNewSteelHistoryObject, function (err, parentCreatedNewSteelHistory) {
                                                    if (!err && parentCreatedNewSteelHistory) {
                                                        ////////// create history that child steel inventory record was created ///////
                                                        var childSteelCreatedHistoryObject = {
                                                            'steelInventoryId': newSteelInventory.steelInventoryId,
                                                            'date': new Date(),
                                                            'employeeId': userId,
                                                            'steelHistoryStatusId': 1,
                                                            'dataChangedTo': new Date()
                                                        }
                                                        SteelHistory.create(childSteelCreatedHistoryObject, function (err, childSteelCreatedHistory) {
                                                            if (!err && childSteelCreatedHistory) {
                                                                ///// create history that child location set to manufacturing /////
                                                                var childSteelLocationHistoryObject = {
                                                                    'steelInventoryId': newSteelInventory.steelInventoryId,
                                                                    'date': new Date(),
                                                                    'employeeId': userId,
                                                                    'steelHistoryStatusId': 6,
                                                                    'dataChangedTo': 27
                                                                }
                                                                SteelHistory.create(childSteelLocationHistoryObject, function (err, childSteelLocationHistory) {
                                                                    if (!err && childSteelLocationHistory) {
                                                                        ///// create history that child was assigned a job # ////////
                                                                        var childSteelAssignedJobHistoryObject = {
                                                                            'steelInventoryId': newSteelInventory.steelInventoryId,
                                                                            'date': new Date(),
                                                                            'employeeId': userId,
                                                                            'steelHistoryStatusId': 7,
                                                                            'dataChangedTo': job.jobId
                                                                        }
                                                                        SteelHistory.create(childSteelAssignedJobHistoryObject, function (err, childSteelAssignedJobHistory) {
                                                                            if (!err && childSteelAssignedJobHistory) {
                                                                                ///// create history that child status set //////
                                                                                var childSteelStatusHistoryObject = {
                                                                                    'steelInventoryId': newSteelInventory.steelInventoryId,
                                                                                    'date': new Date(),
                                                                                    'employeeId': userId,
                                                                                    'steelHistoryStatusId': 8,
                                                                                    'dataChangedTo': 2
                                                                                }
                                                                                SteelHistory.create(childSteelStatusHistoryObject, function (err, childSteelStatusHistory) {
                                                                                    if (!err && childSteelStatusHistory) {
                                                                                        ////// create history that child length set ////
                                                                                        var childSteelLengthHistoryObject = {
                                                                                            'steelInventoryId': newSteelInventory.steelInventoryId,
                                                                                            'date': new Date(),
                                                                                            'employeeId': userId,
                                                                                            'steelHistoryStatusId': 9,
                                                                                            'dataChangedTo': lengthRemoving
                                                                                        }
                                                                                        SteelHistory.create(childSteelLengthHistoryObject, function (err, childSteelLengthHistory) {
                                                                                            if (!err && childSteelLengthHistory) {
                                                                                                //// create history child qty ///////
                                                                                                var childSteelQtyHistoryObject = {
                                                                                                    'steelInventoryId': newSteelInventory.steelInventoryId,
                                                                                                    'date': new Date(),
                                                                                                    'employeeId': userId,
                                                                                                    'steelHistoryStatusId': 17,
                                                                                                    'dataChangedTo': qtyCutting
                                                                                                }
                                                                                                SteelHistory.create(childSteelQtyHistoryObject, function (err, childSteelQtyHistory) {
                                                                                                    if (!err && childSteelQtyHistory) {
                                                                                                        //////// pass back new serial number /////
                                                                                                        return cb(err, hexSerial);
                                                                                                    } else {
                                                                                                        return cb({
                                                                                                            'message': 'There was a problem creating child quantity steel history record.'
                                                                                                        }, null);
                                                                                                    }
                                                                                                })
                                                                                            } else {
                                                                                                return cb({
                                                                                                    'message': 'There was a problem creating child length steel history record.'
                                                                                                }, null);
                                                                                            }
                                                                                        })
                                                                                    } else {
                                                                                        return cb({
                                                                                            'message': 'There was a problem creating child status steel history record.'
                                                                                        }, null);
                                                                                    }
                                                                                })
                                                                            } else {
                                                                                return cb({
                                                                                    'message': 'There was a problem creating child job assigned steel history record.'
                                                                                }, null);
                                                                            }
                                                                        })
                                                                    } else {
                                                                        return cb({
                                                                            'message': 'There was a problem creating child location steel history record.'
                                                                        }, null);
                                                                    }
                                                                })
                                                            } else {
                                                                return cb({
                                                                    'message': 'There was a problem creating child created steel history record.'
                                                                }, null);
                                                            }
                                                        })
                                                    } else {
                                                        return cb({
                                                            'message': 'There was a problem creating parent created new steel history record.'
                                                        }, null);
                                                    }
                                                })
                                            } else {
                                                return cb({
                                                    'message': 'There was a problem creating parent location history record.'
                                                }, null);
                                            }
                                        })
                                    } else {
                                        return cb({
                                            'message': 'There was a problem creating parent length history record.'
                                        }, null);
                                    }
                                })

                            } else {
                                return cb({
                                    'message': 'There was a problem creating new steel record.'
                                }, null);
                            }
                        })
                    } else {
                        return cb({
                            'message': 'There was a problem updating parent steel.'
                        }, null);
                    }
                })
            } else {
                return cb({
                    'message': 'There was a problem creating new serial.'
                }, null);
            }
        })
    },

    getYardLocations: function (cb) {
        SteelLocations.query('SELECT * FROM manu.SteelLocations WHERE vernalHeadquartersLocationId = 6 OR vernalHeadquartersLocationId = 7', function (err, yardLocations) {
            cb(err, yardLocations);
        });
    },

    finishedCuttingSteel: function (job, finishedCuttingObject, userId, parentSteel, cb) {
        var lengthRemaining = parseInt(finishedCuttingObject.lengthWhole) + parseFloat(finishedCuttingObject.lengthDecimal);
        if (finishedCuttingObject.steelOption === 'yard') {
            console.log("yard");
            ///// update parent steel, putting it back into yard ///////
            SteelInventory.update({
                steelInventoryId: parentSteel.steelInventoryId
            }, {
                steelInventoryStatusId: 1,
                steelLocationId: finishedCuttingObject.yardLocation,
                currentLengthInches: lengthRemaining
            }, function (err, steelInventory) {
                if (!err && steelInventory) {
                    /////// create history for finished cutting length /////
                    var finishedHistoryLength = {
                        'steelInventoryId': parentSteel.steelInventoryId,
                        'date': new Date(),
                        'employeeId': userId,
                        'steelHistoryStatusId': 16,
                        'dataChangedTo': lengthRemaining
                    }
                    SteelHistory.create(finishedHistoryLength, function (err, lengthHistory) {
                        if (!err && lengthHistory) {
                            //// create history for finished cutting back in yard /////
                            var finishedHistoryLocationInYard = {
                                'steelInventoryId': parentSteel.steelInventoryId,
                                'date': new Date(),
                                'employeeId': userId,
                                'steelHistoryStatusId': 6,
                                'dataChangedTo': finishedCuttingObject.yardLocation
                            }
                            SteelHistory.create(finishedHistoryLocationInYard, function (err, locationHistory) {
                                if (!err && locationHistory) {
                                    cb(err, locationHistory);
                                } else {
                                    return cb({
                                        'message': 'There was a problem creating location steel history record.'
                                    }, null);
                                }
                            })
                        } else {
                            return cb({
                                'message': 'There was a problem creating length steel history record.'
                            }, null);
                        }
                    })
                } else {
                    return cb({
                        'message': 'There was a problem updating parent steel record.'
                    }, null);
                }
            });
        } else if (finishedCuttingObject.steelOption === 'job') {
            console.log("job");
            ///// update parent steel, assign job # ///////
            SteelInventory.update({
                steelInventoryId: parentSteel.steelInventoryId
            }, {
                steelInventoryStatusId: 2,
                steelLocationId: 27,
                currentLengthInches: lengthRemaining,
                jobId: job.jobId,
                qtyToManufacture: finishedCuttingObject.qtyCutting
            }, function (err, steelInventory) {
                if (!err && steelInventory) {
                    /////// create history for finished cutting length /////
                    var finishedHistoryLength = {
                        'steelInventoryId': parentSteel.steelInventoryId,
                        'date': new Date(),
                        'employeeId': userId,
                        'steelHistoryStatusId': 16,
                        'dataChangedTo': lengthRemaining
                    }
                    SteelHistory.create(finishedHistoryLength, function (err, lengthHistory) {
                        if (!err && lengthHistory) {
                            //// create history location manufacturing /////
                            var finishedHistoryLocationManufacturing = {
                                'steelInventoryId': parentSteel.steelInventoryId,
                                'date': new Date(),
                                'employeeId': userId,
                                'steelHistoryStatusId': 6,
                                'dataChangedTo': 27
                            }
                            SteelHistory.create(finishedHistoryLocationManufacturing, function (err, locationHistory) {
                                if (!err && locationHistory) {
                                    //// create history assign job # /////////
                                    var finishedHistoryAssignJobNum = {
                                        'steelInventoryId': parentSteel.steelInventoryId,
                                        'date': new Date(),
                                        'employeeId': userId,
                                        'steelHistoryStatusId': 7,
                                        'dataChangedTo': job.jobId
                                    }
                                    SteelHistory.create(finishedHistoryAssignJobNum, function (err, assignJobNumHistory) {
                                        if (!err && assignJobNumHistory) {
                                            ///// create history status changed ////
                                            var finishedHistoryStatus = {
                                                'steelInventoryId': parentSteel.steelInventoryId,
                                                'date': new Date(),
                                                'employeeId': userId,
                                                'steelHistoryStatusId': 8,
                                                'dataChangedTo': 2
                                            }
                                            SteelHistory.create(finishedHistoryStatus, function (err, statusHistory) {
                                                if (!err && statusHistory) {
                                                    //// create history qty added //// 
                                                    var finishedQtyAdded = {
                                                        'steelInventoryId': parentSteel.steelInventoryId,
                                                        'date': new Date(),
                                                        'employeeId': userId,
                                                        'steelHistoryStatusId': 17,
                                                        'dataChangedTo': finishedCuttingObject.qtyCutting
                                                    }
                                                    SteelHistory.create(finishedQtyAdded, function (err, qtyAddedHistory) {
                                                        if (!err && qtyAddedHistory) {
                                                            cb(err, qtyAddedHistory);
                                                        } else {
                                                            return cb({
                                                                'message': 'There was a problem creating, qty added, steel history record.'
                                                            }, null);
                                                        }
                                                    })
                                                } else {
                                                    return cb({
                                                        'message': 'There was a problem creating, status change, steel history record.'
                                                    }, null);
                                                }
                                            })
                                        } else {
                                            return cb({
                                                'message': 'There was a problem creating, assign job, steel history record.'
                                            }, null);
                                        }
                                    })
                                } else {
                                    return cb({
                                        'message': 'There was a problem creating, location manufacturing, steel history record.'
                                    }, null);
                                }
                            })
                        } else {
                            return cb({
                                'message': 'There was a problem creating, length, steel history record.'
                            }, null);
                        }
                    })
                } else {
                    return cb({
                        'message': 'There was a problem updating parent steel record.'
                    }, null);
                }
            });
        } else
        if (finishedCuttingObject.steelOption === 'drop') {
            console.log("drop");
            ////// update parent steel, drop steel ///////
            SteelInventory.update({
                steelInventoryId: parentSteel.steelInventoryId
            }, {
                steelInventoryStatusId: 5,
                steelLocationId: 99,
                currentLengthInches: lengthRemaining,
                dateTotalConsumed: new Date()
            }, function (err, steelInventory) {
                if (!err && steelInventory) {
                    /////// create history for finished cutting length /////
                    var finishedHistoryLength = {
                        'steelInventoryId': parentSteel.steelInventoryId,
                        'date': new Date(),
                        'employeeId': userId,
                        'steelHistoryStatusId': 16,
                        'dataChangedTo': lengthRemaining
                    }
                    SteelHistory.create(finishedHistoryLength, function (err, lengthHistory) {
                        if (!err && lengthHistory) {
                            //// create history for finished cutting drop steel/////
                            var finishedHistoryDrop = {
                                'steelInventoryId': parentSteel.steelInventoryId,
                                'date': new Date(),
                                'employeeId': userId,
                                'steelHistoryStatusId': 8,
                                'dataChangedTo': 5
                            }
                            SteelHistory.create(finishedHistoryDrop, function (err, dropSteel) {
                                if (!err && dropSteel) {
                                    //// create history drop steel location/////
                                    var finishedHistoryLocationDrop = {
                                        'steelInventoryId': parentSteel.steelInventoryId,
                                        'date': new Date(),
                                        'employeeId': userId,
                                        'steelHistoryStatusId': 6,
                                        'dataChangedTo': 99
                                    }
                                    SteelHistory.create(finishedHistoryLocationDrop, function (err, locationDrop) {
                                        if (!err && locationDrop) {
                                            cb(err, locationDrop)
                                        } else {
                                            return cb({
                                                'message': 'There was a problem creating, location drop, steel history record.'
                                            }, null);
                                        }
                                    })
                                } else {
                                    return cb({
                                        'message': 'There was a problem creating, drop, steel history record.'
                                    }, null);
                                }
                            })
                        } else {
                            return cb({
                                'message': 'There was a problem creating, length, steel history record.'
                            }, null);
                        }
                    })
                } else {
                    return cb({
                        'message': 'There was a problem updating parent steel record.'
                    }, null);
                }
            });
        }
    },

    getSteelForCurrentRunningJob: function (jobId, cb) {
        SteelInventory.query('SELECT * FROM manu.SteelInventory where steelInventoryStatusId = 2 AND steelLocationId = 27 AND jobId = ' + jobId, function (err, records) {
            cb(err, records);
        })
    },

    dropSteelAndGetNewSerial: function (job, parentSteelObject, userId, lengthRemoving, cb) {
        ////// Get a new serial number ///////
        InternalSerialGenerator.create({
            createdById: userId
        }, function (err, newGeneratedSerial) {
            if (!err && newGeneratedSerial) {
                var hex = newGeneratedSerial.internalSerialGeneratorId.toString(36);
                var hexSerial = hex.toUpperCase();
                ////// adjust parent steel current length //////
                SteelInventory.update({
                    steelInventoryId: parentSteelObject.steelInventoryId
                }, {
                    currentLengthInches: parentSteelObject.currentLengthInches - lengthRemoving
                }, function (err, steelRecord) {
                    if (!err && steelRecord) {
                        /////// create new steel inventory record from cut ////////
                        var newSteelObject = {
                            'serialNumber': hexSerial,
                            'steelInventoryStatusId': 1,
                            'steelLocationId': 31,
                            'parentSteelInventoryId': parentSteelObject.steelInventoryId,
                            'documentId': parentSteelObject.documentId,
                            'steelTypeId': parentSteelObject.steelTypeId,
                            'diameterInches': parentSteelObject.diameterInches,
                            'diameter8InchFraction': parentSteelObject.diameter8InchFraction,
                            'innerDiameter': parentSteelObject.innerDiameter,
                            'innerDiameter8InchFraction': parentSteelObject.innerDiameter8InchFraction,
                            'beginningLengthInches': lengthRemoving,
                            'currentLengthInches': lengthRemoving,
                            'dateCreated': new Date(),
                            'costPerInch': parentSteelObject.costPerInch,
                            'createdByEmpId': userId
                        };
                        SteelInventory.create(newSteelObject, function (err, newSteelInventory) {
                            if (!err && newSteelInventory) {
                                /////// create history that parent length was changed ////////
                                var parentLengthHistoryObject = {
                                    'steelInventoryId': parentSteelObject.steelInventoryId,
                                    'date': new Date(),
                                    'employeeId': userId,
                                    'steelHistoryStatusId': 22,
                                    'dataChangedTo': parentSteelObject.currentLengthInches - lengthRemoving
                                }
                                SteelHistory.create(parentLengthHistoryObject, function (err, parentLengthHistory) {
                                    if (!err && parentLengthHistory) {
                                        ///// create history that parent dropped piece created new steel inventory record ///////
                                        var parentCreatedNewSteelHistoryObject = {
                                            'steelInventoryId': parentSteelObject.steelInventoryId,
                                            'date': new Date(),
                                            'employeeId': userId,
                                            'steelHistoryStatusId': 21,
                                            'dataChangedTo': newSteelInventory.steelInventoryId
                                        }
                                        SteelHistory.create(parentCreatedNewSteelHistoryObject, function (err, parentCreatedNewSteelHistory) {
                                            if (!err && parentCreatedNewSteelHistory) {
                                                ////////// create history that child steel inventory record was created ///////
                                                var childSteelCreatedHistoryObject = {
                                                    'steelInventoryId': newSteelInventory.steelInventoryId,
                                                    'date': new Date(),
                                                    'employeeId': userId,
                                                    'steelHistoryStatusId': 1,
                                                    'dataChangedTo': new Date()
                                                }
                                                SteelHistory.create(childSteelCreatedHistoryObject, function (err, childSteelCreatedHistory) {
                                                    if (!err && childSteelCreatedHistory) {
                                                        ///// create history that child location set to manufacturing material holding /////
                                                        var childSteelLocationHistoryObject = {
                                                            'steelInventoryId': newSteelInventory.steelInventoryId,
                                                            'date': new Date(),
                                                            'employeeId': userId,
                                                            'steelHistoryStatusId': 6,
                                                            'dataChangedTo': 31
                                                        }
                                                        SteelHistory.create(childSteelLocationHistoryObject, function (err, childSteelLocationHistory) {
                                                            if (!err && childSteelLocationHistory) {
                                                                ///// create history that child status set //////
                                                                var childSteelStatusHistoryObject = {
                                                                    'steelInventoryId': newSteelInventory.steelInventoryId,
                                                                    'date': new Date(),
                                                                    'employeeId': userId,
                                                                    'steelHistoryStatusId': 8,
                                                                    'dataChangedTo': 1
                                                                }
                                                                SteelHistory.create(childSteelStatusHistoryObject, function (err, childSteelStatusHistory) {
                                                                    if (!err && childSteelStatusHistory) {
                                                                        ////// create history that child length set ////
                                                                        var childSteelLengthHistoryObject = {
                                                                            'steelInventoryId': newSteelInventory.steelInventoryId,
                                                                            'date': new Date(),
                                                                            'employeeId': userId,
                                                                            'steelHistoryStatusId': 9,
                                                                            'dataChangedTo': lengthRemoving
                                                                        }
                                                                        SteelHistory.create(childSteelLengthHistoryObject, function (err, childSteelLengthHistory) {
                                                                            if (!err && childSteelLengthHistory) {
                                                                                //////// pass back new serial number /////
                                                                                return cb(err, hexSerial);
                                                                            } else {
                                                                                return cb({
                                                                                    'message': 'There was a problem creating child length steel history record.'
                                                                                }, null);
                                                                            }
                                                                        })
                                                                    } else {
                                                                        return cb({
                                                                            'message': 'There was a problem creating child status steel history record.'
                                                                        }, null);
                                                                    }
                                                                })

                                                            } else {
                                                                return cb({
                                                                    'message': 'There was a problem creating child location steel history record.'
                                                                }, null);
                                                            }
                                                        })
                                                    } else {
                                                        return cb({
                                                            'message': 'There was a problem creating child created steel history record.'
                                                        }, null);
                                                    }
                                                })
                                            } else {
                                                return cb({
                                                    'message': 'There was a problem creating parent created new steel history record.'
                                                }, null);
                                            }
                                        })
                                    } else {
                                        return cb({
                                            'message': 'There was a problem creating parent length history record.'
                                        }, null);
                                    }
                                })

                            } else {
                                return cb({
                                    'message': 'There was a problem creating new steel record.'
                                }, null);
                            }
                        })
                    } else {
                        return cb({
                            'message': 'There was a problem updating parent steel.'
                        }, null);
                    }
                })
            } else {
                return cb({
                    'message': 'There was a problem creating new serial.'
                }, null);
            }
        })
    },

    scrapSteel: function (job, parentSteelObject, userId, qtyScrapping, cb) {
        var newQtyScrap = parentSteelObject.qtyScrapped + qtyScrapping;
        //////////////// scrap steel //////////
        SteelInventory.update({
            steelInventoryId: parentSteelObject.steelInventoryId
        }, {
            qtyScrapped: newQtyScrap
        }, function (err, steelRecord) {
            if (!err && steelRecord) {
                ////////// history that steel was scrapped ////////////
                var parentScrap1HistoryObject = {
                    'steelInventoryId': parentSteelObject.steelInventoryId,
                    'date': new Date(),
                    'employeeId': userId,
                    'steelHistoryStatusId': 18,
                    'dataChangedTo': newQtyScrap
                }
                SteelHistory.create(parentScrap1HistoryObject, function (err, parentScrap1History) {
                    if (!err && parentScrap1History) {
                        ///// if qty to manufacture is equal to qty scrap set status and location of parent steel to scrap/ not available////////
                        if (parentSteelObject.qtyToManufacture === newQtyScrap) {
                            SteelInventory.update({
                                steelInventoryId: parentSteelObject.steelInventoryId
                            }, {
                                steelInventoryStatusId: 4,
                                steelLocationId: 99,
                                dateTotalConsumed: new Date()
                            }, function (err, steelUpdateRecord) {
                                if (!err && steelUpdateRecord) {
                                    ////// history status //////
                                    var parentScrapStatusHistoryObject = {
                                        'steelInventoryId': parentSteelObject.steelInventoryId,
                                        'date': new Date(),
                                        'employeeId': userId,
                                        'steelHistoryStatusId': 8,
                                        'dataChangedTo': 4
                                    }
                                    SteelHistory.create(parentScrapStatusHistoryObject, function (err, parentScrapStatusHistory) {
                                        if (!err && parentScrapStatusHistory) {
                                            ////// history location //////
                                            var parentScrapLocationHistoryObject = {
                                                'steelInventoryId': parentSteelObject.steelInventoryId,
                                                'date': new Date(),
                                                'employeeId': userId,
                                                'steelHistoryStatusId': 6,
                                                'dataChangedTo': 99
                                            }
                                            SteelHistory.create(parentScrapLocationHistoryObject, function (err, parentScrapLocationHistory) {
                                                if (!err && parentScrapLocationHistory) {
                                                    return cb(err, steelRecord);
                                                } else {
                                                    return cb({
                                                        'message': 'There was a problem updating history for location change.'
                                                    })
                                                }
                                            })
                                        } else {
                                            return cb({
                                                'message': 'There was a problem updating history for status change.'
                                            }, null);
                                        }
                                    })
                                } else {
                                    return cb({
                                        'message': 'There was a problem updating parent steel status and location.'
                                    }, null);
                                }
                            })
                        } else {
                            return cb(err, steelRecord);
                        }
                    } else {
                        return cb({
                            'message': 'There was a problem creating history that steel was scrapped.'
                        }, null);
                    }
                })
            } else {
                return cb({
                    'message': 'There was a problem scrapping steel.'
                }, null);
            }
        })
    },
    
    getSteelTypes: function (cb){
        SteelType.query('SELECT * FROM SteelType where steelTypeStatusId != 99', function(err, steelTypes){
            if(!err && steelTypes){
                return cb(err,steelTypes)
            } else {
                return cb({
                    'message': 'There was a problem getting steel types.'
                }, null);
            }
        })
    },
}