var Q = require('q');

module.exports = {
    getPnoSubPnosByPno: function (pnoId) {
        var deferred = Q.defer();
        SdpiContext.query('call GetPnoSubPnosByPno(' + pnoId + ')', function (err, records) {
            if (!err) {
                deferred.resolve(records);
            } else {
                deferred.reject(err);
            }
        });
        return deferred.promise;
    },
    getParentPnoIds: function (pnoId, cb) {
        SdpiContext.query('SELECT * FROM PartNumberedObject_subPartNumberedObject WHERE subPartNumberedObjectId = ' + pnoId, function (err, pnoSubPnos) {
            if (!err) {
                var parentPnoIds = [];
                for (var psp of pnoSubPnos) {
                    parentPnoIds.push(psp.partNumberedObjectId);
                }
                return cb(err, parentPnoIds);
            }
            return cb(err, null);
        });
    },
    getTopTierPNOs: function (cb) {
        SdpiContext.query('SELECT * FROM PartNumberedObject WHERE isProductLine = 1 AND partNumberedObjectStatusId != 99 ORDER BY partNumberedObjectName ', function (err, pnos) {
            if (!err && pnos) {
                return cb(err, pnos)
            } else {
                return cb({
                    'message': 'There was a problem getting top tier pnos.'
                }, null);
            }
        })
    }
}