var Q = require('q');

module.exports = {
	getOpenWorkOrders: function (workOrderTypes, cb) {
		SdpiContext.query('call GetOpenWorkOrders(' + workOrderTypes + ')', function (err, records) {
			cb(err, records);
		});
	},
	getWorkOrders: function (workOrderTypes, workOrderStatuses) {
		var deferred = Q.defer();
		SdpiContext.query('call GetWorkOrders(' + workOrderTypes + ', ' + workOrderStatuses + ')', function (err, records) {
			if (!err) deferred.resolve(records[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},

	/**
	 * Gets a list of work order part inventory objects. All parameters are required. If you don't want one of them to have a value, then set it to 'null'. This is required because the parameters will be used directly in a stored procedure.
	 * @private
	 * @method getWorkOrderPnoPartInventories
	 * @param {Object} workOrderId
	 * @param {Object} partId
	 * @param {Object} getClosedRecords
	 * @return {Object} description
	 */
	getWorkOrderPnoPartInventories: function (workOrderId, cb2) {
		console.log('2');
		SdpiContext.query('call GetWorkOrderPartSubParts(' + workOrderId + ')', function (err, records) {
			if (!err) {
				console.log('#############################');
				console.log('#############################');
				console.log('#############################');
				console.log('#############################');
				console.log('#############################');
				cb2(null, records);
			} else {
				console.log(err);
				cb2(err, null);
			}
		});
	},

	createNewWpspsFromWorkOrder: function (workOrder, cb1) {
		console.log('1');
		if (workOrder) {
			SdpiContext.query('SELECT * FROM WorkOrder_Part_SubPart_PartInventory WHERE workOrderId = ' + workOrder.workOrderId, function (err, wpsp) {
				if (!err) {
					if (wpsp.length > 0) {
						// return these wpsp's because they already exist for this work order
						WorkOrderService.getWorkOrderPnoPartInventories(workOrder.workOrderId, function (err, records) {
							console.log('3');
							cb1(err, records);
						});
					} else {
						if (workOrder.partInventoryId) {
							var query = 'SELECT * FROM WorkOrder WHERE partInventoryId = ' + workOrder.partInventoryId;
						} else if (workOrder.toolId) {
							var query = 'SELECT * FROM WorkOrder WHERE toolId = ' + workOrder.toolId;
						} else {
							cb1(true, null);
						}

						SdpiContext.query(query, function (err, workOrders) {
							if (!err) {

								console.log('hit it');
								// TODO: get pno of latest work order and create new empty wpsp's and let them reference the the latest work order
								SdpiContext.query('call GetPnoSubPnosByPno(' + workOrder.partNumberedObjectId + ')', function (err, spnos) {
									if (!err) {
										console.log('create new wpsps');
										var wpspFromNew = [];
										for (var pno of spnos[0]) {
											var wpsp = {
												workOrderId: workOrder.workOrderId,
												isPartInventoryNew: false, // may try 1 instead of true
												subPartNumberedObjectId: pno.subPartNumberedObjectId
											}
											wpspFromNew.push(wpsp);
										}

										var createdArray = [];
										WorkOrderService.createWpsp(wpspFromNew, 0, createdArray, function (err, created) {
											if (!err) {

												if (workOrders.length > 1) {

													// find last workOrder to copy from
													var lastWorkOrder = workOrders[workOrders.length - 2];
													var newWorkOrder = workOrders[workOrders.length - 1];
													// TODO: get wpsp's and copy them and let them reference the latest work order
													SdpiContext.query('SELECT * FROM WorkOrder_Part_SubPart_PartInventory WHERE workOrderId = ' + lastWorkOrder.workOrderId, function (err, records) {
														if (!err) {
															console.log('find last one and copy');
															var wpspFromOld = [];

															var alreadySaved = [];
															var alreadySavedR = [];

															for (var c of created) {
																for (var r of records) {
																	if (c.subPartNumberedObjectId === r.subPartNumberedObjectId) {
																		if (alreadySaved.indexOf(c.WorkOrder_Part_SubPart_PartInventoryId) === -1 && alreadySavedR.indexOf(r.WorkOrder_Part_SubPart_PartInventoryId) === -1) {
																			c.partInventoryId = r.partInventoryId;
																			c.subPartId = r.subPartId;
																			alreadySaved.push(c.WorkOrder_Part_SubPart_PartInventoryId);
																			alreadySavedR.push(r.WorkOrder_Part_SubPart_PartInventoryId);
																		}
																	}
																}
															}

															WorkOrderService.updateWpsp(created, 0, function (err, updated) {
																if (!err) {
																	WorkOrderService.getWorkOrderPnoPartInventories(workOrder.workOrderId, function (err, records) {
																		return cb1(err, records);
																	});
																} else {
																	return cb1(err, null);
																}
															});

															//															var index = 0;
															//															WorkOrder_Part_SubPart_PartInventory.update({
															//																'workOrderId': created[index].workOrderId
															//															}, created[index], function (err, updated) {
															//
															//															})



															//															WorkOrder_Part_SubPart_PartInventory.update({}, created).exec(function (err, updated) {
															//																if (!err) {
															//																	WorkOrderService.getWorkOrderPnoPartInventories(lastWorkOrder.workOrderId, function (err, records) {
															//																		cb1(err, records);
															//																	});
															//																} else {
															//																	cb1(err, null);
															//																}
															//															});
														} else {
															cb1(err, null);
														}
													});
												} else {
													WorkOrderService.getWorkOrderPnoPartInventories(workOrder.workOrderId, function (err, records) {
														cb1(err, records);
													});
												}
											} else {
												cb1(err, null);
											}
										});
									} else {
										cb1(err, null);
									}
								});
							} else {
								cb1(err, null);
							}
						});
					}
				} else {
					cb1(err, null);
				}
			});
		}
	},
	createWpsp: function (wpsps, index, createdArray, cb) {
		wpsps[index].WorkOrder_Part_SubPart_PartInventoryId = null;
		WorkOrder_Part_SubPart_PartInventory.create(wpsps[index]).exec(function (err, created) {
			if (!err) {
				createdArray.push(created);
				if (wpsps[index + 1]) {
					WorkOrderService.createWpsp(wpsps, index + 1, createdArray, cb);
				} else {
					cb(err, createdArray);
				}
			}
		});
	},
	updateWpsp: function (wpsps, index, cb) {
		WorkOrder_Part_SubPart_PartInventory.update({
			'WorkOrder_Part_SubPart_PartInventoryId': wpsps[index].WorkOrder_Part_SubPart_PartInventoryId
		}, wpsps[index], function (err, updated) {
			if (!err) {
				if (wpsps[index + 1]) {
					WorkOrderService.updateWpsp(wpsps, index + 1, cb);
				} else {
					cb(err, updated);
				}
			}
		});
	},
	getSerialsCompatibleWithParentPnoBySubPno: function (pnoId, cb) {
		console.log('get serials');
		var query = 'call GetPartInventoryObjects(' + pnoId + ')';
		console.log(query);
		SdpiContext.query(query, function (err, records) {
			if (!err) {
				cb(err, records[0]);
			} else {
				cb(err, null);
			}
		})
	},
	assignAutoCompleteTextForPartInventoryObjects: function (records, cb) {
		for (var r of records) {
			r.autoCompleteText = r.serial + ' / ' + r.subPnoPartNumber + ' - Qty: ' + r.quantityAvailable;
		}
		console.log(records);
		cb(null, records);
	},
	updateSubPnoWithPartInventoryObject: function (subPnoWorkOrder, newPartInventoryObject, workOrder, cb) {
		var updateObject = {
			'subPartId': newPartInventoryObject.partId,
			'partInventoryId': newPartInventoryObject.partInventoryId,
			'isPartInventoryNew': true
		}

		if (workOrder) {
			updateObject.isACollectionOfThisWorkOrderId = workOrder.workOrderId;
		}

		WorkOrder_Part_SubPart_PartInventory.update({
			'WorkOrder_Part_SubPart_PartInventoryId': subPnoWorkOrder.WorkOrder_Part_SubPart_PartInventoryId
		}, updateObject, function (err, records) {
			cb(err, records);
		});
	},
	getWorkOrderByPartInventoryObject: function (partInventoryId, cb) {
		WorkOrder.find({
			'where': {
				'partInventoryId': partInventoryId
			},
			'sort': 'workOrderId DESC'
		}, function (err, wos) {
			if (!err) {
				if (wos && wos.length > 0) {
					var lastWorkOrder = wos[0];
					return cb(null, lastWorkOrder);
				} else {
					return cb(null, null);
				}
			} else {
				return cb(err, null);
			}
		});
	},
	getPnoByPartId: function (partId, cb) {
		SdpiContext.query('SELECT pno.* FROM Part part LEFT JOIN PartNumberedObject pno ON part.partNumberedObjectId = pno.partNumberedObjectId WHERE part.partId = ' + partId, function (err, records) {
			cb(err, records);
		});
	},
	associateBodyWithWorkOrder: function (workOrder, steelBody, isPartInventoryNew) {
		var deferred = Q.defer();

		var wpsp = {
			workOrderId: workOrder.workOrderId,
			partId: workOrder.partId,
			subPartId: steelBody.parentPartId,
			subPartNumberedObjectId: steelBody.partNumberedObjectId,
			partInventoryId: steelBody.partInventoryId,
			
		}
		
		if(isPartInventoryNew) wpsp.isPartInventoryNew = 1;

		console.log('-----spsp-----');
		console.log(wpsp);

		var getPartInventoryPromise = PartInventoryService.getPartInventoryById(steelBody.partInventoryId);

		getPartInventoryPromise.then(function (result) {
			console.log('-----result-----');
			console.log(result);

			var addWpspPromise = WorkOrderService.addOneWpsp(wpsp);
			var pi = result;
			console.log(pi);
			pi.quantityAvailable--;
			if (pi.quantityAvailable === 0) {
				pi.partInventoryStatus = 6;
			} else if (pi.quantityAvailable < 0) {
				deferred.reject(new Error('Quantity cannot be -1. Looks like your part inventory quanity was already 0.'));
			}

			var setQtyPromise = PartInventoryService.setQtyFromPartInventory(pi.quantityAvailable, pi.partInventoryStatus, steelBody.partInventoryId);
			addWpspPromise.then(setQtyPromise).then(function (data) {
				deferred.resolve(data);
			}).catch(function (err) {
				console.log(err);
				deferred.reject(err);
			});
		}).catch(function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	addOneWpsp: function (wpsp) {
		var deferred = Q.defer();
		WorkOrder_Part_SubPart_PartInventory.create(wpsp).exec(function (err, record) {
			if (!err) deferred.resolve(record);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	changeWorkOrderStatus: function (workOrderId, newStatusId) {
		var deferred = Q.defer();
		WorkOrder.update({
			workOrderId: workOrderId
		}, {
			workOrderStatusId: newStatusId
		}, function (err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	createWorkOrder: function(wo) {
		var deferred = Q.defer();
		if(!wo.dateCreated) {
			wo.dateCreated = new Date();
		}
		WorkOrder.create(wo).exec(function(err, w) {
			if(!err) deferred.resolve(w);
			else deferred.reject(err);
		});
		return deferred.promise;
	}
}