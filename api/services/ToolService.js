var request = require('request-promise');
var Q = require('q');
var fs = require('fs');

module.exports = {
	updateToolPart: function(toolId, partId, partNumberedObjectId, cb) {
		Tools.update({
			'toolId': toolId
		}, {
			'partId': partId,
			'partNumberedObjectId': partNumberedObjectId
		}, function(err, updatedTool) {
			return cb(err, updatedTool);
		});
	},
	getToolsByUser: function(user, cb) {
		CustomerService.getCompany(user, function(err, customer) {
			if (!err) {
				var query;
				if (customer === 'nousercustomer') {
					query = 'call GetTools()';
				} else {
					query = 'call GetToolsByUser(' + user.userId + ',' + customer.customerId + ')';
				}

				SdpiToolsContext.query(query, function(err, records) {
					var tools = records[0];
					return cb(null, tools);
				});
			} else {
				return cb(err, null);
			}
		});
	},
	generateDnrStrapsheet: function(tool, cb) {
		var options = {
			uri: Global.brainBaseUrl + 'BrainMobileApp/ToolListView/GenerateDnrSheetPdf',
			method: 'POST',
			body: {
				ValueList: [
					tool.serial
				]
			},
			json: true
		};

		request(options).then(function(res) {
			if (res) {
				return cb(null, res);
			} else {
				return cb('Serial not found', null);
			}
		}).catch(function(err) {
			return cb(err, null);
		});
	},
	getTools: function(cb) {

	},
	getTool: function(toolSerial) {
		var deferred = Q.defer();
		console.log('------toolSerial------');
		console.log(toolSerial);
		Tools.findOne({
			serial: toolSerial
		}).exec(function(err, tool) {
			if (!err) {
				if (tool) {
					deferred.resolve(tool);
				} else deferred.reject(new Error('Could not locate tool with such serial number.'));
			} else {
				console.log(err);
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	getToolsInRepair: function(locationId, serial) {
		var deferred = Q.defer();

		SdpiToolsContext.query('CALL SDPI.GetToolsInRepair(?)', [locationId], function(err, results) {
			if (!err && results) {
				var toolsRepairs = results[0];
				console.log(toolsRepairs);
				if (serial && toolsRepairs && toolsRepairs.length > 0) {
					toolsRepairs = _.find(toolsRepairs, function(tr) {
						return tr.serial === serial;
					});
				}
				deferred.resolve(toolsRepairs);
			} else deferred.reject(err);
		});
		return deferred.promise;
	},
	getArrivingTools: function(cb) {
		SdpiContext.query('call GetNewToolsAndToolsThatAreNotInRepair()', function(err, tools) {
			if (!err) {
				cb(err, tools[0]);
			} else {
				return cb(err, null);
			}
		});
	},
	getReceivingTools: function(req) {
		var deferred = Q.defer();

		if(req && req.session && req.session.facility) {
			SdpTools_Tools.getDatastore().sendNativeQuery('CALL GetReceivingTools()', function (err, tools) {
				console.log(err);
				console.log(tools.rows[0].length);
				if (!err) {
					if([3,4,5,8,9,10].includes(req.session.facility.facilityId)) {
						tools.rows[0] = tools.rows[0].filter(tool => {
							let custId = parseInt(tool.customerId);
							return custId === 49 || custId === 50 // weatherford customer
						})
					}
					deferred.resolve(tools.rows[0]);
				} else {
					deferred.reject(err);
				}
			});
		} else {
			throw new Error('Login session has expired')
			deferred.reject(new Error('Login session has expired'))
		}

		return deferred.promise;
	},
	changeStatusAndStockPoint: function(toolId, statusId, stockPointId) {
		var deferred = Q.defer();

		Tools.update({
			toolId: toolId
		}, {
			toolStatusId: statusId,
			stockPointId: stockPointId
		}).exec(function(err, updated) {
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	getToolsJustAssembled: function() {
		var deferred = Q.defer();

		return deferred.promise;
	},
	getToolBySerial: function(toolSerial, cb) {
		SdpiContext.query('CALL GetToolBySerial(?)', [toolSerial], function(err, tool) {
			if (!err && tool) {
				return cb(err, tool[0]);
			} else {
				return cb({
					'message': 'There was a problem getting the tool.'
				}, null);
			}
		});
	},
	getToolsWithCuttersInDullGrade: function() {
		var deferred = Q.defer();
		SdpiContext.query('CALL GetToolsWithCuttersInRepair(?)', [Enums.dnrRepairStatus.dullGrade], function(err, results) {
			console.log(results);
			if (!err) deferred.resolve(results[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getToolsWithCuttersInPullCutters: function() {
		var deferred = Q.defer();
		SdpiContext.query('CALL GetToolsWithCuttersInRepair(?)', [Enums.dnrRepairStatus.pullCutters], function(err, results) {
			console.log(results);
			if (!err) deferred.resolve(results[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getToolsWithCuttersInBraze: function() {
		var deferred = Q.defer();
		SdpiContext.query('CALL GetToolsWithCuttersInRepair(?)', [Enums.dnrRepairStatus.braze], function(err, results) {
			if (!err) deferred.resolve(results[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	createOrUpdateNonSdpTool: function(nonSdpTool, users) {
		var deferred = Q.defer();

		console.log(nonSdpTool);
		if (nonSdpTool.nonSdpToolId) {
        	// make toolId the same as nonSdpToolId because the correct column name in NonSdpTools.Tools table is toolId. nonSdpToolId is used when we initally
        	// retrieve the list of tools, because SdpTools.Tools and NonSdpTools.Tools are merged, so we change NonSdpTools.Tools identifier to
        	// nonSdpToolId in order to determine between an sdp tool and a non sdp tool.
        	nonSdpTool.toolId = nonSdpTool.nonSdpToolId;
        	// change non sdp tool status
        	var updateNonSdpToolPromise = ToolService.updateNonSdpTool({toolId: nonSdpTool.toolId, statusId: 9, repairCustomerId: nonSdpTool.repairCustomerId}, users);
        	updateNonSdpToolPromise.then(function(updatedNonSdpTool) {
        		console.log(updatedNonSdpTool);
        		deferred.resolve(updatedNonSdpTool);
        	}).catch(function(err) {
        		console.log(err);
        		deferred.reject(err);
        	});
		} else {
			var validTool = {
				serial: nonSdpTool.serial,
				statusId: 9,
				partId: nonSdpTool.partId,
				toolFamilyId: nonSdpTool.toolFamilyId,
				repairCustomerId: nonSdpTool.repairCustomerId
			};
			NonSdpTools_Tools.create(validTool).exec(function(err, createdTool) {
				if (!err) {
					// now create tool history
					var promise = ToolService.createNonSdpToolHistory({
						toolId: createdTool.toolId,
						previousValue: null,
						updatedValue: createdTool.toolId,
						fieldAffected: 'Tools.toolId',
						userId: users[0].userId,
						date: new Date()
					});

					promise.then(function(data) {
						console.log(data);
						deferred.resolve(createdTool);
					}).catch(function(err) {
						console.log('**********************');
						console.log(err);
						deferred.reject({ err: err, additionalInfo: 'Tool was created but history record failed.' });
					});
				} else deferred.resolve(data);
			});
		}
		return deferred.promise;
	},
	updateNonSdpTool: function(updatedToolNotSavedYet, users) {
		var deferred = Q.defer();
		NonSdpTools_Tools.findOne({ toolId: updatedToolNotSavedYet.toolId }).exec(function(err, oldNonSdpTool) {
			if (!err) {
				NonSdpTools_Tools.update({toolId: updatedToolNotSavedYet.toolId}, updatedToolNotSavedYet).exec(function(err, updatedNonSdpTool) {
					if (!err) {
						updatedNonSdpTool = updatedNonSdpTool[0];
						var promises = [];

						// checking if status is different
						if (oldNonSdpTool.statusId !== updatedNonSdpTool.toolStatusId) {
							promises.push(ToolService.createNonSdpToolHistory({
								toolId: updatedNonSdpTool.toolId,
								previousValue: oldNonSdpTool.statusId,
								updatedValue: updatedNonSdpTool.statusId,
								fieldAffected: 'Tool.statusId',
								userId: users[0].userId,
								date: new Date()
							}));
						}

						Q.all(promises).then(function(data) {
							deferred.resolve(updatedNonSdpTool);
						}, function(err) {
							deferred.reject(err);
						});
					} else {
						deferred.reject(err);
					}
				});
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	updateToolSimple: (tool) => {
		let deferred = Q.defer()
		SdpTools_Tools.update({toolId: tool.toolId}, tool).exec((err, updatedTool) => {
			if(!err) deferred.resolve(updatedTool)
			else deferred.reject(err)
		})
		return deferred.promise
	},
	updateTool: function(updatedToolNotSavedYet, users) {
		var deferred = Q.defer();
		console.log(updatedToolNotSavedYet.toolId);
		SdpTools_Tools.findOne({ toolId: updatedToolNotSavedYet.toolId }).exec(function(err, oldTool) {
			if (!err) {
				SdpTools_Tools.update({ toolId: updatedToolNotSavedYet.toolId }, updatedToolNotSavedYet).exec(function(err, updatedTool) {
					if (!err) {
						updatedTool = updatedTool[0];
						// var promises = [];
						// // checking if toolStatus is different
						// if (oldTool.toolStatusId !== updatedTool.toolStatusId) {
						// 	promises.push(ToolService.createToolHistory({
						// 		toolId: updatedTool.toolId,
						// 		previousValue: oldTool.toolStatusId,
						// 		updatedValue: updatedTool.toolStatusId,
						// 		fieldAffected: 'Tool.toolStatusId',
						// 		userId: users[0].userId,
						// 		date: new Date()
						// 	}));
						// }
						// // checking if stockPointId is different
						// if (oldTool.stockPointId !== updatedTool.stockPointId) {
						// 	promises.push(ToolService.createToolHistory({
						// 		toolId: updatedTool.toolId,
						// 		previousValue: oldTool.stockPointId,
						// 		updatedValue: updatedTool.stockPointId,
						// 		fieldAffected: 'Tool.stockPointId',
						// 		userId: users[0].userId,
						// 		date: new Date()
						// 	}));
						// }
						deferred.resolve(updatedTool);
						// Q.all(promises).then(function(data) {
						// 	console.log(data);
						// 	deferred.resolve(updatedTool);
						// }, function(err) {
						// 	deferred.reject(err);
						// });
					} else {
						deferred.reject(err);
					}
				});
			} else {
				deferred.reject(err);
			}
		});
		return deferred.promise;
	},
	createToolHistory: function(toolHistoryObject) {
		var deferred = Q.defer();
		SdpTools_ToolHistory.create(toolHistoryObject).exec(function(err, created) {
			console.log(err);
			console.log(created);
			if (!err) deferred.resolve(created);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	createNonSdpToolHistory: function(toolHistoryObject) {
		var deferred = Q.defer();
		NonSdpTools_ToolHistory.create(toolHistoryObject).exec(function(err, created) {
			if (!err) deferred.resolve(created);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	updateToolLengthsAndODs: function(toolInfo) {
		var deferred = Q.defer();
		SdpTools_Tools.update({toolId: toolInfo.toolId},toolInfo).exec(function(err, updated){
			if (!err) deferred.resolve(updated);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getHeatCert: function (data, cb) {
		var options = {
			url: Global.brainBaseUrl + 'Machine/FileManager/GetDocumentByDocumentIdNode',
			method: 'POST',
			body: {
				Id: data.documentId,
				UserToken: data.token
			},
			json: true
		};

		let fileStream = fs.createWriteStream(data.documentId + ".pdf");
		request(options).pipe(fileStream);
		fileStream.on('finish', function () {
			cb(null, fileStream)
		})

	},
};
