module.exports = {
	cutterAction: {
		new: 1,
		rotate: 2,
		leaveIt: 3
	},
	cutterCondition: {
		chipped: 1,
		broken: 2,
		cracked: 3,
		erodedAlloy: 4,
		erodedCutter: 5,
		heatCracked: 6,
		worn1: 7,
		worn2: 8,
		worn3: 9,
		lost: 10,
		flameSpray: 11,
		fo: 12,
		emptyNew: 13
	},
	dnrHistoryType: {
		repairCreated: 1,
		toolArrived: 2,
		statusChanged: 3, // repair status changed
		toolDullGraded: 4,
		toolFlameSprayed: 5,
		toolBrazed: 6,
		toolInspected: 7
	},
	'DnrRepairApp': 20,
	dnrRepairLocation: {
		incomingRepairs: 1,
		rework: 3,
		dullGrade: 5,
		pullCutters: 9,
		braze: 11,
		inspect: 14,
		ship: 17,
		waitingForJimToReceiveTool: 20,
		complete: 21,
		cutout: 22		
	},
	dnrRepairStatus: {
		arrival: 1,
		dullGrade: 2,
		pullCutters: 3,
		flameSpray: 4,
		braze: 5,
		inspection: 6,
		shipping: 7,
		complete: 8,
		arrivalWash: 9,
		arrivalSandblast: 10,
		postBrazeSandblast: 11,
		paint: 12
	},
	groupId: {
		'Executives': 1,
		'HumanResourcesL1': 2,
		'HumanResourcesL2': 3,
		'HardRockRepresentatives': 4,
		'AccountingL1': 5,
		'AccountingL2': 6,
		'InformationTechnology': 7,
		'Controller': 8,
		'RepairL1': 9,
		'RepairL2': 10,
		'AccountsReceivableL1': 11,
		'SDPIRentalRepairSystem': 12,
		'InvestorRelationsAdvisorL1': 13,
		'InvestorRelationsAdvisorL2': 14,
		'SteelInventory': 15,
		'ToolTrackingApp': 16,
		'AssemblyAndRepairApp': 17,
		'LaborTrackingApp': 18,
		'machinist': 18,
        'DnrRepairApp': 20,
        'JobCreation': 21,
        'ToolRun': 22
	},
	group: {
		dnrDullgradeUsers: 23,
		dnrBrazeUsers: 24,
		dnrArrivalUsers: 25,
		dnrPullcuttersUsers: 26
	},
	laborTimeStampType: {
		manuMachining: 1,
		manuSetup: 2,
		steelSaw:3,
		qa: 4,
		programming: 5,
		dnrRepair: 6
	},
	workOrderType: {
		striderAssembly: 1,
		striderRepair: 2,
		striderCartridgeAssembly: 3,
		striderCartridgeRepair: 4,
		dnrAssembly: 5,
		dnrRepair: 6
	},	
	stockPoint: {
		vernalManufacturing: 2,
		vernalRepair: 3,
		vernal: 4,
		fortCollins: 5,
		williston: 6,
		elkCity: 7,
		notInCirculation: 8,
		midland: 9,
		pennsylvania: 10,
		dti: 11
	},
	toolStatus: {
		repairBeingRepaired: 9,
		repairAvailable: 19
	},
	repairType: {
		standardRepair: 1,
		usedToolFirstTimeRepair: 2,
		newToolAssembly: 3
	},
};