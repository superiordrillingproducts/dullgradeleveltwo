var Q = require('q');

module.exports = {
	getPartFromSubPartInventoryObjects: function(parentPno, subPartInventoryObjects, cb) {
		// get sub part inventory objects quantity
		var subPiQty = subPartInventoryObjects.length;

		PartService.getPartSubPartsByPno(parentPno.partNumberedObjectId, function(err, partSubParts) {
			if (!err) {
				var partToReturn;
				var tempArray = [];
				var indexCounter = 0;
				var refPartInfoArray = [];
				var partExists = false;
				var part;
				var tempArrayCopy; // used to keep track of subparts that no longer belong to part
				var refPartMatchesCopy; // user to keep track of subparts that now belong to a part that did not before
				for (var psp of partSubParts) {
					if (tempArray.length === 0) {
						tempArray.push(psp);
					} else {
						var nextIndex = indexCounter + 1;
						if (tempArray[0].partId !== psp.partId ||
																												!partSubParts[nextIndex]) {

							// if element is the last one, then push it on to the temp array
							if (!partSubParts[nextIndex]) {
								tempArray.push(psp);
							}

							// check that size of tempArray is equal to subPartsQty
							if (tempArray.length === subPiQty) {
								var matches = 0;

								// loop thru tempArray and compare all its subPartIds to corresponding subPart's partIds
								var refChangeString = '';
								var refPartInfo = {
									'matches': []
								};

								for (var ta of tempArray) {
									for (var spc of subPartInventoryObjects) {
										if (ta.sub_partId === spc.partInventoryObject.partId) {
											matches++;
											refPartInfo.matches.push(ta);
											break;
										}
									}
								}

								// TODO: Test existing. But perhaps a better implementation will be to check right here if refPartInfo.matches > 0 before moving on. This would. If it is 0, then you could break and go to the next part in the loop.

								// do part reference stuff
								refPartInfo.partId = tempArray[0].partId;
								refPartInfo.nonmatches = [];
								tempArrayCopy = tempArray.slice();


								for (var matchPart of refPartInfo.matches) {
									for (var i = 0; i < tempArrayCopy.length; i++) {
										if (tempArrayCopy[i].sub_partId === matchPart.sub_partId) {
											tempArrayCopy.splice(i, 1);
											break;
										}
									}
								}

								// TEST
								refPartMatchesCopy = refPartInfo.matches.slice();
								for (var spco of subPartInventoryObjects) {
									for (var i = 0; i < refPartMatchesCopy.length; i++) {
										if (refPartMatchesCopy[i].sub_partId === subPartInventoryObjects.sub_partId) {
											refPartMatchesCopy.splice(i, 1);
											break;
										}
									}
								}






								refPartInfo.nonmatches = tempArrayCopy;
								refPartInfo.nonmatchNew = refPartMatchesCopy;

								refPartInfoArray.push(refPartInfo);

								if (matches === subPiQty) {
									partExists = true;
									break;
								}
							}
							tempArray = [];
							tempArray.push(psp);
						} else {
							tempArray.push(psp);
						}
					}
					indexCounter++;
				}
				if (partExists) {
					PartService.getPartByPartId(partSubParts[indexCounter - 1].partId, function(err, part) {
						if (!err) {
							PartService.updateWorkOrderWithPartId(subPartInventoryObjects[0].workOrderId, part.partId, 11, function(err, wo) {
								if (!err) {
									if (subPartInventoryObjects[0].partInventoryId) {
										PartInventoryService.updatePartInventoryPart(subPartInventoryObjects[0].partInventoryId, part.partId, 5, 1, function(err, pi) {
											return cb(err, part);
										});
									} else if (subPartInventoryObjects[0].toolId) {
										ToolService.updateToolPart(subPartInventoryObjects[0].toolId, part.partId, part.partNumberedObjectId, function(err, pi) {
											return cb(err, part);
										});
									} else {
										return cb(err, part);
									}
								} else {
									return cb(err, null);
								}
							});
						} else {
							return cb(err, null);
						}
					});
				} else {
					// find the best match, but if there are no matching subparts then don't reference anything
					PartService.getPartsAndPnoByPartIds(function(err, parts) {
						if (refPartInfoArray[0]) {
							var leastNonmatchRefPartObject = refPartInfoArray[0]; // set the first one
							// loop through and update it anytime you find a better match
							for (var rpi of refPartInfoArray) {
								if (rpi.nonmatches.length < leastNonmatchRefPartObject.nonmatches.length) {
									leastNonmatchRefPartObject = rpi;
								}
							}

							if (leastNonmatchRefPartObject.matches.length > 0) {
								var refString = '';
								for (var sp of subPartInventoryObjects) {
									for (var x = 0; x < leastNonmatchRefPartObject.nonmatches.length; x++) {

										var refPart;
										for (var p of parts) {
											if (parseInt(p.partId) === parseInt(leastNonmatchRefPartObject.nonmatches[x].sub_partId)) {
												refPart = p;
												break;
											}
										}

										// if pnos match up then add to ref part string
										if (sp.subPartNumberedObjectId === refPart.partNumberedObjectId) {
											refString += sp.subPartNumberedObjectName + ' ' + refPart.partId + ' -  replaced by ' + sp.subPartId + '; ';
											leastNonmatchRefPartObject.nonmatches.splice(x, 1);
										}

										break;
									}
								}
								var part = {
									'refPartChanges': refString,
									'refPartId': leastNonmatchRefPartObject.partId
								};

								// TEST: record subparts that no longer belong to part and subparts that now belong to part but didnt before
								if (leastNonmatchRefPartObject.nonmatches > 0) {
									refString += 'Removed: ';
									for (var nmOld of leastNonmatchRefPartObject.nonmatches) {
										refString += nmOld.partId + ' ';
									}
									refString += '; ';
								}
								if (leastNonmatchRefPartObject.nonmatchNew > 0) {
									refString += 'Added: ';
									for (var nmNew of leastNonmatchRefPartObject.nonmatchNew) {
										refString += nmNew.partId + ' ';
									}
									refString += '; ';
								}
							}
						}

						if (!part) {
							var part = {};
						}

						part.partNumberedObjectId = parentPno.partNumberedObjectId;
						PartService.getNextPartNumberInfoForPno(part.partNumberedObjectId, function(err, partNumberInfo) {
							part.partNumberedObjectChrono = partNumberInfo.partNumberedObjectChrono;
							part.partNumber = partNumberInfo.partNumber;
							part.whoCreated = 'david@teamsdp.com';
							part.whenCreated = new Date();

							PartService.savePartAndReturnPartId(part, function(err, partIdJustInserted) {
								if (!err) {
									PartService.getPartByPartId(partIdJustInserted, function(err, part) {
										if (!err) {
											// create part sub parts with subparts
											var partSubParts = [];

											// TODO: Change this so that we use the dynamic query
											for (var sp of subPartInventoryObjects) {
												var partSubPart = {
													'partId': partIdJustInserted,
													'sub_partId': sp.partInventoryObject.partId
												};
												partSubParts.push(partSubPart);
											}
											PartService.savePartSubParts(partSubParts, function(err, created) {
												if (!err) {
													PartService.updateWorkOrderWithPartId(subPartInventoryObjects[0].workOrderId, part.partId, 11, function(err, wo) {
														if (!err) {
															if (subPartInventoryObjects[0].partInventoryId) {
																PartInventoryService.updatePartInventoryPart(subPartInventoryObjects[0].partInventoryId, part.partId, 5, 1, function(err, pi) {
																	part.isNew = true;
																	return cb(err, part);
																});
															} else if (subPartInventoryObjects[0].toolId) {
																ToolService.updateToolPart(subPartInventoryObjects[0].toolId, part.partId, part.partNumberedObjectId, function(err, pi) {
																	part.isNew = true;
																	return cb(err, part);
																});
															} else {
																part.isNew = true;
																return cb(err, part);
															}
														} else {
															return cb(err, null);
														}
													});
												} else {
													return cb(err, null);
												}
											});
										} else {
											return cb(err, null);
										}
									});
								} else {
									return cb(err, null);
								}
							});
						});
					});
				}
			} else {
				return cb(err, null);
			}
		});
	},
	updateWorkOrderWithPartId: function(workOrderId, partId, status, cb) {
		if (workOrderId) {
			WorkOrder.update({
				'workOrderId': workOrderId
			}, {
				'partId': partId,
				'workOrderStatusId': status
			}, function(err, wo) {
				if (!err) {
					WorkOrder_Part_SubPart_PartInventory.update({
						'workOrderId': workOrderId
					}, {
						'partId': partId
					}, function(err, wpsps) {
						if (!err) {
							return cb(null, wo);
						} else {
							err.updatedMessage = 'WorkOrder was updated but not the WPSPs!';
							return cb(err, null);
						}
					});
				} else {
					return cb(err, null);
				}
			});
		} else {
			return cb({
				'message': 'No workOrderId was provided'
			}, null);
		}
	},
	savePartSubParts: function(partSubParts, cb) {
		if (partSubParts && partSubParts.length > 0) {
			Part_SubPart.create(partSubParts, function(err, created) {
				return cb(null, created);
			});
		} else {
			return cb({
				'message': 'No records found.'
			}, null);
		}
	},
	savePartAndReturnPartId: function(part, cb) {
		Part.create(part, function(err, part) {
			if (!err && part) {
				return cb(null, part.partId);
			} else {
				return cb(err, null);
			}
		});
	},
	getAllPartSubParts: function(cb) {
		SdpiContext.query('SELECT * FROM Part_SubPart', function(err, records) {
			return cb(err, records);
		});
	},
	getPartSubPartsByPno: function(pnoId, cb) {6
		SdpiContext.query('SELECT psp.*, p.partNumberedObjectId FROM SDPI.Part_SubPart psp LEFT JOIN SDPI.Part p on psp.partId = p.partId WHERE p.partNumberedObjectId =' + pnoId, function(err, records) {
			return cb(err, records);
		});
	},
	getPartByPartId: function(partId, cb) {
		SdpiContext.query('SELECT * FROM Part WHERE partId = ' + partId, function(err, records) {
			var part;
			if (records.length > 0) {
				part = records[0];
				return cb(err, part);
			} else {
				return cb(err, records);
			}
		});
	},
	getPartsAndPnoByPartIds: function(cb) {
		SdpiContext.query('CALL GetPartsAndPno()', function(err, records) {
			cb(err, records[0]);
		});
	},
	getNextPartNumberInfoForPno: function(pnoId, cb) {
		var pno3LetterAbbreviation;
		SdpiContext.query('SELECT * FROM PartNumberedObject WHERE partNumberedObjectId = ' + pnoId, function(err, pnos) {
			if (!err) {
				var pno = pnos[0];
				var pno3LetterAbbreviation = pno.threeCharAbbreviation;
				SdpiContext.query('SELECT * FROM Part Where PartNumberedObjectId = ' + pnoId, function(err, parts) {
					if (!err) {
						var lastElement = parts[parts.length - 1];
						var newChrono = lastElement.partNumberedObjectChrono + 1;
						var newPartNumber = pno3LetterAbbreviation + ' ' + newChrono;

						var partNumberedInfoObject = {
							'partNumberedObjectChrono': newChrono,
							'partNumber': newPartNumber
						};
						return cb(null, partNumberedInfoObject);
					} else {
						return cb(err, null);
					}
				});
			} else {
				return cb(err, pnos);
			}
		});
	},
	updateParentPartWithRefDescription: function(partId, refPartChanges, cb) {
		Part.update({
			'partId': partId
		}, {
			'refPartChanges': refPartChanges
		}, function(err, updated) {
			cb(err, updated);
		});
	},
	getAllProductsLines: function(cb) {},
	getParts: function(cb) {},
	getPartsForPno: function(pnoId, cb) {
		Part.query('SELECT p.*,bpst.steelTypeId, gst.steelTypeName, d.originalDocumentName FROM Part p LEFT JOIN BPSteelType bpst ON p.partId = bpst.partId LEFT JOIN GlobalValues.SteelType gst ON bpst.steelTypeId = gst.steelTypeID LEFT JOIN BPDrawingNumbers bpdn on p.partId = bpdn.partId LEFT JOIN Documents d on bpdn.documentId = d.documentId WHERE partNumberedObjectId = ' + pnoId + ' AND partStatusId != 99 ORDER BY refPartChanges', function(err, parts) {
			if (!err && parts) {
				return cb(err, parts);
			} else {
				return cb({
					'message': 'There was a problem getting parts.'
				}, null);
			}
		});
	},
	getBodyPartsWithCutters: function() {
		var deferred = Q.defer();
		SdpiContext.query('CALL GetPartsByPnoIds(?)', ["40,63,157"], function(err, results) {
			if (!err) deferred.resolve(results[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getPocketStructure: function(partId, toolFamilyId) {
		var deferred = Q.defer();
		if (toolFamilyId === 1) {
			SdpiContext.query('CALL GetPartCutterStructure(?)', [partId], function(err, results) {
				if (!err) {
					deferred.resolve(results[0]);
				} else {
					deferred.reject(err);
				}
			});
		} else if (toolFamilyId === 4) {
			NonSdpToolsContext.query('CALL GetBakerBitPartCutterStructure(?)', [partId], function(err, results) {
				if (!err) {
					deferred.resolve(results[0]);
				} else {
					deferred.reject(err);
				}
			});
		}

		return deferred.promise;
	},
	getCutterStructureRecordsWithEs6Promise: function(identifier, toolFamilyId) {
		return new Promise((resolve, reject) => {
			if ([1,2].indexOf(toolFamilyId) !== -1) {
				Sdpi_CutterStructure.getDatastore().sendNativeQuery('CALL GetCutterStructure($1)', [identifier], function(err, results) {
					if (!err) {
						resolve(results.rows[0]);
					} else {
						reject(err);
					}
				});
			} else if (toolFamilyId === 4) {
				NonSdpTools_Tools.getDatastore().sendNativeQuery('CALL GetBakerBitPartCutterStructure($1)', [identifier], function(err, results) {
					if (!err) {
						resolve(results.rows[0]);
					} else {
						reject(err);
					}
				});
			}
		})
	},
	getCutterStructureRecords: function(identifier, toolFamilyId) {
		var deferred = Q.defer();
		if ([1,2].indexOf(toolFamilyId) !== -1) {
			SDPI_BPCutterStructure.getDatastore().sendNativeQuery('CALL GetCutterStructure($1)', [identifier], function (err, results) {
				if (!err) {
					deferred.resolve(results.rows[0]);
				} else {
					deferred.reject(err);
				}
			});
		} else if (toolFamilyId === 4) {
			NonSdpToolsContext.query('CALL GetBakerBitPartCutterStructure(?)', [identifier], function(err, results) {
				if (!err) {
					deferred.resolve(results[0]);
				} else {
					deferred.reject(err);
				}
			});
		}

		return deferred.promise;
	},
	getPocketStructureForPart: function(partId, toolFamilyId) {
		var deferred = Q.defer();

		var promise = PartService.getPocketStructure(partId, toolFamilyId);
		promise.then(function(records) {
			var groups = [];
			if (records && records.length > 0) {
				var cs = records;
				groups = [{
					blades: [{
						order: 1,
						rows: [{
							order: 1,
							pockets: []
						}]
					}]
				}];
				_.each(cs, function(record) {
					record.type = record.key;
					PartService.findPlaceInCutterStructure(record, groups);
				});
			}
			console.log('groups');
			console.log(groups);
			deferred.resolve(groups);
		}).catch(function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	getPartStructureForNonBitPart: (identifier, toolFamilyId) => {
		return new Promise((resolve, reject) => {
			SDPI_BPCutterStructure.findOne({partId: identifier}).exec((err, bpRecord) => {
				if(!err) {
					// continue
					if(bpRecord) {
						PartService.getCutterStructureRecords(identifier, toolFamilyId)
							.then(function(structure) {
								var groups = [];
								if (structure && structure.length > 0) {
									groups = [{
										blades: [{
											order: 1,
											rows: [{
												order: 1,
												pockets: []
											}]
										}]
									}];
									_.each(structure, function(record) {
										record.type = record.key;
										PartService.findPlaceInCutterStructure(record, groups);
									});
								}
								resolve(groups);
							}).catch(function(err) {
								reject(err);
							})
					} else resolve([])
				} else reject(err)
			})
		})
	},
	getCutterStructure: function(identifier, toolFamilyId, hashId) {
		var deferred = Q.defer();

		let promises = []
		promises.push(PartService.getCutterStructureRecordsWithEs6Promise(identifier, toolFamilyId))
		if(hashId) promises.push(PartService.getHashStructureRecords(hashId))

		Promise.all(promises).then(function(data) {
			let bpPartStructure = data[0]
			let hashStructure = data[1]

			// if hash structure exists, merge hash and part structure
			if(hashStructure) {
				// merge the two
				for (let bp of bpPartStructure) {
					for (let hs of hashStructure) {
						if(bp.group === hs.group && bp.blade === hs.blade && bp.row === hs.row && bp.pocket === hs.pocket) {
							bp.hashId = hs.hashId
							bp.hashStructureId = hs.hashStructureId
							bp.cutterId = hs.cutterId
						}
					}
				}
			}

			let structure = bpPartStructure
			var groups = [];
			if (structure && structure.length > 0) {
				groups = [{
					blades: [{
						order: 1,
						rows: [{
							order: 1,
							pockets: []
						}]
					}]
				}];
				_.each(structure, function(record) {
					record.type = record.key;
					PartService.findPlaceInCutterStructure(record, groups);
				});
			}
			deferred.resolve(groups);
		}).catch(function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	getHashStructureRecords: hashId => {
		return new Promise((resolve, reject) => {
			Repair_Repair.getDatastore().sendNativeQuery('CALL GetHashStructureRecords($1)', [hashId], function (err, results) {
				if (!err) {
					resolve(results.rows[0]);
				} else {
					reject(err);
				}
			});
		})
	},
	getHashStructure: hashId => {
		return new Promise((resolve, reject) => {
			let promise = PartService.getHashStructureRecords(hashId)
			promise.then(records => {
				let groups = []
				if(records && records.length > 0) {
					groups = [{
						blades: [{
							order: 1,
							rows: [{
								order: 1,
								pockets: []
							}]
						}]
					}];
					for (let record of records) {
						record.type = record.key
						PartService.findPlaceInCutterStructure(record, groups)
					}
				}
				resolve(groups)
			}).catch(err => reject(err))
		})
	},
	ensureCorrectPocketOrder: function(row) {
		return _.sortBy(row.pockets, ['group', 'blade', 'row', 'pocket']);
	},
	findPlaceInCutterStructure: function(record, groups) {
		var gFound = false;
		for (var g = 0; g < groups.length; g++) {
			if (record.group === g + 1) {
				gFound = true;
				var bFound = false;
				for (var b = 0; b < groups[g].blades.length; b++) {
					if (record.blade === b + 1) {
						bFound = true;
						var rFound = false;
						for (var r = 0; r < groups[g].blades[b].rows.length; r++) {
							if (record.row === r + 1) {

								rFound = true;
								groups[g].blades[b].rows[r].pockets.push(record);
								groups[g].blades[b].rows[r].pockets = _.sortByOrder(groups[g].blades[b].rows[r].pockets, ['pocket'], ['asc']);



								//groups[g].blades[b].rows[r] = ensureCorrectPocketOrder(groups[g].blades[b].rows[r]);
							}
						}
						if (!rFound) {
							groups[g].blades[b].rows.push({
								order: groups[g].blades[b].rows.length + 1,
								pockets: []
							});
							PartService.findPlaceInCutterStructure(record, groups);
						}
					}
				}
				if (!bFound) {
					groups[g].blades.push({
						order: groups[g].blades.length + 1,
						rows: []
					});
					PartService.findPlaceInCutterStructure(record, groups);
				}
			}
		}
		if (!gFound) {
			groups.push({
				order: groups.length + 1,
				blades: []
			});
			PartService.findPlaceInCutterStructure(record, groups);
		}
	},
	assignImportantParamsToPockets: function(groups) {
		var deferred = Q.defer();
		var promise = CutterService.getCutterPockets();
		promise.then(function(cutterPockets) {
			var gC = 1;
			_.each(groups, function(g) {
				var bC = 1;
				_.each(g.blades, function(b) {
					var rC = 1;
					_.each(b.rows, function(r) {
						var pC = 1;
						_.each(r.pockets, function(p) {
							var cp = _.find(cutterPockets, function(c) {
								return p.cutterPocketSizeId === c.cutterPocketSizeId && p.cutterPocketTypeId === c.cutterPocketTypeId;
							});
							p.cutterPocketId = cp.cutterPocketId;
							p.group = gC;
							p.blade = bC;
							p.row = rC;
							p.pocket = pC;
							pC++;
						});
						rC++;
					});
					bC++;
				});
				gC++;
			});
			deferred.resolve(groups);
		}).catch(function(err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},

	/**
	 * @description: removes any dullgrades that do not have a cutter pocket associated with it in the part structure
	 * @param identifier: typically either partId or partBakerBitId
	 * @param dullgradeId
	 * @param toolFamilyId: indicated what tool family this is from (DNR, Baker Bit, etc)
	 */
	alignDullGradesWithMarkup: function(identifier, dullGradeId, toolFamilyId) {
		let deferred = Q.defer();
		if(toolFamilyId === 4) {
			// Baker Bit
			var promises = [];
			// get dullgrades
			promises.push(RepairService.getDullGradesForRepairByDullGrade(dullGradeId));
			// get part structure
			promises.push(PartService.getCutterStructureRecords(identifier, toolFamilyId));

			Q.all(promises).then(data => {
				// continue
				var cutterDullGrades = data[0];
				var markup = data[1];

				removeDgPromises = [];
				console.log('===== cutterDullGrades ==========');
				console.log(cutterDullGrades);
				console.log('===== markup size ==========');
				console.log(markup.length);
				cutterDullGrades.map(dg => {
					var markupRecordThatMatchesDg = _.find(markup, m => {
						return m.group === dg.group && m.blade === dg.blade && m.row === dg.row && m.pocket === dg.pocket && m.cutterPocketId === dg.cutterPocketId;
					});
					if(!markupRecordThatMatchesDg) {
						console.log('===== dg without match ==========');
						console.log(dg);
						removeDgPromises.push(RepairService.removeDullGrade(dg));
					}
				});

				Q.all(removeDgPromises).then(data => {
					deferred.resolve(data);
				}, err => {
					deferred.reject(err);
				})
			}, err => {
				deferred.reject(err);
			});


		} else {
			// TODO
		}

		return deferred.promise;
	},
	savePocketStructureForPart: function(selectedTool, groups) {
		var deferred = Q.defer();
		var pocketsToSave = [];
		var p = PartService.assignImportantParamsToPockets(groups);
		p.then(function(groups) {
			if (selectedTool.toolFamilyId === 1) {
				var convertToHashPromise = PartService.convertCutterStructureToHashValue(groups);
				convertToHashPromise.then(function(hash) {
					var getCutterStructurePromise = PartService.getExistingOrSaveNewCutterStructure(selectedTool, hash, groups);
					getCutterStructurePromise.then(function(cutterStructure) {
						deferred.resolve(cutterStructure);
					}).catch(function(err) {
						deferred.reject(err);
					});
				}).catch(function(err) {
					deferred.reject(err);
				});
			} else if (selectedTool.toolFamilyId === 4) {
				NonSdpTools_PartBakerBit_CutterStructure.destroy({
					partBakerBitId: selectedTool.partId
				}).exec(function(err) {
					if (!err) {
						var getCutterPocketsPromise = CutterService.getCutterPockets();
						getCutterPocketsPromise.then(function(cutterPockets) {
							// Save new pockets for this part
							for (var g = 0; g < groups.length; g++) {
								for (var b = 0; b < groups[g].blades.length; b++) {
									for (var r = 0; r < groups[g].blades[b].rows.length; r++) {
										for (var p = 0; p < groups[g].blades[b].rows[r].pockets.length; p++) {
											var pock = groups[g].blades[b].rows[r].pockets[p];
											var cutterPocket = _.find(cutterPockets, function(cp) {
												return pock.cutterPocketTypeId === cp.cutterPocketTypeId && pock.cutterPocketSizeId === cp.cutterPocketSizeId;
											});
											var pocket = {
												partBakerBitId: selectedTool.partId,
												group: g + 1,
												blade: b + 1,
												row: r + 1,
												pocket: p + 1,
												cutterPocketId: cutterPocket.cutterPocketId,
												cutterId: pock.cutterId
											};
											pocketsToSave.push(pocket);
										}
									}
								}
							}
							NonSdpTools_PartBakerBit_CutterStructure.create(pocketsToSave).exec(function(err, records) {
								if (!err) deferred.resolve(records);
								else deferred.reject(err);
							});
						}).catch(function(err) {
							deferred.reject(err);
						});
					} else deferred.reject(err);
				});
			}
		}).catch(function(err) {
			deferred.reject(err);
		});

		return deferred.promise;
	},
	getBodyPartId: function(parentPartId) {
		var deferred = Q.defer();
		SdpiContext.query('CALL GetSubPartBody(?)', [parentPartId], function(err, data) {
			if (!err) deferred.resolve(data[0][0]);
			else deferred.reject(err);
		});

		return deferred.promise;
	},
	getPartIdWithCutterStructure: function(parentPartId, toolFamilyId) {
		var deferred = Q.defer();
		if (parseInt(toolFamilyId) === 4) deferred.resolve(parentPartId);
		else {
			var promise = PartService.getBodyPartId(parentPartId);
			promise.then(function(data) {
				deferred.resolve(data.partId);
			}).catch(function(err) {
				deferred.reject(err);
			});
		}

		return deferred.promise;
	},
	convertPocketStructureToHashValue: (sortedStructure) => {
		// create hash value
		var bStr = '',
			rStr = '',
			pStr = '',
			cStr = '';
		for (let g of sortedStructure) {
			let bL = g.blades.length;
			bStr += bL < 10 ? bL : (bL < 100 ? '@' + bL : (bL < 1000 ? '#' + bL : '$' + bL));
			for (let b of g.blades) {
				let rL = b.rows.length;
				rStr += rL < 10 ? rL : (rL < 100 ? '@' + rL : (rL < 1000 ? '#' + rL : '$' + rL));
				for (let r of b.rows) {
					let pL = r.pockets.length;
					pStr += pL < 10 ? pL : (pL < 100 ? '@' + pL : (pL < 1000 ? '#' + pL : '$' + pL));
					for (let p of r.pockets) {
						cStr += p.cutterId < 10 ? p.cutterId : (p.cutterId < 100 ? '@' + p.cutterId : (p.cutterId < 1000 ? '#' + p.cutterId : '$' + p.cutterId));
					}
				}
			}
		}
		// return new hash value
		return bStr + ',' + rStr + ',' + pStr + ',' + cStr;
	},
	convertCutterStructureToHashValue: function(sortedStructure) {
		var deferred = Q.defer();

		var strArr = [];
		var bStr = '',
			rStr = '',
			pStr = '',
			cStr = '';
		_.each(sortedStructure, function(g) {
			var bL = g.blades.length;
			bStr += bL < 10 ? bL : (bL < 100 ? '@' + bL : (bL < 1000 ? '#' + bL : '$' + bL));
			_.each(g.blades, function(b) {
				var rL = b.rows.length;
				rStr += rL < 10 ? rL : (rL < 100 ? '@' + rL : (rL < 1000 ? '#' + rL : '$' + rL));
				_.each(b.rows, function(r) {
					var pL = r.pockets.length;
					pStr += pL < 10 ? pL : (pL < 100 ? '@' + pL : (pL < 1000 ? '#' + pL : '$' + pL));
					_.each(r.pockets, function(p) {
						cStr += p.cutterId < 10 ? p.cutterId : (p.cutterId < 100 ? '@' + p.cutterId : (p.cutterId < 1000 ? '#' + p.cutterId : '$' + p.cutterId));
					});
				});
			});
		});

		var hashStr = bStr + ',' + rStr + ',' + pStr + ',' + cStr;
		deferred.resolve(hashStr);

		return deferred.promise;
	},
	getExistingOrSaveNewCutterStructure: function(toolRepair, hash, groups) {
		var deferred = Q.defer();
		if (hash) {
			Repair_HashStructure.findOne({ hash: hash }).exec(function(err, record) {
				if (!err) {
					if (record) {
						Repair_CutterStructure.find({ cutterStructureChronoId: record.cutterStructureChronoId }).exec(function(err, structure) {
							if (!err) {
								Tools.update({ toolId: toolRepair.toolId }, { cutterStructureChronoId: record.cutterStructureChronoId }).exec(function(err, tool) {
									if (!err) deferred.resolve(structure);
									else deferred.reject(err);
								});
							} else deferred.reject(err);
						});
					} else {
						Repair_CutterStructureChrono.create({ hash: hash }).exec(function(err, record) {
							if (!err) {
								var cutterStructureRecords = [];
								_.each(groups, function(g) {
									_.each(g.blades, function(b) {
										_.each(b.rows, function(r) {
											_.each(r.pockets, function(p) {
												var structureRecord = {
													cutterStructureChronoId: record.cutterStructureChronoId,
													group: p.group,
													blade: p.blade,
													row: p.row,
													pocket: p.pocket,
													cutterPocketId: p.cutterPocketId,
													cutterId: p.cutterId
												};
												cutterStructureRecords.push(structureRecord);
											});
										});
									});
								});
								Repair_CutterStructure.create(cutterStructureRecords).exec(function(err, structure) {
									if (!err) {
										Tools.update({ toolId: toolRepair.toolId }, { cutterStructureChronoId: record.cutterStructureChronoId }).exec(function(err, tool) {
											if (!err) deferred.resolve(structure);
											else deferred.reject(err);
										});
									} else deferred.reject(err);
								});
							} else deferred.reject(err);
						});
					}
				} else deferred.reject(err);
			});
		}
		return deferred.promise;
	},
	getPartsWithAppendix: function(partNumber) {
		return new Promise((resolve, reject) => {
			NonSdpToolsContext.query('SELECT * FROM NonSdpTools.PartBakerBit WHERE partNumber LIKE "?-%"', [partNumber]).exec((err, data) => {
				if(!err) {
					resolve(data);
				} else {
					reject(err);
				}
			});
		});
	},
	getPartsByFamily: (toolFamilyId) => {
		return new Promise((resolve, reject) => {
			if(toolFamilyId === 4) {
				// baker bit parts
				NonSdpTools_PartBakerBit.find({}).exec((err, data) => {
					if(!err) resolve(data);
					else reject(err);
				});
			} else {
				// TODO: implement DNR parts

				// Part.find({partNumberedObjectId: [1,2,39,62]}).exec((err,data) => {
				// 	if(!err) resolve(data);
				// 	else reject(err);
				// });

				resolve([]);
			}
		});
	}
};
