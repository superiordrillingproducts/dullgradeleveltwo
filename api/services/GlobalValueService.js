var Q = require('q');

module.exports = {
    getStates: function (cb){
        GlobalValuesContext.query('SELECT * FROM GlobalValues.States', function(err,states){
            if(!err && states){
                cb(err, states)
            } else {
                return cb ({
                    'message': 'There was a problem getting states.'
                }, null);
            }
        });
    },
    getCutterConditions: function() {
        var deferred = Q.defer();
        GlobalValues_CutterCondition.find({}).exec(function(err, data) {
            if(!err) deferred.resolve(data);
            else deferred.reject(err);
        });
        return deferred.promise;
    },
    getCutterActions: function() {
        var deferred = Q.defer();
        GlobalValues_CutterAction.find({}).exec(function(err, data) {
            if(!err) deferred.resolve(data);
            else deferred.reject(err);
        });
        return deferred.promise;
    },
    getConditionActionCombos: function() {
        var deferred = Q.defer();
        GlobalValues_ConditionActionHotKey.find({}).exec(function(err, data) {
            if(!err) deferred.resolve(data);
            else deferred.reject(err);
        });
        return deferred.promise;
    },
    getToolFamilies: function() {
        var deferred = Q.defer();
        GlobalValues_ToolFamily.find({}).exec(function(err, data) {
            if(!err) deferred.resolve(data);
            else deferred.reject(err);
        });
        return deferred.promise;
    }
};
