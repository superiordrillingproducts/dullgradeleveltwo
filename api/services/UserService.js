//var https = require('https');
//var querystring = require('querystring');

var request = require('request-promise');
var Q = require('q');

module.exports = {
	getUser: function (userId) {
		var deferred = Q.defer();
		Users.find({
			userId: userId
		}).exec(function (err, data) {
			if (!err) deferred.resolve(data);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	authenticateUserAndReturnUser: function (username, password, cb) {
		var credentials = {
			Username: username,
			Password: password
		};
		var credentialsStr = JSON.stringify(credentials);

		var options = {
			//uri: 'https://brain.superiordrillingproducts.net/GlobalUsers/AuthenticateUserAndReturnUser',
			uri: Global.brainBaseUrl + 'GlobalUsers/AuthenticateUserAndReturnUser',
			method: 'POST',
			body: credentials,
			json: true
		};

		request(options).then(function (res) {
			if (res) {
				// get list of groups that user is apart of
				UserService.getUserGroups(res.userId, function (err, groups) {
					if (!err && groups.length > 0) {
						res.groups = groups;
					}
					return cb(null, res);
				});
			} else {
				return cb('Invalid credentials', null);
			}
		}).catch(function (err) {
			return cb(err, null);
		});
	},
	getUserGroups: function (userId, cb) {
		UserGroupJunction.find({
			userId: userId
		}).exec(function (err, ugjList) {
			return cb(err, ugjList);
		});
	},
	isTokenAuthentic: function (token, cb) {
		var options = {
			//uri: 'https://brain.superiordrillingproducts.net/GlobalUsers/AuthenticateUserAndReturnUser',
			uri: Global.brainBaseUrl + 'GlobalUsers/IsTokenValid',
			method: 'POST',
			body: {
				'token': token
			},
			json: true
		};
		request(options).then(function (res) {
			if (res) {
				return cb(null, res);
			} else {
				return cb('Token not authenticated.', null);
			}
		}).catch(function (err) {
			return cb(err, null);
		});
	},
	changePassword: function (userId, token, newPassword, cb) {
		var options = {
			//uri: 'https://brain.superiordrillingproducts.net/GlobalUsers/AuthenticateUserAndReturnUser',
			uri: Global.brainBaseUrl + 'GlobalUsers/InsertNewPasswordReturnUser',
			method: 'POST',
			body: {
				'userId': userId,
				'token': token,
				'password': newPassword
			},
			json: true
		};
		request(options).then(function (res) {
			if (res) {
				return cb(null, res);
			} else {
				return cb('Error', null);
			}
		}).catch(function (err) {
			return cb(err, null);
		});
	},
	isPartOfGroups: function (groupIds) {
	},
	getUsersForWorkStation: function (workStationId) {
		var deferred = Q.defer();
		UsersContext.query('CALL GetUsersForWorkStation(?)', [workStationId], function (err, data) {
			if (!err) deferred.resolve(data[0]);
			else deferred.reject(err);
		});
		return deferred.promise;
	},
	getUsersFromLaborTimeStamps: function (laborTimeStamps) {
		var deferred = Q.defer();
		var promises = [];
		var userIds = [];
		_.each(laborTimeStamps, function (lts) {
			var userIdExists = _.find(userIds, function (uid) {
				return lts.userId === uid;
			});
			if (!userIdExists) {
				userIds.push(lts.userId);
				promises.push(UserService.getUser(lts.userId));
			}
		});

		Q.all(promises).then(function (data) {
			data = Global.convert2dArrayTo1dArray(data);
			deferred.resolve(data);
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	getFacilityManagers: function (facilityId) {
		var deferred = Q.defer();
		var p1 = RepairService.getFacilities();
		p1.then(function (facilities) {
			var groupIds = [];
			_.each(facilities, function (f) {
				if (facilityId) {
					if (parseInt(facilityId) === parseInt(f.facilityId)) {
						groupIds.push(f.groupIdForManagers);
					}
				} else {
					groupIds.push(f.groupIdForManagers);
				}
			});
			var p2 = UserService.getUsersFromGroups(groupIds);
			p2.then(function (users) {
				var p3 = [];
				_.each(users, function (u) {
					p3.push(UserService.getUser(u.userId));
				});
				Q.all(p3).then(function (manager) {
					deferred.resolve(manager);
				}, function (err) {
					deferred.reject(err);
				});
			}).catch(function (err) {
				deferred.reject(err);
			});
		}).catch(function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	},
	getUsersFromGroups: function (groupIds) {
		var deferred = Q.defer();
		UserGroupJunction.find({
			groupId: groupIds
		}).exec(function (err, users) {
			if (!err) deferred.resolve(users);
			else deferred.reject(err);
		});
		return deferred.promise;
	}
};
