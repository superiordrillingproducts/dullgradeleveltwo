var Q = require('q');

module.exports = function isAuthenticated(req, res, next) {
	if (req.session && req.session.user) {

		UserService.isTokenAuthentic(req.session.user.token, function (err, isValid) {

			if (!err && isValid) {
				if (req.session.facility && req.session.workStation) {
					next();
				} else {
					return res.redirect('/user/setfacilityandworkstation');
				}
			} else {
				req.session.message = 'Invalid login.';
				req.session.messageType = 'danger';
				// since some frameworks like Angular remove xhr from the request, a second condition option is used to detect ajax request.
				if (req.xhr || req.headers.accept.indexOf('json') > -1) {
					// send your xhr response here
					return res.status(500).send({
						'message': 'YOU ARE LOGGED OUT! The session has expired and you have been logged out. Please log back in to proceed.',
						'sdpiUserSessionExpired': true
					});
				} else {
					// send your normal response here
					req.session.redirectUrl = req.url;
					return res.redirect('/user/login');
				}
			}
		});
	} else {
		req.session.message = 'Invalid login.';
		req.session.messageType = 'danger';

		sails.user = null;
		// since some frameworks like Angular remove xhr from the request, a second condition option is used to detect ajax request.
		if (req.xhr || req.headers.accept.indexOf('json') > -1) {
			// send your xhr response here
			return res.status(500).send({
				'message': 'YOU ARE LOGGED OUT! The session has expired and you have been logged out. Please log back in to proceed.',
				'sdpiUserSessionExpired': true
			});
		} else {
			// send your normal response here
			if(req.url === '/'){
				req.session.redirectUrl = req.url + 'repair';
			} else {
				req.session.redirectUrl = req.url;
			}
			return res.redirect('/user/login');
		}
	}
};
