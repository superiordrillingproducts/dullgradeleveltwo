module.exports = {
    'datastore': 'globalvalues',
    tableName: 'ToolFamily',
    primaryKey: 'toolFamilyId',
    schema: false,
    attributes: {
        toolFamilyId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		name: 'string',
		shortName: 'string',
        tableForAncillaryInformationOfRepairRecord: 'string',
        tableOfToolRecords: 'string',
        tableForPartInfomation: 'string'
    }
};
