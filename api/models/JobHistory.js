module.exports = {
    'datastore': 'manu',
    tableName: 'JobHistory',
    primaryKey: 'jobHistoryId',
    schema: false,
    attributes: {
        jobHistoryId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		jobId: 'number',
        date: 'ref',
        employeeId: 'number',
        jobHistoryStatusId: 'number',
        dataChangedTo: 'string'
    }
};