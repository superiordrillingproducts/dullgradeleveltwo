module.exports = {
    'datastore': 'sdpi',
    tableName: 'Tool_Document',
    primaryKey: 'toolDocumentId',
    schema: false,
    attributes: {
        toolDocumentId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        toolId: 'number',
        documentId: 'number'
    }
};
