module.exports = {
	'datastore': 'sdptools',
	tableName: 'Tools',
	primaryKey: 'toolId',
	schema: false,
	attributes: {
		toolId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		serial: 'string',
		partNumberedObjectId: 'number',
		partId: 'number',
		toolStatusId: 'number',
		stockPointId: 'number',
		hashId: 'number'
	}
};
