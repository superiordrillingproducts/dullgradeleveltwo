module.exports = {
	'datastore': 'sdptools',
	primaryKey: 'toolId',
	schema: false,
	attributes: {
		toolId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		serial: 'string',
		partNumberedObjectId: 'number',
		partId: 'number',
		cutterStructureChronoId: 'number',
		toolStatusId: 'number',
		stockPointId: 'number'
	}
};