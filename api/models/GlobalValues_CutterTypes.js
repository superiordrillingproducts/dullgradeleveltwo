module.exports = {
    'datastore': 'globalvalues',
    tableName: 'CutterTypes',
    primaryKey: 'cutterTypeId',
    schema: false,
    attributes: {
        cutterTypeId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		cutterTypeName: 'string'
    }
};