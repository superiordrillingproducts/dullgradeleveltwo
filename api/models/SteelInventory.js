module.exports = {
    'datastore': 'manu',
    tableName: 'SteelInventory',
    primaryKey: 'steelInventoryId',
    schema: false,
    attributes: {
        steelInventoryId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        }
    }
};