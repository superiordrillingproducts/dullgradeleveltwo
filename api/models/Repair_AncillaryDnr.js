module.exports = {
	'datastore': 'repair',
	tableName: 'AncillaryDnr',
	primaryKey: 'ancillaryDnrId',
	schema: false,
	attributes: {
		ancillaryDnrId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		repairId: 'number',
		thirdPartyInspectorCompany: {
			type: 'ref',
			defaultsTo: null
		},
		thirdPartyInspectionNumber: {
			type: 'ref',
			defaultsTo: null
		},
		thirdPartyInspectorCompanyTwo: {
			type: 'ref',
			defaultsTo: null
		},
		thirdPartyInspectionNumberTwo: {
			type: 'ref',
			defaultsTo: null
		},
		thirdPartyInspectorsName: {
			type: 'ref',
			defaultsTo: null
		},
		thirdPartyInspectorsAcceptance: {
			type: 'number'
		},
		paintOnly: 'number',
		length1: {
			type: 'ref',
			defaultsTo: null
		},
		length2: {
			type: 'ref',
			defaultsTo: null
		},
		length3: {
			type: 'ref',
			defaultsTo: null
		},
		length4: {
			type: 'ref',
			defaultsTo: null
		},
		length5: {
			type: 'ref',
			defaultsTo: null
		},
		lengthTotal: {
			type: 'ref',
			defaultsTo: null
		},
		topOdFractionalInches: {
			type: 'ref',
			defaultsTo: null
		},
		midOdFractionalInches: {
			type: 'ref',
			defaultsTo: null
		},
		bottomOdFractionalInches: {
			type: 'ref',
			defaultsTo: null
		},
		totalDamagedCutters: {
			type: 'ref',
			defaultsTo: null
		}, 
		totalDamagedDomes: {
			type: 'ref',
			defaultsTo: null
		},
		totalMissingCutters: {
			type: 'ref',
			defaultsTo: null
		},
		totalMissingDomes: {
			type: 'ref',
			defaultsTo: null
		},
		unusualDamage: {
			type: 'number',
		},
		doesBodyHaveCracks: {
			type: 'number',
		},
		doesBoxConnectionNeedRecut: {
			type: 'number',
		},
		doesBoxHaveBoreBack: {
			type: 'number',
		},
		doesPinConnectionNeedRecut: {
			type: 'number',
		},
		doesPinHaveReliefGroove: {
			type: 'number',
		}
	}
};
