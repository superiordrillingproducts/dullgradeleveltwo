module.exports = {
	'datastore': 'users',
	tableName:'Users',
	primaryKey: 'userId',
	schema: false,
	attributes: {
		userId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		nameFirst: 'string',
		nameMiddle: {
			type: 'string',
			allowNull: true
		},
		nameLast: 'string',
		phoneCell: {
			type: 'string',
			allowNull: true
		},
		phoneHome: {
			type: 'string',
			allowNull: true
		},
		phoneWork: {
			type: 'string',
			allowNull: true
		},
		password: 'string',
		token: 'string',
		email: 'string',
		groupId: {
			type: 'number',
			allowNull: true
		},
		controllerpwd: {
			type: 'string',
			allowNull: true
		},
		Sha256Pwd: {
			type: 'string',
			allowNull: true,
		},
		assemblyAndRepairPwd: {
			type: 'string',
			allowNull: true,
		},
		userAvatarUrl: {
			type: 'string',
			allowNull: true,
		},
		rate: {
			type: 'number',
			allowNull: true,
		}
	}
};