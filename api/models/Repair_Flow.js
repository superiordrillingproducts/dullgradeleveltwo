module.exports = {
    'datastore': 'repair',
    tableName: 'Flow',
    primaryKey: 'flowId',
    schema: false,
    attributes: {
        flowId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        familyId: 'number',
        facilityId: 'number',
        chrono: 'number',
        workStationId: 'number',
        flowStatusId: 'number',
        name: 'string',
        description: 'string',
        flowGroupId: 'number'
    }
};
