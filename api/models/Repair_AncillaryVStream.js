module.exports = {
	'datastore': 'repair',
	tableName: 'AncillaryVStream',
	primaryKey: 'ancillaryVStreamId',
	schema: false,
	attributes: {
		ancillaryVStreamId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		repairId: 'number',
		thirdPartyInspectorCompany: 'string',
		thirdPartyInspectionNumber: 'string',
		thirdPartyInspectorCompanyTwo: 'string',
		thirdPartyInspectionNumberTwo: 'string',
		paintOnly: 'number',
		length1: 'string',
		length2: 'string',
		length3: 'string',
		lengthTotal: 'string',
		topOdFractionalInches: 'string',
		midOdFractionalInches: 'string',
		bottomOdFractionalInches: 'string'

	}
};
