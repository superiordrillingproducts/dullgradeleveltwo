module.exports = {
    'datastore': 'nonsdptools',
    tableName: 'Tools',
    primaryKey: 'toolId',
    schema: false,
    attributes: {
        toolId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		serial: 'string',
		statusId: 'number',
		partId: 'number',
		toolFamilyId: 'number',
        repairCustomerId: 'number'
    }
};
