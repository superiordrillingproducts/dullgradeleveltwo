/**
 * LaborTimeStamps.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    'datastore': 'sdpi',
    tableName: 'LaborTimeStamps',
    primaryKey: 'laborTimeStampsId',
    schema: false,
    attributes: {
        laborTimeStampsId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        startTime: 'ref',
        stopTime: 'ref',
        totalElapsedTime: 'string',
        laborTimeStampTypeId: 'number',
		didPrintDrawing: {type: 'boolean', defaultsTo: false}
    }
};