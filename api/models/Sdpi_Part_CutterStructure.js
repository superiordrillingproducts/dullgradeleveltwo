module.exports = {
    'datastore': 'sdpi',
    tableName: 'Part_CutterStructure',
    primaryKey: 'part_CutterStructureId',
    schema: false,
    attributes: {
        part_CutterStructureId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		partId: 'number',
		group: 'number',
		blade: 'number',
		pocket: 'number',
		row: 'number',
		cutterPocketId: 'number',
        cutterId: 'number'
    }
};