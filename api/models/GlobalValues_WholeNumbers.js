module.exports = {
    'datastore': 'globalvalues',
    tableName: 'wholeNumbers',
    primaryKey: 'wholeNumberId',
    schema: false,
    attributes: {
        wholeNumberId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		Number: 'number'
    }
};
