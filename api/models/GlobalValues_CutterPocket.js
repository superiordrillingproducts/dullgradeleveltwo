module.exports = {
    'datastore': 'globalvalues',
    tableName: 'CutterPocket',
    primaryKey: 'cutterPocketId',
    schema: false,
    attributes: {
        cutterPocketId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		cutterPocketTypeId: 'number',
		cutterPocketSizeId: 'number'
    }
};