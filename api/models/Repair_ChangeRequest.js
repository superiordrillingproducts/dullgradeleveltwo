module.exports = {
    'datastore': 'repair',
    tableName: 'ChangeRequest',
    primaryKey: 'changeRequestId',
    schema: false,
    attributes: {
        changeRequestId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		requestTypeId: 'number',
		number: 'string',
		request: 'string',
		statusId: 'number',
        dateRequested: 'ref',
        whoRequestedId: 'number',
        requestOneTime: 'number'
    }
};
