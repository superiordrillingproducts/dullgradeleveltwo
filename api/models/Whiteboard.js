module.exports = {
    'datastore': 'manu',
    tableName: 'Whiteboard',
    primaryKey: 'whiteboardId',
    schema: false,
    attributes: {
        whiteboardId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        partId: 'number',
        jobId: 'number',
        dateCreated: 'ref',
        comments : 'string'
    }
};