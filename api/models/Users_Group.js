module.exports = {
    'datastore': 'users',
    primaryKey: 'groupId',
    schema: false,
    attributes: {
        groupId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        groupName: 'ref'
    }
};