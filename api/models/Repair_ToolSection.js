module.exports = {
    'datastore': 'repair',
    tableName: 'ToolSection',
    primaryKey: 'toolSectionId',
    schema: false,
    attributes: {
        toolSectionId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        toolSection: 'string'
    }
};
