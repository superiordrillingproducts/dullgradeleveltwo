module.exports = {
    'datastore': 'sdpi',
    tableName: 'DnrRepair_Documents',
    primaryKey: 'dnrRepairDocumentsId',
    schema: false,
    attributes: {
        dnrRepairDocumentsId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		dnrRepairId: 'number',
		documentId: 'number'
    }
};