module.exports = {
    'datastore': 'globalvalues',
    tableName: 'ConditionActionHotKey',
    primaryKey: 'conditionActionHotKeyId',
    schema: false,
    attributes: {
        conditionActionHotKeyId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		cutterConditionId: 'number',
		cutterActionId: 'number',
        keyCodeValue: 'string'
    }
};