module.exports = {
    'datastore': 'globalvalues',
    tableName: 'CutterAction',
    primaryKey: 'cutterActionId',
    schema: false,
    attributes: {
        cutterActionId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		cutterAction: 'string',
		cutterActionCode: 'string',
        cutterActionIcon: 'string',
        keyCodeValue: 'string'
    }
};