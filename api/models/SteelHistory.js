module.exports = {
    'datastore': 'manu',
    tableName: 'SteelHistory',
    primaryKey: 'steelHistoryId',
    schema: false,
    attributes: {
        steelHistoryId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        }
    }
};