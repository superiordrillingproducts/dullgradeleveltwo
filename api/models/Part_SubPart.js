/**
 * Part_SubPart.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	'datastore': 'sdpi',
	tableName: 'Part_SubPart',
	primaryKey: 'Part_SubPartId',
	schema: false,
	attributes: {
		Part_SubPartId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		}
	}
};