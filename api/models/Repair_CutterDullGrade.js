module.exports = {
	'datastore': 'repair',
	tableName: 'CutterDullGrade',
	primaryKey: 'cutterDullGradeId',
	schema: false,
	attributes: {
		cutterDullGradeId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		group: 'number',
		blade: 'number',
		row: 'number',
		pocket: 'number',
		cutterPocketId: 'number',
		cutterId: 'number',
		cutterConditionId: {
			type:'number',
			allowNull: true
		},
		cutterActionId: {
			type: 'number',
			allowNull: true
		},
		dullGradeId: 'number',
		isFlameSpray: 'boolean',
		replaceWithCutterId: {
			type: 'number',
			allowNull: true
		},
		isReclaim: 'boolean',
		lotNumber: {
			type: 'string',
			allowNull: true
		},
		isAddOnRecord: 'boolean',
		reasonForChange: {
			type:'string',
			allowNull: true
		}
	}
};