module.exports = {
    'datastore': 'manu',
    tableName: 'Job_ThirdPartyAdditional',
    primaryKey: 'job_ThirdPartyAdditionalId',
    schema: false,
    attributes: {
        job_ThirdPartyAdditionalId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		jobId: 'number',
        thirdPartCustomerId: 'number',
        originalDocumentName: 'string',
        documentId: 'number'
    }
};