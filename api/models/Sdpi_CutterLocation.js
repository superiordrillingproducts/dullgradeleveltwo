module.exports = {
    'datastore': 'sdpi',
    tableName: 'CutterLocation',
    primaryKey: 'cutterLocationId',
    schema: false,
    attributes: {
        cutterLocationId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		bladeGroup: 'number',
		blade: 'number',
		cutter: 'number',
		Z: 'number',
		R: 'number',
		Q: 'number',
		cutterId: 'number',
		toolId: 'number'
    }
};