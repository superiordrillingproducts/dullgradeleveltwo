module.exports = {
    'datastore': 'repair',
    tableName: 'Facility',
    primaryKey: 'facilityId',
    schema: false,
    attributes: {
        facilityId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        facilityName: 'string',
        groupIdForManagers: 'number',
        programId: 'number'
    }
};