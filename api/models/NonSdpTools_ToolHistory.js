module.exports = {
    'datastore': 'nonsdptools',
    tableName: 'ToolHistory',
    primaryKey: 'toolHistoryId',
    schema: false,
    attributes: {
        toolHistoryId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        toolId: 'number',
        previousValue: 'string',
        updatedValue: 'string',
        fieldAffected: 'string',
        userId: 'number',
        date: 'ref'
    }
};