module.exports = {
    'datastore': 'repair',
    tableName: 'RepairCustomer',
    primaryKey: 'repairCustomerId',
    schema: false,
    attributes: {
        repairCustomerId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        customerName: 'string'
    }
};
