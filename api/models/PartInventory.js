/**
 * PartInventory.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	'datastore': 'sdpi',
	tableName: 'PartInventory',
	primaryKey: 'partInventoryId',
	schema: false,
	attributes: {
		partInventoryId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		partId: 'number',
		serial: 'string',
		partInventoryStatus: 'number',
		quantityOriginal: 'number',
		quantityAvailable: 'number',
		quantityScrapped: 'number',
		inheritedCostPerPart: 'number',
		inventoryCheck: 'string'
	}
};