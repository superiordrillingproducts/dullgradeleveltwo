module.exports = {
    'datastore': 'globalvalues',
    tableName: 'CutterPocketType',
    primaryKey: 'cutterPocketTypeId',
    schema: false,
    attributes: {
        cutterPocketTypeId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		name: 'string',
		key: 'string'
    }
};