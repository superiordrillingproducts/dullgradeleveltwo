module.exports = {
	'datastore': 'sdpi',
	tableName: 'CutterStructure',
	primaryKey: 'primaryKeyId',
	schema: false,
	attributes: {
		primaryKeyId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		cutterStructureId: 'number',
		group: 'number',
		blade: 'number',
		row: 'number',
		pocket: 'number',
		cutterPocketId: 'number'
	}
};
