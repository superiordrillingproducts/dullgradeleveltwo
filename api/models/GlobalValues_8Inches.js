module.exports = {
    'datastore': 'globalvalues',
    tableName: '8Inches',
    primaryKey: '8inchesId',
    schema: false,
    attributes: {
        '8inchesId': {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		'8fraction': 'number',
		'8decimal': 'number'
    }
};
