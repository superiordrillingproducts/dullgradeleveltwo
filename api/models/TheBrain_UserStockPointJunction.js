module.exports = {
    'datastore': 'thebrain',
    tableName: 'UserStockPointJunction',
    primaryKey: 'junctionId',
    schema: false,
    attributes: {
        junctionId: {
            type: 'number',
            required: true,
            unique: true
        },
        userId: 'number',
        stockPointId: 'number'
    }
};