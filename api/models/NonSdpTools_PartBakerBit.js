module.exports = {
    'datastore': 'nonsdptools',
    tableName: 'PartBakerBit',
    primaryKey: 'partBakerBitId',
    schema: false,
    attributes: {
        partBakerBitId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        partNumber: 'string',
        sizeFraction: 'string',
        sizeDecimal: 'number',
        style: 'string'
    }
};
