module.exports = {
    'datastore': 'sdpi',
    tableName: 'InternalSerialGenerator',
    primaryKey: 'internalSerialGeneratorId',
    schema: false,
    attributes: {
        internalSerialGeneratorId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        }
    }
};