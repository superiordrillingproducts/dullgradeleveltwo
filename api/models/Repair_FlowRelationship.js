module.exports = {
    'datastore': 'repair',
    tableName: 'FlowRelationship',
    primaryKey: 'flowRelationshipId',
    schema: false,
    attributes: {
        flowRelationshipId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        currentFlowId: 'number',
        nextFlowId: 'number',
        utilizesProgrammingChrono: 'number',
        programmingChrono: 'number'
    }
};
