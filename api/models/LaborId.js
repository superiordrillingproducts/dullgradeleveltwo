module.exports = {
    'datastore': 'sdpi',
    tableName: 'LaborId',
    primaryKey: 'laborId',
    schema: false,
    attributes: {
        laborId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        costCenterId: 'number',
    }
};