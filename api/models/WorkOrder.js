/**
 * WorkOrder.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	'datastore': 'sdpi',
	tableName: 'WorkOrder',
	primaryKey: 'workOrderId',
	schema: false,
	attributes: {
		workOrderId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		}
	}
};