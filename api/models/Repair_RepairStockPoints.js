module.exports = {
    'datastore': 'repair',
    tableName: 'RepairStockPoint',
    primaryKey: 'repairStockPointId',
    schema: false,
    attributes: {
        repairStockPointId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        city: 'string',
        stateId: 'number',
        address: 'string',
        zipCode: 'number'
    }
};
