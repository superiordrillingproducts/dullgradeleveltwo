module.exports = {
	'datastore': 'repair',
	tableName: 'Hash',
	primaryKey: 'hashId',
	schema: false,
	attributes: {
		hashId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		hash: 'string'
	}
};
