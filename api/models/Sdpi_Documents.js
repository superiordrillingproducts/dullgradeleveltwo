module.exports = {
    'datastore': 'sdpi',
    tableName: 'Documents',
    primaryKey: 'documentId',
    schema: false,
    attributes: {
        documentId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		documentTypeId: 'number',
		documentLocation: 'string',
		name: 'string',
		uploadedByUserId: 'number',
		dateUploaded: 'ref',
		originalDocumentName: 'string',
		fileExtension: 'string',
		encryptedName: 'string'
    }
};