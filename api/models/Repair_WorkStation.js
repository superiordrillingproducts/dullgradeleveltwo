module.exports = {
    'datastore': 'repair',
    tableName: 'WorkStation',
    primaryKey: 'workStationId',
    schema: false,
    attributes: {
        workStationId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        workStationName: 'string',
        facilityId: 'number',
        description: 'string',
        hourlyCost: 'number',
        url: 'string',
        chrono: 'number',
        status: 'number'
    }
};
