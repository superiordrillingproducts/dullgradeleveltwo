module.exports = {
    'datastore': 'globalvalues',
    tableName: 'ProgrammingVariable',
    primaryKey: 'progVarId',
    schema: false,
    attributes: {
        progVarId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		varName: 'string',
		varValue: 'string',
		unitType: 'string',
		description: 'string'
    }
};