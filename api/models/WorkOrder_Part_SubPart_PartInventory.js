/**
 * WorkOrder_Part_SubPart_PartInventory.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	'datastore': 'sdpi',
	tableName: 'WorkOrder_Part_SubPart_PartInventory',
	primaryKey: 'WorkOrder_Part_SubPart_PartInventoryId',
	schema: false,
	attributes: {
		WorkOrder_Part_SubPart_PartInventoryId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		workOrderId: 'number',
		partId: 'number',
		subPartId: 'number',
		subPartNumberedObjectId: 'number',
		partInventoryId: 'number',
		commentsAboutPart: 'string',
		numberOfPartsScrapped: 'number',
		partInventoryIdsOfScrappedParts: 'string',
		isACollectionOfThisWorkOrderId: 'boolean',
		isPartInventoryNew: 'boolean'
	}
};