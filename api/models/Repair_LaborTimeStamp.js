module.exports = {
    'datastore': 'repair',
    tableName: 'LaborTimeStamp',
    primaryKey: 'laborTimeStampId',
    schema: false,
    attributes: {
        laborTimeStampId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        repairId: 'number',
        workStationId: 'number',
        workStationRate: 'number',
        userId: 'number',
        userRate: 'number',
        totalRate: 'number',
        startTime: 'ref',
        stopTime: 'ref',
        totalElapsedTime: 'string',
        timeToMultiplyByTotalRate: 'string',
        totalValueOfRecord: 'number'
    }
};