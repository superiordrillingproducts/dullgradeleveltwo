module.exports = {
    'datastore': 'repair',
    tableName: 'InspectionIssue',
    primaryKey: 'inspectionIssueId',
    schema: false,
    attributes: {
        inspectionIssueId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        inspectionIssue: 'string'
    }
};
