module.exports = {
	'datastore': 'globalvalues',
	tableName: '32Inches',
	primaryKey: '32inchesId',
	schema: false,
	attributes: {
		'32inchesId': {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		'32fraction': 'number',
		'32decimal': 'number'
	}
};
