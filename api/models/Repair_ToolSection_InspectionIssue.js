module.exports = {
    'datastore': 'repair',
    tableName: 'ToolSection_InspectionIssue',
    schema: false,
    attributes: {
        id: {
            type: 'number',
            columnName: 'toolSectionId',
            required: true,
            unique: true,
        },
        inspectionIssueId: {
            model: 'Repair_InspectionIssue'
        }
    }
};
