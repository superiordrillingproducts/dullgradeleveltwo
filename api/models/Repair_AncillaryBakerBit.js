module.exports = {
    'datastore': 'repair',
    tableName: 'AncillaryBakerBit',
    primaryKey: 'ancillaryBakerBitId',
    schema: false,
    attributes: {
        ancillaryBakerBitId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        repairId: 'number',
        inspectionIssue: 'string',
        nozzleComments: 'string',
        gaugeOd: 'string',
        grade: 'string',
        visualAndDyePenComments: 'string',
        shippingNeeds: 'string',
        addPaintOnly: 'number',
        flameUpFortyFive: 'number',
        hardFaceHours: 'string',
        flamesprayHours: 'string',
        rechaseThreads: 'number',
        reshankNumber: 'string',
        orderNumber: 'string',
        orderPrice: 'number',
        totalTimesRepaired: 'number',
        lastUserId: 'number'
    }
};
