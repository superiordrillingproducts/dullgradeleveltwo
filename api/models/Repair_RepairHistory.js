module.exports = {
    'datastore': 'repair',
    tableName: 'RepaihHistory',
    primaryKey: 'repairHistoryId',
    schema: false,
    attributes: {
        repairHistoryId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        repairId: 'number',
        date: 'ref',
        userId: 'number',
        schema: 'string',
        table: 'string',
        field: 'string',
        tabelRecordId: 'number',
        previousValue: 'string',
        updatedValue: 'string',
        firstQueryFromSchema: 'string',
        firstQueryFromTable: 'string',
        firstQueryWhereField: 'string',
        firstQuerySelectField: 'string',
        secondQueryFromSchema: 'string',
        secondQueryFromTable: 'string',
        secondQueryWhereField: 'string',
        secondQuerySelectField: 'string',
    }
};
