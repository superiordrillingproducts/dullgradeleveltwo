module.exports = {
	'datastore': 'repair',
	tableName: 'HashStructure',
	primaryKey: 'hashStructureId',
	schema: false,
	attributes: {
		hashStructureId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		hashId: 'number',
		group: 'number',
		blade: 'number',
		row: 'number',
		pocket: 'number',
		cutterPocketId: 'number',
		cutterId: 'number'
	}
};
