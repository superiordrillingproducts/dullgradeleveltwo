module.exports = {
    'datastore': 'sdpi',
    tableName: 'DnrHistory',
    primaryKey: 'dnrHistoryId',    
    schema: false,
    attributes: {
        dnrHistoryId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		dnrRepairId: 'number',
		dnrHistoryTypeId: 'number',
		date: 'ref',
		userId: 'number',
        comments: 'string'
    }
};