module.exports = {
    'datastore': 'sdpi',
    tableName: 'CostCenters',
    primaryKey: 'costCenterId',
    schema: false,
    attributes: {
        costCenterId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        costCenterName: 'string',
        currentHourlyLaborRate: 'number'
    }
};