module.exports = {
    'datastore': 'repair',
    tableName: 'DullGrade',
    primaryKey: 'dullGradeId',
    schema: false,
    attributes: {
        dullGradeId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        repairId: 'number'
    }
};
