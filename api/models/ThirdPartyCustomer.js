module.exports = {
    'datastore': 'manu',
    tableName: 'ThirdPartyCustomer',
    primaryKey: 'thirdPartyCustomerId',
    schema: false,
    attributes: {
        thirdPartyCustomerId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		customerName: 'string'
    }
};