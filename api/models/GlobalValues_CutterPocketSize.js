module.exports = {
    'datastore': 'globalvalues',
    tableName: 'CutterPocketSize',
    primaryKey: 'cutterPocketSizeId',
    schema: false,
    attributes: {
        cutterPocketSizeId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		sizeFraction: 'string',
		sizeDecimal: 'number',
		sizeMillimeter: 'number',
        chrono: 'number'
    }
};