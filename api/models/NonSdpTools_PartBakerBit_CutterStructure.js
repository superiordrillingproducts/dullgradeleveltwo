module.exports = {
    'datastore': 'nonsdptools',
    tableName: 'PartBakerBit_CutterStructure',
    primaryKey: 'partBakerBit_CutterStructureId',
    schema: false,
    attributes: {
        partBakerBit_CutterStructureId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        partBakerBitId: 'number',
		group: 'number',
		blade: 'number',
		pocket: 'number',
		row: 'number',
		cutterPocketId: 'number',
        cutterId: 'number'
    }
};
