module.exports = {
	'datastore': 'sdptools',
	tableName: 'StockPoints',
	primaryKey: 'stockPointId',
	schema: false,
	attributes: {
		stockPointId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		stockPointTypeId: 'number',
		stockPointStatusId: 'number',
		stockCity: 'string',
		stockStateId: 'number',
		stockName: 'string',
		adminRepId: 'number'
	}
};
