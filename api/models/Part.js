
module.exports = {
	'datastore': 'sdpi',
	tableName: 'Part',
	primaryKey: 'partId',
	schema: false,
	attributes: {
		partId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		}
	}
};