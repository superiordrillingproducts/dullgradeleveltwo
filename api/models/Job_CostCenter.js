module.exports = {
    'datastore': 'manu',
    tableName: 'Job_CostCenter',
    primaryKey: 'job_CostCenterId',
    schema: false,
    attributes: {
        job_CostCenterId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		jobId: 'number',
        costCenterId: 'number'
    }
};