module.exports = {
	'datastore': 'sdpi',
	tableName: 'BPCutterStructure',
	primaryKey: 'bpCutterStructureId',
	schema: false,
	attributes: {
		bpCutterStructureId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		partId: 'number',
		cutterStructureId: 'number'
	}
};
