module.exports = {
	'datastore': 'repair',
	tableName: 'Repair_Documents',
	primaryKey: 'repairDocumentsId',
	schema: false,
	attributes: {
		repairDocumentsId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		repairId: 'number',
		documentId: 'number'
	}
};
