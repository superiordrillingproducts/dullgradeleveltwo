module.exports = {
    'datastore': 'manu',
    tableName: 'SteelLocations',
    primaryKey: 'steelLocationId',
    schema: false,
    attributes: {
        steelLocationId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        }
    }
};