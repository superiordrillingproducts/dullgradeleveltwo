module.exports = {
    'datastore': 'globalvalues',
    tableName: 'SteelType',
    primaryKey: 'steelTypeId',
    schema: false,
    attributes: {
        steelTypeId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
        steelTypeName: 'string',
        steelTypeDescription: 'string'
    }
};