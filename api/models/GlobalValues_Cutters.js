module.exports = {
    'datastore': 'globalvalues',
    tableName: 'Cutters',
    primaryKey: 'cutterId',
    schema: false,
    attributes: {
        cutterId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		cutterName: 'string',
        partNumber: 'string',
		cutterPocketId: 'number',
		manufacturer: 'string',
		sdpPartNumber: 'string',
		status: 'number',
        isObsolete: 'boolean'
    }
};