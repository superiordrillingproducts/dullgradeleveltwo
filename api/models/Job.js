module.exports = {
    'datastore': 'manu',
    tableName: 'Job',
    primaryKey: 'jobId',
    schema: false,
    attributes: {
        jobId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		jobNumber: 'string',
        jobTypeId: 'number',
		jobStatusId: 'number',
		partId: 'number',
		qtyToManufacture: 'number',
		dateCreated: 'ref',
        dateDue: 'ref',
		createdByEmpId: 'number',
		laborId: 'number',
		steelTypeId: 'number',
		totalJobCost: 'number',
		qtyDrawingPrinted: 'number'
    }
};