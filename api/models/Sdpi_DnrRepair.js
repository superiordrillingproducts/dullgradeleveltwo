module.exports = {
    'datastore': 'sdpi',
    tableName: 'DnrRepair',
    primaryKey: 'dnrRepairId',
    schema: false,
    attributes: {
        dnrRepairId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		statusId: 'number',
		toolId: 'number',
		repairTypeId: 'number',
		threadInspectionNumber: 'string',
        estimatedFinishDate: 'ref',
        dueDate: 'ref',
        completionDate: 'ref',
        dateRecordCreated: 'ref'
    }
};