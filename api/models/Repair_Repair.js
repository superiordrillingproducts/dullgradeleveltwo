module.exports = {
	'datastore': 'repair',
	tableName: 'Repair',
	primaryKey: 'repairId',
	attributes: {
		repairId: {
			type: 'number',
			unique: true,
			autoIncrement: true
		},
		toolId: 'number',
		toolFamilyId: 'number',
		flowId: 'number',
		repairStatusId: 'number',
		dateToolArrived: 'ref',
		partIdForThisRepair: 'number',
		isCutOut: 'number',
		cutOutComments: {
			type:'ref',
			defaultsTo: null
		},
		rush: 'number',
		rushComments: {
			type: 'ref',
			defaultsTo: null
		},
		stockPointReceivedFromId: 'number',
		stockPointShipToId: {
			type: 'ref',
			defaultsTo: null
		},
		dateToolShipped: 'ref',
		transitServiceShippedBy: {
			type: 'ref',
			defaultsTo: null
		},
		repairCustomerId: 'number',
		customerOwned: 'boolean',
		ist: {
			type: 'ref',
			defaultsTo: null
		},
		so: {
			type: 'ref',
			defaultsTo: null
		},
		stopWork: 'number',
		additionalShopWork: {
			type: 'ref',
			defaultsTo: null
		},
		interStationChrono: 'number',
		onHold: 'boolean',
		holdComments: {
			type: 'ref',
			defaultsTo: null
		},
		lastUserId: 'number',
		hashId: {
			type: 'ref',
			defaultsTo: null
		}
	}
}
