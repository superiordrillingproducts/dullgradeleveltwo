module.exports = {
	'datastore': 'users',
	tableName: 'UserGroupJunction',
	schema: false,
	attributes: {
		id: {
			type: 'number',
			columnName: 'userId',
			required: true,
			unique: true
		},
		groupId: {
			model: 'Users_Group'
		}
	}
};