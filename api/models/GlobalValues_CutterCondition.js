module.exports = {
    'datastore': 'globalvalues',
    tableName: 'CutterCondition',
    primaryKey: 'cutterConditionId',
    schema: false,
    attributes: {
        cutterConditionId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		cutterCondition: 'string',
		cutterConditionCode: 'string',
        keyCodeValue: 'string'
    }
};