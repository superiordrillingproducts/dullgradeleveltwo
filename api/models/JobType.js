module.exports = {
    'datastore': 'manu',
    tableName: 'JobType',
    primaryKey: 'jobTypeId',
    schema: false,
    attributes: {
        jobTypeId: {
            type: 'number',
            unique: true,
            autoIncrement: true
        },
		jobType: 'string' 
    }
};