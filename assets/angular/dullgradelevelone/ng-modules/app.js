var app = angular.module('app', ['ngRoute', 'angucomplete-alt', 'angular-loading-bar', 'timer', 'ngAnimate', 'ngSails', 'ngIdle', 'angular.filter', 'ngTouch'
]);

app.config(function ($routeProvider) {
	$routeProvider.when('/receiving', {
		controller: 'NgReceivingController',
		templateUrl: '/angular/dullgradelevelone/ng-views/receiving.html'
	}).when('/wash', {
		controller: 'NgWashController',
		templateUrl: '/angular/dullgradelevelone/ng-views/wash.html'
	}).when('/blast', {
		controller: 'NgBlastController',
		templateUrl: '/angular/dullgradelevelone/ng-views/blast.html'
	}).when('/dullgrade', {
		controller: 'NgDullgradeController',
		templateUrl: '/angular/dullgradelevelone/ng-views/dullgrade.html'
	}).when('/dullgrade-bit', {
		controller: 'NgDullgradeController',
		templateUrl: '/angular/dullgradelevelone/ng-views/dullgrade-bit.html'
	}).when('/dullgrade-3d', {
		controller: 'NgDullgradeController',
		templateUrl: '/angular/dullgradelevelone/ng-views/dullgrade-3d.html'
	}).when('/dullgrade-bit-3d', {
		controller: 'NgDullgradeController',
		templateUrl: '/angular/dullgradelevelone/ng-views/dullgrade-bit-3d.html'
	}).when('/flamespray', {
		controller: 'NgFlameSprayController',
		templateUrl: '/angular/dullgradelevelone/ng-views/flamespray.html'
	}).when('/flamespray-bit', {
		controller: 'NgFlameSprayController',
		templateUrl: '/angular/dullgradelevelone/ng-views/flamespray-bit.html'
	}).when('/flamespray-3d', {
		controller: 'NgFlameSprayController',
		templateUrl: '/angular/dullgradelevelone/ng-views/flamespray-3d.html'
	}).when('/flamespray-bit-3d', {
		controller: 'NgFlameSprayController',
		templateUrl: '/angular/dullgradelevelone/ng-views/flamespray-bit-3d.html'
	}).when('/pullcutters-bit', {
		controller: 'NgPullCuttersController',
		templateUrl: '/angular/dullgradelevelone/ng-views/pullcutters-bit.html'
	}).when('/pullcutters', {
		controller: 'NgPullCuttersController',
		templateUrl: '/angular/dullgradelevelone/ng-views/pullcutters.html'
	}).when('/pullcutters-dnr', {
		controller: 'NgDnrPullCuttersController',
		templateUrl: '/angular/dullgradelevelone/ng-views/pullcutters-dnr.html'
	}).when('/fsbraze', {
		controller: 'NgFsBrazeController',
		templateUrl: '/angular/dullgradelevelone/ng-views/fsbraze.html'
	}).when('/markup', {
		controller: 'NgCutterStructureController',
		templateUrl: '/angular/dullgradelevelone/ng-views/markup.html'
	}).when('/markup-bit-3d', {
		controller: 'NgCutterStructureController',
		templateUrl: '/angular/dullgradelevelone/ng-views/markup-bit-3d.html'
	}).when('/markup-3d', {
		controller: 'NgCutterStructureController',
		templateUrl: '/angular/dullgradelevelone/ng-views/markup-3d.html'
	}).when('/markup-restricted-3d', {
		controller: 'NgCutterStructureController',
		templateUrl: '/angular/dullgradelevelone/ng-views/markup-restricted-3d.html'
	}).when('/dullgrade-home', {
		controller: 'NgDullgradeHomeController',
		templateUrl: '/angular/dullgradelevelone/ng-views/dullgrade-home.html'
	}).when('/dullgrade-info', {
		controller: 'NgDullgradeInfoController',
		templateUrl: '/angular/dullgradelevelone/ng-views/dullgrade-info.html'
	}).when('/create', {
		controller: 'NgCreateController',
		templateUrl: '/angular/dullgradelevelone/ng-views/create.html'
	}).when('/viewhistory', {
		controller: 'NgViewHistoryController',
		templateUrl: '/angular/dullgradelevelone/ng-views/viewhistory.html'
	}).when('/clearout-bit', {
		controller: 'NgClearoutController',
		templateUrl: '/angular/dullgradelevelone/ng-views/clearout-bit.html'
	}).when('/clearout', {
		controller: 'NgClearoutController',
		templateUrl: '/angular/dullgradelevelone/ng-views/clearout.html'
	}).when('/bitkiln1', {
		controller: 'NgKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kiln-bit.html'
	}).when('/bitkiln2', {
		controller: 'NgKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kiln-bit.html'
	}).when('/bitkiln3', {
		controller: 'NgKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kiln-bit.html'
	}).when('/bitkiln4', {
		controller: 'NgKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kiln-bit.html'
	}).when('/bitkiln5', {
		controller: 'NgKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kiln-bit.html'
	}).when('/bitkiln6', {
		controller: 'NgKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kiln-bit.html'
	}).when('/grunt-grind', {
		controller: 'NgGruntGrindController',
		templateUrl: '/angular/dullgradelevelone/ng-views/grunt-grind.html'
	}).when('/bondo-paint', {
		controller: 'NgBondoPaintController',
		templateUrl: '/angular/dullgradelevelone/ng-views/bondo-paint.html'
	}).when('/repair-lookup', {
		controller: 'NgLookupController',
		templateUrl: '/angular/dullgradelevelone/ng-views/lookup.html'
	}).when('/bitlocator', {
		controller: 'NgBitLocatorController',
		templateUrl: '/angular/dullgradelevelone/ng-views/bitlocator.html'
	}).when('/dnrlocator', {
		controller: 'NgDNRLocatorController',
		templateUrl: '/angular/dullgradelevelone/ng-views/dnrlocator.html'
	}).when('/rework', {
		controller: 'NgReworkController',
		templateUrl: '/angular/dullgradelevelone/ng-views/rework.html'
	}).when('/rework-dnr', {
		controller: 'NgDNRReworkController',
		templateUrl: '/angular/dullgradelevelone/ng-views/rework-dnr.html'
	}).when('/qa', {
		controller: 'NgQAController',
		templateUrl: '/angular/dullgradelevelone/ng-views/qa.html'
	}).when('/finalinspect', {
		controller: 'NgFinalInspectController',
		templateUrl: '/angular/dullgradelevelone/ng-views/finalinspect.html'
	}).when('/shipping-bit', {
		controller: 'NgShipController',
		templateUrl: '/angular/dullgradelevelone/ng-views/ship.html'
	}).when('/replacecutters', {
		controller: 'NgReplaceCuttersController',
		templateUrl: '/angular/dullgradelevelone/ng-views/replacecutters.html'
	}).when('/thirdPartyInspection', {
		controller: 'NgThirdPartyInspectionController',
		templateUrl: '/angular/dullgradelevelone/ng-views/thirdPartyInspection.html'
	}).when('/finalInspectionDNR', {
		controller: 'NgFinalInspectionDNRController',
		templateUrl: '/angular/dullgradelevelone/ng-views/finalInspectionDNR.html'
	}).when('/dnrKilnHome', {
		controller: 'NgDNRKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kilnHome.html'
	}).when('/dnrKiln', {
		controller: 'NgDNRKilnController',
		templateUrl: '/angular/dullgradelevelone/ng-views/kiln.html'
 	}).when('/shipping', {
		controller: 'NgShippingController',
		templateUrl: '/angular/dullgradelevelone/ng-views/shipping.html'
	}).otherwise({
		redirectTo: '/'
	});
});

app.factory('Global', ['$rootScope', function ($rootScope) {
	// tools data
	var arrivingTools = [];
	var repairs = [];

	// user data
	var users = [];
	var manager = {};
	
	return {

		baseUrl: globalOverrideBaseUrl,
		serverBaseUrl: globalOverrideBrainBaseUrl,

		getArrivingTools: function () {
			return this.arrivingTools;
		},
		setArrivingTools: function (tools, cb) {
			this.arrivingTools = tools;
			$rootScope.$broadcast('tools:updated', tools);
		},
		getRepairs: function () {
			return this.repairs;
		},
		setRepairs: function (repairs) {
			this.repairs = repairs;
			$rootScope.$broadcast('repairs:updated', repairs);
		},
		getManager: function () {
			return this.manager;
		},
		setManager: function (user) {
			this.manager = user;
		},
		getAuthenticatedUsers: function () {
			return this.users;
		},
		setAuthenticatedUsers: function (users) {
			this.users = users;
		}
	};
}]);

app.factory('Helper', function () {
	return {
		convertToFriendlyDate: function (date, dateOnly) {
			if (date) {
				var format = 'M/D/YYYY h:mm A';
				if (dateOnly) format = 'M/D/YYYY';
				return moment(date).format(format);
			} else {
				return undefined;
			}
		},
		getTodayDateInJsStringForm: function () {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1;
			var yyyy = today.getFullYear();
			if (dd < 10) dd = '0' + dd;
			if (mm < 10) mm = '0' + mm;
			today = yyyy + '-' + mm + '-' + dd;

			return today;
		}
	};
});
