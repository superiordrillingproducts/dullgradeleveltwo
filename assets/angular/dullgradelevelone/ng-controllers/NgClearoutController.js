app.controller('NgClearoutController', ['$scope', '$http', '$location', 'Global', function($scope, $http, $location, Global) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = false;
		$scope.$parent.showTimer = true;
		$scope.$parent.buttonTextForSavingInformation = 'STOP';
		$scope.$parent.needDullGradeRecords = true;
		$scope.$parent.dullGrade = undefined
		$scope.$parent.includeCombinedDullgrades = true
		// determines whether cutter info is returned by repairId (ALL) or just by a specific dullgrade.
		$scope.$parent.overrideDefaultStartStopFunctionAfterAuthentication = true
		$scope.$parent.getCutterInfoByDullGradeNotRepair = true
		$scope.$parent.stayLoggedIn = true
		$scope.$parent.selectedTools = [];
		$scope.toolFamily = undefined;
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.ancillaryInformation = {};
		$scope.getStationUsers()

		$scope.getFlowRecords(function() {
			getDullGradeStructureAndCutterNames(function() {
				$scope.getCountOfCutterSizeByAction()
				$scope.getCountOfFlameSpray()
			})
		})
		$scope.getToolFamilies()
		$scope.getConditions()
		$scope.getActions()
	}

	init();

	$scope.$on('selectedTools:updated', function() {
		$scope.callStartStopLaborTracking();
	});
	$scope.$on('startStopTimeStamp:existed', () => {
		$scope.$parent.selectedTools = []
		$scope.$parent.finishedAddingTools = true
		$scope.getFlowRecords()
	})
	$scope.$on('startStopTimeStamp:init', () => {
		$scope.getFlowRecords(function() {
			getDullGradeStructureAndCutterNames(function() {
				$scope.getCountOfCutterSizeByAction()
				$scope.getCountOfFlameSpray()
			})
		})
	})

	$scope.$on('reloadCutterDullGradeAndCutterNames', (e, args) => {
		getDullGradeStructureAndCutterNames(function() {
			$scope.getCountOfCutterSizeByAction()
			$scope.getCountOfFlameSpray()
		})
	})

	$scope.init = function(isLogout) {
		init();
	};

	$scope.completeClearout = function() {
		$scope.$parent.closeRepairOnContinue = true
		$scope.callStartStopLaborTracking()
	}

	function getDullGradeStructureAndCutterNames(cb) {
		$scope.getDullGradeStructureForCurrentRepair(function(dullgrades) {
			if(cb) cb()
			$scope.getCutters(function() {
				$scope.setColorClasses($scope.groups, true, function() {
					$scope.setColorClassesForClearoutObj()
					$scope.getCutterNamesUsedInRepairForDullgrade(function(dullGrade) {
						$scope.isThereAnyCuttersThatNeedLotNumbers(false, function(){
							$scope.$parent.showPullCuttersContinue = true;
							_.each($scope.dullGrade.cutterNames, function(cn) {
								if(cn.markedForLotNumber === true) {
									$scope.$parent.showPullCuttersContinue = false;
								} else if($scope.$parent.showPullCuttersContinue != false) {
									$scope.$parent.showPullCuttersContinue = true;
								}
							})
						})
						if(dullGrade) $scope.setColorsOnClearoutLegend($scope.cutterNames, dullGrade.activeColorClasses, null, dullGrade)
						else $scope.setColorsOnClearoutLegend($scope.cutterNames, $scope.activeColorClasses, null)

					})
				}, dullgrades)
			})
		})
	}

	$scope.printSection = function(toolRepair) {
		debugger;
		var url = Global.baseUrl + 'Pdf/generatePdfCutterPockets';
		var body = {
			toolFamilyId: toolRepair.toolFamilyId,
			partId: toolRepair.partId,
			cutterStructureChronoId: toolRepair.cutterStructureChronoId,
			repairId: toolRepair.repairId
		};

		$http({
			url: url,
			data: $.param({toolFamilyId:body.toolFamilyId}),
			method: 'POST',
			headers: {'Content-Type':'application/form-data; charset=UTF-8'}
		}).success(function(data) {
			debugger;
		}).error(function(err) {
			debugger;
		});
	};

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};
}]);
