app.controller('NgDullgradeController', ['$scope', '$http', '$location', 'Global', function ($scope, $http, $location, Global) {
	function init() {
		// configuration settings
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.needDullGradeRecords = true;
		$scope.$parent.showBitLocator = true;
		$scope.needCutterOrHashStructure = true;

		$scope.$parent.selectedTools = [];
		$scope.toolFamily = undefined;
		$scope.ancillaryInformation = {};

		$scope.getFlowRecords();
		$scope.getToolFamilies();
		$scope.getConditions();
		$scope.getActions();
		$scope.getConditionActionCombos();

		$scope.$parent.inspectionSheetImage = {
			url: '',
			name: 'Initial Inspection Image',
			picTaken: false
		};
	}

	init();

	$scope.init = function (isLogout) {
		init();
	};

	$scope.$on('getFlowRecordsRan', function () {
		$scope.$parent.finishedAddingTools = true;
		$scope.getAncillaryInfo();
		if ($scope.selectedTools.length > 0) {
			$scope.getPicturesForThisRepair($scope.selectedTools[0].repairId)
		}
	});

	$scope.$on('getPicturesForRepairRan', function () {
		// if (!$scope.inspectionSheetImage.picTaken) {
		// 	$('#inspectionSheetImage').modal('show');
		// }
	});

	$scope.$on('selectedTools:updated', function () {
		$scope.getFlowRecords();
		$scope.numberOfBadCutters = 0;
		$scope.numberOfBadDomes = 0;
		$scope.numberOfMissingCutters = 0;
		$scope.numberOfMissingDomes = 0;
	});

	$scope.$on('repairUpdated', function(){
		init();
	});

	$scope.$on('getDullGradeStructureForCurrentRepair:executed',function(){
		_.each($scope.groups, function (g) {
			_.each(g.blades, function (b) {
				_.each(b.rows, function (r) {
					_.each(r.pockets, function (p) {
						debugger;
						if (p.cutterPocketTypeId === 1) {
							if (p.cutterConditionId === 100){
								p.cutterCondition = "LC/N";
								$scope.numberOfMissingCutters++;
							}
							if (p.cutterConditionId === 101) {
								p.cutterCondition = "CR/N";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 102) {
								p.cutterCondition = "EA/C";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 103) {
								p.cutterCondition = "--";
							}
							if (p.cutterConditionId === 104) {
								p.cutterCondition = "BC1/C";
							}
							if (p.cutterConditionId === 105) {
								p.cutterCondition = "BC2/C";
							}
							if (p.cutterConditionId === 106) {
								p.cutterCondition = "BC3/C";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 107) {
								p.cutterCondition = "BC4/C";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 108) {
								p.cutterCondition = "BC+/N";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 109) {
								p.cutterCondition = "EC1/C";
							}
							if (p.cutterConditionId === 110) {
								p.cutterCondition = "EC2/C";
							}
							if (p.cutterConditionId === 111) {
								p.cutterCondition = "EC3/C";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 112) {
								p.cutterCondition = "EC4/C";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 113) {
								p.cutterCondition = "EC+/N";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 114) {
								p.cutterCondition = "W1/C";
							}
							if (p.cutterConditionId === 115) {
								p.cutterCondition = "W2/C";
							}
							if (p.cutterConditionId === 116) {
								p.cutterCondition = "W3/C";
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 117) {
								p.cutterCondition = 'W4/C';
								$scope.numberOfBadCutters++;
							}
							if (p.cutterConditionId === 118) {
								p.cutterCondition = "W+/N";
								$scope.numberOfBadCutters++;
							}
						} else if (p.cutterPocketTypeId === 2) {
							if (p.cutterConditionId === 100) {
								p.cutterCondition = "LC/N";
								$scope.numberOfMissingDomes++;
							}
							if (p.cutterConditionId === 101) {
								p.cutterCondition = "CR/N";
								$scope.numberOfBadDomes++;
							}
							if (p.cutterConditionId === 119) {
								p.cutterCondition = "BC/N";
								$scope.numberOfBadDomes++;
							}
							if (p.cutterConditionId === 114) {
								p.cutterCondition = "W1/--";
							}
							if (p.cutterConditionId === 120) {
								p.cutterCondition = "W+/N";
								$scope.numberOfBadDomes++;
							}
						} else if (p.cutterPocketTypeId === 7) {
							if (p.cutterConditionId === 103) {
								p.cutterCondition = "--";
							}
							if (p.cutterConditionId === 121) {
								p.cutterCondition = "W/FS";
								$scope.numberOfBadDomes++;
							}
						}
					})
				})
			})
		})
	});

	$scope.stop = function () {
		$scope.updateAllDullGrades(function () {
			$('#modalSaveDullgrades').modal('show');
		});
	};

	$scope.cancelTakingPicture = function () {
		$('#inspectionSheetImage').modal('hide');
		$scope.callStartStopLaborTracking(true);
	};

	$scope.markAsPaintOnly = function () {
		var data = {};
		debugger;
		if ($scope.selectedTools[0].toolFamilyId = 1) {
			var url = Global.baseUrl + 'Repair/updateAncillaryDnr';
			if ($scope.paintOnly) {
				data = {
					ancillaryDnrId: $scope.ancillaryId,
					paintOnly: 0
				}
			} else {
				data = {
					ancillaryDnrId: $scope.ancillaryId,
					paintOnly: 1
				};
			}
		} else {
			var url = Global.baseUrl + 'Repair/updateAncillaryVstream';
			if ($scope.paintOnly) {
				data = {
					ancillaryVStreamId: $scope.ancillaryId,
					paintOnly: 0
				}
			} else {
				data = {
					ancillaryVStreamId: $scope.ancillaryId,
					paintOnly: 1
				};
			}
		}
		$http.post(url, data).success(function (paint) {
			toastr.success("Tool was marked as paint only.")
			if ($scope.paintOnly) {
				$scope.paintOnly = false
			} else {
				$scope.paintOnly = true;
			}
		}).error(function (error) {
			toastr.error("There was a problem marking as paint only.");
		})
	};

	$scope.openModalTakePicture = function (currentPicture) {
		$scope.$parent.currentSelectedImage = currentPicture;
		if (currentPicture.name === 'Initial Inspection Image') {
			$scope.$parent.documentTypeId = 9;
			$scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/initialdullgradedocuments';
		}
		$('#captureImg').attr('src', '');
		$('#cameraInput').val('');
		$('#captureImg').hide();
		$('#confirmSavePicture').attr('disabled', true);
		$('#modalTakePicture').modal('show');
	};

	$scope.finishedTakingInspectionSheetImage = function () {
		$('#inspectionSheetImage').modal('hide');
		$scope.getPicturesForThisRepair($scope.selectedTools[0].repairId);
	};

	$scope.updateAndPushToNext = function () {
		var url = Global.baseUrl + 'dullGradeLevelOne/saveThirdPartyInspection';
		var data = {
			repairId: $scope.selectedTools[0].repairId,
			toolFamilyId: $scope.selectedTools[0].toolFamilyId,
			totalDamagedCutters: $scope.numberOfBadCutters,
			totalDamagedDomes: $scope.numberOfBadDomes,
			totalMissingCutters: $scope.numberOfMissingCutters,
			totalMissingDomes: $scope.numberOfMissingDomes,
			unusualDamage: $scope.unusualDamageInt
		};

		$scope.gPost(url, data, function (err, createdAncillary) {
			if (err) toastr.error(err);
			else {
				$scope.getFlowRelationshipForCurrentFlow($scope.selectedTools[0].flowId, false)
			}
		});
	};

	$scope.pushToNext = function () {
		$scope.getFlowRelationshipForCurrentFlow($scope.selectedTools[0].flowId, false)	
	};

	$scope.modalCondition = function (pocket) {
		$scope.selectedPocket = pocket;
		makeClickedPocketActive(pocket)
		$('#modalCondition').modal('show');
	};

	$scope.setConditionAction = function (condition) {
		debugger;
		if($scope.selectedPocket.cutterPocketTypeId === 1){
			if($scope.selectedPocket.cutterConditionId === 100){
				$scope.numberOfMissingCutters--;
			}
			if ($scope.selectedPocket.cutterConditionId === 101 || $scope.selectedPocket.cutterConditionId === 102 || $scope.selectedPocket.cutterConditionId === 106 || $scope.selectedPocket.cutterConditionId === 107 || $scope.selectedPocket.cutterConditionId === 108 || $scope.selectedPocket.cutterConditionId === 111 || $scope.selectedPocket.cutterConditionId === 112 || $scope.selectedPocket.cutterConditionId === 113 || $scope.selectedPocket.cutterConditionId === 116 || $scope.selectedPocket.cutterConditionId === 117 || $scope.selectedPocket.cutterConditionId === 118) {
				$scope.numberOfBadCutters--;
			}
			if (condition === 'Lost') {
				$scope.selectedPocket.cutterCondition = "LC/N";
				$scope.selectedPocket.cutterConditionId = 100;
				$scope.numberOfMissingCutters++;
			}
			if (condition === 'Cracked') {
				$scope.selectedPocket.cutterCondition = "CR/N";
				$scope.selectedPocket.cutterConditionId = 101;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'ErrodedAlloy') {
				$scope.selectedPocket.cutterCondition = "EA/C";
				$scope.selectedPocket.cutterConditionId = 102;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'None') {
				$scope.selectedPocket.cutterCondition = "--";
				$scope.selectedPocket.cutterConditionId = 103;
			}
			if (condition === 'Broken1') {
				$scope.selectedPocket.cutterCondition = "BC1/C";
				$scope.selectedPocket.cutterConditionId = 104;
			}
			if (condition === 'Broken2') {
				$scope.selectedPocket.cutterCondition = "BC2/C";
				$scope.selectedPocket.cutterConditionId = 105;
			}
			if (condition === 'Broken3') {
				$scope.selectedPocket.cutterCondition = "BC3/C";
				$scope.selectedPocket.cutterConditionId = 106;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'Broken4') {
				$scope.selectedPocket.cutterCondition = "BC4/C";
				$scope.selectedPocket.cutterConditionId = 107;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'Broken4Plus') {
				$scope.selectedPocket.cutterCondition = "BC+/N";
				$scope.selectedPocket.cutterConditionId = 108;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'ErrodedCutter1') {
				$scope.selectedPocket.cutterCondition = "EC1/C";
				$scope.selectedPocket.conditionId = 109;
			}
			if (condition === 'ErrodedCutter2') {
				$scope.selectedPocket.cutterCondition = "EC2/C";
				$scope.selectedPocket.cutterConditionId = 110;
			}
			if (condition === 'ErrodedCutter3') {
				$scope.selectedPocket.cutterCondition = "EC3/C";
				$scope.selectedPocket.cutterConditionId = 111;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'ErrodedCutter4') {
				$scope.selectedPocket.cutterCondition = "EC4/C";
				$scope.selectedPocket.cutterConditionId = 112;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'ErrodedCutter4Plus') {
				$scope.selectedPocket.cutterCondition = "EC+/N";
				$scope.selectedPocket.cutterConditionId = 113;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'Worn1') {
				$scope.selectedPocket.cutterCondition = "W1/C";
				$scope.selectedPocket.cutterConditionId = 114;
			}
			if (condition === 'Worn2') {
				$scope.selectedPocket.cutterCondition = "W2/C";
				$scope.selectedPocket.cutterConditionId = 115;
			}
			if (condition === 'Worn3') {
				$scope.selectedPocket.cutterCondition = "W3/C";
				$scope.selectedPocket.conditionId = 116;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'Worn4') {
				$scope.selectedPocket.cutterCondition = 'W4/C';
				$scope.selectedPocket.cutterConditionId = 117;
				$scope.numberOfBadCutters++;
			}
			if (condition === 'Worn4Plus') {
				$scope.selectedPocket.cutterCondition = "W+/N";
				$scope.selectedPocket.cutterConditionId = 118;
				$scope.numberOfBadCutters++;
			}
		} else if ($scope.selectedPocket.cutterPocketTypeId === 2) {
			if ($scope.selectedPocket.cutterConditionId === 100) {
				$scope.numberOfMissingDomes--;
			}
			if ($scope.selectedPocket.cutterConditionId === 101 || $scope.selectedPocket.cutterConditionId === 119 || $scope.selectedPocket.cutterCondition === 120) {
				$scope.numberOfBadDomes--;
			}
			if (condition === 'Lost') {
				$scope.selectedPocket.cutterCondition = "LC/N";
				$scope.selectedPocket.cutterConditionId = 100;
				$scope.numberOfMissingDomes++;
			}
			if (condition === 'Cracked') {
				$scope.selectedPocket.cutterCondition = "CR/N";
				$scope.selectedPocket.cutterConditionId = 101;
				$scope.numberOfBadDomes++;
			}
			if (condition === 'Broken') {
				$scope.selectedPocket.cutterCondition = "BC/N";
				$scope.selectedPocket.cutterConditionId = 119;
				$scope.numberOfBadDomes++;
			}
			if (condition === 'Worn1') {
				$scope.selectedPocket.cutterCondition = "W1/--";
				$scope.selectedPocket.cutterConditionId = 114;
			}
			if (condition === 'Worn1Plus') {
				$scope.selectedPocket.cutterCondition = "W+/N";
				$scope.selectedPocket.cutterConditionId = 120;
				$scope.numberOfBadDomes++;
			}
		} else if ($scope.selectedPocket.cutterPocketTypeId === 7) {
			if ($scope.selectedPocket.cutterConditionId === 121) {
				$scope.numberOfBadDomes--;
			}
			if (condition === 'None') {
				$scope.selectedPocket.cutterCondition = "--";
				$scope.selectedPocket.cutterConditionId = 103;
			}
			if (condition === 'Worn') {
				$scope.selectedPocket.cutterCondition = "W/FS";
				$scope.selectedPocket.cutterConditionId = 121;
				$scope.numberOfBadDomes++;
			}
		}
		
		$('#modalCondition').modal('hide');
	};

	$scope.closeCancelModalSetCondition = function () {
		$('#modalCondition').modal('hide');
	};

	function makeClickedPocketActive(pocket) {
		delete $scope.activePocket.activePocket

		$scope.activePocket = pocket
		$scope.activePocket.activePocket = 'activePocket'
	}

	$scope.clearCondition = function (){
		debugger;
		if ($scope.selectedPocket.cutterPocketTypeId === 1) {
			if ($scope.selectedPocket.cutterConditionId === 100) {
				$scope.numberOfMissingCutters--;
			}
			if ($scope.selectedPocket.cutterConditionId === 101 || $scope.selectedPocket.cutterConditionId === 102 || $scope.selectedPocket.cutterConditionId === 106 || $scope.selectedPocket.cutterConditionId === 107 || $scope.selectedPocket.cutterConditionId === 108 || $scope.selectedPocket.cutterConditionId === 111 || $scope.selectedPocket.cutterConditionId === 112 || $scope.selectedPocket.cutterConditionId === 113 || $scope.selectedPocket.cutterConditionId === 116 || $scope.selectedPocket.cutterConditionId === 117 || $scope.selectedPocket.cutterConditionId === 118) {
				$scope.numberOfBadCutters--;
			}
		} else if ($scope.selectedPocket.cutterPocketTypeId === 2) {
			if ($scope.selectedPocket.cutterConditionId === 100) {
				$scope.numberOfMissingDomes--;
			}
			if ($scope.selectedPocket.cutterConditionId === 101 || $scope.selectedPocket.cutterConditionId === 119 || $scope.selectedPocket.cutterCondition === 120) {
				$scope.numberOfBadDomes--;
			}
		} else if ($scope.selectedPocket.cutterPocketTypeId === 7) {
			if ($scope.selectedPocket.cutterConditionId === 121) {
				$scope.numberOfBadDomes--;
			}
		}
		delete $scope.selectedPocket.cutterConditionId;
		delete $scope.selectedPocket.cutterCondition;
		delete $scope.selectedPocket.cutterActionId;
		delete $scope.selectedPocket.cutterAction;
		$('#modalCondition').modal('hide');
	};
}]);