app.controller('NgDullgradeHomeController', ['$scope', '$http', '$location', 'Global', '$q', function($scope, $http, $location, Global, $q) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = true;
		$scope.$parent.showTimer = false;
		$scope.$parent.buttonTextForSavingInformation = 'STOP';
		$scope.$parent.currentAuthenticatedUsers = $scope.$parent.stayLoggedIn ? $scope.$parent.currentAuthenticatedUsers : [];
		$scope.$parent.stayLoggedIn = true;
		$scope.$parent.selectedTools = [];
		$scope.toolFamily = undefined;
		$scope.$parent.showBitLocator = true;

		$scope.ancillaryInformation = {};

		$scope.getFlowRecords();
		$scope.getToolFamilies();

		$scope.microprograms = [
			{ 'chrono': 1, 'name': 'Ancillary Info', 'url': '#/dullgrade-info', 'visible': false },
			{ 'chrono': 3, 'name': 'Dullgrade', 'url': '#/dullgrade', 'visible': false },
			{ 'chrono': 4, 'name': 'Flame Spray', 'url': '#/flamespray', 'visible': false },
			{ 'chrono': 2, 'name': 'Markup', 'url': '#/markup', 'visible': false }
		];
	}

	init();

	$scope.init = function(isLogout) {
		init();
	};

	// $scope.$on('currentAuthenticatedUsers:updated', function() {
	//     debugger;
	//     if ($scope.currentAuthenticatedUsers && $scope.currentAuthenticatedUsers.length >= $scope.maxUsers) {
	//         $scope.getFlowRecords(function() {
	//             getAllDullGradeInfo();
	//         });
	//     }
	// });
	$scope.$on('getFlowRecordsRan', function() {
		delete $scope.$parent.partNumber;
		delete $scope.$parent.selectedWholeNumber;
		delete $scope.$parent.selectedFraction;
		delete $scope.$parent.style;
		delete $scope.$parent.rush;
		delete $scope.$parent.rushComments;
		delete $scope.$parent.nozzleComments;
		delete $scope.$parent.gaugeOd;
		delete $scope.$parent.grade;
		delete $scope.$parent.visualDyePenComments;
		delete $scope.$parent.shippingNeeds;
		delete $scope.$parent.inspectionIssue;
		delete $scope.$parent.paintOnly;
		delete $scope.$parent.flameUp;
		delete $scope.$parent.hardFaceHours;
		delete $scope.$parent.flamesprayHours;
		delete $scope.$parent.rechaseThreads;
		delete $scope.$parent.reshankNumber;
		delete $scope.$parent.additionalShopWork;
		delete $scope.$parent.stopWork;
		delete $scope.$parent.customerOwned;
		delete $scope.$parent.selectedCustomer;
		delete $scope.$parent.selectedReceivedFromStockPoint;
		delete $scope.$parent.selectedShipToStockPoint;
		delete $scope.$parent.ist;
		delete $scope.$parent.ancillaryId;
		getAllDullGradeInfo();
	});

	function getAllDullGradeInfo() {
		if ($scope.selectedTools && $scope.selectedTools[0]) {
			$scope.everythingThatIsRequiredIsFinished = false;
			// check if all required ancillary info has been provided
			$scope.checkIfHasAllRequiredAncillaryInformation(function(hasAllAncInfo) {
				$scope.microprograms[0].visible = true;
				$scope.microprograms[0].showAdditionalList = true;
				if (hasAllAncInfo) {
					$scope.microprograms[0].done = true;
					// Get markup
					$scope.getCutterStructureForCurrentPart(function(markup) {
						$scope.microprograms[3].visible = true;
						$scope.microprograms[3].url = '#/markup-3d';

						if (markup && markup.length > 0) {
							$scope.microprograms[3].done = true;
							// Get dullgrade structure
							$scope.getDullGradeStructureForCurrentRepair(function(dullgrades) {

								// always use original dullgrade for this workstation. This is used only to determine what links show up in UI
								var dgStructure = dullgrades[0].groups;
								var dullGrade = dullgrades[0];

								$scope.everythingThatIsRequiredIsFinished = true;
								$scope.microprograms[1].visible = true;
								$scope.microprograms[2].visible = true;
								if ($scope.selectedTools[0].toolFamilyId === 4) {
									$scope.microprograms[1].url = '#/dullgrade-bit-3d';
									$scope.microprograms[2].url = '#/flamespray-bit-3d';
								} else {
									$scope.microprograms[1].url = '#/dullgrade-3d';
									$scope.microprograms[2].url = '#/flamespray-3d';
								}

								// check if dgStructure has any dullgrades done to it (action, condition)
								$scope.microprograms[1].done = false;
								_.each(dgStructure, function(g) {
									if ($scope.microprograms[1].done === false) {
										_.each(g.blades, function(b) {
											if ($scope.microprograms[1].done === false) {
												_.each(b.rows, function(r) {
													if ($scope.microprograms[1].done === false) {
														_.each(r.pockets, function(p) {
															if (p.cutterActionId || p.cutterConditionId) {
																$scope.microprograms[1].done = true;
															}
														});
													}
												});
											}
										});
									}
								});
								var doesHaveFlameSpray = doesDullGradesHaveFlamespray(dgStructure);
								if (doesHaveFlameSpray) {
									$scope.microprograms[2].done = true;
								}
							});
						}
					});
				}
			});
		}
	}

	function doesDullGradesHaveFlamespray(dgStructure) {
		debugger;
		var isFound = false;
		if (!isFound)
			_.each(dgStructure, function(group) {
				debugger;
				if (!isFound)
					_.each(group.blades, function(b) {
						debugger;
						if (!isFound)
							_.each(b.rows, function(r) {
								debugger;
								isFlameSpray = _.find(r.pockets, function(p) {
									return p.isFlameSpray;
								});
								if(isFlameSpray) isFound = true;
							});
					});
			});
		return isFound;
	}

	$scope.stop = function() {
		$scope.callStartStopLaborTracking(true);
		$scope.microprograms[0].visible = false;
		$scope.microprograms[0].showAdditionalList = false;
		$scope.microprograms[0].done = false;
		$scope.microprograms[1].visible = false;
		$scope.microprograms[1].done = false;
		$scope.microprograms[2].visible = false;
		$scope.microprograms[2].done = false;
		$scope.microprograms[3].visible = false;
		$scope.microprograms[3].done = false;
		$scope.everythingThatIsRequiredIsFinished = false;
	};

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};

	$scope.$on('selectedTools:updated', function() {
		$scope.callStartStopLaborTracking();
	});

	$scope.dullGradeComplete = function() {
		$scope.callStartStopLaborTracking();
		$scope.microprograms[0].visible = false;
		$scope.microprograms[0].showAdditionalList = false;
		$scope.microprograms[0].done = false;
		$scope.microprograms[1].visible = false;
		$scope.microprograms[1].done = false;
		$scope.microprograms[2].visible = false;
		$scope.microprograms[2].done = false;
		$scope.microprograms[3].visible = false;
		$scope.microprograms[3].done = false;
		$scope.everythingThatIsRequiredIsFinished = false;
	};

	$scope.goToReceiving = function() {
		debugger;
		$scope.$parent.currentUrl = $location.path();
		setTimeout(function() {
			$location.path('/receiving');
			$scope.$apply();
		}, 500);
	};
}]);
