app.controller('NgQAController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', 'Idle', 'Keepalive', '$timeout', function($http, $scope, $location, Global, $filter, $sails, Idle, Keepalive, $timeout) {
	function init() {
		$scope.$parent.maxUsers = 1;
		$scope.$parent.showStopButton = true;
		$scope.$parent.showTimer = false;
		$scope.$parent.showReworkButton = true;
		$scope.$parent.showBitLocator = true;
		$scope.inspectionUsers = [];

		$scope.$parent.selectedTools = [];
		$scope.getFlowRecords();
	}

	init();

	$scope.$on('selectedTools:updated', function() {
		$scope.$parent.finishedAddingTools = true;
		uncheck();
	});

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	}

	function uncheck() {
		$(':checkbox:checked').removeAttr('checked');
	}


}]);
