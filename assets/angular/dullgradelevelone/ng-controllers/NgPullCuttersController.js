app.controller('NgPullCuttersController', ['$scope', '$http', '$location', 'Global', function($scope, $http, $location, Global) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1
		$scope.$parent.finishedAddingTools = true
		$scope.$parent.showStopButton = false
		$scope.$parent.showTimer = true
		$scope.$parent.buttonTextForSavingInformation = 'STOP'
		$scope.$parent.needDullGradeRecords = true
		$scope.$parent.showBitLocator = true;
		$scope.$parent.dullGrade = undefined
		// allows user to rotate bit using next-and-previous-blade-pocket button.
		$scope.$parent.rotateInsteadOfSwitchBlades = true
		// determines whether cutter info is returned by repairId (ALL) or just by a specific dullgrade.
		$scope.$parent.getCutterInfoByDullGradeNotRepair = true
		$scope.$parent.overrideDefaultStartStopFunctionAfterAuthentication = true
		$scope.$parent.stayLoggedIn = true
		$scope.$parent.selectedTools = []
		$scope.toolFamily = undefined
		$scope.$parent.currentAuthenticatedUsers = []
		$scope.ancillaryInformation = {}
		$scope.getStationUsers()


		$scope.getFlowRecords(function() {
			getDullGradeStructureAndCutterNames(function() {
				$scope.getCountOfCutterSizeByAction()
				$scope.getCountOfFlameSpray()
			})
		})
		$scope.getToolFamilies()
		$scope.getConditions()
		$scope.getActions()
	}

	init()

	$scope.$on('selectedTools:updated', function() {
		$scope.callStartStopLaborTracking();
	});

	$scope.init = function() {
		init()
	}

	function getDullGradeStructureAndCutterNames(cb) {
		$scope.getDullGradeStructureForCurrentRepair(function(dullgrades) {
			if(cb) cb()
			$scope.getCutters(function() {
				$scope.setColorClasses($scope.groups, false, function() {
					$scope.getCutterNamesUsedInRepairForDullgrade(function(dullGrade) {
						$scope.isThereAnyCuttersThatNeedLotNumbers(false, function(){
							$scope.$parent.showPullCuttersContinue = true;
							_.each($scope.dullGrade.cutterNames, function(cn) {
								if(cn.markedForLotNumber === true) {
									$scope.$parent.showPullCuttersContinue = false;
								} else if($scope.$parent.showPullCuttersContinue != false) {
									$scope.$parent.showPullCuttersContinue = true;
								}
							})
						})
						if(dullGrade) $scope.setColorsOnClearoutLegend($scope.cutterNames, dullGrade.activeColorClasses, null, dullGrade)
						else $scope.setColorsOnClearoutLegend($scope.cutterNames, $scope.activeColorClasses, null)

					})
				}, dullgrades)
			})
		})
	}

	$scope.$on('startStopTimeStamp:existed', () => {
		$scope.$parent.selectedTools = []
		$scope.$parent.finishedAddingTools = true
		$scope.getFlowRecords()
	})

	$scope.$on('startStopTimeStamp:init', () => {
		$scope.getFlowRecords(function() {
			getDullGradeStructureAndCutterNames(function() {
				$scope.getCountOfCutterSizeByAction()
				$scope.getCountOfFlameSpray()
			})
		})
	})

	$scope.$on('toolHold', function() {
		$scope.callStartStopLaborTracking(true);
	})

	$scope.$on('reloadCutterDullGradeAndCutterNames', (e, args) => {
		getDullGradeStructureAndCutterNames(function() {
			$scope.getCountOfCutterSizeByAction()
			$scope.getCountOfFlameSpray()
		})
	})

	$scope.$on('toolHoldStatusChange', function(e, args) {
		$('#modalSetOnHoldAndComments').modal('hide')
	})

	$scope.printSection = function(toolRepair) {

		var url = Global.baseUrl + 'Pdf/generatePdfCutterPockets'
		var body = {
			toolFamilyId: toolRepair.toolFamilyId,
			partId: toolRepair.partId,
			cutterStructureChronoId: toolRepair.cutterStructureChronoId,
			repairId: toolRepair.repairId
		}

		$http({
			url: url,
			data: $.param({toolFamilyId:body.toolFamilyId}),
			method: 'POST',
			headers: {'Content-Type':'application/form-data; charset=UTF-8'}
		}).success(function() {

		}).error(function() {

		})
	}

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};
}])

app.directive('tooltip', function() {
	return {
		scope: {
			item: '='
		},
		link: function(scope, element, attrs) {
			var title = scope.item.holdComments ? scope.item.holdComments : 'No hold comments available.'
			element.attr('title', title)
			element.hover(function() {
				element.tooltip('show')
			}, function() {
				element.tooltip('hide')
			})

		}
	}
})
