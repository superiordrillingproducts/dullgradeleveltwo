app.controller('NgCutterStructureController', ['$scope', '$http', 'Global', function ($scope, $http, Global) {
	$scope.$parent.maxUsers = 1;
	$scope.$parent.finishedAddingTools = true;

	// user this for both bits and non-bit tools. Do not use both 'needCutterOrHashStructure' and 'needCutterStructure' at the same time here.
	$scope.$parent.needCutterOrHashStructure = true;
	$scope.$parent.needDullGradeRecords = false;
	$scope.$parent.startIdleTimer = true;
	$scope.$parent.showBitLocator = true;
	// allows user to rotate bit using next-and-previous-blade-pocket button.
	$scope.$parent.rotateInsteadOfSwitchBlades = true;
	// $scope.$parent.showStopButton = true;
	// $scope.$parent.showTimer = true;
	$scope.getFlowRecords(null, () => {
		if($scope.selectedTools && $scope.selectedTools[0] && $scope.selectedTools[0].toolFamilyId) getParts();
	});
	$scope.getCutters();

	$scope.$on('technicianIdleTimeout', function() {
		$scope.saveCutterStructureForCurrentPart();
	});

	function getParts() {
		var url = Global.baseUrl + 'part/getPartsByFamily';
		var body = {
			toolFamilyId: $scope.selectedTools[0].toolFamilyId
		};

		debugger;
		$http.post(url, body).success(data => {
			$scope.$parent.parts = data;
			var defaultPart = _.find($scope.$parent.parts, function(p) {
				if(p.partBakerBitId) return p.partBakerBitId === $scope.selectedTools[0].partId;
				else return p.partId === $scope.selectedTools[0].partId;
			});

			if(defaultPart) $scope.selectedPart = JSON.parse(JSON.stringify(defaultPart));
		});
	}

	$scope.pullInMarkup = (part) => {
		debugger;
		if(part) {
			$scope.selectedPart = JSON.parse(JSON.stringify(part));
			var partId;
			if(part.partBakerBitId) partId = part.partBakerBitId;
			else partId = part.partId;
			$scope.getCutterStructureForCurrentPart(null, {
				cutterStructureChronoId: null,
				partId: partId,
				toolFamilyId: $scope.selectedTools[0].toolFamilyId
			}, () => {
				$scope.searchExistingParts = undefined;
			});
		}
	}
}]);
