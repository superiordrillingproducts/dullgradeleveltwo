app.controller('NgBrazeController', ['$scope', '$http', 'Global', function($scope, $http, Global) {
	function init() {
		// configuration
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = false;
		$scope.$parent.needDullGradeRecords = false;
		$scope.$parent.needCutterStructure = false;
		$scope.$parent.showStopButton = true;
		$scope.$parent.showTimer = true;
		$scope.$parent.showBitLocator = true;

		// initiate arrays and objects
		$scope.$parent.selectedTools = [];
		$scope.getFlowRecords();
		$scope.$on('getFlowRecordsRan', function() {
			$scope.flamesprayStationRecords = _.filter($scope.stationRecords, function(sr) {
				return sr.flowGroupId === 1;
			});
			$scope.brazeStationRecords = _.filter($scope.stationRecords, function(sr) {
				return sr.flowGroupId === 2;
			});
			debugger;

			if($scope.flamesprayStationRecords && $scope.flamesprayStationRecords.length) {
				$scope.getFlowRelationshipForCurrentFlow($scope.flamesprayStationRecords[0].flowId, null, null, function(nextFlow) {
					debugger;
					$scope.fsFlowsWithRepair = [];
					if(nextFlow && nextFlow.length) {
						debugger;
						_.each(nextFlow, function(flow) {
							for(var i = 0; i < $scope.allRepairs.length; i++) {
								var repair = $scope.allRepairs[i];
								if(flow.nextFlowId === repair.flowId) {
									flow.repair = repair;
									break;
								}
							}
							$scope.fsFlowsWithRepair.push(flow);
						});
					}
				});
			}
			if($scope.brazeStationRecords && $scope.brazeStationRecords.length) {
				$scope.getFlowRelationshipForCurrentFlow($scope.brazeStationRecords[0].flowId, null, null, function(nextFlow) {
					debugger;
					$scope.brazeFlowsWithRepair = [];
					if(nextFlow && nextFlow.length) {
						_.each(nextFlow, function(flow) {
							for(var i = 0; i < $scope.allRepairs.length; i++) {
								var repair = $scope.allRepairs[i];
								if(flow.nextFlowId === repair.flowId) {
									flow.repair = repair;
									break;
								}
							}
							$scope.brazeFlowsWithRepair.push(flow);
						});
					}
				});
			}

		});
		$scope.getToolFamilies();
	}

	init();

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};

}]);
