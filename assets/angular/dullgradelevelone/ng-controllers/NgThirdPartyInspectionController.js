app.controller('NgThirdPartyInspectionController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', 'Idle', 'Keepalive', '$timeout', function($http, $scope, $location, Global, $filter, $sails, Idle, Keepalive, $timeout) {
	function init() {
		// configuration settings
		$scope.finishedAddingTools = false;
		$scope.newTool = false;
		$scope.toolFamily = undefined;
		$scope.getFlowRecords();
		$scope.$parent.selectedTools = [];
		$scope.measurements = [];
		$scope.$parent.pics = [];
		// $scope.$parent.pictures = [{
		// 	url: '',
		// 	name: 'Box - Blade 1',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Box - Blade 2',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Box - Blade 3',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Box - Blade 4',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Box - Blade 5',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Box - Blade 6',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Pin - Blade 1',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Pin - Blade 2',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Pin - Blade 3',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Pin - Blade 4',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Pin - Blade 5',
		// 	picTaken: false
		// }, {
		// 	url: '',
		// 	name: 'Pin - Blade 6',
		// 	picTaken: false
		// }];

		$scope.$parent.thirdPartyInspectionImage = {
			url: '',
			name: 'DS1 Inspection Image',
			picTaken: false
		};

		$scope.$parent.thirdPartyInspectionAcknowledgement = {
			url: '',
			name: 'DS1 Inspection Acknowledgement Image',
			picTaken: false
		};

		// $scope.$parent.billOfLadingImage = {
		// 	url: '',
		// 	name: 'Bill of Lading Image',
		// 	picTaken: false
		// };

	}
	init();
	$scope.init = function () {
		init();
	};

	$scope.$on('getFlowRecordsRan', function() {
		delete $scope.$parent.thirdPartyInspectionNumber;
		delete $scope.$parent.thirdPartyInspectorCompany;
		$scope.bodyCracks = null;
		$scope.boxCut = null;
		$scope.boreBack = null;
		$scope.pinCut = null;
		$scope.reliefGroove = null;
		$scope.$parent.thirdPartyInspectorsName = '';
		$scope.thirdPartyInspectorsAcceptance = false;
		debugger;
		if($scope.selectedTools.length === 1){
			$scope.getPicturesForThisRepair($scope.selectedTools[0].repairId)
			$scope.getAncillaryInfo();
		} else {
			$scope.finishedAddingTools = false;
			var url = Global.baseUrl + 'dullgradelevelone/getPicturesForThisRepair';
			angular.forEach($scope.stationRecords, function(sr){
				var data = {
					repairId: sr.repairId
				};
				$scope.getAncillaryInfo(sr);
				$scope.gPost(url, data, function (err,res) {
					if (err) toastr.error(err);
					else {
						debugger;
						if (res.length != 0) {
							sr.thirdPartyInspectionImageHasBeenTaken = false;
							sr.billOfLadingImageHasBeenTaken = false;
							sr.thirdPartyInspectionAcknowledgementHasBeenTaken = false;
							angular.forEach(res, function (r) {
								debugger;
								if (sr.thirdPartyInspectionImageHasBeenTaken === false) {
									if (r.documentTypeId === 5) {
										sr.thirdPartyInspectionImageHasBeenTaken = true;
									} else {
										sr.thirdPartyInspectionImageHasBeenTaken = false;
									}
								}
								if (sr.billOfLadingImageHasBeenTaken === false) {
									if (r.documentTypeId === 7) {
										sr.billOfLadingImageHasBeenTaken = true;
									} else {
										sr.billOfLadingImageHasBeenTaken = false;
									}
								}
								if (sr.thirdPartyInspectionAcknowledgementHasBeenTaken === false) {
									if (r.documentTypeId === 8) {
										sr.thirdPartyInspectionAcknowledgementHasBeenTaken = true;
									} else {
										sr.thirdPartyInspectionAcknowledgementHasBeenTaken = false;
									}
								}
							});
						}
					}
				});
			});
		}
	});

	$scope.$on('repairUpdated', function () {
		init();
	});

	$scope.openModalTakePicture = function(currentPicture) {
		debugger;
		$scope.$parent.currentSelectedImage = currentPicture;
		if(currentPicture.name === 'DS1 Inspection Image'){
			$scope.$parent.documentTypeId = 13;
			$scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/visualToolInspection';
			//$scope.$parent.documentLocation = '/Users/home/Documents/sdpidocuments';
		} else if (currentPicture.name === 'Bill of Lading Image') {
			$scope.$parent.documentTypeId = 7;
			$scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/billofladingreceiveddocuments';
			//$scope.$parent.documentLocation = '/Users/home/Documents/sdpidocuments';
		} else if (currentPicture.name === 'DS1 Inspection Acknowledgement Image') {
			$scope.$parent.documentTypeId = 8;
			$scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/thirdpartyinspectiondocuments';
			//$scope.$parent.documentLocation = '/Users/home/Documents/sdpidocuments';
		} else {
			$scope.$parent.documentTypeId = 4;
			$scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/pictures';
			//$scope.$parent.documentLocation = '/Users/home/Documents/sdpidocuments';
		}
		$('#captureImg').attr('src', '');
		$('#cameraInput').val('');
		$('#captureImg').hide();
		$('#confirmSavePicture').attr('disabled', true);
		$('#modalTakePicture').modal('show');
	};

	$scope.saveThirdPartyInspectionAndMoveToNextFlow = function(moveToNextStation) {
		debugger;
		if($scope.selectedTools.length > 1){
			var data = [];
			// first create image record
			angular.forEach($scope.selectedTools, function (tool) {
				data.push({
					repairId: tool.repairId,
					thirdPartyInspectorCompany: $scope.thirdPartyInspectorCompany,
					thirdPartyInspectionNumber: $scope.thirdPartyInspectionNumber,
					toolFamilyId: tool.toolFamilyId,
					ancillaryDnrId: tool.ancillaryId
				})
			});
		} else {
			if($scope.selectedTools[0].toolFamilyId === 1){
				var data = {
					repairId: $scope.selectedTools[0].repairId,
					thirdPartyInspectorCompany: $scope.thirdPartyInspectorCompany,
					thirdPartyInspectionNumber: $scope.thirdPartyInspectionNumber,
					toolFamilyId: $scope.selectedTools[0].toolFamilyId,
					thirdPartyInspectorsName: $scope.thirdPartyInspectorsName
					// length1: $scope.measurements.length1,
					// length2: $scope.measurements.length2,
					// length3: $scope.measurements.length3,
					// length4: $scope.measurements.length4,
					// length5: $scope.measurements.length5,
					// topOdFractionalInches: $scope.measurements.boxOD,
					// midOdFractionalInches: $scope.measurements.midOD,
					// bottomOdFractionalInches: $scope.measurements.pinOD
				}
				var sendToRepair = false;
				if ($scope.thirdPartyInspectorsAcceptance === true){
					data.thirdPartyInspectorsAcceptance = 1;
				} else {
					data.thirdPartyInspectorsAcceptance = 0;
				}
				if ($scope.bodyCracks === "No") {
					data.doesBodyHaveCracks = 0;
					if (sendToRepair === false) {
						sendToRepair = false;
					}
				} else {
					data.doesBodyHaveCracks = 1;
					sendToRepair = true;
				}

				if ($scope.boxCut === "No") {
					data.doesBoxConnectionNeedRecut = 0;
					if (sendToRepair === false) {
						sendToRepair = false;
					}
				} else {
					data.doesBoxConnectionNeedRecut = 1;
					sendToRepair = true;
				}

				if ($scope.boreBack === "No") {
					data.doesBoxHaveBoreBack = 0;
					sendToRepair = true;
				} else {
					data.doesBoxHaveBoreBack = 1;
					if (sendToRepair === false) {
						sendToRepair = false;
					}
				}

				if ($scope.pinCut === "No") {
					data.doesPinConnectionNeedRecut = 0;
					if (sendToRepair === false) {
						sendToRepair = false;
					}
				} else {
					data.doesPinConnectionNeedRecut = 1;
					sendToRepair = true;
				}

				if ($scope.reliefGroove === "No") {
					data.doesPinHaveReliefGroove = 0;
					sendToRepair = true;
				} else {
					data.doesPinHaveReliefGroove = 1;
					if (sendToRepair === false) {
						sendToRepair = false;
					}
				}
				
				if ($scope.ancillaryId) {
					if ($scope.selectedTools[0].toolFamilyId === 1) {
						data.ancillaryDnrId = $scope.ancillaryId;
					} else if ($scope.selectedTools[0].toolFamilyId === 2) {
						data.ancillaryVStreamId = $scope.ancillaryId;
					}
				}
				debugger;
			} else if($scope.selectedTools[0].toolFamilyId === 2) {
				var data = {
					repairId: $scope.selectedTools[0].repairId,
					thirdPartyInspectorCompany: $scope.thirdPartyInspectorCompany,
					thirdPartyInspectionNumber: $scope.thirdPartyInspectionNumber,
					toolFamilyId: $scope.selectedTools[0].toolFamilyId,
					// length1: $scope.measurements.length1,
					// length2: $scope.measurements.length2,
					// length3: $scope.measurements.length3,
					// topOdFractionalInches: $scope.measurements.boxOD,
					// midOdFractionalInches: $scope.measurements.midOD,
					// bottomOdFractionalInches: $scope.measurements.pinOD
				}
			}

			if ($scope.ancillaryId) {
				if ($scope.selectedTools[0].toolFamilyId === 1) {
					data.ancillaryDnrId = $scope.ancillaryId;
				} else if ($scope.selectedTools[0].toolFamilyId === 2) {
					data.ancillaryVStreamId = $scope.ancillaryId;
				}
			}

		}
		debugger;
		if(sendToRepair === true){
			$scope.$parent.sendInForRepair = true;
			toastr.error('Please send to nearest repair center.')
		} else {
			toastr.info('Please continue the evaluation process.')
		}
		var url = Global.baseUrl + 'dullgradelevelone/saveThirdPartyInspection'
		$scope.gPost(url, data, function(err,data) {
			if (err) toastr.error("There was a problem saving third party inspection info.");
			else {
				toastr.success("Third party inspection info was saved correctly.");
				if (moveToNextStation) {
					$scope.$parent.pics = [];
					// $scope.$parent.pictures = [{
					// 	url: '',
					// 	name: 'Box - Blade 1',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Box - Blade 2',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Box - Blade 3',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Box - Blade 4',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Box - Blade 5',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Box - Blade 6',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Pin - Blade 1',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Pin - Blade 2',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Pin - Blade 3',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Pin - Blade 4',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Pin - Blade 5',
					// 	picTaken: false
					// }, {
					// 	url: '',
					// 	name: 'Pin - Blade 6',
					// 	picTaken: false
					// }];

					$scope.$parent.thirdPartyInspectionImage = {
						url: '',
						name: 'DS1 Inspection Image',
						picTaken: false
					};

					$scope.$parent.thirdPartyInspectionAcknowledgement = {
						url: '',
						name: 'DS1 Inspection Acknowledgement Image',
						picTaken: false
					};

					$scope.$parent.billOfLadingImage = {
						url: '',
						name: 'Bill Of Lading Image',
						picTaken: false
					};
					angular.forEach($scope.selectedTools, function(t){
						$scope.getFlowRelationshipForCurrentFlow(t.flowId, false, null, null, null, t);
					});
					
					$('#thirdPartyInspectionImage').modal('hide');
				} else {
					$('#thirdPartyInspectionImage').modal('hide');
					$('#bolImage').modal('hide');
					delete $scope.$parent.thirdPartyInspectionNumber;
					delete $scope.$parent.thirdPartyInspectorCompany;
					$scope.measurements = [];
					angular.forEach($scope.selectedTools, function (tool) {
						$scope.removeToolFromSelectedTools(tool, $scope.stationRecords);
					});
					$scope.getFlowRecords();
				}
			}
		});
	};

	$scope.finishedTakingBOLImage = function () {
		debugger;
		$('#bolImage').modal('hide');
		angular.forEach($scope.selectedTools, function (tool) {
			$scope.removeToolFromSelectedTools(tool, $scope.stationRecords);
		});
		$scope.getFlowRecords();
	};
}]);

app.directive('accessCamera', function() {
	return {
		restrict: 'E', // directive is an Element, not an Attribute
		scope: false, // uses parent scope
		template: // replacement HTML (put scope vars here if you have them)
		'<input id="cameraInput" class="form-control" type="file" accept="*" name="cameraInput" />',
		replace: true, // replace original markup with template
		transclude: false, // do not copy original HTML content
		link: function($scope, $element, $attr) {
			$('#' + $element[0].id).change(function(e) {
				let reader = new FileReader();

				// RESIZE AND UPLOAD
				reader.onload = function(e) {
					debugger
					let img = new Image();
					img.onload = function() {
						debugger
						$('#captureImg').show();
						$('#captureImg').attr('src', e.target.result);

						let canvas = document.createElement('canvas')
						let ctx = canvas.getContext('2d')
						ctx.drawImage(img, 0, 0)

						const MAX_WIDTH = 1200
						const MAX_HEIGHT = 1000
						let width = img.width
						let height = img.height
						if (width > height) {
							if (width > MAX_WIDTH) {
								height *= MAX_WIDTH / width;
								width = MAX_WIDTH;
							}
						} else {
							if (height > MAX_HEIGHT) {
								width *= MAX_HEIGHT / height;
								height = MAX_HEIGHT;
							}
						}
						canvas.width = width
						canvas.height = height
						ctx = canvas.getContext('2d')
						ctx.drawImage(img, 0, 0, width, height)

						let dataurl = canvas.toDataURL('image/jpeg')
						$('#captureImg').attr('src', dataurl);
					}
					img.src = e.target.result
				}

				debugger
				reader.readAsDataURL($('#cameraInput')[0].files[0]);

				//$scope.$parent.imageCapture = $(this).val();
				$('#confirmSavePicture').attr('disabled', false);
			});

			$('#confirmSavePicture').click(function() {
				// first create image record
				$('#confirmSavePicture').attr('disabled', true);
				if ($scope.currentSelectedImage.documentId) {
					var sImg = $scope.currentSelectedImage;

					var xhr = new XMLHttpRequest();
					var baseUrl = $scope.baseUrl;
					xhr.open('POST', baseUrl + 'dullgradelevelone/saveImage', true);

					var filename = sImg.documentId + '.' + sImg.fileExtension;

					var formData = new FormData();
					formData.append('filename', filename);

					formData.append('myfile', $('#cameraInput')[0].files[0]);


					xhr.onload = function(e) {
						if (this.status === 200) {
							console.log('The image has been updated successfully.');
							$('#modalTakePicture').modal('hide');
							toastr.success('The image has been updated successfully.');

							$scope.getPicturesForThisRepair($scope.selectedTools[0].repairId);
						} else {
							toastr.error('There was a problem trying to save the picture.');
						}
					};

					xhr.send(formData);
				} else {
					debugger;
					if($scope.selectedTools.length > 1){
						var imgRecord = [];
						angular.forEach($scope.selectedTools, function (tool) {
							if(tool.repairId){
								imgRecord.push({
									documentTypeId: $scope.documentTypeId,
									documentLocation: $scope.documentLocation,
									name: 'multiple tools' + ' | ' + $scope.currentSelectedImage.name + ' | multiple repairs ',
									originalDocumentName: $('#cameraInput')[0].files[0].name,
									fileExtension: getFileExtension($('#cameraInput')[0].files[0].name),
									repairId: tool.repairId
								});
							} else {
								imgRecord.push({
									documentTypeId: $scope.documentTypeId,
									documentLocation: $scope.documentLocation,
									name: 'multiple tools' + ' | ' + $scope.currentSelectedImage.name,
									originalDocumentName: $('#cameraInput')[0].files[0].name,
									fileExtension: getFileExtension($('#cameraInput')[0].files[0].name),
									toolId: tool.toolId
								});
							}
						});
					} else {
						if($scope.selectedTools[0].repairId){
							var imgRecord = {
								documentTypeId: $scope.documentTypeId,
								documentLocation: $scope.documentLocation,
								name: $scope.selectedTools[0].serial + ' | ' + $scope.currentSelectedImage.name + ' | RepairId: ' + $scope.selectedTools[0].repairId,
								originalDocumentName: $('#cameraInput')[0].files[0].name,
								fileExtension: getFileExtension($('#cameraInput')[0].files[0].name),
								repairId: $scope.selectedTools[0].repairId
							};
						} else {
							var imgRecord = {
								documentTypeId: $scope.documentTypeId,
								documentLocation: $scope.documentLocation,
								name: $scope.selectedTools[0].serial + ' | ' + $scope.currentSelectedImage.name + ' | ToolId: ' + $scope.selectedTools[0].toolId,
								originalDocumentName: $('#cameraInput')[0].files[0].name,
								fileExtension: getFileExtension($('#cameraInput')[0].files[0].name),
								toolId: $scope.selectedTools[0].toolId
							};
						}
					}
					debugger;
					$scope.grantCsrfToken().then(token => {
						$scope.createImageRecord(imgRecord, function (data) {
							debugger;
							var xhr = new XMLHttpRequest();
							var baseUrl = $scope.baseUrl;
							xhr.open('POST', baseUrl + 'dullgradelevelone/saveImage', true);
							xhr.setRequestHeader('x-csrf-token', token);

							var filename = data.documentId + '.' + data.fileExtension;

							var formData = new FormData();
							formData.append('filename', filename);
							formData.append('dirName', data.documentLocation)
							formData.append('fileExtension', data.fileExtension)
							formData.append('myfile', $('#cameraInput')[0].files[0]);

							xhr.onload = function (e) {
								if (this.status === 200) {
									console.log('image saved successfully');
									$('#modalTakePicture').modal('hide');
									toastr.success('The image was saved successfully.');
									debugger;
									if ($scope.selectedTools[0].repairId && !$scope.forEvaluation) {
										$scope.getPicturesForThisRepair($scope.selectedTools[0].repairId);
									} else {
										$scope.$broadcast('imageSaved');
									}
								} else {
									toastr.error('There was a problem trying to save the picture.');
								}
							};

							xhr.send(formData);
						});
					})
				}
			});
		}
	};
});

function getFileExtension(filename) {
	var arr = filename.split('.');
	if (arr.length <= 1) {
		return arr[0];
	} else {
		return arr[arr.length - 1];
	}
}
