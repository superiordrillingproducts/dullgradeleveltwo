app.controller('NgDNRKilnController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', '$route', function ($http, $scope, $location, Global, $filter, $sails, $route) {
	function init () {
		// configuration settings
		$scope.$parent.maxUsers = 2;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = true;
		$scope.$parent.showTimer = true;
		$scope.getFlowRecords();
		$scope.currentIndex = 0;
		$scope.showMenu = true;
		$scope.$parent.isLatestDullGrade = true;
		$scope.$parent.showCompatibleCuttersInKiln = true;
		$scope.selectedGroup = 0;
		$scope.$parent.moveToDnrKilnAndStartTime = true;
	}

	$scope.$on('getFlowRecordsRan', function () {
		$scope.$parent.finishedAddingTools = true;
		$scope.kilnHomeFlows = [];
		if ($scope.workStation.facilityId === 2) {
			kilnHomeFlows = [64, 65]
		} else if ($scope.workStation.facilityId === 6) {
			kilnHomeFlows = [93, 94]
		} else if ($scope.workStation.facilityId === 7) {
			kilnHomeFlows = [110]
		}
		$scope.getFlowsWithToolRepairs(kilnHomeFlows);
		$scope.$parent.finishedAddingTools = true;
		$scope.getCountOfCutterSizeByAction();
		$scope.getCountOfFlameSpray();
		getAllCutterInformation();
	});

	function getAllCutterInformation (cb) {
		$scope.getDullGradeAndHashStructureForCurrentRepair(function() {
			debugger;
			$scope.getCutters(function() {
				$scope.setColorClasses($scope.$parent.groups, true, function() {
					debugger;
					$scope.getCutterNamesUsedInRepairForDullgrade(function() {
						debugger;
						$scope.setColorsOnClearoutLegend($scope.$parent.cutterNames, $scope.activeColorClasses, function(cutterNames) {
							$scope.isThereAnyCuttersThatNeedLotNumbers(false, function(){
								debugger;
								var adjustedCutterNames = [];
								angular.forEach(cutterNames, function (cn, index) {
									if (index === 0) {
										if (cn.replacedCutterId !== null) {
											adjustedCutterNames.push({
												cutterId: cn.replacedCutterId,
												cutterName: cn.replacedCutterName,
												count: cn.count,
												colorClass: cn.colorClass
											});
										} else {
											adjustedCutterNames.push({
												cutterId: cn.cutterId,
												cutterName: cn.cutterName,
												count: cn.count,
												colorClass: cn.colorClass
											});
										}
									} else {
										var found = false;
										angular.forEach(adjustedCutterNames, function (acn, index2) {
											if (found === false) {
												if (cn.replacedCutterId !== null) {
													if (cn.replacedCutterId === acn.cutterId) {
														found = true;
														acn.count = acn.count + cn.count;
														if (!acn.colorClass) {
															acn.colorClass = cn.colorClass;
														}
													} else {
														if (adjustedCutterNames.length === index2 + 1) {
															adjustedCutterNames.push({
																cutterId: cn.replacedCutterId,
																cutterName: cn.replacedCutterName,
																count: cn.count,
																colorClass: cn.colorClass
															});
														}
													}
												} else {
													if (cn.cutterId === acn.cutterId) {
														found = true;
														acn.count = acn.count + cn.count;
														if (!acn.colorClass) {
															acn.colorClass = cn.colorClass;
														}
													} else {
														if (adjustedCutterNames.length === index2 + 1) {
															adjustedCutterNames.push({
																cutterId: cn.cutterId,
																cutterName: cn.cutterName,
																count: cn.count,
																colorClass: cn.colorClass
															});
														}
													}
												}
											}
										});
									}
								});
								debugger;
								if (cb) cb();
							});
						});
					});
				});
			});
		});
	};

	$scope.stop = function (notCompleteInStation) {
		var dnrHomeWorkStation = {workStationId: 25};
		if(notCompleteInStation){
			var dnrHomeFlow;
			if ($scope.selectedTools[0].toolFamilyId === 1 && $scope.workStation.facilityId === 2){
				dnrHomeFlow = 64
			} else if ($scope.selectedTools[0].toolFamilyId === 2 && $scope.workStation.facilityId === 2){
				dnrHomeFlow = 65
			} else if ($scope.selectedTools[0].toolFamilyId === 1 && $scope.workStation.facilityId === 6){
				dnrHomeFlow = 93
			} else if ($scope.selectedTools[0].toolFamilyId === 2 && $scope.workStation.facilityId === 6) {
				dnrHomeFlow = 94
			} else if ($scope.selectedTools[0].toolFamilyId === 1 && $scope.workStation.facilityId === 7) {
				dnrHomeFlow = 110
			}
			$scope.nextFlowSelectedWithoutTimestamp({nextFlowId: dnrHomeFlow}, null, function(returnData){
				if(returnData){
					$scope.goToDnrKilnHome(dnrHomeWorkStation, function() {
						$scope.callStartStopLaborTracking(notCompleteInStation);
					})
				}
			})
		} else {
			$scope.callStartStopLaborTracking(false, function() {
				$scope.goToDnrKilnHome(dnrHomeWorkStation, function() {
					$scope.$parent.selectedTools = [];
					$scope.$parent.finishedAddingTools = false;
					$scope.$parent.getFlowRecords();
				});
			});
		}
	};

	init();

	$scope.userLogout = function () {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};

	$scope.rotateDNR = function (left, right, index) {
		debugger;
		if (left === true) {
			$scope.left = true;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.groups[$scope.selectedGroup].blades.length - 1;
			} else {
				$scope.currentIndex = $scope.currentIndex - 1;
			}
		}
		if (right === true) {
			$scope.left = false;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else if ($scope.currentIndex + 1 < $scope.groups[$scope.selectedGroup].blades.length) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else {
				$scope.currentIndex = 0;
			}
		}
	};

	$scope.cutterSize = function (row) {
		var style;
		if (row.pockets.length <= 10) {
			style = {'width': '90px', 'height': '90px'};
			$scope.pocketFont = {'font-size': '25px'};
			$scope.exclamationiconkiln = {
				'margin-left': '36px',
				'width': '13px',
				'height': '13px',
				'border-radius':'10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.9em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		} else if (row.pockets.length <= 16) {
			style = {'width': '50px', 'height': '50px'};
			$scope.pocketFont = {'font-size': '15px'};
			$scope.exclamationiconkiln = {
				'margin-left': '17px',
				'width': '11px',
				'height': '10px',
				'border-radius':'10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.7em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		} else {
			style = {'width': '35px', 'height': '50px'};
			$scope.pocketFont = {'font-size': '12px'};
			$scope.exclamationiconkiln = {
				'margin-left': '17px',
				'width': '13px',
				'height': '13px',
				'border-radius':'10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.9em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		}
	};

	$scope.pocketNumbering = function (row) {
		var style;
		if (row.pockets.length <= 10) {
			style = {
				'margin-left': '37px',
				'margin-top': '-3px',
				'position': 'fixed'
			}	
			return style;
		} else if (row.pockets.length <= 16) {
			style = {
				'margin-left': '18px',
				'margin-top': '-6px',
				'position': 'fixed',
				'font-size': '12px'
			}
			return style;
		} else {
			style = {
				'margin-left': '17px',
				'margin-top': '-7px',
				'position': 'fixed'
			}
			return style;
		}
	};

	$scope.$on('reloadCutterDullGradeAndCutterNames', function () {
		$('.modal').modal('hide');
		getAllCutterInformation(function () {
			_.each($scope.dullGrade.cutterNames, function(cn) {
				if(cn.markedForLotNumber === true) {
					$('#showCuttersUsedInRepair').modal('show');
				}
			});
		});
	});

	$scope.changeGroup = function (group) {
		$scope.selectedGroup = group;
		$scope.currentIndex = 0;
	}

	$scope.goToKiln = function (kiln) {
		var url = Global.baseUrl + 'User/setfacilityandworkstation';
		var data = {
			workstation: kiln.workStationId,
			facility: $scope.$parent.workStation.facilityId
		};
		$http.post(url, data).success(function(success) {
			$('#modalViewKilns').modal('hide');
			var goToUrl = '/dnrKiln';
			setTimeout(function() {
				$location.path(goToUrl);
				$scope.getStationUsers();
				$scope.$apply();
			}, 500);
		}).error(function(err){
			toastr.error('There was a problem choosing a kiln.');
		});
	};
}]);

app.directive('rotatednrkeypress', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: true,
		link: function postLink(scope, iElement, iAttrs) {

			jQuery(document).on('keypress', function(e) {
				if(e.charCode === 45){
					var left = true;
				} else if (e.charCode === 61){
					var right = true;
				}
				if(left === true || right === true){
					scope.$apply(scope.rotateDNR(left, right));
				}
			});
		}
	};
});
