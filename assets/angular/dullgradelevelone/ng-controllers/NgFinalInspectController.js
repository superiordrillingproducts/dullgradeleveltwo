app.controller('NgFinalInspectController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', 'Idle', 'Keepalive', '$timeout', function($http, $scope, $location, Global, $filter, $sails, Idle, Keepalive, $timeout) {
	$scope.$parent.maxUsers = 1;
	$scope.$parent.showBitLocator = true;
	$scope.getFlowRecords();

	$scope.$on('selectedTools:updated', function() {
		$scope.$parent.finishedAddingTools = true;
	});
}]);
