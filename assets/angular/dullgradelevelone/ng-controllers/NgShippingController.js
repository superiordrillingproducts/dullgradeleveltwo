app.controller('NgShippingController', ['$scope', '$http', '$location', 'Global','Helper', '$filter', '$route', function($scope, $http, $location, Global, Helper, $filter, $route) {

	function init() {
		$scope.$parent.finishedAddingTools = false;
		$scope.dnrShippingClicked = false;
		$scope.$parent.selectedTools = [];
		$scope.getFlowRecords();
		$scope.getDNRShipToStockPoints();
		$scope.getUserStockPoints();
		$scope.$parent.pics = [];
		$scope.inEvaluation = true;
		$scope.inStock = false;
		// set max date for arrival time
		$scope.maxArrivalDate = Helper.getTodayDateInJsStringForm();

		$scope.$parent.DNRShippingImage = {
			url: '',
			name: 'Shipping Document Image',
			picTaken: false
		};

		$scope.$parent.PackingListImage = {
			url: '',
			name: 'Packing List Image',
			picTaken: false
		};
	}
	init();

	$scope.init = function (){
		init();
	};

	$scope.openModalAddNewBillOfLading = function() {
		$scope.filteredShipToStockPoints = [];
		angular.forEach($scope.userStockPoints, function (usp) {
			var filtered;
			filtered = ( $filter('filter')($scope.DNRShipStockPoints, function (dssp) {
				return dssp.stockPointId === usp.stockPointId;
			}));
			$scope.filteredShipToStockPoints.push(filtered[0]);
		});
		var date = new Date();
		$scope.DNRShipDate = date;
		$('#addNewBillOfLading').modal('show');
	};

	$scope.openModalTakePicture = function(currentPicture) {
		$scope.$parent.currentSelectedImage = currentPicture;
		$scope.$parent.documentTypeId = 6;
		$scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/shippingdocuments';
		//$scope.$parent.documentLocation = '/Users/home/Documents/sdpidocuments';
		$('#captureImg').attr('src', '');
		$('#cameraInput').val('');
		$('#captureImg').hide();
		$('#modalTakePicture').modal('show');
	};

	$scope.saveDNRShippingInfo = function() {
		angular.forEach($scope.selectedTools, function(st){
			delete st.moreInfo;
		});
		$scope.dnrShippingClicked = true;
		var url = $scope.baseUrl + 'dullgradelevelone/saveDNRShippingInfo';
		var data = {
			selectedTools: $scope.selectedTools,

			repairInfo: {
				transitServiceShippedBy: $scope.DNRShipTransitService,
				dateToolShipped: moment($scope.DNRShipDate).format("YYYY-MM-DD HH:mm:ss"),
				repairStatusId: 1,
				stockPointShipToId: $scope.selectedDNRShipToStockPoint.stockPointId,

			},
			toolInfo: {
				toolStatusId: 22
			}
		};
		$scope.gPost(url, data, function (err, success) {
			if (err) toastr.error("There was a problem saving shipping info and closing the repair.");
			else {
				$scope.DNRShipTransitService = "";
				if ($scope.selectedTools.length > 1) {
					var count = 0;
					if($scope.selectedTools[0].repairId){
						angular.forEach($scope.selectedTools, function (st) {
							$scope.getFlowRelationshipForCurrentFlow(st.flowId, false, null, function (returned) {
								count++;
								if ($scope.selectedTools.length === count) {
									$('#addNewBillOfLading').modal('hide');
									init();
								}
							}, st);
						});
					} else {
						$('#addNewBillOfLading').modal('hide');
						init();
					}
				} else {
					if($scope.selectedTools[0].repairId){
						$scope.getFlowRelationshipForCurrentFlow($scope.selectedTools[0].flowId, false, null, function (returned) {
							$('#addNewBillOfLading').modal('hide');
							init();
						});
					} else {
						$('#addNewBillOfLading').modal('hide');
						init();
					}
				}
			}
		});
	};

	$scope.openModalMoveToStock = function() {
		var date = new Date();
		$scope.DNRShipDate = date;
		$scope.filteredShipToStockPoints = [];
		angular.forEach($scope.userStockPoints, function (usp) {
			var filtered;
			filtered = ($filter('filter')($scope.DNRShipStockPoints, function (dssp) {
				return dssp.stockPointId === usp.stockPointId;
			}));
			$scope.filteredShipToStockPoints.push(filtered[0]);
		});
		$('#moveToStock').modal('show');
	};

	$scope.moveToStock = function(){
		$scope.dnrShippingClicked = true;
		var url = $scope.baseUrl + 'dullgradelevelone/saveDNRShippingInfo';
		var data = {
			selectedTools: $scope.selectedTools,

			repairInfo: {
				dateToolShipped: moment($scope.DNRShipDate).format("YYYY-MM-DD HH:mm:ss"),
				repairStatusId: 1,
				stockPointShipToId: $scope.selectedDNRShipToStockPoint.stockPointId,

			},
			toolInfo: {
				toolStatusId: 22
			}
		};
		$scope.gPost(url, data, function (err, success) {
			if (err) toastr.error("There was a problem saving shipping info and closing the repair.");
			else {
				$scope.DNRShipTransitService = "";
				if ($scope.selectedTools.length > 1) {
					var count = 0;
					angular.forEach($scope.selectedTools, function (st) {
						$scope.getFlowRelationshipForCurrentFlow(st.flowId, false, null, null, function (returned) {
							count++;
							if ($scope.selectedTools.length === count) {
								$('#moveToStock').modal('hide');
								init();
							}
						}, st);
					});
				} else {
					$scope.getFlowRelationshipForCurrentFlow($scope.selectedTools[0].flowId, false, null, null, function (returned) {
						$('#moveToStock').modal('hide');
						init();
					});
				}
			}
		});
	};

	$scope.filterTools = function(showStock){
		if(showStock === true){
			$scope.inStock = true;
			$scope.inEvaluation = false;
		} else {
			$scope.inStock = false;
			$scope.inEvaluation = true;
		}
	};

	$scope.$on('getFlowRecordsRan', function () {
		$scope.getReceivingTools();
	});

	$scope.$on('getReceivingToolsRan',function(){
		$scope.toolsInStock = [];
		angular.forEach($scope.tools,function(t){
			if ($scope.workStation.facilityId === 3) {
				if(t.stockPointId === 25){
					$scope.toolsInStock.push(t);
				}
			} else if ($scope.workStation.facilityId === 4) {
				if (t.stockPointId === 29) {
					$scope.toolsInStock.push(t);
				}
			} else if ($scope.workStation.facilityId === 5) {
				if (t.stockPointId === 27) {
					$scope.toolsInStock.push(t);
				}
			} else if ($scope.workStation.facilityId === 8) {
				if (t.stockPointId === 31) {
					$scope.toolsInStock.push(t);
				}
			} else if ($scope.workStation.facilityId === 9) {
				if (t.stockPointId === 33) {
					$scope.toolsInStock.push(t);
				}
			} else if ($scope.workStation.facilityId === 10) {
				if (t.stockPointId === 22) {
					$scope.toolsInStock.push(t);
				}
			}
		});
	});

	$scope.$on('imageSaved',function(){
		debugger;
		if ($scope.$parent.currentSelectedImage.name === $scope.DNRShippingImage.name) {
			$scope.$apply(function () {
				$scope.$parent.DNRShippingImage.picTaken = true;
			});
		} else if ($scope.$parent.currentSelectedImage.name === $scope.PackingListImage.name) {
			$scope.$apply(function () {
				$scope.$parent.PackingListImage.picTaken = true;
			});	
		}
	});

	$scope.$on('getPicturesForRepairRan', function () {
		debugger;
		if ($scope.$parent.currentSelectedImage.name === $scope.DNRShippingImage.name) {
			$scope.$parent.DNRShippingImage.picTaken = true;
		} else if ($scope.$parent.currentSelectedImage.name === $scope.PackingListImage.name) {
			$scope.$parent.PackingListImage.picTaken = true;
		}
	});

	$scope.$on('finishedAddingTools', function (){
		if($scope.inEvaluation){
			angular.forEach($scope.selectedTools, function (st) {
				st.moreInfo = _.find($scope.tools, function (t) {
					return t.toolId === st.toolId;
				});
			})
		}
	});

	$scope.printStrapSheet = function (selectedTool) {
		debugger;
		if(selectedTool.moreInfo) {
			var sendTool = JSON.parse(JSON.stringify(selectedTool));
			delete sendTool.moreInfo;
		} else {
			var sendTool = selectedTool;
		}
		var newTab = window.open();
		var url = Global.baseUrl + 'dullgradelevelone/printDNRStrapSheet';
		$scope.gPost(url, sendTool, function (err, returned) {
				var file = new Blob([returned], {
					type: 'application/pdf'
				});
				var fileUrl = URL.createObjectURL(file);
				newTab.location = fileUrl;
		}, {
			responseType: 'arraybuffer'
		});
	};

	$scope.getHeatCert = function (tool) {
		debugger;
		var url = $scope.baseUrl + 'dullgradelevelone/getHeatCert';
		if(tool.documentLocation){
			var data = {
				documentLocation: tool.documentLocation,
				fileExtension: tool.fileExtension
			}
			if (tool.documentId) {
				data.documentId = tool.documentId
			} else {
				data.documentId = tool.documentIdIfNoSteelID
			}
		} else {
			var data = {
				documentLocation: tool.moreInfo.documentLocation,
				fileExtension: tool.moreInfo.fileExtension
			}
			if (tool.moreInfo.documentId) {
				data.documentId = tool.moreInfo.documentId
			} else {
				data.documentId = tool.moreInfo.documentIdIfNoSteelID
			}
		}
		var newTab = window.open();
		$scope.gPost(url, data, function (err, heatCert){
			debugger;
			if (err) toastr.error("'Failed to open heat cert.'");
			else {
				var file = new Blob([heatCert], {
					type: 'application/pdf'
				});
				if (window.navigator && window.navigator.msSaveOrOpenBlob) {
					window.navigator.msSaveOrOpenBlob(file);
				} else {
					var fileUrl = URL.createObjectURL(file);
					newTab.location = fileUrl;
				}
			}
		},{
			responseType: 'arraybuffer'
		});
	};

	$scope.getVTI = function (tool) {
		debugger;
		var url = Global.baseUrl + 'dullGradeLevelOne/getVTI'
		let data;
		if (tool.moreInfo) {
			data = {
				documentName: tool.moreInfo.VTIDocumentId + '.' + tool.moreInfo.VTIFileExtension,
				documentLocation: tool.moreInfo.VTIDocumentLocation
			};
		} else {
			data = {
				documentName: tool.VTIDocumentId + '.' + tool.VTIFileExtension,
				documentLocation: tool.VTIDocumentLocation
			};
		}
		var newTab = window.open();
		$scope.gPost(url, data, function(err, doc){
			if (err) toastr.error('Failed to open document.');
			else {
				if(tool.moreInfo){
					if (tool.moreInfo.VTIFileExtension === 'jpg') {
						var file = new Blob([doc], {
							type: 'image/jpeg'
						});
						if (window.navigator && window.navigator.msSaveOrOpenBlob) {
							window.navigator.msSaveOrOpenBlob(file);
						} else {
							var fileUrl = URL.createObjectURL(file);
							newTab.location = fileUrl;
						}
					} else {
						var file = new Blob([doc], {
							type: 'application/pdf'
						});
						if (window.navigator && window.navigator.msSaveOrOpenBlob) {
							window.navigator.msSaveOrOpenBlob(file);
						} else {
							var fileUrl = URL.createObjectURL(file);
							newTab.location = fileUrl;
						}
					}
				} else {
					if (tool.VTIFileExtension === 'jpg') {
						var file = new Blob([doc], {
							type: 'image/jpeg'
						});
						if (window.navigator && window.navigator.msSaveOrOpenBlob) {
							window.navigator.msSaveOrOpenBlob(file);
						} else {
							var fileUrl = URL.createObjectURL(file);
							newTab.location = fileUrl;
						}
					} else {
						var file = new Blob([doc], {
							type: 'application/pdf'
						});
						if (window.navigator && window.navigator.msSaveOrOpenBlob) {
							window.navigator.msSaveOrOpenBlob(file);
						} else {
							var fileUrl = URL.createObjectURL(file);
							newTab.location = fileUrl;
						}
					}	
				} 
			}
		},{
				responseType: 'arraybuffer'
		});
	};
}]);

app.directive('accessCamera2', function() {
	return {
		restrict: 'E', // directive is an Element, not an Attribute
		scope: false, // uses parent scope
		template: // replacement HTML (put scope vars here if you have them)
		'<input id="cameraInput2" class="form-control" type="file" accept="image/*" capture="camera" name="cameraInput2" />',
		replace: true, // replace original markup with template
		transclude: false, // do not copy original HTML content
		link: function($scope, $element, $attr) {
			$('#' + $element[0].id).change(function(e) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#captureImg2').show();
					$('#captureImg2').attr('src', e.target.result);
				}
				reader.readAsDataURL($('#cameraInput2')[0].files[0]);
				$scope.DNRorignalDocumentName = $('#cameraInput2')[0].files[0].name;
				$scope.DNRFileExtension = getFileExtension($('#cameraInput2')[0].files[0].name);
				$scope.DNRoriginalFile = $('#cameraInput2')[0].files[0];
			});
		}
	};
});

function saveDNRShippingImage(data, imgRecord, baseUrl, myFile, token, cb) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', baseUrl + 'dullgradelevelone/saveImage', true);
	xhr.setRequestHeader('x-csrf-token', token);

	var filename = data.documentId + '.' + imgRecord.fileExtension;

	var formData = new FormData();
	formData.append('filename', filename);
	formData.append('dirName', imgRecord.documentLocation);
	formData.append('fileExtension', imgRecord.fileExtension);
	formData.append('myfile', myFile);

	xhr.onload = function (e) {
		if (this.status === 200) {
			console.log('image saved successfully');
			if (cb) cb();
			toastr.success('The image was saved successfully.');
		} else {
			toastr.error('There was a problem trying to save the picture.');
		}
	};

	xhr.send(formData);
}


