app.controller('NgKilnController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', '$route', function ($http, $scope, $location, Global, $filter, $sails, $route) {
	function init () {
		// configuration settings
		$scope.$parent.maxUsers = 2;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = true;
		$scope.$parent.showTimer = true;
		$scope.getFlowRecords();
		$scope.currentIndex = 0;
		$scope.showMenu = true;
		$scope.$parent.isLatestDullGrade = true;
		$scope.$parent.showBitLocator = true;
		$scope.$parent.showCompatibleCuttersInKiln = true;
	}

	$scope.$on('getFlowRecordsRan', function () {
		$scope.$parent.finishedAddingTools = true;
		$scope.getCountOfCutterSizeByAction();
		$scope.getCountOfFlameSpray();
		getAllCutterInformation();
	});

	function getAllCutterInformation (cb) {
		$scope.getDullGradeStructureForCurrentRepair(function() {
			debugger;
			$scope.$parent.selectedDullGrade = $scope.dullGrade;
			$scope.getCutters(function() {
				angular.forEach($scope.dullgrades, function(dg) {
					$scope.$parent.dullGrade = dg;
					$scope.setColorClasses(dg.groups, true, function() {
						$scope.getCutterNamesUsedInRepairForDullgrade(function() {
							$scope.setColorsOnClearoutLegend(dg.cutterNames, $scope.activeColorClasses, function(cutterNames) {
								$scope.isThereAnyCuttersThatNeedLotNumbers();
								var adjustedCutterNames = [];
								angular.forEach(cutterNames, function(cn, index) {
									if (index === 0) {
										if (cn.replacedCutterId !== null) {
											adjustedCutterNames.push({cutterId: cn.replacedCutterId, cutterName: cn.replacedCutterName, count: cn.count, colorClass: cn.colorClass});
										} else {
											adjustedCutterNames.push({cutterId: cn.cutterId, cutterName: cn.cutterName, count: cn.count, colorClass: cn.colorClass});
										}
									} else {
										var found = false;
										angular.forEach(adjustedCutterNames, function(acn, index2) {
											if (found === false) {
												if (cn.replacedCutterId !== null) {
													if (cn.replacedCutterId === acn.cutterId) {
														found = true;
														acn.count = acn.count + cn.count;
														if(!acn.colorClass) {
															acn.colorClass = cn.colorClass;
														}
													} else {
														if (adjustedCutterNames.length === index2 + 1 ) {
															adjustedCutterNames.push({cutterId: cn.replacedCutterId, cutterName: cn.replacedCutterName, count: cn.count, colorClass: cn.colorClass});
														}
													}
												} else {
													if (cn.cutterId === acn.cutterId) {
														found = true;
														acn.count = acn.count + cn.count;
														if(!acn.colorClass) {
															acn.colorClass = cn.colorClass;
														}
													} else {
														if (adjustedCutterNames.length === index2 + 1 ) {
															adjustedCutterNames.push({cutterId: cn.cutterId, cutterName: cn.cutterName, count: cn.count, colorClass: cn.colorClass});
														}
													}
												}
											}
										});
									}
								});
								debugger;
								if(cb) cb();
							});
						});
					});
				});
			});
		});
	};

	$scope.stop = function (notCompleteInStation) {
		$scope.callStartStopLaborTracking(notCompleteInStation);
		if(notCompleteInStation){
			$scope.getStationUsers();
			$scope.$parent.currentAuthenticatedUsers = [];
		}
	};

	init();

	$scope.userLogout = function () {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};

	$scope.rotateBit = function (left, right) {
		if (left === true) {
			$scope.left = true;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.groups[0].blades.length - 1;
			} else {
				$scope.currentIndex = $scope.currentIndex - 1;
			}
		}
		if (right === true) {
			$scope.left = false;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else if ($scope.currentIndex + 1 < $scope.groups[0].blades.length) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else {
				$scope.currentIndex = 0;
			}
		}
	};

	$scope.cutterSize = function (row) {
		var style;
		if (row.pockets.length <= 9) {
			style = {'width': '90px', 'height': '90px'};
			$scope.pocketFont = {'font-size': '25px'};
			$scope.exclamationiconkiln = {
				'margin-left': '36px',
				'width': '13px',
				'height': '13px',
				'border-radius':'10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.9em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		} else if (row.pockets.length <= 16) {
			style = {'width': '50px', 'height': '50px'};
			$scope.pocketFont = {'font-size': '15px'};
			$scope.exclamationiconkiln = {
				'margin-left': '17px',
				'width': '11px',
				'height': '10px',
				'border-radius':'10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.7em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		} else {
			style = {'width': '50px', 'height': '35px'};
			$scope.pocketFont = {'font-size': '15px'};
			$scope.exclamationiconkiln = {
				'margin-left': '17px',
				'width': '13px',
				'height': '13px',
				'border-radius':'10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.9em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		}
	};

	$scope.$on('reloadCutterDullGradeAndCutterNames', function () {
		$('.modal').modal('hide');
		getAllCutterInformation(function () {
			_.each($scope.dullGrade.cutterNames, function(cn) {
				if(cn.markedForLotNumber === true) {
					$('#showCuttersUsedInRepair').modal('show');
				}
			});
		});
	});

	$scope.getAllWorkStationsForFacility = function () {
		var url = Global.baseUrl + 'User/getWorkStationsByFacility';
		$http.get(url).success(function (workStations) {
			$scope.kilnWorkStations = $filter('filter')(workStations, function(ws) {
				return ws.workStationId > 7 && ws.workStationId < 14;
			});
		}).error(function(err) {
			toastr.error('There was a problem getting other kilns.');
		});
	};

	$scope.selectKiln = function () {
		var url = Global.baseUrl + 'User/setfacilityandworkstation';
		var data = {
			workstation: $scope.$parent.selectedKiln.workStationId,
			facility: $scope.$parent.selectedKiln.facilityId
		};
		$http.post(url, data).success(function(success) {
			var goToUrl = $scope.$parent.selectedKiln.url.slice(1);
			setTimeout(function() {
				$location.path(goToUrl);
				$scope.getStationUsers();
				$scope.$apply();
			}, 500);
		}).error(function(err){
			toastr.error('There was a problem choosing a kiln.');
		});
	};

	$scope.getAllWorkStationsForFacility();
}]);

app.directive('rotatebitkeypress', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: true,
		link: function postLink(scope, iElement, iAttrs) {

			jQuery(document).on('keypress', function(e) {
				if(e.charCode === 45){
					var left = true;
				} else if (e.charCode === 61){
					var right = true;
				}
				if(left === true || right === true){
					scope.$apply(scope.rotateBit(left, right));
				}
			});
		}
	};
});
