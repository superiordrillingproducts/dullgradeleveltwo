app.controller('NgReceivingController', ['$http', '$scope', '$location', 'Global', 'Helper', '$filter', function($http, $scope, $location, Global, Helper, $filter) {
	function init() {
		// configuration settings
		$scope.getFlowRecords();
		$scope.$parent.noTimeStamp = true;
		$scope.$parent.finishedAddingTools = false;
		$scope.$parent.forStock = false;
		$scope.$parent.forEvaluation = false;
		$scope.$parent.userTimeoutTimeInSeconds = 300; // 5 minutes

		// set max date for arrival time
		$scope.maxArrivalDate = Helper.getTodayDateInJsStringForm();

		
		$scope.getToolFamilies();

		$scope.$parent.selectedTools = [];
		$scope.toolFamily = undefined;
		$scope.ancillaryInformation = {};

		$scope.$parent.billOfLadingImage = {
			url: '',
			name: 'Bill of Lading Image',
			picTaken: false
		};

		$scope.$parent.packingListImage = {
			url: '',
			name: 'Packing List Image',
			picTaken: false
		};

	}

	$scope.userLogout = function() {
		$scope.removeAllUsers();
	};

	init();

	$scope.init = function(isLogout) {
		$scope.$parent.currentAuthenticatedUsers = [];
		$('#bolImage').modal('hide');
		init();
	};

	$scope.setAncillaryInfo = function() {
		debugger;
		if (typeof $scope.selectedTools !== 'undefined' && $scope.selectedTools && $scope.selectedTools.length > 0) {
			var date = new Date();
			$scope.ancillaryInformation.dateToolArrived = new Date();

			if (!$scope.selectedTools[0].toolId) {
				$scope.toolFamily = _.find($scope.toolFamilies, function(fam) {
					return fam.toolFamilyId === 4;
				});
			} else {
				$scope.toolFamily = _.find($scope.toolFamilies, function(fam) {
					return fam.toolFamilyId === $scope.selectedTools[0].toolFamilyId;
				});
			}
		}
	};

	$scope.$on('getFlowRecordsRan', function(){
		$scope.getReceivingTools([9, 19, 29, 30, 32]); // optional: pass an array of toolStatusIds that are not angu selectable
	});

	$scope.$on('selectedTools:updated', function() {
		debugger;
		console.log('selected tools updated');
		if ($scope.selectedTools.length === 0) {
			toastr.info("This tool is in the process of being repaired.");
			init();
		} else {
			// if($scope.selectedTools[0].cutterPocketTypeId === 3){
			// 	$scope.hasReplaceableDomes = true;
			// 	$('#modelHasReplaceableDomes').modal('show')
			// } else {
			// 	$scope.hasReplaceableDomes = false;
			// }
			$scope.setAncillaryInfo();
		}
	});

	$scope.$on('getReceivingToolsRan', function() {
		if($scope.workStation.facilityId === 2 || $scope.workStation.facilityId === 6 || $scope.workStation.facilityId === 7) {
			$scope.toolsByFacility = $filter('filter')($scope.tools, function(tool) {
				return tool.toolFamilyId === 1 || tool.toolFamilyId === 2;
			})
		} else if ($scope.workStation.facilityId === 1) {
			$scope.toolsByFacility = $filter('filter')($scope.tools, function(tool) {
				return tool.toolFamilyId === 4;
			})
		} else if ([3,4,5,8,9,10].includes($scope.workStation.facilityId)) {
			$scope.toolsByFacility = [];
			// NOTE: For security purposes, we filter the tools by facilityId on the server for all requests with these facility IDs.
			// Thus we can assign all the tools returned without any further filtering.
			angular.forEach($scope.tools, function(t){
				if (t.stockPointId === 36 || t.stockPointId === 31 || t.stockPointId === 37 || t.stockPointId === 25 || t.stockPointId === 38 || t.stockPointId === 39) {
					$scope.toolsByFacility.push(t);
				}
			});
		}
	});

	$scope.$on('currentAuthenticatedUsers:updated', function() {
		console.log('authenticated users updated');
		$scope.setAncillaryInfo();
	});

	$scope.insertTodaysDateInArriving = function() {
		$scope.dateToolArrived = new Date();
	};

	$scope.createRepair = function() {
		debugger;
		if ($scope.selectedTools.length >= 1 && $scope.ancillaryInformation.dateToolArrived && $scope.toolFamily) {
			$scope.saving = true;
			if($scope.forStock){
				var receivingFor = 'stock';
			} else {
				var receivingFor = 'evaluation';
			}
			var url = Global.baseUrl + 'dullgradelevelone/createRepair';
			var body = {
				tool: $scope.selectedTools,
				receivingFor: receivingFor,
				ancillaryInformation: {
					dateToolArrived: moment($scope.ancillaryInformation.dateToolArrived).format("YYYY-MM-DD HH:mm:ss"),
					toolFamilyId: $scope.toolFamily.toolFamilyId
				}
			};
			debugger;
			$scope.gPost(url, body, function(err, data) {
				debugger;
				if(err) toastr.error(err);
				else {
					debugger;
					if($scope.forStock){
						toastr.success('Tool was received for stock.');
						$('#bolImage').modal('hide');
						init();
					} else {
						angular.forEach(data, function (d) {
							angular.forEach($scope.selectedTools, function (st) {
								debugger;
								if (d.toolId === st.toolId) {
									st.repairId = d.repairId;
								}
							});
						});
						toastr.success('Repair Created.');
						$('#bolImage').modal('show');
					}
				}
			});
		} else {
			toastr.warning('There was a problem with your request.');
		}
	};

	$scope.$on('getPicturesForRepairRan',function(){
		debugger;
		$scope.hasReplaceableDomes = false;
		$('#modelHasReplaceableDomes').modal('hide');
		if ($scope.currentUrl) {
			$scope.returnFromReworkToLastUrl();
		} else {
			if ($scope.$parent.currentSelectedImage.name === 'Bill of Lading Image') {
				$scope.$apply(function () {
					$scope.$parent.billOfLadingImage.picTaken = true;
				});
			} else {
				$scope.$apply(function () {
					$scope.$parent.packingListImage.picTaken = true;
				});
			}
		}
		$scope.saving = false;
	});

	$scope.$on('imageSaved', function () {
		debugger;
		$scope.hasReplaceableDomes = false;
		$('#modelHasReplaceableDomes').modal('hide');
		if ($scope.currentUrl) {
			$scope.returnFromReworkToLastUrl();
		} else {
			if ($scope.$parent.currentSelectedImage.name === 'Bill of Lading Image'){
				$scope.$apply(function(){
					$scope.$parent.billOfLadingImage.picTaken = true;
				});
			} else {
				$scope.$apply(function () {
					$scope.$parent.packingListImage.picTaken = true;
				});
			}
		}
		$scope.saving = false;
	});

	$scope.openModalTakePicture = function (currentPicture) {
		debugger;
		$scope.$parent.currentSelectedImage = currentPicture;
		$scope.$parent.documentTypeId = 7;
		 $scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/billofladingreceiveddocuments';
		//$scope.$parent.documentLocation = '/Users/home/Documents/sdpidocuments';

		$('#captureImg').attr('src', '');
		$('#cameraInput').val('');
		$('#captureImg').hide();
		$('#confirmSavePicture').attr('disabled', true);
		$('#modalTakePicture').modal('show');
	};
}]);
