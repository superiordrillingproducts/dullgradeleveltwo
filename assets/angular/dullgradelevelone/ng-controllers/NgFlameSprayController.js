app.controller('NgFlameSprayController', ['$scope', '$http', '$location', 'Global', function($scope, $http, $location, Global) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = false;
		$scope.$parent.showTimer = true;
		$scope.$parent.buttonTextForSavingInformation = 'STOP';
		$scope.$parent.needDullGradeRecords = true;
		$scope.$parent.showBitLocator = true;
		// allows user to rotate bit using next-and-previous-blade-pocket button.
		$scope.$parent.rotateInsteadOfSwitchBlades = true;

		$scope.$parent.selectedTools = [];
		$scope.toolFamily = undefined;
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.ancillaryInformation = {};

		$scope.getFlowRecords();
		$scope.getToolFamilies();
		$scope.getConditions();
		$scope.getActions();
	}

	init();

	$scope.init = function(isLogout) {
		init();
	};

	$scope.selectForFlameSpray = function(pocket) {
		debugger;
		pocket.fs = typeof pocket.fs === 'undefined' ? {} : pocket.fs;

		if (pocket.fs.isFlameSpray || pocket.isFlameSpray && typeof pocket.fs.isFlameSpray === 'undefined') {
			pocket.fs.isFlameSpray = false;
			pocket.fs.cutterActionId = pocket.cutterActionId;
			pocket.fs.cutterAction = pocket.cutterAction;
		} else {
			pocket.fs.isFlameSpray = true;

			if (!pocket.cutterActionId || parseInt(pocket.cutterActionId) === 3) {
				// if action is NULL or Leave It, make the action rotate
				pocket.fs.cutterActionId = 2;
				var action = _.find($scope.availableActions, function(a) {
					return a.cutterActionId === pocket.fs.cutterActionId;
				});
				if (action) {
					pocket.fs.cutterAction = action.cutterActionCode;
				}
			}
		}
	};

	$scope.saveFlameSpray = function() {
		var flamesprays = [];
		_.each($scope.groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {
					_.each(r.pockets, function(p) {

						if (p.fs) {
							p.isFlameSpray = p.fs.isFlameSpray;
							p.cutterActionId = p.fs.cutterActionId;
							p.cutterAction = p.fs.cutterAction;
						} flamesprays.push(p);
					});
				});
			});
		});

		if (flamesprays.length > 0) {
			var url = Global.baseUrl + 'dullgradelevelone/saveFlameSprayInfo';
			var body = flamesprays;
			$http.post(url, body).success(function(data) {
				debugger;
				toastr.success('Flame spray information saved successfully.');
				setTimeout(function() {
					debugger;
					$location.path('/dullgrade-home');
					$scope.$apply();
				}, 2000);
			}).error(function(err) {
				toastr.error('There was a problem trying to save the flame spray information.');
			});
		} else {
			toastr.warning('No flame spray information to save...');
			setTimeout(function() {
				debugger;
				$location.path('/dullgrade-home');
				$scope.$apply();
			}, 2000);
		}
	};

	$scope.stop = function() {
		$scope.updateAllDullGrades(function() {
			$('#modalSaveDullgrades').modal('show');
		});
	};
}]);
