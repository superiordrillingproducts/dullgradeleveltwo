app.controller('NgGruntGrindController', ['$http', '$scope', '$location', 'Global', '$filter', function ($http, $scope, $location, Global, $filter) {
	function init() {
		// configuration
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.needDullGradeRecords = false;
		$scope.$parent.needCutterStructure = false;
		$scope.showBackButtonOnStop = true;
		$scope.$parent.showBitLocator = true;
		$scope.gruntGrindUsers = [];

		// initiate arrays and objects
		$scope.$parent.selectedTools = [];
		$scope.getFlowRecords();
	}

	$scope.$on('getFlowRecordsRan', function() {
		debugger;
		if ($scope.gruntGrindUsers.length === 0) {
			$scope.gruntGrindUsers = $scope.$parent.allowedUsers;
		}
		if ($scope.$parent.allowedUsers.length !== $scope.gruntGrindUsers.length) {
			$scope.$parent.allowedUsers = $scope.gruntGrindUsers;
		}
		$scope.gruntStationRecords = _.filter($scope.stationRecords, function(sr) {
			return sr.flowGroupId === 1;
		});
		$scope.grindStationRecords = _.filter($scope.stationRecords, function(sr) {
			return sr.flowGroupId === 2;
		});

		angular.forEach($scope.gruntStationRecords, function(gsr) {
			var running = _.filter($scope.currentRunningLaborTimeStamps, function(crlts) {
				if(crlts.repairId === gsr.repairId){
					_.filter($scope.currentAuthenticatedUsers, function(cau){
						if(crlts.userId === cau.userId){
							gsr.currentUser = cau;
							gsr.runningTimeStamp = crlts;
							gsr.running = true;
							return crlts.repairId === gsr.repairId;
						}
					});
				}
			});
		});
		angular.forEach($scope.grindStationRecords, function(grsr) {
			var running = _.filter($scope.currentRunningLaborTimeStamps, function(crlts) {
				if(crlts.repairId === grsr.repairId){
					_.filter($scope.currentAuthenticatedUsers, function(cau){
						if(crlts.userId === cau.userId){
							grsr.currentUser = cau;
							grsr.runningTimeStamp = crlts;
							grsr.running = true;
							return crlts.repairId === grsr.repairId;
						}
					});
				}
			});
		});

		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.$parent.currentRunningLaborTimeStamps = [];
		$scope.$parent.selectedTools = [];
		$scope.$parent.finishedAddingTools = true;
	});

	init();

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};

	$scope.showStopScreen = function(selected, show) {
		if(show === false) {
			$scope.$parent.showStop = false;
			$scope.$parent.currentAuthenticatedUsers = [];
			$scope.$parent.currentRunningLaborTimeStamps = [];
			$scope.$parent.selectedTools = [];
		} else {
			$scope.$parent.openTimeStampStartTime = selected.runningTimeStamp.startTime;
			$scope.$parent.currentAuthenticatedUsers.push(selected.currentUser);
			$scope.$parent.currentRunningLaborTimeStamps.push(selected.runningTimeStamp);
			$scope.$parent.selectedTools.push(selected);
			$scope.$parent.showStop = true;
		}

	};
}]);
