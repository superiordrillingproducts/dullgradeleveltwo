app.controller('NgDNRReworkController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', function ($http, $scope, $location, Global, $filter, $sails) {
	function init() {
		$scope.$parent.maxUsers = 2;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = true;
		$scope.$parent.showTimer = true;
		$scope.getFlowRecords();
		$scope.getConditions();
		$scope.getActions();
		$scope.getConditionActionCombos();
		$scope.currentIndex = 0;
		$('#selectToolForRework').modal('hide');
		$scope.currentIndex = 0;
		$scope.$parent.isLatestDullGrade = true;
		$scope.$parent.showCompatibleCuttersInKiln = true;
		$scope.selectedGroup = 0;
		$scope.$parent.moveToDnrKilnAndStartTime = true;
	}

	init();

	$scope.$on('getFlowRecordsRan', function () {
		$scope.kilnHomeFlows = [];
		if ($scope.workStation.facilityId === 2) {
			kilnHomeFlows = [64, 65]
		} else if ($scope.workStation.facilityId === 6) {
			kilnHomeFlows = [93, 94]
		} else if ($scope.workStation.facilityId === 7) {
			kilnHomeFlows = [110]
		}
		$scope.getFlowsWithToolRepairs(kilnHomeFlows);
		$scope.$parent.finishedAddingTools = true;
		$scope.getCountOfCutterSizeByAction();
		$scope.getCountOfFlameSpray();
		getAllCutterInformation();
	});

	function getAllCutterInformation(cb) {
		$scope.getDullGradeAndHashStructureForCurrentRepair(function () {
			debugger;
			$scope.getCutters(function () {
				$scope.setColorClasses($scope.$parent.groups, true, function () {
					debugger;
					$scope.getCutterNamesUsedInRepairForDullgrade(function () {
						debugger;
						$scope.setColorsOnClearoutLegend($scope.$parent.cutterNames, $scope.activeColorClasses, function (cutterNames) {
							debugger;
							var adjustedCutterNames = [];
							angular.forEach(cutterNames, function (cn, index) {
								if (index === 0) {
									if (cn.replacedCutterId !== null) {
										adjustedCutterNames.push({
											cutterId: cn.replacedCutterId,
											cutterName: cn.replacedCutterName,
											count: cn.count,
											colorClass: cn.colorClass
										});
									} else {
										adjustedCutterNames.push({
											cutterId: cn.cutterId,
											cutterName: cn.cutterName,
											count: cn.count,
											colorClass: cn.colorClass
										});
									}
								} else {
									var found = false;
									angular.forEach(adjustedCutterNames, function (acn, index2) {
										if (found === false) {
											if (cn.replacedCutterId !== null) {
												if (cn.replacedCutterId === acn.cutterId) {
													found = true;
													acn.count = acn.count + cn.count;
													if (!acn.colorClass) {
														acn.colorClass = cn.colorClass;
													}
												} else {
													if (adjustedCutterNames.length === index2 + 1) {
														adjustedCutterNames.push({
															cutterId: cn.replacedCutterId,
															cutterName: cn.replacedCutterName,
															count: cn.count,
															colorClass: cn.colorClass
														});
													}
												}
											} else {
												if (cn.cutterId === acn.cutterId) {
													found = true;
													acn.count = acn.count + cn.count;
													if (!acn.colorClass) {
														acn.colorClass = cn.colorClass;
													}
												} else {
													if (adjustedCutterNames.length === index2 + 1) {
														adjustedCutterNames.push({
															cutterId: cn.cutterId,
															cutterName: cn.cutterName,
															count: cn.count,
															colorClass: cn.colorClass
														});
													}
												}
											}
										}
									});
								}
							});
							debugger;
							if (cb) cb();
						});
					});
				});
			});
		});
	};

	$scope.rotateDNR = function (left, right, index) {
		debugger;
		if (left === true) {
			$scope.left = true;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.groups[$scope.selectedGroup].blades.length - 1;
			} else {
				$scope.currentIndex = $scope.currentIndex - 1;
			}
		}
		if (right === true) {
			$scope.left = false;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else if ($scope.currentIndex + 1 < $scope.groups[$scope.selectedGroup].blades.length) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else {
				$scope.currentIndex = 0;
			}
		}
	};

	$scope.cutterSize = function (row) {
		var style;
		if (row.pockets.length <= 10) {
			style = {
				'width': '90px',
				'height': '90px'
			};
			$scope.pocketFont = {
				'font-size': '25px'
			};
			$scope.exclamationiconkiln = {
				'margin-left': '36px',
				'width': '13px',
				'height': '13px',
				'border-radius': '10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.9em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		} else if (row.pockets.length <= 16) {
			style = {
				'width': '50px',
				'height': '50px'
			};
			$scope.pocketFont = {
				'font-size': '15px'
			};
			$scope.exclamationiconkiln = {
				'margin-left': '17px',
				'width': '11px',
				'height': '10px',
				'border-radius': '10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.7em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		} else {
			style = {
				'width': '35px',
				'height': '50px'
			};
			$scope.pocketFont = {
				'font-size': '12px'
			};
			$scope.exclamationiconkiln = {
				'margin-left': '17px',
				'width': '13px',
				'height': '13px',
				'border-radius': '10px',
				'background-color': 'black'
			};
			$scope.exclamationiconkilnI = {
				'font-size': '0.9em',
				'display': 'inherit',
				'color': 'white'
			};
			return style;
		}
	};
	
	$scope.changeGroup = function (group) {
		$scope.selectedGroup = group;
		$scope.currentIndex = 0;
	};
	
	$scope.$on('reloadCutterDullGradeAndCutterNames', function () {
		$('.modal').modal('hide');
		getAllCutterInformation(function () {
			_.each($scope.dullGrade.cutterNames, function (cn) {
				if (cn.markedForLotNumber === true) {
					$('#showCuttersUsedInRepair').modal('show');
				}
			});
		});
	});
}]);