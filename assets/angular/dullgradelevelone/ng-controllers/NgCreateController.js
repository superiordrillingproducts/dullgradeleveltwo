app.controller('NgCreateController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', function ($http, $scope, $location, Global, $filter, $sails) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = true;
		$scope.$parent.showTimer = false;
		$scope.emailList = false;
		$scope.$parent.stayLoggedIn = true;
		$scope.$parent.showReasonForChange = true;
		$scope.$parent.showBitLocator = true;
		$scope.getFlowRecords();

	}

	init();

	$scope.$on('getFlowRecordsRan', function() {
		$scope.getAncillaryInfo();
		$scope.getCountOfFlameSpray();
		$scope.getCountOfCutterSizeByAction();
		$scope.createRecords = [];
		$scope.emailListRecords = [];
		$scope.awaitingApprovalRecords = [];
		$scope.orderListRecords = [];
		$scope.allEmailListOnHold = false;
		$scope.allOrderListOnHold = false;
		var emailListCount = 0;
		var orderListCount = 0;
		angular.forEach($scope.stationRecords, function(r) {
			if (r.interStationChrono === 1) {
				$scope.createRecords.push(r);
			} else  if (r.interStationChrono === 2) {
				if(r.onHold === 1){
					emailListCount ++;
				}
				$scope.emailListRecords.push(r);
			} else if (r.interStationChrono === 3) {
				$scope.awaitingApprovalRecords.push(r);
			} else if (r.interStationChrono === 4) {
				if(r.onHold === 1){
					orderListCount ++;
				}
				$scope.orderListRecords.push(r);
			}
		});
		if($scope.emailListRecords.length === emailListCount){
			$scope.allEmailListOnHold = true;
		}
		if($scope.orderListRecords.length === orderListCount){
			$scope.allOrderListOnHold = true;
		}
		getDullGradeStructureAndCutterNames();
	});

	function getDullGradeStructureAndCutterNames() {
		$scope.getDullGradeStructureForCurrentRepair(function(dullgrades) {
			$scope.getCutters(function() {
				$scope.setColorClasses($scope.groups, false, function() {
					$scope.getCutterNamesUsedInRepairForDullgrade(function(dullGrade) {
						if(dullGrade) $scope.setColorsOnClearoutLegend($scope.cutterNames, dullGrade.activeColorClasses, null, dullGrade);
						else $scope.setColorsOnClearoutLegend($scope.cutterNames, $scope.activeColorClasses, null);
					});
				}, dullgrades);
			});
		});
	}

	$scope.$on('reloadCutterDullGradeAndCutterNames', function() {
		delete $scope.$parent.scr;
		getDullGradeStructureAndCutterNames();
	});

	$scope.$on('selectedTools:updated', function() {
		$scope.callStartStopLaborTracking();
	});

	$scope.$on('toolHold', function() {
		$scope.saveCreate(false);
	});

	$scope.$on('toolHoldStatusChange', function() {
		$('#modalSetOnHoldAndComments').modal('hide')
	});

	$scope.stop = function() {
		$scope.callStartStopLaborTracking(true);
	};

	$scope.saveCreate = function(move) {
		debugger;
		var url = Global.baseUrl + 'dullgradelevelone/saveCreate';
		var data;
		if (move) {
			data = {
				orderNumber : $scope.orderNumber,
				orderPrice : $scope.orderPrice,
				totalTimesRepaired : $scope.totalTimesRepaired,
				ancillaryBakerBitId: $scope.ancillaryId,
				repairId: $scope.selectedTools[0].repairId,
				interStationChrono: $scope.selectedTools[0].interStationChrono,
				userId: $scope.currentAuthenticatedUsers[0].userId
			};
		} else {
			data = {
				orderNumber : $scope.orderNumber,
				orderPrice : $scope.orderPrice,
				totalTimesRepaired : $scope.totalTimesRepaired,
				ancillaryBakerBitId: $scope.ancillaryId,
				repairId: $scope.selectedTools[0].repairId,
				userId: $scope.currentAuthenticatedUsers[0].userId
			};
		};

		$http.post(url, data).success(function(saved){
			$scope.callStartStopLaborTracking(true);
		}).error(function(err) {
			toastr.error('There was a problem saving create information.');
		});
	};

	$scope.bitApprovedMoveForward = function(tool) {
		var url = Global.baseUrl + 'dullgradelevelone/createProgramMoveForward';
		var data = {
			repairId: tool.repairId,
			interStationChrono: tool.interStationChrono
		};

		$http.post(url,data).success(function(moved) {
			toastr.success('Bit Approved!');
		}).error(function(err) {
			toastr.error('There was a problem moving bit forward.');
		});
	};

	$scope.generateEmail = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getAllAncillaryBakerBitInfo';
		$http.get(url).success(function(ancillaryInfo) {

			var emailListNotOnHold = [];
			angular.forEach($scope.emailListRecords, function(elr) {
				if (elr.onHold != 1) {
					emailListNotOnHold.push(elr);
				};
			});
			var orderedEmailListNotOnHold = $filter('orderBy')(emailListNotOnHold, 'stockPointShipToId', false);
			var count = 0;
			var emailListToSend = [];
			angular.forEach(ancillaryInfo, function(ai) {
				angular.forEach(orderedEmailListNotOnHold, function(els) {
					if(ai.repairId === els.repairId) {
						els = Object.assign(els, ai);
					}
				});
			});
			angular.forEach(orderedEmailListNotOnHold, function(els) {
				if (count === 0) {
					emailListToSend[count] = new Array(els);
					count ++;
				} else if (els.stockPointShipToId === emailListToSend[count-1][0].stockPointShipToId) {
					emailListToSend[count-1].push(els);
				} else {
					emailListToSend[count] = new Array(els);
					count++;
				}
			});
			var url = Global.baseUrl + 'dullgradelevelone/generateEmail';
			$http.post(url, emailListToSend).success(function(emailed) {
				toastr.success('Email Sent!');
			}).error(function(err) {
				toastr.error('There was a problem generating email.');
			});
		}).error(function(err) {
			toastr.error('There was a problem getting all baker bit ancillary');
		});
	};

	$scope.generateCutterOrder = function() {
		debugger;
		var url = Global.baseUrl + 'dullgradelevelone/generateCutterOrder';
		var data = {
			flowId : $scope.orderListRecords[0].flowId,
			user: $scope.currentAuthenticatedUsers
		};
		$http.post(url, data).success(function(emailed) {
			toastr.success('Email Sent!');
			$('#cutterOrderModal').modal('hide');
		}).error(function(err) {
			toastr.error('There was a problem generating email.');
		});
	};

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};

}]);
