app.controller('NgViewHistoryController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', function ($http, $scope, $location, Global, $filter, $sails) {

	$scope.history = [];
	$scope.$parent.showBitLocator = true;

	$scope.getHistoryByRepairId = function () {
		var url = Global.baseUrl + 'dullgradelevelone/getHistoryByRepairId';
		var data = {
			repairId: 1
		};
		$http.post(url, data).success(function(history) {
			angular.forEach(history, function(h) {
				debugger;
				if (h.field === "holdComments") {
					h.valueName = 'Hold Comments';
				}
				if (h.field === "onHold") {
					h.valueName = 'Put On/Off Hold';
					if (h.previousValue === "0") {
						h.actualPreviousValue = 'Off Hold';
					} else {
						h.actualPreviousValue = 'On Hold';
					}
					if (h.updatedValue === "0") {
						h.actualUpdatedValue = 'Off Hold';
					} else {
						h.actualUpdatedValue = 'On Hold';
					}
				}
				if (h.field === "orderNumber") {
					h.valueName = 'Order Number'
				}
				if (h.field === "flowId") {
					h.valueName = 'Workstation Changed'
				}
				if (h.field === "isCutOut") {
					h.valueName = 'Is a Cut Out / Not a Cut Out'
					if (h.previousValue === "0") {
						h.actualPreviousValue = 'Not a Cut Out'
					} else {
						h.actualPreviousValue = 'Is a Cut Out'
					}
					if (h.updatedValue === "0") {
						h.actualUpdatedValue = 'Not a Cut Out'
					} else {
						h.actualUpdatedValue = 'Is a Cut Out'
					}
				}
				if (h.field === "cutOutComments") {
					h.valueName = 'Cut Out Comments'
				}
			})
			$scope.history = history;
		}).error(function(err, error){
			debugger;
			toastr.error('There was a problem getting the history for repair.')
		})
	},

	$scope.getHistoryByRepairId();

}]);
