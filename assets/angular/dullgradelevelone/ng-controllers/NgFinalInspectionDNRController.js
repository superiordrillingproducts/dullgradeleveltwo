app.controller('NgFinalInspectionDNRController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', 'Idle', 'Keepalive', '$timeout', function($http, $scope, $location, Global, $filter, $sails, Idle, Keepalive, $timeout) {
	function init() {
		// configuration settings
		$scope.$parent.finishedAddingTools = true;
		$scope.toolFamily = undefined;
		$scope.getFlowRecords();
		$scope.$parent.selectedTools = [];
		$scope.$parent.sendInForRepair = false;
		$scope.$parent.pics = [];
		$scope.$parent.thirdPartyInspectionImageTwo = {
			url: '',
			name: 'Packaged DNR Image',
			picTaken: false
		};
		$scope.$parent.paintedBoxEndImage = {
			url: '',
			name: 'Painted Box End Image',
			picTaken: false
		};
		$scope.$parent.paintedPinEndImage = {
			url: '',
			name: 'Painted Pin End Image',
			picTaken: false
		};
	}
	init();

	$scope.init = function() {
		init();
	};

	$scope.$on('getFlowRecordsRan', function() {
		$scope.finishedAddingTools = true;
		if($scope.selectedTools[0]){
			$scope.getPicturesForThisRepair($scope.selectedTools[0].repairId)
			$scope.getAncillaryInfo();
			$scope.get32Inches(function(data) {
				var boxHasDecimal = $scope.selectedTools[0].boxOD % 1
				if($scope.selectedTools[0].boxOD && boxHasDecimal != 0){
					var boxOD = String($scope.selectedTools[0].boxOD)
					var boxSplit = boxOD.split('.');
					var boxD = Number("."+ boxSplit[1]).toFixed(5).replace('0','');
					var boxDecimal = parseFloat(boxD);
					debugger;
					var boxFilteredData = $filter('filter')(data, function(d) {
						return d.decimal === boxDecimal
					})
					$scope.selectedTools[0].boxOD = boxOD[0] + "-" + boxFilteredData[0].fraction
				}
				var pinHasDecimal = $scope.selectedTools[0].pinOD % 1
				if($scope.selectedTools[0].pinOD && pinHasDecimal != 0){
					var pinOD = String($scope.selectedTools[0].pinOD)
					var pinSplit = pinOD.split('.')
					var pinD = Number("."+ pinSplit[1]).toFixed(5).replace('0','');
					var pinDecimal = parseFloat(pinD);
					var pinFilteredData = $filter('filter')(data, function(d) {
						return d.decimal === pinDecimal
					})
					$scope.selectedTools[0].pinOD = pinOD[0] + "-" + pinFilteredData[0].fraction
				}

			})
			if($scope.selectedTools[0].length1 === null){
				$scope.selectedTools[0].length1 = 0;
			}
			if($scope.selectedTools[0].length2 === null){
				$scope.selectedTools[0].length2 = 0;
			}
			if($scope.selectedTools[0].length3 === null){
				$scope.selectedTools[0].length3 = 0;
			}
			if($scope.selectedTools[0].length4 === null){
				$scope.selectedTools[0].length4 = 0;
			}
			if($scope.selectedTools[0].length5 === null){
				$scope.selectedTools[0].length5 = 0;
			}
			if($scope.selectedTools[0].midOdFractionalInches === null){
				$scope.selectedTools[0].midOdFractionalInches = 0;
			}
		}

	});

	$scope.$on('selectedTools:updated', function () {
		$scope.getFlowRecords();
	});

	$scope.$on('repairUpdated', function () {
		if ($scope.unusualDamage === 3) {
			init();
		} else {
			var url = $scope.baseUrl + 'dullgradelevelone/closeRepairMoveToStock';
			var data = {
				selectedTools: $scope.selectedTools,

				repairInfo: {
					repairStatusId: 1

				},
				toolInfo: {
					toolStatusId: 22
				}
			};
			$scope.gPost(url, data, function (err, success) {
				if (err) toastr.error("There was a problem saving review and package info.");
				else {
					toastr.success("Review and package info was saved correctly.");
					init();
				}
			});
		}
	});

	$scope.openModalTakePicture = function(currentPicture) {
		$scope.$parent.currentSelectedImage = currentPicture;
		$scope.$parent.documentTypeId = 10;
		$scope.$parent.documentLocation = '/home/ubuntusdpi/sdpidocuments/finalQualityImage';
		//$scope.$parent.documentLocation = '/Users/home/Documents/sdpidocuments';
		//$scope.$parent.documentLocation = '/sdpidocuments/thirdpartyinspectiondocuments';
		$('#captureImg').attr('src', '');
		$('#cameraInput').val('');
		$('#captureImg').hide();
		$('#confirmSavePicture').attr('disabled', true);
		$('#modalTakePicture').modal('show');
	}

	$scope.calculateTotalLength = function () {
		debugger;
		$scope.measurements.lengthTotal = parseFloat((parseFloat($scope.measurements.length1) + parseFloat($scope.measurements.length2) + parseFloat($scope.measurements.length3) + parseFloat($scope.measurements.length4) + parseFloat($scope.measurements.length5)).toFixed(2));
	}

	$scope.saveFinalInspectionAndPrintStrapSheet = function (moveToNextStation) {
		$('#confirmSavePicture').attr('disabled', true);
		var data = {
			ancillaryInfo: {
				repairId: $scope.selectedTools[0].repairId,
				length1: $scope.measurements.length1,
				length2: $scope.measurements.length2,
				length3: $scope.measurements.length3,
				length4: $scope.measurements.length4,
				length5: $scope.measurements.length5,
				lengthTotal: $scope.measurements.lengthTotal,
				topOdFractionalInches: $scope.measurements.boxOD,
				midOdFractionalInches: $scope.measurements.midOD,
				bottomOdFractionalInches: $scope.measurements.pinOD,
				toolFamilyId: $scope.selectedTools[0].toolFamilyId
			},
			updateToolInfo: {
				toolId: $scope.selectedTools[0].toolId,
				length1: $scope.measurements.length1,
				length2: $scope.measurements.length2,
				length3: $scope.measurements.length3,
				length4: $scope.measurements.length4,
				length5: $scope.measurements.length5,
				lengthTotal: $scope.measurements.lengthTotal,
				topOdFractionalInches: $scope.measurements.boxOD,
				midOdFractionalInches: $scope.measurements.midOD,
				bottomOdFractionalInches: $scope.measurements.pinOD
			},
			strapSheetInfo: {
				serial: $scope.selectedTools[0].serial,
				drillSize: $scope.selectedTools[0].cuttingDiameterFraction,
				driftSize: $scope.selectedTools[0].passThroughFraction,
				boxConnection: $scope.selectedTools[0].boxThreadName,
				boxTorqueFTLB: $scope.selectedTools[0].boxTorqueFTLB,
				pinConnection: $scope.selectedTools[0].pinThreadName,
				pinTorqueFTLB: $scope.selectedTools[0].pinTorqueFTLB,
				innerDiameter: $scope.selectedTools[0].innerDiameterFraction,
				customerOwned: $scope.selectedTools[0].customerOwned
			}
		}
		if($scope.ancillaryId){
			if($scope.selectedTools[0].toolFamilyId === 1) {
				data.ancillaryInfo.ancillaryDnrId = $scope.ancillaryId
			} else if ($scope.selectedTools[0].toolFamilyId === 2){
				data.ancillaryInfo.ancillaryVStreamId = $scope.ancillaryId
			}
		}
		var newTab = window.open();
		var url = Global.baseUrl + 'dullgradelevelone/saveFinalInspectionAndPrintStrapSheet'
		$scope.gPost(url, data,function(err,updated) {
			if (err) toastr.error("There was a problem saving third party inspection info.");
			else {
				toastr.success("Third party inspection info was saved correctly.");

				$scope.$parent.pics = [];
				$scope.$parent.thirdPartyInspectionImageTwo = {
					url: '',
					name: 'Packaged DNR Image',
					picTaken: false
				};
				$scope.$parent.paintedBoxEndImage = {
					url: '',
					name: 'Painted Box End Image',
					picTaken: false
				};
				$scope.$parent.paintedPinEndImage = {
					url: '',
					name: 'Painted Pin End Image',
					picTaken: false
				};
				var file = new Blob([updated], {
					type: 'application/pdf'
				});
				var fileUrl = URL.createObjectURL(file);
				newTab.location = fileUrl;
				$scope.getFlowRelationshipForCurrentFlow($scope.selectedTools[0].flowId, false);
			}
		}, {
			responseType: 'arraybuffer'
		});
	};

	$scope.closeRepairMoveToStock = function(){
		if($scope.unusualDamage === 3){
			$scope.$parent.sendInForRepair = true;
		}
		$scope.getFlowRelationshipForCurrentFlow($scope.selectedTools[0].flowId);
	};

}]);

app.directive('showFocus', function($timeout) {
	return function(scope, element, attrs) {
		scope.$watch(attrs.showFocus,
			function (newValue) {
				$timeout(function() {
					newValue && element.focus();
				});
			},true);
	};
});
