app.controller('NgDnrPullCuttersController', ['$scope', '$http', 'Global', function ($scope, $http, Global) {
	$scope.$parent.maxUsers = 1;
	$scope.$parent.finishedAddingTools = true;

	// use this for both bits and non-bit tools. Do not use both 'needCutterOrHashStructure' and 'needCutterStructure' at the same time here.
	$scope.$parent.needCutterOrHashStructure = false;
	$scope.$parent.needDullGradeRecords = true;
	$scope.$parent.startIdleTimer = true;
	$scope.$parent.showBitLocator = true;

	$scope.$parent.skipDefaultGetFlowRecords = true
	// allows user to rotate bit using next-and-previous-blade-pocket button.
	// $scope.$parent.showStopButton = true;
	// $scope.$parent.showTimer = true;
	$scope.didSendEmail = false;
	$scope.dullGradeEmailInfo = null;
	$scope.getDNRShipToStockPoints();

	function init() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getFlowRecords(function() {
			debugger
			$scope.getDullGradeStructureForCurrentRepair(function() {
				debugger
				$scope.getCutters(function() {
					debugger
					$scope.setColorClasses($scope.$parent.groups, true);
					$scope.getAncillaryInfo();
				})
			}, null, true)
		})
	}

	init()

	$scope.$on('getFlowRecordsRan', function () {
		$scope.$parent.finishedAddingTools = true;
		if($scope.selectedTools[0].stockPointShipToId !== null){
			$scope.didSendEmail = true;
		}
	});

	$scope.stop = function (notCompleteInStation) {
		$scope.callStartStopLaborTracking(notCompleteInStation, function(){
			$scope.getStationUsers();
			$scope.$parent.currentAuthenticatedUsers = [];
			$scope.$parent.selectedTools = []
			$scope.finishedAddingTools = false
			$scope.getFlowRecords()
		});
	};


	$scope.$on('skippedDefaultGetFlowRecordsAndUserAuthenticated', function() {
		init()
	});

	$scope.openModalAddInfoForEmail = function() {
		$('#addInfoForEmail').modal('show');
	};

	$scope.sendEmail = function(){
		debugger;
		var url = Global.baseUrl + 'pdf/generateDNRPdfCutterPockets'
		var data = {
			toolFamilyId : $scope.$parent.selectedTools[0].toolFamilyId,
			partId : $scope.$parent.selectedTools[0].partIdNewForDNR,
			hashId : $scope.$parent.selectedTools[0].hashId,
			repairId : $scope.$parent.selectedTools[0].repairId,
			newCuttersUsed: $scope.$parent.activeColorClasses,
			receivedDate: $scope.$parent.selectedTools[0].dateToolArrived,
			receivedFromStockPoint: $scope.$parent.selectedTools[0].stockPointReceivedFromName,
			boxThreadConnection: $scope.$parent.selectedTools[0].boxThreadName,
			pinThreadConnection: $scope.$parent.selectedTools[0].pinThreadName,
			stockPointShipToId: $scope.selectedDNRShipToStockPoint.stockPointId,
			shipToStockPoint: $scope.selectedDNRShipToStockPoint.stockName,
			extraInfo: $scope.dullGradeEmailInfo

		}
		$http.post(url, data, {
			responseType: 'arraybuffer'}).success(function(updated) {
			$('#addInfoForEmail').modal('hide');
			$scope.didSendEmail = true;
			var file = new Blob([updated], {
				type: 'application/pdf'
			});
			var fileUrl = URL.createObjectURL(file);
			window.open(fileUrl)
		}).error(function(err) {
			toastr.error("Fail");
		});
	};

	$scope.getToolFamilies();
	$scope.getStationUsers();
	$scope.getConditions();
	$scope.getActions();
	$scope.getConditionActionCombos();

}]);
