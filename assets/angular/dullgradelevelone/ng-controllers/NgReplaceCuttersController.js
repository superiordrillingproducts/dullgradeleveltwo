app.controller('NgReplaceCuttersController', ['$scope', '$http', '$location', 'Global', function($scope, $http, $location, Global) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = false;
		$scope.$parent.showTimer = true;
		$scope.$parent.buttonTextForSavingInformation = 'STOP';
		$scope.$parent.needDullGradeRecords = true;
		$scope.$parent.showBitLocator = true;
		$scope.$parent.dullGrade = undefined;
		// allows user to rotate bit using next-and-previous-blade-pocket button.
		$scope.$parent.rotateInsteadOfSwitchBlades = true;
		// determines whether cutter info is returned by repairId (ALL) or just by a specific dullgrade.
		$scope.$parent.getCutterInfoByDullGradeNotRepair = true;

		$scope.$parent.selectedTools = [];
		$scope.toolFamily = undefined;
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.ancillaryInformation = {};
		$scope.getStationUsers();

		$scope.getFlowRecords(function() {
			getDullGradeStructureAndCutterNames(function() {
				$scope.getCountOfCutterSizeByAction();
				$scope.getCountOfFlameSpray();
			});
		});
		$scope.getToolFamilies();
		$scope.getConditions();
		$scope.getActions();
	}

	init();

	$scope.init = function(isLogout) {
		init();
	};

	function getDullGradeStructureAndCutterNames(cb) {
		$scope.getDullGradeStructureForCurrentRepair(function(dullgrades) {
			if(cb) cb();
			$scope.getCutters(function() {
				$scope.setColorClasses($scope.groups, true, function() {
					debugger;
					$scope.getCountOfAllCuttersWithCutterNamesBakerBit(function (dullGrade) {
						debugger;
						if(dullGrade) $scope.setColorsOnClearoutLegend($scope.cutterNames, dullGrade.activeColorClasses, null, dullGrade);
						else $scope.setColorsOnClearoutLegend($scope.cutterNames, $scope.activeColorClasses, null);
					}, $scope.dullGrade);
					// $scope.getCutterNamesUsedInRepairForDullgrade(function(dullGrade) {
					// 	debugger;
					// 	if(dullGrade) $scope.setColorsOnClearoutLegend($scope.cutterNames, dullGrade.activeColorClasses, null, dullGrade);
					// 	else $scope.setColorsOnClearoutLegend($scope.cutterNames, $scope.activeColorClasses, null);
					// });
				}, dullgrades);
			});
		});
	}

	$scope.$on('reloadCutterDullGradeAndCutterNames', function(e, args) {
		debugger;
		getDullGradeStructureAndCutterNames(function() {
			$scope.getCountOfCutterSizeByAction();
			$scope.getCountOfFlameSpray();
		});
	});

}]);
