app.controller('NgDullgradeInfoController', ['$scope', '$http', 'Global', '$location', function ($scope, $http, Global, $location) {
	$scope.$parent.maxUsers = 1;
	$scope.$parent.startIdleTimer = true;
	$scope.$parent.showBitLocator = true;
	// $scope.$parent.showStopButton = true;
	// $scope.$parent.showTimer = true;
	$scope.getFlowRecords();
	$scope.get8InchFractions();
	$scope.getWholeNumbers();
	$scope.getToolSections();
	$scope.getRepairCustomers();

	$scope.$on('getFlowRecordsRan', function() {
		$scope.getFlags();
	});

	$scope.openModalInspectionIssue = function () {
		$('#modalInspectionIssue').modal('show');
	};

	$scope.$on('technicianIdleTimeout', function(){
		debugger;
		//////////////// Set Save Info if Idle timeout ////////////////
	});

	$scope.backToDullGradeHome = function () {
		setTimeout(function() {
			$location.path('/dullgrade-home');
			$scope.$apply();
		}, 1000);
	};

	$scope.getFlags = function () {
		var url = Global.baseUrl + 'dullgradelevelone/getFlags';
		if($scope.selectedTools[0].partNumber){
			var data = {
				serial: $scope.selectedTools[0].serial,
				partNumber: $scope.selectedTools[0].partNumber
			};
		} else {
			var data = {
				serial: $scope.selectedTools[0].serial
			};
		}
		$http.post(url, data).success(function(flags){
			$scope.flags = flags;
		}).error(function(error, err) {
			toastr.error("There was a problem getting flags.");
		});
	};

}]);
