app.controller('NgLookupController', ['$scope', '$http', '$location', 'Global', function($scope, $http, $location, Global) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.showStopButton = false;
		$scope.$parent.showTimer = true;
		$scope.$parent.buttonTextForSavingInformation = 'STOP';
		$scope.$parent.needDullGradeRecords = true;
		$scope.$parent.showBitLocator = true;
		// allows user to rotate bit using next-and-previous-blade-pocket button.
		$scope.$parent.rotateInsteadOfSwitchBlades = true;

		$scope.$parent.selectedTools = [];
		$scope.toolFamily = undefined;
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.ancillaryInformation = {};

		$scope.getAllRepairRecords();
		$scope.getAllBitAndDnrTools();
		$scope.getToolFamilies();
		$scope.getConditions();
		$scope.getActions();
	}

	$scope.selectTool = function(tool) {
		$scope.$parent.groups = [];
		debugger;
		if(tool.toolId) {
			$scope.currentTool = tool;
			$scope.selectedTools[0] = null;
			$scope.repairsForThisTool = _.filter($scope.repairs, function(repair) {
				return repair.toolId === $scope.currentTool.toolId;
			});
		} else if(tool.nonSdpToolId) {
			$scope.currentTool = tool;
			$scope.selectedTools[0] = null;
			$scope.repairsForThisTool = _.filter($scope.repairs, function(repair) {
				return repair.nonSdpToolId === $scope.currentTool.nonSdpToolId;
			});
		}

		if($scope.currentTool) $scope.searchToolRepair = null;
	};

	$scope.selectRepairFromDropdown = function(repair) {
		debugger;
		getLaborTimeStampsForRepair(repair.repairId);
		$scope.$parent.selectedTools.push(repair);
		$scope.$parent.groups = [];
		$scope.getCountOfCutterSizeByAction();
		$scope.getCountOfFlameSpray();

		$scope.getDullGradeStructureForCurrentRepair(function() {
			debugger;
			$scope.getCutters(function() {
				debugger;
				$scope.setColorClasses($scope.groups, true, function() {
					debugger;
					$scope.getCutterNamesUsedInRepair(function(currentDullGrade) {
						$scope.setColorsOnClearoutLegend(cutterNames, $scope.activeColorClasses, currentDullGrade);
					});
				});
			});
		});
	};

	$scope.timeStampData = [];
	function getLaborTimeStampsForRepair(repairId) {
		var url = Global.baseUrl + 'dullgradelevelone/getLaborTimeStampsForRepair';
		var body = {
			repairId: repairId
		};
		$http.post(url, body).success(function(obj) {
			var data = obj.timeStampData;
			var repairTotalTime = obj.repairTotalTime;
			$scope.repairTotalTime = repairTotalTime;
			$scope.timeStampData = data;
		}).error(function(err) {

		});
	}

	$scope.selectRepair = function(repair) {
		$scope.$parent.groups = [];
		if(repair.toolId) {
			$scope.currentTool = _.find($scope.tools, function(tool) {
				return tool.toolId === repair.toolId;
			});
			$scope.selectedTools[0] = repair;
			$scope.repairsForThisTool = _.filter($scope.repairs, function(repair) {
				return repair.toolId === $scope.currentTool.toolId;
			});
		} else if(repair.nonSdpToolId) {
			$scope.currentTool = _.find($scope.tools, function(tool) {
				return tool.nonSdpToolId === repair.nonSdpToolId;
			});
			$scope.selectedTools[0] = repair;
			$scope.repairsForThisTool = _.filter($scope.repairs, function(repair) {
				return repair.nonSdpToolId === $scope.currentTool.nonSdpToolId;
			});
		}
		getLaborTimeStampsForRepair(repair.repairId);

		if($scope.currentTool) $scope.searchToolRepair = null;

		$scope.getCountOfCutterSizeByAction();
		$scope.getCountOfFlameSpray();

		$scope.getDullGradeStructureForCurrentRepair(function() {
			debugger;
			$scope.getCutters(function() {
				debugger;
				$scope.setColorClasses($scope.groups, true, function() {
					debugger;
					$scope.getCutterNamesUsedInRepair(function(cutterNames) {
						console.log(cutterNames);
						$scope.setColorsOnClearoutLegend(cutterNames, $scope.activeColorClasses);
					});
				});
			});
		});
	};

	$scope.closeSearchBox = function() {
		$scope.searchToolRepair = null;
	};

	init();

	$scope.init = function(isLogout) {
		init();
	};
}]);
