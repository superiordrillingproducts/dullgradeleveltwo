app.controller('NgDNRLocatorController', ['$scope', '$http', '$location', 'Global', '$filter', '$route', function($scope, $http, $location, Global, $filter, $route) {
	function init() {
		$scope.$parent.maxUsers = 1;
		$scope.getAllOpenRepairRecords();
	};

	$scope.getAllOpenRepairRecords = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getAllOpenRepairRecords';
		$http.get(url).success(function(repairs) {
			$scope.allOpenRepairRecords = repairs;
			$scope.toolsAtLocation = $filter('filter')($scope.allOpenRepairRecords, function (aorr) {
				return aorr.facilityId === $scope.workStation.facilityId;
			});
		}).error(function() {
			toastr.error('There was a problem getting open repair records.');
		});
	};

	$scope.getFlowRecordsFromSerial = function(tool) {
		debugger;
		$scope.$parent.selectedTools = [];
		if(tool !== null){
			$scope.$parent.selectedTools.push(tool.originalObject);
			if ($scope.selectedTools.length === 0) {
				$scope.serialNotFound = true;
			} else {
				$scope.serialNotFound = false;
				var url = Global.baseUrl + 'dullgradelevelone/getFlowRecordsFromSerial';
				var data = {
					familyId: $scope.selectedTools[0].toolFamilyId,
					facilityId: $scope.selectedTools[0].facilityId
				}
				$scope.rushComments = $scope.selectedTools[0].rushComments;
				$scope.checkboxModel = $scope.selectedTools[0].rush;
				// if ($scope.getLaborTimeStamps.repairId = $scope.selectedTools[0].repairId)
				// $scope.getLaborTimeStamps();
				$http.post(url, data).success(function (serialFlow) {
					var toBeSpliced = [];
					angular.forEach(serialFlow, function (sf, i) {
						angular.forEach($scope.flowsWithTools, function (fwt) {
							if (fwt.toolRepairs.length > 0) {
								if (fwt.toolRepairs[0].serial !== $scope.searchedSerial) {
									if (sf.workStationId === fwt.workStationId) {
										toBeSpliced.push(i);
									}
								}
							}
						})
					});
					toBeSpliced.slice().reverse().forEach(function (tbs) {
						serialFlow.splice(tbs, 1);
					});
					$scope.flowForFamilyFromSerial = serialFlow;
					$scope.getOpenTimeStampWithRepairId();
				}).error(function () {
					toastr.error('There was a problem getting flow for family from the serial.');
				});
			}
		} else {
			toastr.error('There is nothing that matches your search.')
		}
	};

	$scope.openModal = function(selected) {
		$scope.selectedFlow = selected;
		$('#editFlowForFamilyModal').modal('show');
	};

	$scope.updateFlowForSerial = function () {
		var url = Global.baseUrl + 'dullgradelevelone/updateFlowForSerial';
		var data = {
			flowId: $scope.selectedFlow.flowId,
			repairId: $scope.selectedTools[0].repairId
		};

		$http.post(url,data).success(function(updatedFlow) {
			toastr.success('Success updated');
			$('#editFlowForFamilyModal').modal('hide');
			$scope.selectedTools[0].flowId = $scope.selectedFlow.flowId;
			if($scope.currentRunningLaborTimeStamps.length > 0) {
				$scope.callStartStopLaborTracking(true, function() {
				});
			}
		}).error(function(err) {
			toastr.error('There was a problem posting the updated workstation.');
		});
	};

	$scope.getOpenTimeStampWithRepairId = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getOpenTimeStampWithRepairId';
		debugger;
		var data = {
			repairId: $scope.selectedTools[0].repairId
		};
		$http.post(url,data).success(function(results) {
			$scope.$parent.currentRunningLaborTimeStamps = results;
			if ($scope.currentRunningLaborTimeStamps.length > 0) {
				$scope.selectedTools[0].timeStampExists = true;
			} else {
				$scope.selectedTools[0].timeStampExists = false;
			}
		}).error(function() {
			toastr.error('There was a problem getting open time stamps.');
		});
	};

	init();

}]);
