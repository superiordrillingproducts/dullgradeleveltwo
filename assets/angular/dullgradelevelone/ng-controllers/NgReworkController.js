app.controller('NgReworkController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', function ($http, $scope, $location, Global, $filter, $sails) {
	function init() {
		// configuration settings
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.noTimeStamp = true;
		$scope.$parent.buttonTextForSavingInformation = 'STOP';
		$scope.$parent.needCutterStructure = true;
		$scope.$parent.showBitLocator = true;
		$scope.getFlowRecords();
		$scope.getConditions();
		$scope.getActions();
		$scope.getConditionActionCombos();
		$scope.currentIndex = 0;
		$('#selectToolForRework').modal('hide');
	}

	init();

	$scope.rotateBit = function(left, right) {
		if (left === true) {
			$scope.left = true;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.groups[0].blades.length - 1;
			} else {
				$scope.currentIndex = $scope.currentIndex - 1;
			}
		}
		if (right === true) {
			$scope.left = false;
			if ($scope.currentIndex === 0) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else if ($scope.currentIndex + 1 < $scope.groups[0].blades.length) {
				$scope.currentIndex = $scope.currentIndex + 1;
			} else {
				$scope.currentIndex = 0;
			}
		}
	};

	$scope.cutterSize = function(row) {
		var style = "";
		if (row.pockets.length <= 9) {
			style = {'width': '90px', 'height': '90px'};
			$scope.pocketFont = {'font-size': '25px'};
			return style;
		} else if (row.pockets.length <= 16) {
			style = {'width': '50px', 'height': '50px'};
			$scope.pocketFont = {'font-size': '15px'};
			return style;
		} else {
			style = {'width': '50px', 'height': '35px'};
			$scope.pocketFont = {'font-size': '15px'};
			return style;
		}
	};
}]);
