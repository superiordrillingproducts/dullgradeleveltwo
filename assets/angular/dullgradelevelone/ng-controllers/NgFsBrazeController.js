app.controller('NgFsBrazeController', ['$scope', '$http', 'Global', function ($scope, $http, Global) {
	function init() {
		// configuration
		$scope.$parent.maxUsers = 1
		$scope.$parent.finishedAddingTools = false
		$scope.$parent.needDullGradeRecords = false
		$scope.$parent.needCutterStructure = false
		$scope.$parent.showStopButton = true
		$scope.$parent.showTimer = true
		$scope.$parent.doNotStartTimeStampAfterPinValidation = true
		$scope.$parent.getCutterInfoByDullGradeNotRepair = true
		$scope.$parent.showBitLocator = true;

		// watch for when users are authenticated: BE CAUTIOUS WITH THIS!
		$scope.$on('user-pin-auth', function() {
			$scope.nextFlowSelectedWithoutTimestamp($scope.nextFlow)
		})

		// initiate arrays and objects
		$scope.$parent.selectedTools = []
		$scope.getFlowRecords()
		// get next flow records of 21 (In line for flamespray)
		$scope.getFlowsWithToolRepairs([21, 22])
		$scope.$on('repairUpdated', function () {
			$scope.$parent.selectedTools = [];
			$scope.getFlowRecords()
			$scope.getFlowsWithToolRepairs([21, 22])
			$scope.removeAllUsers()
			$scope.$parent.finishedAddingTools = false
		})

		$scope.$on('getFlowRecordsRan', function () {
			$scope.flamesprayStationRecords = _.filter($scope.stationRecords, function (sr) {
				return sr.flowGroupId === 1
			})
			$scope.brazeStationRecords = _.filter($scope.stationRecords, function (sr) {
				return sr.flowGroupId === 2
			})

			if ($scope.flamesprayStationRecords && $scope.flamesprayStationRecords.length > 0) {
				$scope.getFlowRelationshipForCurrentFlow($scope.flamesprayStationRecords[0].flowId, null, null, function (nextFlow) {

					$scope.fsFlowsWithRepair = []
					if (nextFlow && nextFlow.length) {
						_.each(nextFlow, function (flow) {
							for (var i = 0; i < $scope.allRepairs.length; i++) {
								var repair = $scope.allRepairs[i]
								if (flow.nextFlowId === repair.flowId) {
									flow.repair = repair
									break
								}
							}
							$scope.fsFlowsWithRepair.push(flow)
						})
					}
				})
			}
			if ($scope.brazeStationRecords && $scope.brazeStationRecords.length > 0) {
				$scope.getFlowRelationshipForCurrentFlow($scope.brazeStationRecords[0].flowId, null, null, function (nextFlow) {
					$scope.brazeFlowsWithRepair = []
					if (nextFlow && nextFlow.length) {
						_.each(nextFlow, function (flow) {
							for (var i = 0; i < $scope.allRepairs.length; i++) {
								var repair = $scope.allRepairs[i]
								if (flow.nextFlowId === repair.flowId) {
									flow.repair = repair
									break
								}
							}
							$scope.brazeFlowsWithRepair.push(flow)
						})
					}
				})
			}
			$scope.getFlowsWithToolRepairs([21, 22])
		})
		$scope.getToolFamilies()
	}

	init()

	$scope.userLogout = function () {
		$scope.$parent.currentAuthenticatedUsers = []
		$scope.getStationUsers()
	}

	$scope.openModalKilnCutterDullGrades = function (kiln) {
		if (kiln && kiln.toolRepairs && kiln.toolRepairs.length > 0) {
			$scope.$parent.currentKiln = kiln

			// assign tool to selected tools
			$scope.selectedTools[0] = kiln.toolRepairs[0]

			// get cutter dullgrades and misc info for tool repair
			$scope.getCountOfCutterSizeByAction()
			$scope.getCountOfFlameSpray()

			$scope.getDullGradeStructureForCurrentRepair(function () {
				$scope.getCutters(function () {
					$scope.setColorClasses($scope.groups, true, function () {
						$scope.getCutterNamesUsedInRepairForDullgrade(function (cutterNames) {
							console.log(cutterNames)
							$scope.setColorsOnClearoutLegend(cutterNames, $scope.activeColorClasses)
							$scope.getCountOfCutterSizeByAction()
							$scope.getCountOfFlameSpray()

							$('#modalKilnCutterDullGrades').modal('show')
						})
					})
				})

			})
		}
	}

	$('#modalKilnCutterDullGrades').on("hidden.bs.modal", function () {
		$scope.$parent.selectedTools = [];
		$scope.$apply();
	});

	$('#possibleNextFlowsModal').on("hidden.bs.modal", function () {
		if(!$scope.finishedAddingTools) {
			$scope.$parent.selectedTools = [];
			$scope.$apply();
		}
	});

}])
