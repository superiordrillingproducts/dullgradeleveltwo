app.controller('NgGlobalController', ['$http', '$scope', '$location', 'Global', '$filter', '$sails', '$route', 'Idle', 'Keepalive', '$timeout', function($http, $scope, $location, Global, $filter, $sails, $route, Idle, Keepalive, $timeout) {
	let csrfToken = null
	$scope.grantCsrfToken = () => {
		return new Promise((resolve, reject) => {
			if(csrfToken) return resolve(csrfToken)
			else {
				let url = Global.baseUrl + 'grantCsrfToken'
				$http.get(url).success(tokenObj => {
					csrfToken = tokenObj._csrf
					return resolve(tokenObj._csrf)
				})
			}
		})
	}

	function gPost(url, body, after) {
		$scope.grantCsrfToken().then(token => {
			if(typeof body === 'undefined' || !body) body = {}
			body._csrf = token
			$http.post(url, body).success(function(result) {
				after(undefined, result)
			})
			.error(function(err){
				after(err, undefined)
			})
		})
	}

	$scope.gPost = (url, body, after, additional) => {
		if (additional){
			$scope.grantCsrfToken().then(token => {
				if (typeof body === 'undefined' || !body) body = {}
				body._csrf = token;
				$http.post(url, body, additional).success(function (result) {
						after(undefined, result);
					})
					.error(function (err) {
						after(err, undefined);
					});
			});
		} else {
			$scope.grantCsrfToken().then(token => {
				if (typeof body === 'undefined' || !body) body = {};
				if(body.length){
					body = {
						_csrf: token,
						body
					}
				} else {
					body._csrf = token;
				}
				$http.post(url, body).success(function (result) {
						after(undefined, result)
					})
					.error(function (err) {
						after(err, undefined)
					});
			});
		}
	};

	var urlHash = window.location.hash;
	if (urlHash === '' || urlHash === '#/') {
		var url = Global.baseUrl + 'user/redirectToCorrectUrl';
		$http.get(url).success(function (data) {
			window.location = data.fullUrl;
		}).error(function (err) {
			toastr.error('There was a problem with redirect.')
		});
	} else {
		var url = Global.baseUrl + 'user/redirectToCorrectUrl';
		$http.get(url).success(function (data) {
			if (data.workStationUrl !== urlHash) {
				toastr.error('You need to log out to go to a different workstation.');
				window.location = data.fullUrl;
			}
		}).error(function (err) {
			toastr.error('There was a problem getting redirect.');
		});
	}

	$scope.loading = false;

	$scope.maxCuttersOnOnePocket = 0;

	function getAllOpenRepairRecordsPost(cb) {
		gPost(Global.baseUrl + 'DullGradeLevelOne/getAllOpenRepairRecords', null, function(err,repairs) {
			cb(repairs)
		})
	}

	$sails.on('repair', function(message) {
		debugger;
		$scope.stationRecords = []
		if (message) {
			$scope.dontShowLoading = true

			debugger
			getAllOpenRepairRecordsPost(function(repairs) {
				$scope.allRepairs = repairs
				var notFound = false
				angular.forEach($scope.workStationFilter, function(wsf) {
					$scope.filtered = $filter('filter')($scope.allRepairs, function(repair) {
						return repair.flowId === wsf.flowId
					})
					angular.forEach($scope.filtered, function(f) {
						var cuttingDiameterFraction = typeof f.cuttingDiameterFraction === 'undefined' || !f.cuttingDiameterFraction ? '' : '(' + f.cuttingDiameterFraction + ')'
						var familyName = typeof f.shortName === 'undefined' || !f.shortName ? '' : ': ' + f.shortName
						f.toolString = sprintf('%s %s %s', f.serial, familyName, cuttingDiameterFraction)
						if ($scope.stationRecords.length > 0) {
							for (var i = 0; i < $scope.stationRecords.length; i++) {
								if ($scope.stationRecords[i].repairId === f.repairId) {
									notFound = false
									break
								} else {
									notFound = true
								}
							}
							if (notFound) {
								$scope.stationRecords.push(f)
							}
						} else {
							$scope.stationRecords.push(f)
						}
						$scope.stationRecords = $filter('orderBy')($scope.stationRecords, '-rush');
					})
				})
				$scope.dontShowLoading = false
				$scope.$broadcast('getFlowRecordsRan')
			});
		}
	});

	$scope.dontShowLoading = false;
	$scope.isLoading = function() {
		if ($scope.dontShowLoading) return false
		else {
			if ($http.pendingRequests.length !== 0) {
				$scope.continueLoading = true
				$scope.blur = true
			} else {
				if($scope.continueLoading === true) {
					$scope.blur = false
					$scope.continueLoading = false
				}
			}
			return false
		}
	}

	$scope.getAllBitAndDnrTools = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getReceivingTools'
		$http.get(url).success(function(tools) {
			$scope.tools = tools
		})
	}

	$scope.getReceivingTools = function(arrayOfToolStatusIdsThatShouldNotBeSelectableInAnguComplete) {
		var url = Global.baseUrl + 'dullgradelevelone/getReceivingTools'
		$http.get(url).success(function(tools) {
			//$scope.initializePictures();
			_.each(tools, function(t) {

				t.statusString = getToolStatusString(t)

				// mark the tools that should be unselectable in angucomplete dropdown.
				if (arrayOfToolStatusIdsThatShouldNotBeSelectableInAnguComplete && arrayOfToolStatusIdsThatShouldNotBeSelectableInAnguComplete.length > 0) {
					markUnselectableTools(arrayOfToolStatusIdsThatShouldNotBeSelectableInAnguComplete, tools)
				}

				var cuttingDiameterFraction = typeof t.cuttingDiameterFraction === 'undefined' || !t.cuttingDiameterFraction ? '' : '(' + t.cuttingDiameterFraction + ')'
				var familyName = typeof t.shortName === 'undefined' || !t.shortName ? '' : ': ' + t.shortName
				t.toolString = sprintf('%s %s %s', t.serial, familyName, cuttingDiameterFraction)
			})

			//initializeWashTools(tools);
			//initializeSandblastTools(tools);

			Global.setArrivingTools(tools)
			$scope.tools = tools
			$scope.$broadcast('getReceivingToolsRan')
		}).error(function(error, err) {
			toastr.error(err.message)
		})
	}

	function getToolStatusString(tool) {
		var repairStatusId = 9
		var missingStatusId = 32
		var lostInHoleStatusId = 29
		var cutoutStatusId = 30

		var isRepairStatus = false

		if (tool.toolStatusId === repairStatusId) isRepairStatus = true
		// also check if there is an open repair. If so, mark it as In Repair
		if (tool.repairId) isRepairStatus = true



		if (isRepairStatus) return 'IN REPAIR'
		if (tool.toolStatusId === missingStatusId) return 'MISSING'
		if (tool.toolStatusId === lostInHoleStatusId) return 'LOST IN HOLE'
		if (tool.toolStatusId === cutoutStatusId) return 'CUTOUT'

		return ''
	}

	function markUnselectableTools(repairStatusIds, tools) {
		_.each(tools, function(tool) {
			var found = false
			_.each(repairStatusIds, function(statusId) {
				if (!found && tool.toolStatusId === statusId) {
					tool.anguUnselectable = true
					found = true
				}
			})
		})
	}

	$scope.currentAuthenticatedUsers = []

	$scope.$watch('currentAuthenticatedUsers', function() {
		$scope.$broadcast('currentAuthenticatedUsers:updated')
	})

	$scope.getAllRepairRecords = function(cb) {
		var url = Global.baseUrl + 'dullgradelevelone/getAllRepairRecords'
		$http.get(url).success(function(repairs) {
			$scope.repairs = repairs
			if(cb) cb(repairs)
		}).error(function(err) {
			toastr.error('There was a problem getting the repair records.')
		})
	}

	$scope.getFlowRecords = function(cb, cb2) {
		var urlHash = window.location.hash;
		if (urlHash === '' || urlHash === '#/') {
			var url = Global.baseUrl + 'user/redirectToCorrectUrl';
			$http.get(url).success(function (data) {
				window.location = data.fullUrl;
			}).error(function (err) {
				toastr.error('There was a problem with redirect.')
			})
		} else {
			var url = Global.baseUrl + 'user/redirectToCorrectUrl';
			$http.get(url).success(function (data) {
				if (data.workStationUrl !== urlHash && (urlHash !== '#/rework' && urlHash !== '#/rework-dnr')) {
					toastr.error('You need to log out to go to a different workstation.');
					window.location = data.fullUrl;
				} else {
					if (urlHash !== '#/rework' || urlHash !== '#/rework-dnr'){
						window.location = urlHash;
					}
					if(!cb && $scope.skipDefaultGetFlowRecords) $scope.$broadcast('skippedDefaultGetFlowRecordsAndUserAuthenticated')
					else {
						getAllOpenRepairRecordsPost(function(repairs) {
							$scope.allRepairs = repairs
							Global.setRepairs(repairs)
							var url2 = Global.baseUrl + 'dullgradelevelone/getFlowRecords'
							gPost(url2, null, function(err, flowRecordsObject) {
								if (err) toastr.error(err);
								else {
									var records = flowRecordsObject.records
									$scope.workStation = flowRecordsObject.workStation
									$scope.stationRecords = []
									$scope.facilityFlow = records
									$scope.workStationFilter = $filter('filter')(records, function (flow) {
										return flow.workStationId === flowRecordsObject.workStation.workStationId
									})
									angular.forEach($scope.workStationFilter, function (wsf) {
										$scope.filtered = $filter('filter')($scope.allRepairs, function (repair) {
											return repair.flowId === wsf.flowId
										})
										angular.forEach($scope.filtered, function (f) {
											var cuttingDiameterFraction = typeof f.cuttingDiameterFraction === 'undefined' || !f.cuttingDiameterFraction ? '' : '(' + f.cuttingDiameterFraction + ')'
											var familyName = typeof f.shortName === 'undefined' || !f.shortName ? '' : ': ' + f.shortName
											f.toolString = sprintf('%s %s %s', f.serial, familyName, cuttingDiameterFraction)
											$scope.stationRecords.push(f)
										})
									})
									$scope.stationRecords = $filter('orderBy')($scope.stationRecords, '-rush');

									var url3 = Global.baseUrl + 'dullgradelevelone/getLaborTrackingInfo'
									gPost(url3, null, function (err, laborTimeStamps) {
										if (err) toastr.error(err);
										else {
											if (laborTimeStamps.users.length > 0) {
												$scope.currentAuthenticatedUsers = laborTimeStamps.users
												if ($scope.startIdleTimer) {
													Idle.watch()
												}
											}
											$scope.currentRunningLaborTimeStamps = []
											if (laborTimeStamps.items.length > 0) {
												angular.forEach(laborTimeStamps.items, function (ltsi) {
													angular.forEach(ltsi, function (i) {
														$scope.currentRunningLaborTimeStamps.push(i)
													})
												})
												$scope.timer(laborTimeStamps.items[0][0].startTime)
											}
											if (laborTimeStamps.repairs.length > 0) {
												$scope.selectedTools = []
												angular.forEach(laborTimeStamps.repairs, function (r) {
													$scope.otsrFiltered = $filter('filter')($scope.allRepairs, function (repair) {
														return repair.repairId === r.repairId
													})
													angular.forEach($scope.otsrFiltered, function (f) {
														$scope.selectedTools.push(f)
													})
												})
											}
											if (cb) {
												cb()
											} else {
												if ($scope.selectedTools.length === 0) {
													if (!$scope.stayLoggedIn) {
														$scope.currentAuthenticatedUsers = []

													}
												} else if ($scope.selectedTools[0]) {
													if ($scope.needDullGradeRecords) $scope.getDullGradeStructureForCurrentRepair()
													if ($scope.needCutterStructure) $scope.getCutterStructureForCurrentPart()
													if ($scope.needCutterOrHashStructure) $scope.getCutterOrHashStructure()
												}

												if (cb2) cb2();
											}
											$scope.$broadcast('getFlowRecordsRan')
										}
									})
								}
									
							})
						})
					}
				}
			}).error(function (err) {
				toastr.error('There was a problem getting redirect.')
			})
		}
	}

	$scope.selectedTools = []
	$scope.addToolToSelectedToolsFromListFromAngucomplete = function(selected) {
		debugger;
		if (typeof selected.originalObject === 'undefined') {
			// add tool and set arrival date
			var nonSdpTool = {
				serial: selected,
				toolString: selected + ' : Baker Bit'
			}
			$scope.selectedTools.push(nonSdpTool)
			$scope.$broadcast('selectedTools:updated')
		} else {
			if (!selected.originalObject.anguUnselectable) $scope.addToolToSelectedToolsFromList(selected.originalObject)
			else {
				var e = document.getElementById('anguToolSelection')
				var value = e.value
				setTimeout(function() {
					$scope.$broadcast('angucomplete-alt:changeInput', 'anguToolSelection', value)
				}, 1000)
			}
		}
	}
	$scope.addToolToSelectedToolsFromList = function(selectedTool, records) {
		$scope.selectedTools.push(selectedTool)
		$scope.$broadcast('selectedTools:updated')
		if (records) $scope.updateCheckMarksOnTools(records)
	}

	$scope.removeToolFromSelectedTools = function(selectedTool, records, keepFinishedAddingToolsAsTrue, handleUserAuthenticatedOnLocalController) {
		if ($scope.currentRunningLaborTimeStamps && $scope.currentRunningLaborTimeStamps[0] && $scope.currentRunningLaborTimeStamps[0].startTime && !$scope.currentRunningLaborTimeStamps[0].stopTime) {
			toastr.warning('You cannot deselect this tool because there are current time stamps running.')
		} else {
			$scope.selectedTools = _.filter($scope.selectedTools, function(t) {
				if (t.toolId) {
					return t.toolId !== selectedTool.toolId
				} else if (t.nonSdpToolId) {
					return t.nonSdpToolId !== selectedTool.nonSdpToolId
				}

			})
			if ($scope.selectedTools.length === 0) {
				if (keepFinishedAddingToolsAsTrue) $scope.finishedAddingTools = true
				if (handleUserAuthenticatedOnLocalController) $scope.$broadcast('selectedTools:updated')
				else removeAllUsers()
			}
			if (records) $scope.updateCheckMarksOnTools(records)
		}
	}

	$scope.updateCheckMarksOnTools = function(records) {
		_.each(records, function(tool) {
			var isFound = false
			_.each($scope.selectedTools, function(ct) {
				if (tool.toolId) {
					if (tool.toolId === ct.toolId) {
						isFound = true
					}
				} else if (tool.nonSdpToolId) {
					if (tool.nonSdpToolId === ct.nonSdpToolId) {
						isFound = true
					}
				}
			})
			if (isFound) tool.checked = true
			else tool.checked = false
		})
	}

	$scope.finishAddingTools = function() {
		$scope.finishedAddingTools = true
		$scope.$broadcast('finishedAddingTools');
	}

	$scope.allowedUsers = []
	$scope.getStationUsers = function() {
		debugger;
		var url = Global.baseUrl + 'dullgradelevelone/getStationUsers'
		$http.post(url).success(function(users) {
			// if there are already authenticated users, it removes those users from the allowedUsers bucket.
			$scope.allowedUsers = []
			_.each(users, function(user) {
				var found = false
				for (var i = 0; i < $scope.currentAuthenticatedUsers.length; i++) {
					var aUser = $scope.currentAuthenticatedUsers[i]
					if (user.userId === aUser.userId) {
						found = true
						break
					}
				}
				if (!found) {
					$scope.allowedUsers.push(user)
				}
			})
		}).error(function(err) {
			toastr.error('There was a problem trying to get station users.')
		})
	}

	function removeUser(user) {
		$scope.currentAuthenticatedUsers = _.filter($scope.currentAuthenticatedUsers, function(cau) {
			return cau.userId !== user.userId
		})
		$scope.allowedUsers.push(user)
	}

	function removeAllUsers() {
		_.each($scope.currentAuthenticatedUsers, function(cau) {
			removeUser(cau)
		})
	}

	$scope.removeAllUsers = function() {
		removeAllUsers()
	}

	$scope.openModalAuthenticateUser = function(user) {
		$scope.currentPin = null
		$scope.hiddenPin = null
		$scope.currentUserToAuthenticate = user
		$scope.inputUserPin = ''
		$scope.keyPadValue = ''
		$('#modalAuthenticateUser').modal('show')
		setTimeout(function() {
			$('#EnterUserPin').focus()
		}, 500)
	}

	$scope.checkIfPinIsValid = function(keyPadValue, cb) {
		$scope.keyPadValue = keyPadValue
		if ($scope.currentPin === null) {
			$scope.currentPin = ""
		}
		if (keyPadValue === "back" || keyPadValue === "Backspace") {
			$scope.currentPin = $scope.currentPin.slice(0, -1)
			$scope.hiddenPin = $scope.hiddenPin.slice(0, -1)
		} else {
			$scope.currentPin = $scope.currentPin + "" + keyPadValue
			$scope.createHiddenPin()
		}
		if ($scope.currentPin === $scope.currentUserToAuthenticate.pin) {
			$('#modalAuthenticateUser').modal('hide')
			$scope.hiddenPin = ""
			$scope.addUserToAuthenticatedUsers()
			$scope.$broadcast('user-pin-auth')
			if($scope.doNotStartTimeStampAfterPinValidation) {
				$scope.$broadcast('pinValidated')
			} else if(cb) cb()
			else {
				if ($scope.currentAuthenticatedUsers.length === $scope.maxUsers && !$scope.noTimeStamp && !$scope.moveToDnrKilnAndStartTime) {
					if(!$scope.overrideDefaultStartStopFunctionAfterAuthentication) {
						startStopLaborTracking()
					}
				} else if ($scope.currentAuthenticatedUsers.length === $scope.maxUsers && $scope.noTimeStamp) {
					// add timeout time to user(s)

					// default: 300 seconds = 5 minutes
					var userTimeout = 300
					if ($scope.userTimeoutTimeInSeconds) userTimeout = $scope.userTimeoutTimeInSeconds
					$scope.userTimeoutTime = moment(new Date()).add(userTimeout, 's') // in seconds
					isTimeExpired()
				} else if ($scope.currentAuthenticatedUsers.length === $scope.maxUsers && $scope.startIdleTimer) {
					Idle.watch()
				} else if ($scope.currentAuthenticatedUsers.length === $scope.maxUsers && $scope.moveToDnrKilnAndStartTime){
					getFlowRelationshipForCurrentFlow($scope.selectedTools[0].flowId, null, null, null, function(returnData){
						if(returnData){
							var url = Global.baseUrl + 'User/setfacilityandworkstation';
							var data = {
								workstation: returnData[0].workStationId,
								facility: $scope.selectedTools[0].facilityId
							};
							$http.post(url, data).success(function(success) {
								var goToUrl = '/dnrKiln';
								setTimeout(function() {
									$location.path(goToUrl);
									$scope.getStationUsers();
									$scope.$apply();
								}, 500);
							}).error(function(err){
								toastr.error('There was a problem going to kiln.');
							});
						}
					})
				}
			}
		}
	}

	$scope.addUserToAuthenticatedUsers = function() {

		$scope.currentAuthenticatedUsers.push($scope.currentUserToAuthenticate)
		$scope.allowedUsers = _.filter($scope.allowedUsers, function(u) {
			return u.userId !== $scope.currentUserToAuthenticate.userId
		})
	}

	$scope.createHiddenPin = function() {
		if ($scope.hiddenPin) {
			$scope.hiddenPin = $scope.hiddenPin + "" + $scope.currentPin.replace($scope.currentPin, '*')
		} else {
			$scope.hiddenPin = $scope.currentPin.replace($scope.currentPin, '*')
		}
	}

	function generateCutterPocketsPdf(printConfig) {
		var url = Global.baseUrl + 'pdf/generatePdfCutterPockets'
		var body = printConfig
		$http.post(url, post).success(function(url) {
			toastr.success('Success: ' + url)
		}).error(function(err) {
			toastr.error('There was a problem generating the PDF.')
		})
	}

	function sendToNextFlowWithoutTimeStamp (repair, nextFlow, cb) {
		var url = Global.baseUrl + 'dullgradelevelone/updateRepair'
		var body = {
			repairId: repair.repairId,
			flowId: nextFlow.nextFlowId
		}

		gPost(url, body,function(err,data) {
			if(err) toastr.error(err);
			else {
				if (cb) cb(data)
				else $scope.$broadcast('repairUpdated')
			}
		})
	}

	function isKilnOpen(nextFlow) {
		var open = true
		_.each($scope.flowsWithTools, function(f) {
			if(nextFlow.workStationId === f.workStationId && f.toolRepairs && f.toolRepairs.length > 0) {
				open = false
			}
		})
		return open
	}

	$scope.nextFlowSelectedWithoutTimestamp = function(nextFlow, idOfModal, cb) {
		var isOpen = isKilnOpen(nextFlow)
		if(isOpen) {
			$scope.nextFlow = nextFlow
			$scope.finishedAddingTools = true
			if($scope.currentAuthenticatedUsers && $scope.currentAuthenticatedUsers.length) {
				sendToNextFlowWithoutTimeStamp($scope.selectedTools[0], nextFlow, cb)
			}

		} else toastr.warning('A tool is already in that Kiln! Wait until it is empty or choose different one.')

		if(!idOfModal) {
			idOfModal = 'possibleNextFlowsModal'
		}
		$('#' + idOfModal).modal('hide')
	}

	$scope.nextFlowSelected = function (nextFlow, idOfModal, printConfig, cb) {
		$scope.nextFlow = nextFlow
		$scope.finishedAddingTools = true

		if($scope.currentAuthenticatedUsers && $scope.currentAuthenticatedUsers.length > 0) {
			var noLaborTimeStamps = false
			angular.forEach($scope.currentRunningLaborTimeStamps, function (clts) {
				noLaborTimeStamps = true
				clts.newFlowId = nextFlow.nextFlowId
			})
			if(!noLaborTimeStamps) {
				startStopLaborTracking(null, null, nextFlow.nextFlowId, cb)
			} else {
				startStopLaborTracking()
			}

			if(printConfig) {
				generatePdf(printConfig)
			}
		}
		if(!idOfModal) {
			idOfModal = 'possibleNextFlowsModal'
		}
		$('#' + idOfModal).modal('hide')
		if(cb) cb()
	}

	$scope.removeSelectedNextFlow = function(idOfModal) {
		$scope.nextFlow = undefined
		$scope.finishedAddingTools = false
		$scope.currentAuthenticatedUsers = []
		$scope.selectedTools = []
		$('#' + idOfModal).modal('hide')
	}

	$scope.callStartStopLaborTracking = function(notCompleteInStation, cb, flowId) {
		if ($scope.currentRunningLaborTimeStamps && $scope.currentRunningLaborTimeStamps.length > 0) {
			if (notCompleteInStation) {
				startStopLaborTracking(notCompleteInStation, null, null, cb)
			} else {
				var flowId = $scope.selectedTools[0].flowId
				getFlowRelationshipForCurrentFlow(flowId, notCompleteInStation, null, null, cb)
			}
		} else {
			startStopLaborTracking(notCompleteInStation, null, null, cb)
		}
	}

	$scope.getFlowRelationshipForCurrentFlow = function(flowId, notCompleteInStation, modalElementId, cb, cb2, tool) {
		getFlowRelationshipForCurrentFlow(flowId, notCompleteInStation, modalElementId, cb, cb2, tool)
	}

	$scope.getFlowsWithToolRepairs = function(currentFlowIds) {
		var url = Global.baseUrl + 'dullgradelevelone/getFlowsWithToolRepairs'
		var body = {
			currentFlowIds : currentFlowIds
		}

		$http.post(url, body).success(function (flowsWithTools) {
			$scope.flowsWithTools = flowsWithTools
		}).error(function (err) {
			toastr.error("There was a problem getting flows with repairs.")
		})
	}

	function getFlowRelationshipForCurrentFlow(flowId, notCompleteInStation, modalElementId, cb, cb2, tool) {
		var urlFlow = Global.baseUrl + 'dullgradelevelone/getFlowRelationshipForCurrentFlow'
		var flowData = {
			currentFlowId : flowId
		}
		gPost(urlFlow, flowData,function (err,nextFlow) {
			if (err) toastr.error("There was a problem getting flow relationships.");
			else {
				if (nextFlow.length === 0 && $scope.closeRepairOnContinue) {
					nextFlow = [{
						nextFlowId: $scope.selectedTools[0].flowId
					}]
				}
				if (cb) cb(nextFlow)
				else {
					if (nextFlow.length === 1) {
						// check if the next flow is a "Completed" flow. If so, then an additional step needs to be made
						// to change the status of the tool and of the repair (both need to be closed at this point)
						if(tool){
							sendToNextFlowWithoutTimeStamp(tool, nextFlow[0], cb2)
						} else {
							sendToNextFlowWithoutTimeStamp($scope.selectedTools[0], nextFlow[0], cb2)
						}
						

						// angular.forEach($scope.currentRunningLaborTimeStamps, function (clts) {
						// 	clts.newFlowId = nextFlow[0].nextFlowId
						// })
						// startStopLaborTracking(notCompleteInStation, null, null, cb2)
					} else {
						if (nextFlow[0].utilizesProgrammingChrono || nextFlow[0].utilizesProgrammingChrono === 1) {
							/////// Use programming to decide next flow //////
							var setProgrammingNextFlow;
							if(!$scope.sendInForRepair){
								setProgrammingNextFlow = $filter('filter')(nextFlow, function (nf) {
									return nf.programmingChrono === 1
								})
							} else {
								setProgrammingNextFlow = $filter('filter')(nextFlow, function (nf) {
									return nf.programmingChrono === 2
								})
							}
							sendToNextFlowWithoutTimeStamp($scope.selectedTools[0],setProgrammingNextFlow[0])
							// angular.forEach($scope.currentRunningLaborTimeStamps, function (clts) {
							// 	if ($scope.selectedTools[0].toolFamilyId === 1 || $scope.selectedTools[0].toolFamilyId === 2) {
							// 		var setProgrammingNextFlow
							// 		// if(!$scope.selectedTools[0].hashId){
							// 		// 	setProgrammingNextFlow = $filter('filter')(nextFlow, function(nf){
							// 		// 		return nf.programmingChrono === 1
							// 		// 	})
							// 		// } else {
							// 		// 	setProgrammingNextFlow = $filter('filter')(nextFlow, function(nf){
							// 		// 		return nf.programmingChrono === 2
							// 		// 	})
							// 		// }
							// 		if (!$scope.paintOnly) {
							// 			setProgrammingNextFlow = $filter('filter')(nextFlow, function (nf) {
							// 				return nf.programmingChrono === 1
							// 			})
							// 		} else {
							// 			setProgrammingNextFlow = $filter('filter')(nextFlow, function (nf) {
							// 				return nf.programmingChrono === 2
							// 			})
							// 		}
							// 		clts.newFlowId = setProgrammingNextFlow[0].nextFlowId
							// 	}
							// })
							// startStopLaborTracking(notCompleteInStation, null, null, null)
						} else {
							// User chooses next flow
							$scope.possibleNextFlowsToBeSelected = nextFlow
							if ($scope.possibleNextFlowsToBeSelected && $scope.possibleNextFlowsToBeSelected.length > 0 && $scope.flowsWithTools && $scope.flowsWithTools.length > 0) {
								_.each($scope.possibleNextFlowsToBeSelected, function (f) {
									if (!isKilnOpen(f)) {
										f.disabled = true
									}
								})
							}
							if (!$scope.moveToDnrKilnAndStartTime) {
								if (!modalElementId) {
									modalElementId = 'possibleNextFlowsModal'
								}

								$('#' + modalElementId).modal('show')
							} else {
								var disabledCount = 0;
								_.each($scope.possibleNextFlowsToBeSelected, function (f) {
									if (f.disabled === true) {
										disabledCount++
									}
								})
								if (disabledCount === $scope.possibleNextFlowsToBeSelected.length) {
									toastr.error("All kilns are full at this time.");
								} else if (disabledCount > 0) {
									_.each($scope.possibleNextFlowsToBeSelected, function (f) {
										if (!f.disabled) {
											$scope.nextFlowSelected(f, null, null, cb2)
										}
									})
								} else {
									debugger;
									$scope.nextFlowSelected($scope.possibleNextFlowsToBeSelected[0], null, null, cb2)
								}
							}
						}
					}
				}
			}
		})
	}

	$scope.startStopLaborTracking = function(notCompleteInStation, idleTime, updateRepairWithThisNewFlowId, cb) {
		startStopLaborTracking(notCompleteInStation, idleTime, updateRepairWithThisNewFlowId, cb)
	}

	function startStopLaborTracking(notCompleteInStation, idleTime, updateRepairWithThisNewFlowId, cb) {
		if ($scope.currentAuthenticatedUsers.length === $scope.maxUsers && $scope.selectedTools && $scope.selectedTools.length > 0) {
			var currentLaborTimeStamps = []
			var existing = false;
			if ($scope.currentRunningLaborTimeStamps && $scope.currentRunningLaborTimeStamps.length > 0) {
				existing = true;
				if (notCompleteInStation) {
					_.each($scope.currentRunningLaborTimeStamps, function(rts) {
						rts.notComplete = notCompleteInStation
					})
				}
				currentLaborTimeStamps = $scope.currentRunningLaborTimeStamps
			} else {
				_.each($scope.selectedTools, function(t) {
					var laborTimeStamp = {
						repairId: t.repairId,
						newFlowId: updateRepairWithThisNewFlowId
					}
					// user information comes from the users being sent in the post data. start time is handled on server side.

					currentLaborTimeStamps.push(laborTimeStamp)
				})
			}
			var url = Global.baseUrl + 'dullgradelevelone/startStopLaborTracking'
			var body = {
				items: currentLaborTimeStamps,
				users: $scope.currentAuthenticatedUsers,
				idleTime: idleTime,
				closeRepairOnContinue: $scope.closeRepairOnContinue,
				tools: $scope.selectedTools
			}
			$sails.post(url, body).success(function(data) {
				if(cb){
					cb(data)
				} else {
					if($scope.overrideDefaultStartStopFunctionAfterAuthentication) {
						if(existing) $scope.$broadcast('startStopTimeStamp:existed')
						else $scope.$broadcast('startStopTimeStamp:init')
					} else {
						$scope.selectedTools = []
						$scope.finishedAddingTools = false
						$scope.getFlowRecords()
					}
    			}
			}).error(function(err) {
				toastr.error('There was a problem starting or stopping timestamp.')
			})
		}
	}

	$scope.timer = function(startTime) {
		$scope.deviceDateTime = new Date()
		$scope.deviceUnix = moment($scope.deviceDateTime).valueOf()
		$scope.serverUnix = moment(startTime).valueOf()
		$scope.unixDiff = $scope.serverUnix - $scope.deviceUnix
		if ($scope.unixDiff > 0) {
			$scope.adjustedStartTime = $scope.serverUnix - $scope.unixDiff
			startTime = $scope.adjustedStartTime
		}
		$scope.openTimeStampStartTime = startTime
	}

	$scope.toolFamilies = []
	$scope.getToolFamilies = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getToolFamilies'
		$scope.gPost(url, null, function(err, data) {
			if (err) toastr.error("There was a problem getting tool families.");
			else {
				$scope.toolFamilies = data
			}
		})
	}

	function isTimeExpired() {
		var startDate = moment(new Date())

		var diff = moment.duration(moment($scope.userTimeoutTime).diff(moment(startDate)))
		if (diff.asMilliseconds() <= 0) {
			$scope.currentAuthenticatedUsers = []
		} else {
			setTimeout(function() {
				isTimeExpired()
			}, 5000)
		}
	}

	$scope.holdRepair = function(isOnHold, toolRepair) {
		debugger;
		var url = Global.baseUrl + 'dullgradelevelone/holdRepair'
		if (isOnHold === 0) {
			$scope.selectedTools[0].holdComments = null
    	}
		var data
		var requireCommentsIfOnHold = false
    	if(toolRepair) {
			requireCommentsIfOnHold = true
    		data = {
    			onHold: toolRepair.onHold,
    			holdComments: toolRepair.onHold ? toolRepair.holdComments : null,
				repairId: toolRepair.repairId,
    			userId: $scope.currentAuthenticatedUsers[0].userId
			}
    	} else {
    		data = {
    			onHold: isOnHold,
    			holdComments: $scope.selectedTools[0].holdComments,
				repairId: $scope.selectedTools[0].repairId,
    			userId: $scope.currentAuthenticatedUsers[0].userId
			}
    	}

		var valid = false
    	if(requireCommentsIfOnHold) {
    		if(data.onHold) {
				if(data.holdComments) valid = true
			} else valid = true
		} else valid = true
    	if(valid) {
    		$http.post(url, data).success(function(hold) {
				debugger;

    			if (hold[0].onHold) {
					$scope.$broadcast('toolHold')
    			}

				$scope.$broadcast('toolHoldStatusChange', {toolRepair: hold[0]})
				$scope.selectedTools[0].onHold = hold[0].onHold
				$scope.selectedTools[0].holdComments = hold[0].holdComments
    		}).error(function(err) {
				toastr.error('There was a problem holding or unholding repaired.')
			})
    	} else {
			toastr.warning('If you are putting this on hold, hold comments are require.')
    	}
	}

	$scope.openModalManagerLogout = function() {
		$('#modalManagerLogout').modal('show')
	}
	$scope.openModalManagerToolLocatorAccess = function() {
		$('#modalManagerToolLocatorAccess').modal('show')
	}
	$scope.managerLogout = function(password) {

		if (!password) toastr.warning('You must provide a password to logout.')
		else {
			var url = Global.baseUrl + 'User/managerLogout'
			var body = {
				password: password
			}

			$scope.managerPassord = null

			gPost(url, body,function(err,data) {
				if (err) toastr.error('You did not provide the correct password.');
				else window.location.href = '/dullgradelevelone'
			})
		}
	}


	$scope.$on('IdleTimeout', function() {
		$scope.timedOut = true
		Idle.unwatch()
		$scope.$broadcast('technicianIdleTimeout')
	})

	$scope.setColorsOnClearoutLegend = function(cutterNames, activeColorClasses, cb, currentDullGrade) {
		debugger;
		if(currentDullGrade) {
			cutterNames = currentDullGrade.cutterNames
		}
		_.each(cutterNames, function(cn) {
			_.each(activeColorClasses, function(acc) {
				var cutterId = cn.cutterId
				if(cutterId === acc.cutterId) {
					var thisCutter = _.find($scope.cutters, function(cutter) {
						return cutter.cutterId === cn.cutterId
					})

					debugger
					cn.colorClass = acc.class

					cn.cutterName = thisCutter.cutterName
					cn.partNumber = thisCutter.partNumber
					cn.cutterPocketId = thisCutter.cutterPocketId
					cn.sdpPartNumber = thisCutter.sdpPartNumber
					cn.name = thisCutter.name
					cn.cutterPocketSizeId = thisCutter.cutterPocketSizeId
					cn.cutterPocketTypeId = thisCutter.cutterPocketTypeId
					cn.sizeFraction = thisCutter.sizeFraction
				}
			})
		})
		if(cb){
			cb(cutterNames)
		}
	}

	/**
	 * Iterates through clearoutObj in the combined dullGrade object and sets the color classes for new cutters and add on cutters.
	 * @param dullGrade
	 * @returns Number
	 */
	// $scope.setColorsOnClearoutObject = function(dullGrade) {
	// 	if(dullGrade.clearoutObj && dullGrade.activeColorClasses) {
	// 		let clearoutObj = dullGrade.clearoutObj
	// 		let activeColorClasses = dullGrade.activeColorClasses
	// 		for (let dg of clearoutObj.dullgrades) {
	// 			for (let nc of dg.newCutters) {
	// 				for (let acc of activeColorClasses) {
	// 					let cutterId =
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	$scope.goToRework = function(selectedTool, addCutter, isDNR) {
		debugger;
		$scope.currentUrl = $location.path()
		$scope.reworkTypeAddCutter = false
		$scope.isReworkNotAddCutter = false
		if(addCutter) {
			$scope.reworkTypeAddCutter = true
		} else {
			$scope.isReworkNotAddCutter = true
		}
		if(selectedTool){
			$scope.selectedTools.push(selectedTool)
		}
		if($scope.selectedTools.length === 0 || $scope.selectedTools.length > 1){
			$('#selectToolForRework').modal('show')
		} else if (!isDNR) {
			setTimeout(function() {
				$location.path('/rework')
				$scope.$apply()
			}, 500)
		} else {
			setTimeout(function () {
				$location.path('/rework-dnr')
				$scope.$apply()
			}, 500)
		}
	}

	$scope.returnFromReworkToLastUrl = function () {
		$scope.noTimeStamp = false
		if($scope.originalAvailableActions){
			$scope.availableActions = $scope.originalAvailableActions
		}
		setTimeout(function() {
			$location.path($scope.currentUrl)
			$scope.$apply()
		}, 500)
	}

	$scope.returnToLastUrl = function () {
		setTimeout(function() {
			$location.path($scope.currentUrl)
			$scope.$apply()
		}, 500)
	}

	// *********************************************************
	// Dullgrade Stuff
	// *********************************************************

	$scope.set3dProperties = function() {
		set3dProperties($scope.groups)
	}

	function setDullGradeStrings(dullgrades) {
		var index = 0
		_.each(dullgrades, function(dg) {
			if(index === 0) dg.dullGradeString = 'Original'
			else {
				dg.dullGradeString = dg.dullGradeString ? dg.dullGradeString : 'Rework ' + index
			}
			index++
		})
	}

	$scope.changeDullGrade = function(dullGrade) {
		$timeout(function() {
			if(dullGrade) {

				console.log('change dullgrade')
				$scope.groups = dullGrade.groups
				$scope.dullGrade = dullGrade
				$scope.persistedDullGradeId = dullGrade.dullGradeId

				$scope.$broadcast('reloadCutterDullGradeAndCutterNames', {dullGrade: $scope.dullGrade})
			}
		}, 300)
	}

	function getPocketFromStructuredCutterDullGrades(groups, p) {
		let gr = p.group;
		let bl = p.blade;
		let ro = p.row;
		let po = p.pocket;
		let found = false;
		let pocketToReturn;
		_.each(groups, group => {
			if(!found)
				_.each(group.blades, blade => {
					if(!found)
						_.each(blade.rows, row => {
							if(!found)
								_.each(row.pockets, pocket => {
									if(!found)
										if(pocket.group === gr && pocket.blade === bl && pocket.row === ro && pocket.pocket === po) {
											found = true;
											pocketToReturn = JSON.parse(JSON.stringify(pocket));
										}
								})
						})
				})
		})

		return pocketToReturn;
	}

	function combineDullGradesAndReturn(dullgrades) {
		let combinedDullgrade = {
			repairId: dullgrades[0].repairId,
			dullGradeString: 'Combined'
		};
		if(dullgrades && dullgrades.length > 0) {
			let clearoutObj = {
				dullgrades: []
			}
			let reworksCombined = {
				newCutters: [],
				addOnCutters: [],
				rotates: [],
				flamesprays: [],
				dullGradeString: 'Reworks Combined',
				repairId: dullgrades[0].repairId
			};
			let totals = {
				newCutters: [],
				addOnCutters: [],
				rotates: [],
				flamesprays: [],
				dullGradeString: 'TOTAL',
				repairId: dullgrades[0].repairId
			}
			let combinedGroups = dullgrades[0].groups

			let counter = 0;
			for(let dg of dullgrades) {
				let dullgradeInfo = {
					dullGradeId: dg.dullGradeId,
					repairId: dg.repairId,
					dullGradeString: dg.dullGradeString ? dg.dullGradeString : (counter === 0 ? 'Original' : 'Rework ' + counter),
					newCutters: [],
					addOnCutters: [],
					rotates: [],
					flamesprays: [],
				};
				for(let group of dg.groups) {
					for(let blade of group.blades) {
						for(let row of blade.rows) {
							for(let pocket of row.pockets) {
								if(pocket.replaceWithCutterId) {
									pocket.cutterIdThatWasReplaced = pocket.cutterId
									pocket.cutterNameThatWasReplaced = pocket.cutterName
									pocket.cutterId = pocket.replaceWithCutterId
									pocket.cutterName = pocket.replaceWithCutterName
								}
								pocket.cutterId = pocket.replaceWithCutterId ? pocket.replaceWithCutterId : pocket.cutterId

								// record new cutters, add on cutters, rotates, and flamesprays
								if(pocket.cutterActionId === 1) dullgradeInfo.newCutters.push(pocket);
								if(pocket.isAddOnRecord) dullgradeInfo.addOnCutters.push(pocket);
								if(pocket.cutterActionId === 2) dullgradeInfo.rotates.push(pocket);
								if(pocket.isFlameSpray) dullgradeInfo.flamesprays.push(pocket);
								// record new cutters, add on cutters, rotates, and flamesprays for TOTAL
								if(pocket.cutterActionId === 1) totals.newCutters.push(pocket);
								if(pocket.isAddOnRecord) totals.addOnCutters.push(pocket);
								if(pocket.cutterActionId === 2) totals.rotates.push(pocket);
								if(pocket.isFlameSpray) totals.flamesprays.push(pocket);

								// check that its not the first dullgrade
								if(counter > 0) {
									// record new cutters, add on cutters, rotates, and flamesprays for combined reworks
									if(pocket.cutterActionId === 1) reworksCombined.newCutters.push(pocket);
									if(pocket.isAddOnRecord) reworksCombined.addOnCutters.push(pocket);
									if(pocket.cutterActionId === 2) reworksCombined.rotates.push(pocket);
									if(pocket.isFlameSpray) reworksCombined.flamesprays.push(pocket);

									// override this pocket
									let currentCombinedPocket = getPocketFromStructuredCutterDullGrades(combinedGroups, pocket)

									if(currentCombinedPocket) {
										// precedence: latest new dullgrade, latest rotate
										if(pocket.cutterActionId === 1) {
											currentCombinedPocket = JSON.parse(JSON.stringify(pocket));
											combinedGroups[pocket.group-1].blades[pocket.blade-1].rows[pocket.row-1].pockets[pocket.pocket-1] = JSON.parse(JSON.stringify(currentCombinedPocket));
										} else if(pocket.cutterActionId === 2 && currentCombinedPocket.cutterActionId !== 1) {
											currentCombinedPocket = JSON.parse(JSON.stringify(pocket));
											combinedGroups[pocket.group-1].blades[pocket.blade-1].rows[pocket.row-1].pockets[pocket.pocket-1] = JSON.parse(JSON.stringify(currentCombinedPocket));

										}
									}
								}
							}
						}
					}
				}
				debugger;
				counter++;

				clearoutObj.dullgrades.push(dullgradeInfo);
			}

			combinedDullgrade.groups = combinedGroups;
			combinedDullgrade.clearoutObj = clearoutObj;
			$scope.reworksCombined = reworksCombined;
			$scope.dullGradeTotals = totals;

			// consolidate all new cutters, add on cutters, rotates, and flamesprays
			combinedDullgrade.clearoutObj.dullgrades.map(dg => {
				consolidateNewAddonRotateFlamesprayForDullgrade(dg)
			})
			consolidateNewAddonRotateFlamesprayForDullgrade($scope.reworksCombined);
			consolidateNewAddonRotateFlamesprayForDullgrade($scope.dullGradeTotals);

			return combinedDullgrade;
		}
	}

	$scope.setColorClassesForClearoutObj = function() {
		debugger
		if($scope.dullGrade && $scope.dullGrade.clearoutObj && $scope.dullGrade.activeColorClasses) {
			let combinedDullGrade = $scope.dullGrade
			let activeColorClasses = combinedDullGrade.activeColorClasses
			let clearoutObj = combinedDullGrade.clearoutObj
			for (let dg of clearoutObj.dullgrades) {
				setColorsForClearoutObjHelper(dg, activeColorClasses)
			}
			setColorsForClearoutObjHelper($scope.reworksCombined, activeColorClasses)
			setColorsForClearoutObjHelper($scope.dullGradeTotals, activeColorClasses)

		}
	}

	function setColorsForClearoutObjHelper(dg, activeColorClasses) {
		// new cutters
		for (let newC of dg.newCutters.consolidated) {
			for (let color of activeColorClasses) {
				if(newC.key === color.cutterId) newC.colorClass = color.class
			}
		}
		// add on cutters
		for (let addC of dg.addOnCutters.consolidated) {
			for (let color of activeColorClasses) {
				if(addC.key === color.cutterId) addC.colorClass = color.class
			}
		}
		// rotated cutters
		for (let rC of dg.rotates.consolidated) {
			for (let color of activeColorClasses) {
				if(rC.key === color.cutterId) rC.colorClass = color.class
			}
		}
		// flamesprayed cutters
		for (let fC of dg.flamesprays.consolidated) {
			for (let color of activeColorClasses) {
				if(fC.key === color.cutterId) fC.colorClass = color.class
			}
		}
	}

	function consolidateNewAddonRotateFlamesprayForDullgrade(dullgrade) {
		let dg = dullgrade
		// consolidate new cutters
		let newCutters = JSON.parse(JSON.stringify(dg.newCutters))
		dg.newCutters = {}
		dg.newCutters.cutters = newCutters
		dg.newCutters.consolidated = groupByArray(dg.newCutters.cutters, 'cutterId')

		// consolidate add on cutters
		let addOnCutters = JSON.parse(JSON.stringify(dg.addOnCutters))
		dg.addOnCutters = {}
		dg.addOnCutters.cutters = addOnCutters
		dg.addOnCutters.consolidated = groupByArray(dg.addOnCutters.cutters, 'cutterId')

		// consolidate rotates
		let rotateCutters = JSON.parse(JSON.stringify(dg.rotates))
		dg.rotates = {}
		dg.rotates.cutters = rotateCutters
		dg.rotates.consolidated = groupByArray(dg.rotates.cutters, 'cutterId')

		// consolidate flamesprays
		let flamesprayCutters = JSON.parse(JSON.stringify(dg.flamesprays))
		dg.flamesprays = {}
		dg.flamesprays.cutters = flamesprayCutters
		dg.flamesprays.consolidated = groupByArray(dg.flamesprays.cutters, 'cutterId')
	}

	/**
	 * Groups an array by any key in the array
	 * @param xs
	 * @param key
	 * @returns array
	 */
	function groupByArray(xs, key) {
		return xs.reduce(function (rv, x) {
			let v = key instanceof Function ? key(x) : x[key];
			let el = rv.find((r) => r && r.key === v);
			if (el) {
				el.values.push(x);
			} else {
				rv.push({key: v, values: [x]});
			}
			return rv;
		}, []);
	}

	$scope.toggleShowReworks = function() {
		if($scope.showReworks) $scope.showReworks = false
		else $scope.showReworks = true
	}

	$scope.getDullGradeStructureForCurrentRepair = function(cb, cb2, onlyLoadNecessary, persistedCurrentDullGrade) {
		if ($scope.selectedTools[0]) {
			$scope.groups = []
			var url = Global.baseUrl + 'dullgradelevelone/getDullGradesAndMarkupStructureForTool'
			var body = {
				toolRepair: $scope.selectedTools[0]
			}

			$scope.persistedDullGradeId = $scope.persistedDullGradeId ? $scope.persistedDullGradeId : (persistedCurrentDullGrade && persistedCurrentDullGrade.dullGradeId ? persistedCurrentDullGrade.dullGradeId : null)

			$scope.gPost(url, body, function(err, dullgrades) {
				if (err) toastr.error('There was a problem getting the pocket structure for this part.')
				else {
					$scope.dullgrades = dullgrades

					if ($scope.includeCombinedDullgrades) {
						let combinedDullGrade = combineDullGradesAndReturn(dullgrades)
						$scope.dullgrades.push(combinedDullGrade);
					}

					setDullGradeStrings($scope.dullgrades)
					var groups = [],
						dullGrade = {}
					if (dullgrades.length > 0) {
						if ($scope.persistedDullGradeId) {
							dullGrade = _.find(dullgrades, function (dg) {
								return dg.dullGradeId === $scope.persistedDullGradeId
							})
							groups = dullGrade.groups
						} else {
							dullGrade = dullgrades[dullgrades.length - 1]
							groups = dullgrades[dullgrades.length - 1].groups
						}

						$scope.latestDullGrade = dullgrades[dullgrades.length - 1]
					}

					$scope.dullGrade = dullGrade
					$scope.groups = groups

					if (cb) cb($scope.dullgrades)
					else {
						_.each($scope.dullgrades, function (dg) {

							if (dg.groups.length === 0) toastr.warning('This repair does not have any dullgrade info associated with it.')
							else {
								setSizeClasses(dg.groups)
								getAllPockets1D(dg.groups)
								debugger
								if (!onlyLoadNecessary) {
									setFirstPocketAsActive(dg.groups)
									set3dProperties(dg.groups)
								}
							}
						})
						if (cb2) cb2($scope.dullgrades)
					}
					$scope.$broadcast('getDullGradeStructureForCurrentRepair:executed')
				}	
			})
		}
	}

	$scope.getDullGradeAndHashStructureForCurrentRepair = function(cb, cb2, onlyLoadNecessary, persistedCurrentDullGrade) {
		debugger
		if ($scope.selectedTools[0]) {
			$scope.groups = []
			var url = Global.baseUrl + 'dullgradelevelone/getDullGradeAndHashStructureForCurrentRepair'
			var body = {
				toolRepair: $scope.selectedTools[0]
			}

			$scope.persistedDullGradeId = $scope.persistedDullGradeId ? $scope.persistedDullGradeId : (persistedCurrentDullGrade && persistedCurrentDullGrade.dullGradeId ? persistedCurrentDullGrade.dullGradeId : null)

			$http.post(url, body).success(function(dullgrades) {
				$scope.dullgrades = dullgrades

				if($scope.includeCombinedDullgrades) {
					let combinedDullGrade = combineDullGradesAndReturn(dullgrades)
					$scope.dullgrades.push(combinedDullGrade);
				}

				setDullGradeStrings($scope.dullgrades)
				var groups = [], dullGrade = {}
				if(dullgrades.length > 0) {
					if($scope.persistedDullGradeId) {
						dullGrade = _.find(dullgrades, function(dg) {
							return dg.dullGradeId === $scope.persistedDullGradeId
						})
						groups = dullGrade.groups
					} else {
						dullGrade = dullgrades[dullgrades.length - 1]
						groups = dullgrades[dullgrades.length - 1].groups
					}

					$scope.latestDullGrade = dullgrades[dullgrades.length - 1]
				}

				$scope.dullGrade = dullGrade
				$scope.groups = groups

				if (cb) cb($scope.dullgrades)
				else {
					_.each($scope.dullgrades, function(dg) {

						if (dg.groups.length === 0) toastr.warning('This repair does not have any dullgrade info associated with it.')
						else {
							setSizeClasses(dg.groups)
							getAllPockets1D(dg.groups)
							debugger
							if (!onlyLoadNecessary) {
								setFirstPocketAsActive(dg.groups)
								set3dProperties(dg.groups)
							}
						}
					})
					if (cb2) cb2($scope.dullgrades)
				}
				$scope.$broadcast('getDullGradeStructureForCurrentRepair:executed')
			}).error(function(err) {
				toastr.error('There was a problem getting the pocket structure for this part.')
			})
		}
	}

	function setSizeClassesWithoutGettingSizesAndTypes(groups) {
		_.each(groups, function(group) {
			if (group.blades && group.blades.length > 0) {
				// loop through blades
				_.each(group.blades, function(blade) {
					if (blade.rows && blade.rows.length > 0) {
						// loop through rows
						_.each(blade.rows, function(row) {
							if (row.pockets && row.pockets.length > 0) {
								// loop through pockets
								_.each(row.pockets, function(pocket) {
									var size = _.find($scope.pocketSizes, function(size) {
										return size.cutterPocketSizeId === pocket.cutterPocketSizeId
									})
									pocket.size = size.sizeFraction
									pocket.sizeClass = size.sizeClass
								})
							}
						})
					}
				})
			}
		})
	}

	function setSizeClasses(groups) {

		// first get pocket sizes
		getPocketSizes(function() {
			// then get pocket types
			getPocketTypes(function() {
				// now set class sizes
				_.each(groups, function(group) {
					if (group.blades && group.blades.length > 0) {
						// loop through blades
						_.each(group.blades, function(blade) {
							if (blade.rows && blade.rows.length > 0) {
								// loop through rows
								_.each(blade.rows, function(row) {
									if (row.pockets && row.pockets.length > 0) {
										// loop through pockets
										_.each(row.pockets, function(pocket) {
											var size = _.find($scope.pocketSizes, function(size) {
												return size.cutterPocketSizeId === pocket.cutterPocketSizeId
											})
											pocket.size = size.sizeFraction
											pocket.sizeClass = size.sizeClass
										})
									}
								})
							}
						})
					}
				})
			})
		})
	}

	function setFirstPocketAsActive(groups) {
		$scope.activePocket = groups[0].blades[0].rows[0].pockets[0]
		$scope.activePocket.activePocket = 'activePocket'
	}

	$scope.pocketSizes = []

	function getPocketSizes(cb) {
		var url = Global.baseUrl + 'Part/getCutterPocketSizes'
		var datas = {}
		$scope.gPost(url, datas, function (err, data) {
			if (err) toastr.error('There was a problem getting pocket sizes, therefore could not finish rendering dull grades.')
			else {
				// assume that the sizes are coming in order of size, smallest to largest
				// TODO: split array in half, and assign appropriate sizeClass to each.
				var l = data.length
				var startSize = 6
				if (l > 1) {
					var middleIndex = parseInt(l / 2) - 1 // take smaller first half, if its an odd length

					startSize = 6
					// start with middle index and go down to 0
					for (var i = middleIndex; i >= 0; i--) {
						data[i].sizeClass = 's' + startSize--
					}
					startSize = 7
					// start with 1 more than middle index and go up to length
					for (var j = middleIndex + 1; j < l; j++) {
						data[j].sizeClass = 's' + startSize++
					}
				} else {
					data[0].sizeClass = 's' + startSize
				}

				$scope.pocketSizes = data
				cb()
			}
		})
	}

	$scope.pocketTypes = []

	function getPocketTypes(cb) {
		var url = Global.baseUrl + 'Part/getCutterPocketTypes'
		var data = {}
		$scope.gPost(url, data, function(err,data) {
			if (err) toastr.error('There was a problem getting pocket types, therefore could not finish rendering dull grades.')
			else {
				console.log('tried to get pocket types')
				$scope.pocketTypes = data
				cb()
			}
		})
	}

	$scope.openModalSetConditionActionCutter = function(pocket) {
		if(!$scope.originalAvailableActions){
			$scope.originalAvailableActions = $scope.availableActions
		} else {
			$scope.availableActions = $scope.originalAvailableActions
		}
		makeClickedPocketActive(pocket)
		debugger;
		$scope.currentPocket = JSON.parse(JSON.stringify(pocket))
		$scope.currentPocketToSave = pocket

		if ($scope.reworkTypeAddCutter) {
			var availableActions
			if(!pocket.oldInfo){
				pocket.oldInfo = {
					cutterActionId: pocket.cutterActionId,
					cutterAction: pocket.cutterAction,
					cutterConditionId: pocket.cutterConditionId,
					cutterCondition: pocket.cutterCondition
				}
			}
			_.each($scope.dullGrade.groups, function(g){
				_.each(g.blades, function(b) {
					_.each(b.rows, function(r) {
						_.each(r.pockets, function(p) {

							if($scope.currentPocket.group === p.group && $scope.currentPocket.blade === p.blade && $scope.currentPocket.row === p.row && $scope.currentPocket.pocket === p.pocket) {
								if(p.cutterActionId === 3 || !p.cutterActionId){
									availableActions = $filter('filter')($scope.availableActions, {cutterActionId: "!3"})
									$scope.availableActions = availableActions
								} else {
									availableActions = $filter('filter')($scope.availableActions, {cutterActionId: "1"})
									$scope.availableActions = availableActions
								}
							}
						})
					})
				})
			})
		}
		if($scope.isReworkNotAddCutter) {
			availableActions = $filter('filter')($scope.availableActions, {cutterActionId: "!3"})
			$scope.availableActions = availableActions
		}
		$('#modalSetConditionActionCutter').modal('show')
	}
	$scope.closeCancelModalSetConditionActionCutter = function() {
		$scope.currentPocket.cutter = null
		$scope.currentPocket.condition = null
		$scope.currentPocket.action = null
		$('#modalSetConditionActionCutter').modal('hide')
	}

	$scope.availableConditions = []
	$scope.availableActions = []
	$scope.availableConditionActionCombos = []
	$scope.getConditions = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getCutterConditions'
		var data = {}
		$scope.gPost(url, data,function(err, data) {
			if (err) toastr.error('There was a problem getting cutter conditions.')
			else {
				$scope.availableConditions = data
			}
		})
	}
	$scope.getActions = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getCutterActions'
		var data = {}
		$scope.gPost(url, data, function(err, data) {
			if (err) toastr.error('There was a problem getting cutter actions.')
			else {
				$scope.availableActions = data
			}
		})	
	}
	$scope.getConditionActionCombos = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getConditionActionCombos'
		var data = {}
		$scope.gPost(url, data, function(err, data) {
			if (err) toastr.error('There was a problem getting condition/action combos.')
			else {
				$scope.availableConditionActionCombos = data
			}
		})
	}

	$scope.setCutter = function(cutter) {
		$scope.currentPocket.cutter = cutter
	}
	$scope.setCondition = function(condition) {
		$scope.currentPocket.cutterConditionId = condition.cutterConditionId
		$scope.currentPocket.cutterCondition = condition.cutterConditionCode
	}
	$scope.setAction = function(action) {
		$scope.currentPocket.cutterActionId = action.cutterActionId
		$scope.currentPocket.cutterAction = action.cutterActionCode

		$scope.currentPocketToSave.cutterConditionId = $scope.currentPocket.cutterConditionId
		$scope.currentPocketToSave.cutterCondition = $scope.currentPocket.cutterCondition
		$scope.currentPocketToSave.cutterActionId = $scope.currentPocket.cutterActionId
		$scope.currentPocketToSave.cutterAction = $scope.currentPocket.cutterAction

		if ($scope.reworkTypeAddCutter){
			if ($scope.currentPocketToSave.oldInfo.cutterActionId === $scope.currentPocketToSave.cutterActionId && $scope.currentPocketToSave.oldInfo.cutterConditionId === $scope.currentPocketToSave.cutterConditionId){
				$scope.currentPocketToSave.hasBeenChanged = false;
			} else {
				$scope.currentPocketToSave.hasBeenChanged = true;
			}	
		}
		//$scope.saveDullGrade();
		$('#modalSetConditionActionCutter').modal('hide')
		makeNextPocketActive()
	}

	$scope.clearConditionAndAction = function() {
		delete $scope.currentPocket.cutterConditionId
		delete $scope.currentPocket.cutterCondition
		delete $scope.currentPocket.cutterActionId
		delete $scope.currentPocket.cutterAction

		delete $scope.currentPocketToSave.cutterConditionId
		delete $scope.currentPocketToSave.cutterCondition
		delete $scope.currentPocketToSave.cutterActionId
		delete $scope.currentPocketToSave.cutterAction

		if ($scope.reworkTypeAddCutter) {
			if ($scope.currentPocketToSave.oldInfo.cutterActionId === $scope.currentPocketToSave.cutterActionId && $scope.currentPocketToSave.oldInfo.cutterConditionId === $scope.currentPocketToSave.cutterConditionId) {
				$scope.currentPocketToSave.hasBeenChanged = false;
			} else {
				$scope.currentPocketToSave.hasBeenChanged = true;
			}
		}

		$('#modalSetConditionActionCutter').modal('hide')
	}

	$scope.updateAllDullGrades = function() {
		var cutterDullGrades = []
		_.each($scope.groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {
					_.each(r.pockets, function(p) {
						if (p.fs) {
							p.isFlameSpray = p.fs.isFlameSpray ? 1 : 0
							p.cutterActionId = p.fs.cutterActionId
							p.cutterAction = p.fs.cutterAction
						}
						if (p.cutterDullGradeId || p.cutterActionId || p.cutterConditionId || p.isFlameSpray) {
							p.dnrRepairId = $scope.selectedTools[0].repairId
							cutterDullGrades.push(p)
						}
					})
				})
			})
		})

		var url = Global.baseUrl + 'dullgradelevelone/updateDullGrades'
		var body = {
			dullgrades: cutterDullGrades,
			dullGradeId: $scope.dullGrade.dullGradeId,
			repairId: $scope.selectedTools[0].repairId
		}
		debugger;
		$scope.gPost(url, body, function(err,data) {
			if (err) {
				toastr.error('There was a problem saving the cutter dullgrades.')
				$scope.getFlowRecords()
			} else {
				toastr.success('Dullgrades were successfully saved.')
				// $route.reload();
				// if($scope.selectedTools[0].toolFamilyId === 4) {
				// 	setTimeout(function() {
				// 		$location.path('/dullgrade-home')
				// 		$scope.$apply()
				// 	}, 500)
				// }
			}
		})
	}

	$scope.saveReworkCutterDullGrade = function() {
		debugger;
		if($scope.originalAvailableActions){
			$scope.availableActions = $scope.originalAvailableActions
		}
		var dullgrades = []
		_.each($scope.groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {
					_.each(r.pockets, function(p) {
						if(p.hasBeenChanged === true){
							if (p.fs) {
								p.isFlameSpray = p.fs.isFlameSpray ? 1 : 0
								p.cutterActionId = p.fs.cutterActionId
								p.cutterAction = p.fs.cutterAction
							}
							if (p.cutterDullGradeId || p.cutterActionId || p.cutterConditionId || p.isFlameSpray) {
								dullgrades.push(p)
							}
							if ($scope.reworkTypeAddCutter) {
								p.isAddOnRecord = 1
							}
						}	
					})
				})
			})
		})
		debugger;
		var url = Global.baseUrl + 'dullgradelevelone/saveReworkCutterDullGrade'
		var body = {}
		if($scope.dullGrade){
			body = {
				dullGrades: dullgrades,
				dullGradeId: $scope.dullGrade.dullGradeId
			}
		} else {
			body = {
				dullGrades: dullgrades,
				repairId: $scope.selectedTools[0].repairId
			}
		}

		$http.post(url, body).success(function(data) {
			debugger;
			toastr.success("Rework was successfully saved")
			if($scope.reworkTypeAddCutter){
				setTimeout(function() {
					$location.path($scope.currentUrl)
					$scope.$apply()
				}, 500)
			} else {
				debugger;
				var needsToGoToBraze = false;
				var needsToGoToPullCutters = false;
				_.each($scope.groups, function(g) {
					_.each(g.blades, function(b) {
						_.each(b.rows, function(r) {
							_.each(r.pockets, function(p) {
								if(p.cutterActionId === 1) {
									needsToGoToPullCutters = true
								}
								if(p.cutterActionId === 2) {
									needsToGoToBraze = true
								}
							})
						})
					})
				})
				debugger;
				if($scope.currentRunningLaborTimeStamps && $scope.currentRunningLaborTimeStamps.length > 0){
					if(needsToGoToPullCutters) {
						// send to pull cutters for baker bit and vernal facility
						$scope.nextFlowSelected({nextFlowId: 14}, null, null, function () {
							$scope.getFlowRecords();
							setTimeout(function() {
								$location.path($scope.currentUrl)
								$scope.$apply()
							}, 500)
						})
					} else if(needsToGoToBraze) {
						// send to braze for baker bit in vernal facility
						$scope.nextFlowSelected({nextFlowId: 22}, null, null, function () {
							$scope.getFlowRecords();
							setTimeout(function() {
								$location.path($scope.currentUrl)
								$scope.$apply()
							}, 500)
						})
					} else {
						setTimeout(function() {
							$location.path($scope.currentUrl)
							$scope.$apply()
						}, 500)
					}
				} else {
					if(needsToGoToPullCutters) {
						// send to pull cutters for baker bit and vernal facility
						sendToNextFlowWithoutTimeStamp($scope.selectedTools[0], {nextFlowId: 14}, function () {
							$scope.getFlowRecords();
							setTimeout(function() {
								$location.path($scope.currentUrl)
								$scope.$apply()
							}, 500)
						})
					} else if(needsToGoToBraze) {
						// send to braze for baker bit in vernal facility
						sendToNextFlowWithoutTimeStamp($scope.selectedTools[0], {nextFlowId: 22}, function () {
							$scope.getFlowRecords();
							setTimeout(function() {
								$location.path($scope.currentUrl)
								$scope.$apply()
							}, 500)
						})
					} else {
						setTimeout(function() {
						   $location.path($scope.currentUrl)
						   $scope.$apply()
					   }, 500)
					}
				}
			}
		}).error(function(err) {
			debugger;
			toastr.error('There was a problem saving the dullgrades.')
			$scope.getFlowRecords()
		})
	}

	$scope.saveDullGrade = function() {
		var url = Global.baseUrl + 'dullgradelevelone/saveDullGrade'
		var body = $scope.currentPocketToSave
		$http.post(url, body).success(function(data) {
			// be silent
		}).error(function(err) {
			toastr.error('There was a problem trying to save the dullgrade record.')
			$scope.getDullGradeStructureForCurrentRepair()
		})
	}

	$scope.completeDullGrade = function() {
		var url = Global.baseUrl + 'dullgradelevelone/completeDullGrade'
		var body = $scope.currentTool
		$http.post(url, body).success(function(data) {
			toastr.success('Saved successfully and passed on to Pull Cutters')
			$scope.currentTool = null

			$scope.removeAllUsers()

			$scope.getToolsWithCuttersInDullGrade()
			$('#modalConfirmCompleteDullGrade').modal('hide')
		}).error(function(err) {
			toastr.error('There was a problem trying to save dullgrade information.')
		})
	}

	$scope.pockets1D = []

	function getAllPockets1D(groups) {
		var pockets = []
		_.each(groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {
					_.each(r.pockets, function(p) {
						pockets.push(p)
					})
				})
			})
		})
		$scope.pockets1D = _.chain(pockets).sortBy('pocket').sortBy('row').sortBy('blade').sortBy('group').value()
	}

	$scope.nextPocketActive = function() {
		makeNextPocketActive()
	}
	$scope.firstPocketOnNextBlade = function() {
		firstPocketOnNextBlade()
	}
	$scope.firstPocketOnPreviousBlade = function() {
		firstPocketOnPreviousBlade()
	}

	function firstPocketOnNextBlade() {
		var cloneOfActive = JSON.parse(JSON.stringify($scope.activePocket))
		delete $scope.activePocket.activePocket

		if ($scope.groups[cloneOfActive.group - 1].blades[cloneOfActive.blade]) {
			// There is a next blade on this group, so set first pocket in this blade as active
			$scope.activePocket = $scope.groups[cloneOfActive.group - 1].blades[cloneOfActive.blade].rows[0].pockets[0]
			$scope.activePocket.activePocket = 'activePocket'
		} else {
			// There is not a next blade on this group, so try and go to next group
			if ($scope.groups[cloneOfActive.group]) {
				// There is a next group, so go to next group and set first pocket of first blade as active
				$scope.activePocket = $scope.groups[cloneOfActive.group].blades[0].rows[0].pockets[0]
				$scope.activePocket.activePocket = 'activePocket'
			} else {
				// There is not a next group, so go to first group and set the first pocket of first blade as active
				$scope.activePocket = $scope.groups[0].blades[0].rows[0].pockets[0]
				$scope.activePocket.activePocket = 'activePocket'
			}
		}
		detectGroupOrBladeChangeAndExecuteMisc(cloneOfActive, $scope.activePocket)
	}

	function firstPocketOnPreviousBlade() {
		var cloneOfActive = JSON.parse(JSON.stringify($scope.activePocket))
		delete $scope.activePocket.activePocket

		if ($scope.groups[cloneOfActive.group - 1].blades[cloneOfActive.blade - 2]) {
			// There is a previous blade on this group, so set first pocket in this blade as active
			$scope.activePocket = $scope.groups[cloneOfActive.group - 1].blades[cloneOfActive.blade - 2].rows[0].pockets[0]
			$scope.activePocket.activePocket = 'activePocket'
		} else {
			// There is not a previous blade on this group, so try and go to previous group
			if ($scope.groups[cloneOfActive.group - 2]) {
				// There is a previous group, so go to previous group and set first pocket of first blade as active
				$scope.activePocket = $scope.groups[cloneOfActive.group - 2].blades[0].rows[0].pockets[0]
				$scope.activePocket.activePocket = 'activePocket'
			} else {
				// There is not a previous group, so go to last group and set the last pocket of last blade as active
				var lastGroup = $scope.groups[$scope.groups.length - 1]
				var lastBlade = lastGroup.blades[lastGroup.blades.length - 1]
				var lastRow = lastBlade.rows[lastBlade.rows.length - 1]
				$scope.activePocket = lastRow.pockets[0]
				$scope.activePocket.activePocket = 'activePocket'
			}
		}
		detectGroupOrBladeChangeAndExecuteMisc(cloneOfActive, $scope.activePocket)
	}

	$scope.translateZValue = '-383'

	function detectGroupOrBladeChangeAndExecuteMisc(oldPocket, newPocket) {

		if (oldPocket.group) {
			var familyId = $scope.selectedTools[0].toolFamilyId
			if (oldPocket.group !== newPocket.group || oldPocket.blade !== newPocket.blade) {
				setTimeout(function() {
					$('html, body').animate({
						scrollTop: $('.activePocket:first').offset().top - 300
					}, 700)
				}, 200)

				if (oldPocket.group === newPocket.group) {
					var currentRotateYDegree = typeof $scope.groups[newPocket.group - 1].currentRotateYDegree !== 'undefined' ? $scope.groups[newPocket.group - 1].currentRotateYDegree : 0

					// just change blades
					if (oldPocket.blade < newPocket.blade && Math.abs(newPocket.blade - oldPocket.blade) === 1) {
						// positive rotation
						currentRotateYDegree = currentRotateYDegree + $scope.degreeIncrement
					} else if (oldPocket.blade > newPocket.blade && newPocket.blade - oldPocket.blade < -1) {
						// positive rotation
						currentRotateYDegree = currentRotateYDegree + $scope.degreeIncrement
					} else {
						// negative rotation
						currentRotateYDegree = currentRotateYDegree - $scope.degreeIncrement
					}
					$scope.groups[newPocket.group - 1].currentRotateYDegree = currentRotateYDegree

					var carouselClass = 'carousel-horiz'
					var carouselStyle = 'transform: translateZ( ' + $scope.translateZValue + 'px ) rotateX(' + currentRotateYDegree + 'deg)'
					if (familyId === 4) {
						carouselClass = 'carousel'
						carouselStyle = 'transform: translateZ( ' + $scope.translateZValue + 'px ) rotateY(' + currentRotateYDegree + 'deg)'
					}
					var carousels = $('.' + carouselClass)
					var carousel = carousels.get([newPocket.group - 1])
					carousel.style = carouselStyle
					carousel.dataset.deg = currentRotateYDegree
					$scope.groups[newPocket.group - 1].currentRotateYDegree = currentRotateYDegree
				} else {
					// continue to spin the previous group in positive direction
					var carouselClass = 'carousel-horiz'
					var carouselStyle = 'transform: translateZ( ' + $scope.translateZValue + 'px ) rotateX(' + currentRotateYDegree + 'deg)'
					if (familyId === 4) {
						carouselClass = 'carousel'
						carouselStyle = 'transform: translateZ( ' + $scope.translateZValue + 'px ) rotateY(' + currentRotateYDegree + 'deg)'
					}
					var carousels = $('.' + carouselClass)
					var carousel = carousels.get([oldPocket.group - 1])
					carousel.style = carouselStyle
					carousel.dataset.deg = currentRotateYDegree
					$scope.groups[oldPocket.group - 1].currentRotateYDegree = currentRotateYDegree
				}
			}
		}
	}

	function makePreviousPocketActive() {
		var index = 0
		var isFound = false

		var cloneOfActive = _.find($scope.pockets1D, function(p) {
			if (p.cutterStructureId && p.cutterStructureId === $scope.activePocket.cutterStructureId) {
				isFound = true
				return true
			} else if (p.partBakerBit_CutterStructureId && p.partBakerBit_CutterStructureId === $scope.activePocket.partBakerBit_CutterStructureId) {
				isFound = true
				return true
			} else {
				if (!isFound) index++
			}
		})

		if (typeof cloneOfActive !== 'undefined' && cloneOfActive) {
			delete $scope.activePocket.activePocket

			var prevCloneOfActive
			if (!$scope.pockets1D[index - 1]) {
				prevCloneOfActive = $scope.pockets1D[$scope.pockets1D.length - 1]
			} else {
				prevCloneOfActive = $scope.pockets1D[index - 1]
			}

			isFound = false
			_.each($scope.groups, function(g) {
				if (!isFound) {
					_.each(g.blades, function(b) {
						if (!isFound) {
							_.each(b.rows, function(r) {
								if (!isFound) {
									_.each(r.pockets, function(p) {
										if (p.cutterStructureId && prevCloneOfActive.cutterStructureId) {
											if (p.cutterStructureId === prevCloneOfActive.cutterStructureId) {

												$scope.activePocket = p
												$scope.activePocket.activePocket = 'activePocket'
												detectGroupOrBladeChangeAndExecuteMisc(cloneOfActive, $scope.activePocket)
												isFound = true
											}
										} else if (p.partBakerBit_CutterStructureId && prevCloneOfActive.partBakerBit_CutterStructureId) {
											if (p.partBakerBit_CutterStructureId === prevCloneOfActive.partBakerBit_CutterStructureId) {

												$scope.activePocket = p
												$scope.activePocket.activePocket = 'activePocket'
												detectGroupOrBladeChangeAndExecuteMisc(cloneOfActive, $scope.activePocket)
												isFound = true
											}
										}
									})
								}
							})
						}
					})
				}
			})
		}
	}

	function makeNextPocketActive() {
		var index = 0
		var isFound = false

		var cloneOfActive = _.find($scope.pockets1D, function(p) {
			if (p.cutterStructureId && p.cutterStructureId === $scope.activePocket.cutterStructureId) {
				isFound = true
				return true
			} else if (p.partBakerBit_CutterStructureId && p.partBakerBit_CutterStructureId === $scope.activePocket.partBakerBit_CutterStructureId) {
				isFound = true
				return true
			} else {
				if (!isFound) index++
			}
		})

		if (typeof cloneOfActive !== 'undefined' && cloneOfActive) {

			delete $scope.activePocket.activePocket

			var nextCloneOfActive
			if ($scope.pockets1D[index + 1]) {
				nextCloneOfActive = $scope.pockets1D[++index]
			} else {
				nextCloneOfActive = $scope.pockets1D[0]
			}

			isFound = false
			_.each($scope.groups, function(g) {
				if (!isFound) {
					_.each(g.blades, function(b) {
						if (!isFound) {
							_.each(b.rows, function(r) {
								if (!isFound) {
									_.each(r.pockets, function(p) {

										if (!isFound) {
											if (p.cutterStructureId && nextCloneOfActive.cutterStructureId) {
												if (p.cutterStructureId === nextCloneOfActive.cutterStructureId) {

													$scope.activePocket = p
													$scope.activePocket.activePocket = 'activePocket'
													detectGroupOrBladeChangeAndExecuteMisc(cloneOfActive, $scope.activePocket)
													isFound = true
												}
											} else if (p.partBakerBit_CutterStructureId && nextCloneOfActive.partBakerBit_CutterStructureId) {
												if (p.partBakerBit_CutterStructureId === nextCloneOfActive.partBakerBit_CutterStructureId) {

													$scope.activePocket = p
													$scope.activePocket.activePocket = 'activePocket'
													detectGroupOrBladeChangeAndExecuteMisc(cloneOfActive, $scope.activePocket)
													isFound = true
												}
											}
										}
									})
								}
							})
						}
					})
				}
			})
		}
	}

	function makeClickedPocketActive(pocket) {
		delete $scope.activePocket.activePocket

		$scope.activePocket = pocket
		$scope.activePocket.activePocket = 'activePocket'
	}

	$scope.activePocket = {}
	$scope.assignConditionActionToActivePocket = function(condition, action) {

		if (!condition && !action) {
			$scope.activePocket.cutterConditionId = null
			$scope.activePocket.cutterCondition = null
			$scope.activePocket.cutterActionId = null
			$scope.activePocket.cutterAction = null
			makeNextPocketActive()
		} else {
			if (condition && action) {
				$scope.activePocket.cutterConditionId = condition.cutterConditionId
				$scope.activePocket.cutterCondition = condition.cutterConditionCode
				$scope.activePocket.cutterActionId = action.cutterActionId
				$scope.activePocket.cutterAction = action.cutterActionCode
				makeNextPocketActive()
			} else if (action) {
				$scope.activePocket.cutterActionId = action.cutterActionId
				$scope.activePocket.cutterAction = action.cutterActionCode
				makeNextPocketActive()
			} else {
				$scope.activePocket.cutterConditionId = condition.cutterConditionId
				$scope.activePocket.cutterCondition = condition.cutterConditionCode
			}
		}
	}

	$scope.checkIfHasAllRequiredAncillaryInformation = function(cb) {
		var allRequiredAncInfoEntered = false

		$scope.getAncillaryInfo()

		if ($scope.selectedTools && $scope.selectedTools[0] && $scope.selectedTools[0].partId && $scope.selectedTools[0].stockPointReceivedFromId) {
			allRequiredAncInfoEntered = true
		}

		if (cb) {
			cb(allRequiredAncInfoEntered)
		}
	}

	var timeBetweenKeyPresses
	$scope.keyPressed = function(e, cb) {
		debugger;
		var now = new Date().getTime()
		if (timeBetweenKeyPresses && Math.abs(timeBetweenKeyPresses - now) < 100) {} else {
			timeBetweenKeyPresses = now
			var codeForGoToNextPocket = 105
			var codeForGoToPreviousPocket = 104
			var codeForGoToNextBladePocket = 106
			var codeForGoToPreviousBladePocket = 107

			var goRight = 61
			var goLeft = 45

			if (e.charCode === codeForGoToNextBladePocket && $scope.rotateInsteadOfSwitchBlades) e.charCode = goRight
			if (e.charCode === codeForGoToPreviousBladePocket && $scope.rotateInsteadOfSwitchBlades) e.charCode = goLeft

			if (e.charCode === goRight || e.charCode === goLeft) {
				var groupIndex = $scope.groupIndex ? $scope.groupIndex : 0
				var currentDeg = $(e.target).data('deg')

				var transformProp = Modernizr.prefixed('transform')
				var currentRotateYDegree = typeof groupIndex === 'undefined' || typeof $scope.groups[groupIndex].currentRotateYDegree === 'undefined' ? 0 : $scope.groups[groupIndex].currentRotateYDegree

				if (e.charCode === goLeft) {
					currentRotateYDegree = currentRotateYDegree + $scope.degreeIncrement
				}
				if (e.charCode === goRight) {
					currentRotateYDegree = currentRotateYDegree - $scope.degreeIncrement
				}

				var classString = 'carousel'
				var stringRotateYorX = 'rotateY'
				if ($scope.selectedTools[0].toolFamilyId !== 4) {
					classString = 'carousel-horiz'
					stringRotateYorX = 'rotateX'
				}

				var carousel = $('.' + classString).get(groupIndex)

				console.log(currentDeg + ' | ' + currentRotateYDegree)

				carousel.style = 'transform: translateZ( ' + $scope.translateZValue + 'px ) ' + stringRotateYorX + '(' + currentRotateYDegree + 'deg)'
				carousel.style.webkitTransform = 'translateZ( ' + $scope.translateZValue + 'px ) ' + stringRotateYorX + '(' + currentRotateYDegree + 'deg)'
				carousel.dataset.deg = currentRotateYDegree

				$scope.groups[groupIndex].currentRotateYDegree = currentRotateYDegree
				didTouchMove = true
			} else {

				// vertical view
				$('#modalSetConditionActionCutter').modal('hide')

				if (e.charCode === codeForGoToNextPocket) {
					makeNextPocketActive()
				} else if (e.charCode === codeForGoToPreviousPocket) {
					makePreviousPocketActive()
				} else if (e.charCode === codeForGoToNextBladePocket) {
					firstPocketOnNextBlade()
				} else if (e.charCode === codeForGoToPreviousBladePocket) {
					firstPocketOnPreviousBlade()
				} else {
					var conditionActionCombo = _.find($scope.availableConditionActionCombos, function(combo) {

						return parseInt(combo.keyCodeValue) === parseInt(e.keyCode)
					})

					if (conditionActionCombo) {
						var thisCondition = _.find($scope.availableConditions, function(condition) {
							return condition.cutterConditionId === conditionActionCombo.cutterConditionId
						})
						var thisAction = _.find($scope.availableActions, function(action) {
							return action.cutterActionId === conditionActionCombo.cutterActionId
						})
						$scope.assignConditionActionToActivePocket(thisCondition, thisAction)
					} else {
						var thisAction = _.find($scope.availableActions, function(action) {
							return parseInt(action.keyCodeValue) === parseInt(e.keyCode)
						})
						if (thisAction) {
							$scope.assignConditionActionToActivePocket(null, thisAction)

						} else {
							var thisCondition = _.find($scope.availableConditions, function(condition) {
								return parseInt(condition.keyCodeValue) === parseInt(e.keyCode)
							})
							if (thisCondition) {
								$scope.assignConditionActionToActivePocket(thisCondition, null)
							}
						}
					}
				}
			}
			if (cb) cb()
		}
	}

	// *********************************************************
	// Cutter Structure Stuff
	// *********************************************************
	$scope.getCutterStructureForCurrentPart = function(cb, args, cb2) {
		debugger
		if ($scope.selectedTools[0]) {
			$scope.groups = []
			var url = Global.baseUrl + 'Part/GetPocketStructureForPart'

			var body = {
				cutterStructureChronoId: $scope.selectedTools[0].cutterStructureChronoId,
				partId: $scope.selectedTools[0].partId,
				toolFamilyId: $scope.selectedTools[0].toolFamilyId
			}
			if(args) body = args;

			$http.post(url, body).success(function(groups) {
				$scope.groups = groups
				$scope.doesMarkupExist = doesMarkupExist(groups);
				if (cb) {
					cb(groups)
				} else {
					if (groups.length === 0) {
						if ($scope.selectedTools[0].toolFamilyId === 4) {
							$scope.addGroup()
						}
						toastr.warning('This part does not have any cutter structure associated with it.')
					} else {
						$('#markupModal').modal('show')
					}

					$scope.createCutterTypeLabels(groups)
					$scope.setColorClasses(groups, true)
					setSizeClasses($scope.groups)
					debugger
					set3dProperties($scope.groups)

					if(cb2) cb2();
				}
			}).error(function(err) {
				toastr.error('There was a problem getting the pocket structure for this part.')
			})
		}
	}

	/**
	 * Gets the hash structure if there exists a hashId OR defaults to getting the part cutter structure if only a partId exists. Right now this is specific to Non Bit (Non SDPI) Tools.
	 * @param cb
	 * @param args
	 * @param cb2
	 */
	$scope.getCutterOrHashStructure = function(cb, args, cb2) {
		if ($scope.selectedTools[0]) {
			$scope.groups = []
			if([4].indexOf($scope.selectedTools[0].toolFamilyId) !== -1) {
				// if its a bit, then execute original getCutterStructureByPart function
				$scope.getCutterStructureForCurrentPart(cb, args, cb2);
			} else {
				let url = Global.baseUrl + 'part/getCutterOrHashStructure'
				let body = {
					hashId: $scope.selectedTools[0].hashId,
					toolRepair: $scope.selectedTools[0],
					toolFamilyId: $scope.selectedTools[0].toolFamilyId
				}
				if(args) body = args;

				gPost(url, body, function(err, groups){
					if (err) toastr.error(err);
					else {
						$scope.$broadcast('getCutterOrHashStructure:executed')
						$scope.groups = groups
						$scope.doesMarkupExist = doesMarkupExist(groups);
						if (cb) {
							cb(groups)
						} else {
							if (groups.length === 0) {
								if ($scope.selectedTools[0].toolFamilyId === 4) {
									$scope.addGroup()
								}
								toastr.warning('This part does not have any cutter structure associated with it.')
							} else {
								$('#markupModal').modal('show')
							}

							// $scope.createCutterTypeLabels(groups)
							// $scope.setColorClasses(groups, true)
							// setSizeClasses($scope.groups)
							// debugger
							// set3dProperties($scope.groups)

							if (cb2) cb2();
						}
						$scope.$broadcast('getCutterOrHashStructure:executed')
					}
				})
			}
		}
	}

	function doesMarkupExist(groups) {
		var hasMarkup = false;
		groups.map(group => {
			group.blades.map(blade => {
				blade.rows.map(row => {
					if(row.pockets && row.pockets.length > 0) hasMarkup = true;
				})
			})
		})
		return hasMarkup;
	}

	function set3dProperties(groups) {
		var familyId = $scope.selectedTools[0].toolFamilyId

		if (!$scope.bladeWidth) familyId === 4 ? $scope.bladeWidth = 288 : $scope.bladeWidth = 293
		_.each($scope.groups, function(g) {
			var qty = g.blades.length
			var radians = getRadians($scope.bladeWidth, qty)
			var deg = getDegree(qty)
			$scope.degreeIncrement = deg

			var currentDeg = 0

			if (familyId === 4) {
				// vertical view
				_.each(g.blades, function(b) {
					b.deg = currentDeg
					b.radians = radians
					b.class3d = '-webkit-transform: rotateY(' + currentDeg + 'deg) translateZ(' + radians + 'px); -moz-transform: rotateY(' + currentDeg + 'deg) translateZ(' + radians + 'px); -o-transform: rotateY(' + currentDeg + 'deg) translateZ(' + radians + 'px); transform: rotateY(' + currentDeg + 'deg) translateZ(' + radians + 'px);'
					b.class3dbg = 'background: hsla(19, 0%, 36%, 0.95 );'
					currentDeg -= deg
				})
			} else {
				// horizontal view
				_.each(g.blades, function(b) {
					b.deg = currentDeg
					b.radians = radians
					b.class3d = '-webkit-transform: rotateX(' + currentDeg + 'deg) translateZ(' + radians + 'px); -moz-transform: rotateX(' + currentDeg + 'deg) translateZ(' + radians + 'px); -o-transform: rotateX(' + currentDeg + 'deg) translateZ(' + radians + 'px); transform: rotateX(' + currentDeg + 'deg) translateZ(' + radians + 'px);'
					b.class3dbg = 'background: hsla(19, 0%, 36%, 0.95 );'
					currentDeg -= deg
				})
			}
			//$scope.threeDimensionalPanelContainerTransformStyle = 'transform: rotateY(' + $scope.degreeIncrement + 'deg) translateZ(-288px);';
		})

		var classString = 'carousel'
		var stringRotateYorX = 'rotateY'
		if (familyId !== 4) {
			classString = 'carousel-horiz'
			stringRotateYorX = 'rotateX'
		}

		setTimeout(function() {
			var count = 0
			while (count < 5) {
				var groupIndex = count
				var carousel = $('.' + classString).get(groupIndex)
				if (carousel) {
					debugger;
					carousel.style = 'transform: translateZ( ' + $scope.translateZValue + 'px ) ' + stringRotateYorX + '(' + 0 + 'deg)'
					carousel.dataset.deg = $currentRotateYDegree

					$scope.groups[groupIndex].currentRotateYDegree = currentRotateYDegree
					didTouchMove = true

				}
				count++
			}

			var obj = {
				charCode: 61
			}
			$scope.keyPressed(obj)
			obj.charCode = 45
			$scope.keyPressed(obj)
		}, 100)

		//$scope.$apply();
	}

	function getRadians(panelWidth, numberOfPanels) {

		return Math.round((panelWidth / 2) / Math.tan(((Math.PI * 2) / numberOfPanels) / 2))
	}

	function getDegree(qty) {
		return 360 / qty
	}

	$scope.cutterStructure = []

	$scope.addGroup = function() {
		$scope.groups.push({
			'order': $scope.groups.length + 1,
			blades: [{
				order: 1,
				rows: [{
					order: 1
				}]
			}]
		})
	}
	$scope.bladeIndex = 1
	$scope.addBlade = function(group) {
		if (!group.blades || group.blades.length === 0) {
			group.blades = []
		}
		group.blades.push({
			'index': $scope.bladeIndex++
		})
		group.blades[group.blades.length - 1].rows = []
		group.blades[group.blades.length - 1].rows.push({
			order: 1
		})

		group.bladeQty = group.blades.length

		$scope.reorderBlades(group)
		debugger
		set3dProperties($scope.groups)
	}

	$scope.pseudoId = 1

	$scope.addPocket = function(row) {
		if (!row.pockets || row.pockets === 0) {
			row.pockets = []
		}
		var pocket = {}

		if ($scope.selectedTools[0].toolFamilyId !== 4) {
			pocket.part_CutterStructureId = 'pseudo-' + $scope.pseudoId
			pocket.type = 'empty'
		} else {
			pocket.partBakerBit_CutterStructureId = 'pseudo-' + $scope.pseudoId
			pocket.type = 'empty'
		}

		if ($scope.activePocketType && $scope.activePocketSize) {
			pocket.type = $scope.activePocketType.key
			pocket.cutterPocketTypeId = $scope.activePocketType.cutterPocketTypeId
			pocket.sizeClass = $scope.activePocketSize.sizeClass
			pocket.cutterPocketSizeId = $scope.activePocketSize.cutterPocketSizeId
			pocket.size = $scope.activePocketSize.sizeFraction
			// assign cutter if one is active
			if ($scope.selectedCutter) {
				pocket.cutterId = $scope.selectedCutter.cutterId
			}
		}
		row.pockets.push(pocket)

		reassignPocketIndexes(row)
		$scope.setColorClasses($scope.groups, true)

		$scope.pseudoId++
	}

	$scope.setActivePocketType = function(type) {
		$scope.trashActive = false
		if ($scope.activePocketType && $scope.activePocketType.key === type.key) {
			// deselect it
			_.each($scope.pocketTypes, function(t) {
				t.isActive = false
			})
			$scope.activePocketType = undefined
		} else {
			// select it
			_.each($scope.pocketTypes, function(t) {
				if (type.cutterPocketTypeId === t.cutterPocketTypeId) {
					t.isActive = true
				} else {
					t.isActive = false
				}
			})
			$scope.activePocketType = type
		}
		// deselect size
		_.each($scope.pocketSizes, function(s) {
			s.isActive = false
		})
		$scope.activePocketSize = undefined

		// deselect cutter
		$scope.selectedCutter = undefined
	}
	$scope.clearOptions = function() {
		_.each($scope.pocketTypes, function(t) {
			t.isActive = false
		})
		$scope.activePocketType = undefined
		// deselect size
		_.each($scope.pocketSizes, function(s) {
			s.isActive = false
		})
		$scope.activePocketSize = undefined

		// deselect cutter
		$scope.selectedCutter = undefined
	}
	$scope.setActivePocketSize = function(size) {
		$scope.trashActive = false
		if ($scope.activePocketSize && $scope.activePocketSize.sizeClass === size.sizeClass) {
			// deselect it
			_.each($scope.pocketSizes, function(s) {
				s.isActive = false
			})
			$scope.activePocketSize = undefined
		} else {
			// select it
			_.each($scope.pocketSizes, function(s) {
				if (size.cutterPocketSizeId === s.cutterPocketSizeId) {
					s.isActive = true
				} else {
					s.isActive = false
				}
			})
			$scope.activePocketSize = size

			// deselect cutter
			$scope.selectedCutter = undefined

			// open cutter selection
			$scope.showCompatibleCuttersToSelectActive()
		}
	}

	$scope.showCompatibleCuttersForCutterMarkupToSelectActive = function() {
		debugger
		$scope.showCompatibleCutters(null, null, null, 'modalSelectActiveCutter', 'Cutter', null, true)
	}

	$scope.showCompatibleCuttersToSelectActive = function() {
		// initialize
		$scope.currentAddCutter = {}
		$scope.cutterEditMode = false
		$scope.addCutterMode = false
		// open cutter selection
		if ($scope.activePocketType && $scope.activePocketSize) {
			var semiPocket = {
				cutterPocketTypeId: $scope.activePocketType.cutterPocketTypeId,
				cutterPocketSizeId: $scope.activePocketSize.cutterPocketSizeId
			}
			// $scope.showCompatibleCutters = function(cutterTypeLabel, optionalCutterPocket, isInclusive, idOfModalToOpen)
			$scope.showCompatibleCutters(null, semiPocket, true, 'modalSelectActiveCutter')
		}
	}
	$scope.removeLastPocket = function(row) {
		$scope.maxCuttersOnOnePocket = 0

		if (row && row.pockets) {
			row.pockets.splice(-1, 1)
		}
		_.each($scope.groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {

					// update max cutters on one pocket and update container height (.container-3d)
					if (r.pockets && r.pockets.length > $scope.maxCuttersOnOnePocket) {

						$scope.maxCuttersOnOnePocket = r.pockets.length
					}
				})
			})
		})
		reassignPocketIndexes(row)
	}
	reassignPocketIndexes = function(row) {
		var index = 1
		_.each(row.pockets, function(p) {
			p.pocket = index
			index++
		})
	}
	$scope.setActiveCutter = function(cutter) {

		$scope.selectedCutter = cutter
		$('#modalSelectActiveCutter').modal('hide')
	}
	$scope.setCutterForHashStructureRecord = function(row, hashStructureId, primaryKeyId, assignAllWithThisPocketSizeType, myPocket) {
		debugger

		for (let pocket of row.pockets) {
			if(!pocket.lotNumber) {
				// check if it has lot number
				if(assignAllWithThisPocketSizeType) {
					if($scope.selectedCutter && $scope.selectedCutter.cutterPocketId === pocket.cutterPocketId) {
						pocket.cutterId = $scope.selectedCutter.cutterId
						$scope.setColorClasses($scope.groups, true)
					}
				} else if(hashStructureId) {
					if(pocket.hashStructureId === hashStructureId && $scope.selectedCutter && $scope.selectedCutter.cutterPocketId === pocket.cutterPocketId) {
						pocket.cutterId = $scope.selectedCutter.cutterId
						$scope.setColorClasses($scope.groups, true)
					}
				} else if(primaryKeyId) {
					if (pocket.primaryKeyId === primaryKeyId && $scope.selectedCutter && $scope.selectedCutter.cutterPocketId === pocket.cutterPocketId) {
						pocket.cutterId = $scope.selectedCutter.cutterId
						$scope.setColorClasses($scope.groups, true)
					}
				} else return
			}
		}
	}
	$scope.setAttributes = function(row, id, optionalCutterPocket, idOfModalToOpen) {

		if ($scope.selectedTools[0].toolFamilyId !== 4) {
			_.each(row.pockets, function(r, key) {

				if (r.part_CutterStructureId === id) {
					if (!$scope.activePocketType && !$scope.activePocketSize && !$scope.trashActive) {
						if (optionalCutterPocket.cutterPocketTypeId && optionalCutterPocket.cutterPocketSizeId) {
							$scope.showCompatibleCutters(null, optionalCutterPocket, true, idOfModalToOpen)
						}
					} else if ($scope.trashActive) {
						var pocketsAfterDelete = _.filter(row.pockets, function(p) {
							return p.part_CutterStructureId !== r.part_CutterStructureId
						})
						row.pockets = pocketsAfterDelete
					} else {
						// SET THE ATTRIBUTES THAT ARE SELECTED
						var validTypeSize = _.filter($scope.cutterPockets, function(cp) {
							return $scope.activePocketType.cutterPocketTypeId === cp.cutterPocketTypeId && $scope.activePocketSize.cutterPocketSizeId === cp.cutterPocketSizeId
						})

						if (validTypeSize.length > 0) {
							if ($scope.activePocketType && $scope.activePocketSize) {
								r.type = $scope.activePocketType.key
								r.cutterPocketTypeId = $scope.activePocketType.cutterPocketTypeId
								r.sizeClass = $scope.activePocketSize.sizeClass
								r.cutterPocketSizeId = $scope.activePocketSize.cutterPocketSizeId
								r.size = $scope.activePocketSize.sizeFraction
								// assign cutter if one is active
								if ($scope.selectedCutter) {
									r.cutterId = $scope.selectedCutter.cutterId
									$scope.setColorClasses($scope.groups, true)
								}
							} else {
								toastr.error('Both type and size need to be selected.')
							}
						} else {
							toastr.warning('There is no such combination of this type and size.')
						}
					}
				}
			})
		} else {
			// all other tools except for bit tools
			_.each(row.pockets, function(r, key) {

				if (r.partBakerBit_CutterStructureId === id) {
					if (!$scope.activePocketType && !$scope.activePocketSize && !$scope.trashActive) {
						if (optionalCutterPocket.cutterPocketTypeId && optionalCutterPocket.cutterPocketSizeId) {
							$scope.showCompatibleCutters(null, optionalCutterPocket, true, idOfModalToOpen)
						}
					} else if ($scope.trashActive) {
						var pocketsAfterDelete = _.filter(row.pockets, function(p) {
							return p.partBakerBit_CutterStructureId !== r.partBakerBit_CutterStructureId
						})
						row.pockets = pocketsAfterDelete
					} else {
						// SET THE ATTRIBUTES THAT ARE SELECTED
						var validTypeSize = _.filter($scope.cutterPockets, function(cp) {
							return $scope.activePocketType.cutterPocketTypeId === cp.cutterPocketTypeId && $scope.activePocketSize.cutterPocketSizeId === cp.cutterPocketSizeId
						})

						if (validTypeSize.length > 0) {
							if ($scope.activePocketType && $scope.activePocketSize) {
								r.type = $scope.activePocketType.key
								r.cutterPocketTypeId = $scope.activePocketType.cutterPocketTypeId
								r.sizeClass = $scope.activePocketSize.sizeClass
								r.cutterPocketSizeId = $scope.activePocketSize.cutterPocketSizeId
								r.size = $scope.activePocketSize.sizeFraction
								// assign cutter if one is active
								if ($scope.selectedCutter) {
									r.cutterId = $scope.selectedCutter.cutterId
									$scope.setColorClasses($scope.groups, true)
								}
							} else {
								toastr.error('Both type and size need to be selected.')
							}
						} else {
							toastr.warning('There is no such combination of this type and size.')
						}
					}
				}
			})
		}
		setSizeClassesWithoutGettingSizesAndTypes($scope.groups)

	}

	$scope.cutterPockets = []
	$scope.getCutterPockets = function(cb) {
		var url = Global.baseUrl + 'Part/getCutterPockets'
		$http.get(url).success(function(data) {
			$scope.cutterPockets = data
			if (cb) cb()
		}).error(function(err) {

		})
	}
	$scope.getCutterPockets()

	$scope.setActiveTrash = function() {
		$scope.activePocketSize = undefined
		$scope.activePocketType = undefined
		$scope.selectedCutter = undefined
		_.each($scope.pocketTypes, function(s) {
			s.isActive = false
		})
		_.each($scope.pocketSizes, function(s) {
			s.isActive = false
		})
		if ($scope.trashActive) $scope.trashActive = false
		else $scope.trashActive = true

	}

	$scope.addRow = function(blade) {
		if (!blade.rows || blade.rows.length === 0) {
			blade.rows = []
		}
		blade.rows.push({
			'index': blade.rows.length + 1
		})

		$scope.reorderRows(blade)
	}

	$scope.removeLastRow = function(blade) {
		if (blade.rows && blade.rows.length > 1) {
			blade.rows.pop()
		} else if (blade.rows && blade.rows.length === 1 && blade.rows[0].pockets && blade.rows[0].pockets.length > 0) {
			blade.rows.pop()
			$scope.addRow(blade)
		}
	}

	$scope.removeLastBlade = function(group) {
		if (group.blades && group.blades.length > 1) {
			group.blades.pop()
		} else if (group.blades && group.blades.length === 1 && group.blades[0].rows && group.blades[0].rows.length > 1) {
			group.blades.pop()
			$scope.addBlade(group)
		}
		debugger
		set3dProperties($scope.groups)
	}

	$scope.removeLastGroup = function() {
		if ($scope.groups && $scope.groups.length > 0) {
			$scope.groups.pop()
		}
	}

	$scope.reorderBlades = function(group) {
		var counter = 1
		_.each(group.blades, function(blade) {
			blade.order = counter++
		})
	}
	$scope.reorderRows = function(blade) {
		var counter = 1
		_.each(blade.rows, function(row) {
			row.order = counter++
		})
	}

	$scope.saveCutterStructureForCurrentPart = function() {
		if ($scope.validateCutterStructure()) {
			var url = Global.baseUrl + 'Part/savePocketStructureForPart'
			var body = {
				groups: $scope.groups,
				tool: $scope.selectedTools[0]
			}
			$http.post(url, body).success(function(data) {
				toastr.success('Pocket structure for this part was saved successfully.')
				if ($scope.timedOut) {
					var idleTime = Idle.getIdle() + Idle.getTimeout()
					startStopLaborTracking(true, idleTime)
					$scope.timedOut = false
				} else {
					/////////////// REDIRECT TO DULLGRADE HOME ////////////////
					setTimeout(function() {
						$location.path('/dullgrade-home')
						$scope.$apply()
					}, 1000)
				}

				$scope.showErrorCheck = false
			}).error(function(err) {
				toastr.error('There was a problem saving the pocket structure for this part.')
			})
		}
	}

	$scope.saveHashStructureForTool = function() {
		if (validateHashStructure()) {
			let url = Global.baseUrl + 'dullgradelevelone/saveHashStructureAndAssignToToolAndRepair'
			let body = {
				groups: $scope.groups,
				tool: $scope.selectedTools[0]
			}
			$http.post(url, body).success(function(data) {
				toastr.success('Pocket structure for this part was saved successfully.')
				$scope.selectedTools[0].hashId = data.newHashId
				$scope.getDullGradeStructureForCurrentRepair(function () {
					$scope.setColorClasses($scope.groups, true)
				}, null, true)
			}).error(function(err) {
				toastr.error('There was a problem saving the hash structure for this part.')
			})
		} else {
			toastr.warning('You must a assign a cutter to every pocket')
		}
	}

	$scope.errors = {
		noGroups: false,
		groupsWithoutBlades: false,
		bladesWithoutRows: false,
		rowsWithoutPockets: false,
		pocketsWithoutType: false
	}

	function validateHashStructure() {
		for (let group of $scope.groups) {
			for (let blade of group.blades) {
				for (let row of blade.rows) {
					for (let pocket of row.pockets) {
						if(!pocket.cutterId) return false
					}
				}
			}
		}
		return true
	}

	$scope.validateCutterStructure = function() {
		$scope.errors = {
			noGroups: false,
			groupsWithoutBlades: false,
			bladesWithoutRows: false,
			rowsWithoutPockets: false,
			pocketsWithoutType: false,
			cuttersNotAssigned: false
		}
		var isErrorFree = true
		$scope.showErrorCheck = true

		if ($scope.groups && $scope.groups.length > 0) {
			// loop through groups
			_.each($scope.groups, function(group) {
				if (group.blades && group.blades.length > 0) {
					// loop through blades
					_.each(group.blades, function(blade) {
						if (blade.rows && blade.rows.length > 0) {
							// loop through rows
							_.each(blade.rows, function(row) {
								if (row.pockets && row.pockets.length > 0) {
									_.each(row.pockets, function(pocket) {
										if (pocket.type === 'empty') {
											$scope.errors.pocketsWithoutType = true
											isErrorFree = false
										}
										if (!pocket.cutterId) {
											$scope.errors.cuttersNotAssigned = true
											isErrorFree = false
										}
									})
								} else {
									$scope.errors.rowsWithoutPockets = true
									isErrorFree = false
								}
							})
						} else {
							$scope.errors.bladesWithoutRows = true
							isErrorFree = false
						}
					})
				} else {
					$scope.errors.groupsWithoutBlades = true
					isErrorFree = false
				}
			})
		} else {
			$scope.errors.noGroups = true
			isErrorFree = false
		}

		if ($scope.errors.noGroups) toastr.error('No groups were added.')
		if ($scope.errors.groupsWithoutBlades) toastr.error('Groups without blades exist.')
		if ($scope.errors.bladesWithoutRows) toastr.error('Blades without rows exist.')
		if ($scope.errors.rowsWithoutPockets) toastr.error('Rows without pockets exist.')
		if ($scope.errors.pocketsWithoutType) toastr.error('Pockets without a type exist.')
		if ($scope.errors.cuttersNotAssigned) toastr.error('A cutter should be assigned to each pocket.')

		return isErrorFree
	}

	// *********************************************************
	// ancillaryInfo Stuff
	// *********************************************************

	$scope.get8InchFractions = function() {
		var url = Global.baseUrl + 'dullgradelevelone/get8InchFractions'
		$http.get(url).success(function(fractions) {
			$scope.fractions = fractions
		}).error(function(err) {
			toastr.error('There was an error getting fractions')
		})
	}

	$scope.getWholeNumbers = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getWholeNumbers'

		$http.get(url).success(function(wholeNumbers) {
			$scope.wholeNumbers = wholeNumbers
		}).error(function(err) {
			toastr.error('There was an error getting whole numbers.')
		})
	}

	$scope.getStockPointsByRepairCustomerId = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getStockPointsByRepairCustomerId'
		var data = {
			repairCustomerId: $scope.selectedCustomer.repairCustomerId
		}
		gPost(url, data, function(err,stockPoints) {
			if (err) toastr.error('There was an error getting stock points.');
			else {
				$scope.stockPoints = stockPoints
				if ($scope.selectedReceivedFromStockPoint) {
					var receivedFromStockPointFilter = $filter('filter')(stockPoints, function (stockPoint) {
						return stockPoint.repairStockPointId === $scope.selectedReceivedFromStockPoint.repairStockPointId
					})
					$scope.selectedReceivedFromStockPoint = receivedFromStockPointFilter[0]
				}
				if ($scope.selectedShipToStockPoint) {
					var shipToStockPointFilter = $filter('filter')(stockPoints, function (stockPoint) {
						return stockPoint.repairStockPointId === $scope.selectedShipToStockPoint.repairStockPointId
					})
					$scope.selectedShipToStockPoint = shipToStockPointFilter[0]
				}
			}
		});
	}

	$scope.getRepairCustomers = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getRepairCustomers'

		$http.get(url).success(function(repairCustomers) {
			$scope.repairCustomers = repairCustomers
		}).error(function(err) {
			toastr.error('There was an error getting repair customers.')
		})
	}

	$scope.getToolSections = function() {
		var url = Global.baseUrl + 'dullgradelevelone/getToolSections'
		$http.get(url).success(function(sections) {
			$scope.toolSections = sections
			var url2 = Global.baseUrl + 'dullgradelevelone/getToolSectionInsepectionIssueJunction'
			$http.post(url2).success(function(junctions) {
				$scope.toolSectionIssueJunctions = junctions
				var url3 = Global.baseUrl + 'dullgradelevelone/getInspectionIssues'
				$http.post(url3).success(function(issues) {
					$scope.inspectionsIssues = issues
				}).error(function(err) {
					toastr.error('There was an error getting tool section issue junctions.')
				})
			}).error(function(err) {
				toastr.error('There was an error getting tool section issue junctions.')
			})
		}).error(function(err) {
			toastr.error('There was an error getting tool sections.')
		})
	}

	$scope.getAncillaryInfo = function(tool) {
		if ($scope.selectedTools[0]) {
			var url = Global.baseUrl + 'dullgradelevelone/getAncillaryInfo'
			var data = {
				tableForAncillary: $scope.selectedTools[0].tableForAncillaryInformationOfRepairRecord,
				repairId: $scope.selectedTools[0].repairId
			}
			gPost(url, data, function(err,info) {
				if (err) toastr.error('There was a problem getting ancillary information.');
				else {
					if (info) {
						$scope.measurements = [];
						if ($scope.selectedTools[0].toolFamilyId === 4) {
							$scope.ancillaryId = info.ancillaryBakerBitId
							$scope.inspectionIssue = info.inspectionIssue
							$scope.nozzleComments = info.nozzleComments
							$scope.gaugeOd = info.gaugeOd
							$scope.grade = info.grade
							$scope.visualDyePenComments = info.visualAndDyePenComments
							$scope.shippingNeeds = info.shippingNeeds
							$scope.hardFaceHours = info.hardFaceHours
							$scope.flamesprayHours = info.flamesprayHours
							$scope.reshankNumber = info.reshankNumber
							$scope.orderNumber = info.orderNumber
							$scope.orderPrice = info.orderPrice
							$scope.totalTimesRepaired = info.totalTimesRepaired
							if (info.rechaseThreads === 0) {
								$scope.rechaseThreads = false
							} else {
								$scope.rechaseThreads = true
							}
							if (info.addPaintOnly === 0) {
								$scope.paintOnly = false
							} else {
								$scope.paintOnly = true
							}
							if (info.flameUpFortyFive === 0) {
								$scope.flameUp = false
							} else {
								$scope.flameUp = true
							}
							$scope.partNumber = $scope.selectedTools[0].partNumber
							$scope.sizeFraction = $scope.selectedTools[0].cuttingDiameterFraction
							$scope.sizeDecimal = $scope.selectedTools[0].cuttingDiameterDecimal
							$scope.style = $scope.selectedTools[0].style
							if ($scope.selectedTools[0].cuttingDiameterFraction) {
								var fractionSplit = $scope.selectedTools[0].cuttingDiameterFraction.split(" ", 2)
								var fractionSplitNumber = parseInt(fractionSplit[0])
								$scope.selectedWholeNumber = {
									Number: fractionSplitNumber
								}
								$scope.selectedFraction = {
									fraction: fractionSplit[1]
								}
							}
						} else if ($scope.selectedTools[0].toolFamilyId === 1) {
							$scope.ancillaryId = info.ancillaryDnrId
							$scope.thirdPartyInspectionNumber = info.thirdPartyInspectionNumber
							$scope.thirdPartyInspectorCompany = info.thirdPartyInspectorCompany
							$scope.thirdPartyInspectionNumberTwo = info.thirdPartyInspectionNumberTwo
							$scope.thirdPartyInspectorCompanyTwo = info.thirdPartyInspectorCompanyTwo
							$scope.unusualDamage = info.unusualDamage
							$scope.measurements.length1 = info.length1
							$scope.measurements.length2 = info.length2
							$scope.measurements.length3 = info.length3
							$scope.measurements.length4 = info.length4
							$scope.measurements.length5 = info.length5
							$scope.measurements.lengthTotal = parseFloat((parseFloat(info.length1) + parseFloat(info.length2) + parseFloat(info.length3) + parseFloat(info.length4) + parseFloat(info.length5)).toFixed(2));
							$scope.measurements.boxOD = info.topOdFractionalInches
							$scope.measurements.midOD = info.midOdFractionalInches
							$scope.measurements.pinOD = info.bottomOdFractionalInches
							if (info.paintOnly === 0) {
								$scope.paintOnly = false
							} else {
								$scope.paintOnly = true
							}
						} else if ($scope.selectedTools[0].toolFamilyId === 2) {
							$scope.ancillaryId = info.ancillaryVStreamId
							$scope.thirdPartyInspectionNumber = info.thirdPartyInspectionNumber
							$scope.thirdPartyInspectorCompany = info.thirdPartyInspectorCompany
							$scope.thirdPartyInspectionNumberTwo = info.thirdPartyInspectionNumberTwo
							$scope.thirdPartyInspectorCompanyTwo = info.thirdPartyInspectorCompanyTwo
							$scope.measurements.length1 = info.length1
							$scope.measurements.length2 = info.length2
							$scope.measurements.length3 = info.length3
							$scope.measurements.lengthTotal = parseFloat((parseFloat(info.length1) + parseFloat(info.length2) + parseFloat(info.length3)).toFixed(2));
							$scope.measurements.boxOD = info.topOdFractionalInches
							$scope.measurements.midOD = info.midOdFractionalInches
							$scope.measurements.pinOD = info.bottomOdFractionalInches
							if (info.addPaintOnly === 0) {
								$scope.paintOnly = false
							} else {
								$scope.paintOnly = true
							}
						}
						if ($scope.selectedTools[0].rush === 0) {
							$scope.rush = false
						} else {
							$scope.rush = true
						}
						if ($scope.selectedTools[0].isCutOut === 0) {
							$scope.isCutOut = false
						} else {
							$scope.isCutOut = true
						}
						$scope.rushComments = $scope.selectedTools[0].rushComments
						$scope.selectedReceivedFromStockPoint = {
							repairStockPointId: $scope.selectedTools[0].stockPointReceivedFromId
						}
						$scope.selectedShipToStockPoint = {
							repairStockPointId: $scope.selectedTools[0].stockPointShipToId
						}
						if ($scope.selectedTools[0].customerOwned === 0) {
							$scope.customerOwned = false
						} else {
							$scope.customerOwned = true
						}

						$scope.ist = $scope.selectedTools[0].ist
						if ($scope.selectedTools[0].stopWork === 0) {
							$scope.stopWork = false
						} else {
							$scope.stopWork = true
						}
						$scope.additionalShopWork = $scope.selectedTools[0].additionalShopWork
						$scope.so = $scope.selectedTools[0].so


						$scope.selectedCustomer = {
							repairCustomerId: $scope.selectedTools[0].customerId
						}
						$scope.getStockPointsByRepairCustomerId()
					}
				}
			});
		} else if (tool) {
			var url = Global.baseUrl + 'dullgradelevelone/getAncillaryInfo'
			var data = {
				tableForAncillary: tool.tableForAncillaryInformationOfRepairRecord,
				repairId: tool.repairId
			}
			gPost(url, data, function (err, info) {
				if (err) toastr.error('There was a problem getting ancillary information.');
				else {
					if (info) {
						if (tool.toolFamilyId === 1) {
							tool.ancillaryId = info.ancillaryDnrId
						} else if (tool.toolFamilyId === 2) {
							tool.ancillaryId = info.ancillaryVStreamId
						}
					}
				}
			});
		}
	}

	$scope.getIssuesForSelectedToolSection = function() {
		$scope.toolSectionIssueFilter = $filter('filter')($scope.toolSectionIssueJunctions, function(junction) {
			return junction.toolSectionId === $scope.selectedToolSection.toolSectionId
		})
		$scope.toolSectionIssues = []
		angular.forEach($scope.toolSectionIssueFilter, function(f) {
			var issueFilter = $filter('filter')($scope.inspectionsIssues, function(issues) {
				return issues.inspectionIssueId === f.inspectionIssueId
			})
			$scope.toolSectionIssues.push(issueFilter[0])
		})
	}

	$scope.saveBakerBitAncillaryInfo = function(moveToNextStation) {
		debugger;
		var url = Global.baseUrl + 'dullgradelevelone/saveBakerBitAncillaryInfo'
		var body = []
		var ids = {
			repairId: $scope.selectedTools[0].repairId,
			nonSdpToolId: $scope.selectedTools[0].nonSdpToolId,
			ancillaryBakerBitId: $scope.ancillaryId
		}
		body.push(ids)
		var partInfo = {}
		var ancillary = {}
		var repairInfo = {}
		if ($scope.partNumber && !$scope.selectedTools[0].partId) {
			partInfo.partNumber = $scope.partNumber
		} else {
			partInfo.partId = $scope.selectedTools[0].partId
		}
		if ($scope.selectedWholeNumber && $scope.selectedFraction) {
			partInfo.sizeFraction = $scope.selectedWholeNumber.Number + " " + $scope.selectedFraction.fraction
			partInfo.sizeDecimal = $scope.selectedWholeNumber.Number + $scope.selectedFraction.decimal
		}
		if ($scope.style) {
			partInfo.style = $scope.style
		}
		body.push(partInfo)
		if ($scope.rush) {
			repairInfo.rush = 1
		} else {
			repairInfo.rush = 0
		}
		if ($scope.rushComments) {
			repairInfo.rushComments = $scope.rushComments
		} else {
			repairInfo.rushComments = null
		}
		if ($scope.nozzleComments) {
			ancillary.nozzleComments = $scope.nozzleComments
		} else {
			ancillary.nozzleComments = null
		}
		if ($scope.gaugeOd) {
			ancillary.gaugeOd = $scope.gaugeOd
		} else {
			ancillary.gaugeOd = null
		}
		if ($scope.grade) {
			ancillary.grade = $scope.grade
		} else {
			ancillary.grade = null
		}
		if ($scope.visualDyePenComments) {
			ancillary.visualAndDyePenComments = $scope.visualDyePenComments
		} else {
			ancillary.visualAndDyePenComments = null
		}
		if ($scope.shippingNeeds) {
			ancillary.shippingNeeds = $scope.shippingNeeds
		} else {
			ancillary.shippingNeeds = null
		}
		if ($scope.inspectionIssue) {
			ancillary.inspectionIssue = $scope.inspectionIssue
		} else {
			ancillary.inspectionIssue = null
		}
		if ($scope.paintOnly) {
			ancillary.addPaintOnly = 1
		} else {
			ancillary.addPaintOnly = 0
		}
		if ($scope.flameUp) {
			ancillary.flameUpFortyFive = 1
		} else {
			ancillary.flameUpFortyFive = 0
		}
		if ($scope.hardFaceHours) {
			ancillary.hardFaceHours = $scope.hardFaceHours
		} else {
			ancillary.hardFaceHours = null
		}
		if ($scope.flamesprayHours) {
			ancillary.flamesprayHours = $scope.flamesprayHours
		} else {
			ancillary.flamesprayHours = null
		}
		if ($scope.rechaseThreads) {
			ancillary.rechaseThreads = 1
		} else {
			ancillary.rechaseThreads = 0
		}
		if ($scope.reshankNumber) {
			ancillary.reshankNumber = $scope.reshankNumber
		} else {
			ancillary.reshankNumber = null
		}
		body.push(ancillary)
		if ($scope.additionalShopWork) {
			repairInfo.additionalShopWork = $scope.additionalShopWork
		} else {
			repairInfo.additionalShopWork = null
		}
		if ($scope.stopWork) {
			repairInfo.stopWork = 1
		} else {
			repairInfo.stopWork = 0
		}
		if ($scope.customerOwned) {
			repairInfo.customerOwned = 1
		} else {
			repairInfo.customerOwned = 0
		}
		if ($scope.selectedCustomer) {
			repairInfo.repairCustomerId = $scope.selectedCustomer.repairCustomerId
		} else {
			repairInfo.repairCustomerId = null
		}
		if ($scope.selectedReceivedFromStockPoint) {
			repairInfo.stockPointReceivedFromId = $scope.selectedReceivedFromStockPoint.repairStockPointId
		} else {
			repairInfo.stockPointReceivedFromId = null
		}
		if ($scope.selectedShipToStockPoint) {
			repairInfo.stockPointShipToId = $scope.selectedShipToStockPoint.repairStockPointId
		} else {
			repairInfo.stockPointShipToId = null
		}
		if ($scope.ist) {
			repairInfo.ist = $scope.ist
		} else {
			repairInfo.ist = null
		}
		if ($scope.cutOut) {
			repairInfo.isCutOut = 1
		} else {
			repairInfo.isCutOut = 0
		}
		if ($scope.cutOutComments) {
			repairInfo.cutOutComments = $scope.cutOutComments
		} else {
			repairInfo.cutOutComments = null
		}
		if ($scope.so) {
			repairInfo.so = $scope.so
		} else {
			repairInfo.so = null
		}
		body.push(repairInfo)
		debugger;
		$http.post(url, body).success(function(data) {
			toastr.success('Ancillary info was saved successfully.')
			if(moveToNextStation){
				if($scope.cutOut){
					// move baker bit to create work stations if it is a cutout
					$scope.nextFlowSelected({nextFlowId: 18}, null, null, function () {
						setTimeout(function() {
							$location.path('/dullgrade-home')
							$scope.$apply()
						}, 1000)
					})
				} else {
					$scope.callStartStopLaborTracking(null,function () {
						setTimeout(function() {
							$location.path('/dullgrade-home')
							$scope.$apply()
						}, 1000)
					})
				}
			} else {
				/////////////// REDIRECT TO DULLGRADE HOME ////////////////
				setTimeout(function() {
					$location.path('/dullgrade-home')
					$scope.$apply()
				}, 1000)
			}
		}).error(function(err) {
			toastr.error('There was a problem saving ancillary info.')
		})
	}

	$scope.saveInspectionIssue = function() {
		if ($scope.inspectionIssue) {
			$scope.textAreaSize = $scope.textAreaSize + 1
			$scope.inspectionIssue = $scope.inspectionIssue + "\n" + $scope.selectedToolSection.toolSection + " : " + $scope.selectedIssue.inspectionIssue + ','
		} else {
			$scope.textAreaSize = 0
			$scope.inspectionIssue = $scope.selectedToolSection.toolSection + " : " + $scope.selectedIssue.inspectionIssue + ','
			$scope.textAreaSize = $scope.textAreaSize + 1
		}
		$('#modalInspectionIssue').modal('hide')
	}

	$scope.editPartInfo = function() {
		$scope.editPart = true
	}

	$scope.undoEditPartInfo = function() {
		$scope.editPart = false
		delete $scope.$parent.partNumber
		delete $scope.$parent.selectedWholeNumber
		delete $scope.$parent.selectedFraction
		delete $scope.$parent.style
	}

	$scope.checkIfPartExist = function() {
		var url = Global.baseUrl + 'dullgradelevelone/checkIfPartExist'
		var data = {
			partNumber: $scope.partNumber
		}
		$http.post(url, data).success(function(part) {
			if (part === "") {
				toastr.info('Part does not exist.')
				// delete $scope.selectedTools[0].partNumber
				// delete $scope.selectedTools[0].partId
				// delete $scope.selectedTools[0].cuttingDiameterFraction
				// delete $scope.selectedTools[0].cuttingDiameterDecimal
				// delete $scope.selectedTools[0].style
				// $scope.style = ''
				// $scope.selectedWholeNumber = {
				// 	Number: ''
				// }
				// $scope.selectedFraction = {
				// 	fraction: ''
				// }
			} else {
				toastr.info('Part already exist.')
				$scope.selectedTools[0].partNumber = part.partNumber
				$scope.selectedTools[0].cuttingDiameterFraction = part.sizeFraction
				$scope.selectedTools[0].cuttingDiameterDecimal = part.sizeDecimal
				$scope.selectedTools[0].style = part.style
				$scope.selectedTools[0].partId = part.partBakerBitId
				$scope.editPart = false
				$scope.style = part.style
				var fractionSplit = part.sizeFraction.split(" ", 2)
				var fractionSplitNumber = parseInt(fractionSplit[0])
				$scope.selectedWholeNumber = {
					Number: fractionSplitNumber
				}
				$scope.selectedFraction = {
					fraction: fractionSplit[1]
				}

			}
			$scope.partChecked = true
		}).error(function(err) {
			toastr.error('There was an error checking if part exist.')
		})
	}

	$scope.setShipToStockPoint = function() {
		if (!$scope.selectedShipToStockPoint) {
			$scope.selectedShipToStockPoint = $scope.selectedReceivedFromStockPoint
		}
	}

	$scope.openModalCutOutComments = () => {
		if($scope.cutOut) $('#modalCutOutComments').modal('show');
		else $scope.cutOutComments = null
	}

	$scope.closeModalCutOutComments = () => {
		$scope.cutOut = false;
		$('#modalCutOutComments').modal('hide');
	}

	$scope.setCutOutComments = () => {
		$('#modalCutOutComments').modal('hide');
	}

	// ********************************
	// Pull Cutters Section
	// ********************************
	$scope.cutters = []
	$scope.getCutters = function(cb) {
		var url = Global.baseUrl + 'dullgradelevelone/getCutters'
		var data = {}
		$http.post(url, data).success(function(data) {
			$scope.cutters = data
			if (cb) cb()
		}).error(function(err) {
			toastr.error('There was a problem getting cutters.')
		})
	}
	$scope.createCutterTypeLabels = function(groups) {
		var cutterTypeLabels = []
		_.each(groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {
					_.each(r.pockets, function(p) {
						$scope.cutterPockets.push(p)

						// first check if action is new
						if (p.cutterActionId === 1) {
							var label = {
								cutterPocketSizeId: p.cutterPocketSizeId,
								size: p.sizeFraction,
								cutterPocketTypeId: p.cutterPocketTypeId,
								type: p.type,
								name: p.name
							}

							var found = false
							_.each(cutterTypeLabels, function(ctl) {
								if (ctl.cutterPocketTypeId === label.cutterPocketTypeId && ctl.cutterPocketSizeId === label.cutterPocketSizeId) {
									found = true
									ctl.qty++
									ctl.cutterDullGradeIds.push(p.cutterDullGradeId)
								}
							})

							if (!found) {
								label.qty = 1
								label.cutterDullGradeIds = []
								label.cutterDullGradeIds.push(p.cutterDullGradeId)
								cutterTypeLabels.push(label)
							}
						}
					})
				})
			})
		})
		$scope.cutterTypeLabels = cutterTypeLabels
	}

	$scope.compatibleCutters = []

	/**
	 * Shows a list of cutters in a modal compatible with the selection made by the user
	 * @param cutterTypeLabel
	 * @param optionalCutterPocket
	 * @param isInclusive
	 * @param idOfModalToOpen
	 * @param optionalAdditionalHeaderMessage
	 * @param cutterPickingType
	 */
	$scope.showCompatibleCutters = function(cutterTypeLabel, optionalCutterPocket, isInclusive, idOfModalToOpen, optionalAdditionalHeaderMessage, cutterPickingType, showAllCutters) {
		debugger;
		let compatibleCutters
		if(showAllCutters) {
			compatibleCutters = $scope.cutters
		} else if (cutterTypeLabel) {
			compatibleCutters = _.filter($scope.cutters, function(cutter) {
				return cutter.cutterPocketTypeId === cutterTypeLabel.cutterPocketTypeId && cutter.cutterPocketSizeId === cutterTypeLabel.cutterPocketSizeId
			})
			$scope.currentCutterTypeLabel = cutterTypeLabel
			$scope.singleCutterPocketUpdate = null
			$scope.activeCutter = cutterTypeLabel;
		} else if (optionalCutterPocket && optionalCutterPocket.cutterActionId === 1 || isInclusive) {
			compatibleCutters = _.filter($scope.cutters, function(cutter) {
				return cutter.cutterPocketTypeId === optionalCutterPocket.cutterPocketTypeId && cutter.cutterPocketSizeId === optionalCutterPocket.cutterPocketSizeId
			})
			$scope.singleCutterPocketUpdate = optionalCutterPocket
			$scope.activeCutter = optionalCutterPocket;
		} else {
			return
		}

		if(cutterPickingType) {
			$scope.activePocketSize = {};
			$scope.activePocketType = {};
			if(cutterTypeLabel) {
				$scope.activePocketSize.sizeFraction = cutterTypeLabel.sizeFraction;
				$scope.activePocketType.name = cutterTypeLabel.name;
			} else {
				$scope.activePocketSize.sizeFraction = optionalCutterPocket.sizeFraction;
				$scope.activePocketType.name = optionalCutterPocket.name;
			}

		}

		$scope.cutterEditMode = false
		$scope.additionalHeaderMessage = optionalAdditionalHeaderMessage ? optionalAdditionalHeaderMessage : ''
		$scope.cutterPickingType = cutterPickingType
		$scope.compatibleCutters = compatibleCutters
		if($scope.activeCutter) {
			$scope.replaceCutterQty = getMaxQtyReplacementCutters($scope.activeCutter)
		}
		delete $scope.reasonForChange;
		$('#' + idOfModalToOpen).modal('show')
	}

	$scope.revertReplacementCutterBack = function(activeCutter) {
		debugger;
		if(activeCutter.replaceWithCutterId) {
			// proceed
			var url = Global.baseUrl + 'dullgradelevelone/revertReplacementCutterBack';
			var body = activeCutter;
			$http.post(url, body).success(data => {
				$scope.$broadcast('reloadCutterDullGradeAndCutterNames')
				$('#modalSelectActiveCutter').modal('hide')
			}).error(err => {
				toastr.error('There was a problem reverting back to the original cutter.');
			})
		} else {
			toastr.error('This cutter is not a replacement. There is nothing to revert.');
		}
	}
	var contains = function(needle) {
		// Per spec, the way to identify NaN is that it is not equal to itself
		var findNaN = needle !== needle
		var indexOf

		if (!findNaN && typeof Array.prototype.indexOf === 'function') {
			indexOf = Array.prototype.indexOf
		} else {
			indexOf = function(needle) {
				var i = -1,
					index = -1

				for (i = 0; i < this.length; i++) {
					var item = this[i]

					if ((findNaN && item !== item) || item === needle) {
						index = i
						break
					}
				}

				return index
			}
		}

		return indexOf.call(this, needle) > -1
	}
	$scope.setCutters = function(cutter, optionalCutterDullGrade) {
		var cutterPocketsToUpdate = []
		if (optionalCutterDullGrade) {
			optionalCutterDullGrade.cutterId = cutter.cutterId
			cutterPocketsToUpdate.push(optionalCutterDullGrade)
		} else {
			_.each($scope.cutterPockets, function(cp) {
				if (contains.call($scope.currentCutterTypeLabel.cutterDullGradeIds, cp.cutterDullGradeId)) {
					cp.cutterId = cutter.cutterId
					cutterPocketsToUpdate.push(cp)
				}
			})
		}

		var data = cutterPocketsToUpdate
		var url = Global.baseUrl + 'dullgradelevelone/updateCutterDullGrades'

		$http.post(url, data).success(function(data) {
			//$scope.getDullGradeStructureForCurrentRepair();
			$scope.$broadcast('cutters:updated')
			$('#modalSelectCutter').modal('hide')
		}).error(function(err) {
			toastr.error('There was a problem setting the cutter')
		})
	}

	$scope.setCuttersWithoutSaving = function(cutter, optionalCutterPocket) {
		var cutterPocketsToUpdate = []
		if (optionalCutterPocket) {
			optionalCutterPocket.cutterId = cutter.cutterId
			cutterPocketsToUpdate.push(optionalCutterPocket)
		} else {
			_.each($scope.cutterPockets, function(cp) {
				// if (contains.call($scope.currentCutterTypeLabel.cutterDullGradeIds, cp.cutterDullGradeId)) {
				//     cp.cutterId = cutter.cutterId;
				//     cutterPocketsToUpdate.push(cp);
				// }
			})
		}

		$scope.setColorClasses($scope.groups, true)

		$('#modalSelectCutterWithoutSaving').modal('hide')
	}

	var lastAppendedPartNumber;
	$scope.getPartsWithAppendix = function(originalPartNumber) {
		debugger;
		if(originalPartNumber !== lastAppendedPartNumber) {
			lastAppendedPartNumber = originalPartNumber;
			var url = Global.baseUrl + 'part/getPartsWithAppendix';
			var body = {
				partNumber: originalPartNumber
			};

			$http.post(url, body).success(function(data) {
				console.log(data);
			}).error(function(err) {

			});
		}
	}

	function updateLotNumbers(activeColorClasses) {
		for (let colorClass of activeColorClasses) {
			colorClass.lots = []
			for (let cutter of colorClass.cutters) {
				if(cutter.lotNumber) {
					let existingLot = colorClass.lots.find(lot => lot.lotNumber === cutter.lotNumber)
					if(existingLot) existingLot.qty++
					else colorClass.lots.push({lotNumber: cutter.lotNumber, qty: 1, isReclaim: cutter.isReclaim})
				}
			}
		}
	}

	function setColorClassesHelper(groups, isInclusive, dullgrade) {
		if(dullgrade) {
			groups = dullgrade.groups
		}
		debugger;
		var colorClasses = [
			{ id: 1, class: 'brightgreen', available: true },
			{ id: 2, class: 'red', available: true },
			{ id: 3, class: 'orange', available: true },
			{ id: 4, class: 'purple', available: true },
			{ id: 5, class: 'realorange', available: true },
			{ id: 6, class: 'blue', available: true },
			{ id: 7, class: 'lightpurple', available: true },
			{ id: 8, class: 'lightblue', available: true },
			{ id: 9, class: 'brown', available: true },
			{ id: 10, class: 'green', available: true },
			{ id: 11, class: 'pink', available: true }
		]

		var activeColorClasses = []
		_.each(groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {
					// update max cutters on one pocket and update container height (.container-3d)
					if (r.pockets && r.pockets.length > $scope.maxCuttersOnOnePocket) {
						$scope.maxCuttersOnOnePocket = r.pockets.length
					}

					_.each(r.pockets, function(p) {
						if (p.cutterId) {
							if (p.cutterActionId === 1 || isInclusive) {
								var cutterId;
								if(p.replaceWithCutterId){
									cutterId = p.replaceWithCutterId
								} else {
									cutterId = p.cutterId
								}
								var colorClass = _.filter(activeColorClasses, function(ac) {
									return cutterId === ac.cutterId;
								});
								if (colorClass && colorClass.length > 0) {
									p.colorClass = colorClass[0].class;
									colorClass[0].qty++;
									if(p.cutterActionId === 1) {
										colorClass[0].newQty++
										colorClass[0].cutters.push(p)
									}
									let cutter = _.find($scope.cutters, function(c) {
										return cutterId === c.cutterId
									})
									if(cutter) p.cutterName = cutter.cutterName
									else debugger
								} else {
									// find first available color class to use
									var nextClass = _.find(colorClasses, function(cc) {
										return cc.available === true;
									});
									_.each(colorClasses, function(cc) {
										if (cc.id === nextClass.id) {
											cc.available = false;
										}
									});
									if(typeof p === 'undefined') debugger

									let cutter = _.find($scope.cutters, function(c) {
										return cutterId === c.cutterId
									})
									if(cutter) p.cutterName = cutter.cutterName
									else debugger

									var newActiveColorClass = {
										cutterId: cutterId,
										class: nextClass.class,
										cutterName: p.cutterName,
										qty: 1
									};

									// For new cutters
									newActiveColorClass.newQty = p.cutterActionId === 1 ? 1 : 0
									newActiveColorClass.cutters = []
									newActiveColorClass.cutters.push(p)

									activeColorClasses.push(newActiveColorClass);

									p.colorClass = newActiveColorClass.class;
								}
							} else {
								p.colorClass = 'newnotset'
							}
						} else {
							p.colorClass = 'notactive'
						}
					})
				})
			})
		})

		updateLotNumbers(activeColorClasses)

		debugger
		if(dullgrade) dullgrade.activeColorClasses = activeColorClasses
		else $scope.activeColorClasses = activeColorClasses

	}

	$scope.setColorClasses = function(groups, isInclusive, cb, dullgrades) {
		if(dullgrades && dullgrades.length > 0) {
			_.each(dullgrades, function(dg) {
				debugger
				setColorClassesHelper(groups, isInclusive, dg)
			})
		} else {
			setColorClassesHelper(groups, isInclusive)
		}

		if(cb) cb()
	}

	function getDullGradeIdFromDullgradeGroup(groups) {
		var found = false
		var dullGradeId
		_.each(groups, function(g) {
			if(!found) {
				_.each(g.blades, function(b) {
					if(!found) {
						_.each(b.rows, function(r) {
							if(!found) {
								_.each(r.pockets, function(p) {
									if(!found) {
										if(p.dullGradeId) {
											dullGradeId = p.dullGradeId
											found = true
										}
									}
								})
							}
						})
					}
				})
			}
		})
		return dullGradeId
	}
	$scope.openModalSetOnHoldAndComments = function(toolRepair) {
		debugger;
		$scope.currentToolRepair = JSON.parse(JSON.stringify(toolRepair))
		if($scope.currentToolRepair.onHold) $scope.currentToolRepair.onHold = !!$scope.currentToolRepair.onHold;
		$('#modalSetOnHoldAndComments').modal('show')
	}

	// *********************************
	// Create Section
	// *********************************

	$scope.getCountOfFlameSpray = function() {
		if ($scope.selectedTools[0]) {
			var url = Global.baseUrl + 'dullgradelevelone/getCountOfFlameSpray'
			var data = {
				partBakerBitId: $scope.selectedTools[0].partId,
				repairId: $scope.selectedTools[0].repairId
			}



			if($scope.getCutterInfoByDullGradeNotRepair) {
            	data.dullGrade = $scope.dullGrade
			}

			$http.post(url, data).success(function(count) {
				$scope.flameSprayCount = count
			}).error(function(er) {
				toastr.error('There was a problem getting flame spray count.')
			})
		}
	}
	$scope.getCutterNamesUsedInRepairForDullgrade = function(cb) {
		debugger;
		if ($scope.selectedTools[0]) {
			var url = Global.baseUrl + 'dullgradelevelone/getCutterNamesUsedInRepair'
			var data = {
				repairId: $scope.selectedTools[0].repairId,
				dullgradeId: $scope.dullGrade.dullGradeId
			}
			var currentDullGrade = $scope.dullGrade
			$http.post(url, data).success(function(cutterNames) {
				debugger;
				$scope.cutterNames = cutterNames
				$scope.cutterNames = _.filter($scope.cutterNames, function(cn) {
					return cn.count !== 0
				})
				$scope.dullGrade.cutterNames = cutterNames
				if(cb) cb($scope.dullGrade)
			}).error(function(er) {
				toastr.error('There was a problem getting cutter names.')
			})
		}
	}
	$scope.getCountOfAllCuttersWithCutterNamesBakerBit = function(cb, dullGrade) {
		debugger;
		if ($scope.selectedTools[0]) {
			var url = Global.baseUrl + 'dullgradelevelone/getCountOfAllCuttersWithCutterNamesBakerBit';
			var data = {
				repairId: $scope.selectedTools[0].repairId,
				dullGradeId: dullGrade.dullGradeId ? dullGrade.dullGradeId : null
			};
			$http.post(url, data).success(function(cutterNames) {
				debugger;
				$scope.cutterNames = cutterNames[0];
				$scope.cutterNames = _.filter($scope.cutterNames, function(cn) {
					return cn.count !== 0;
				});
				$scope.dullGrade.cutterNames = cutterNames[0];
				if(cb) cb($scope.dullGrade);
			}).error(function(er) {
				toastr.error('There was a problem getting cutter names.');
			});
		}
	};
	$scope.getCutterNamesUsedInRepair = function(cb) {
		if ($scope.selectedTools[0]) {
			var url = Global.baseUrl + 'dullgradelevelone/getCutterNames'
			var data = {
				repairId: $scope.selectedTools[0].repairId
			}
			$http.post(url, data).success(function(cutterNames) {
				$scope.cutterNames = cutterNames[0]
				if(cb) cb(cutterNames[0])
			}).error(function(er) {
				toastr.error('There was a problem getting cutter names.')
			})
		}
	}
	$scope.getCountOfCutterSizeByAction = function() {
		if ($scope.selectedTools[0]) {
			var url = Global.baseUrl + 'dullgradelevelone/getCountOfCutterSizeByAction'
			var data = {
				partBakerBitId: $scope.selectedTools[0].partId,
				repairId: $scope.selectedTools[0].repairId
			}

			if($scope.getCutterInfoByDullGradeNotRepair) {
            	data.dullGrade = $scope.dullGrade
			}

			$http.post(url, data).success(function(cutterSizeCount) {
				$scope.newCutterSizeCount = []
				$scope.rotateCutterSizeCount = []
				angular.forEach(cutterSizeCount, function(csc) {
					if (csc.cutterActionId === 1) {
						$scope.newCutterSizeCount.push(csc)
					} else {
						$scope.rotateCutterSizeCount.push(csc)
					}
				})
			}).error(function(er) {
				toastr.error('There was a problem getting cutter size count.')
			})
		}
	}

	// *********************************
	// END Create Section
	// *********************************

	$scope.selectDullGrade = function() {

		$scope.isLatestDullGrade = false
		$scope.dullGrade = $scope.selectedDullGrade
		$scope.groups = $scope.selectedDullGrade.groups
		if($scope.dullGrade.dullGradeId === $scope.dullgrades[$scope.dullgrades.length - 1].dullGradeId){
			$scope.isLatestDullGrade = true
		}
	}

	$scope.cutterEditMode = false
	$scope.toggleEditMode = function() {

		$scope.cutterEditMode = !$scope.cutterEditMode
	}
	$scope.toggleObsolete = function(cutter) {
		cutter.isObsolete = !cutter.isObsolete
		var url = Global.baseUrl + 'dullgradelevelone/updateCutter'
		var body = {
			cutterId: cutter.cutterId,
			isObsolete: cutter.isObsolete
		}
		$scope.dontShowLoading = true

		$http.post(url, body).success(function() {

		}).error(function(err) {
			cutter.isObsolete = !cutter.isObsolete
			toastr.error('There was a problem trying to set obsolete status.')
		}).finally(function() {
			$scope.dontShowLoading = false
		})
	}
	$scope.toggleVisible = function(cutter) {
		var originalValue = parseInt(cutter.status)
		cutter.status = cutter.status !== 99 ? 99 : 0
		var url = Global.baseUrl + 'dullgradelevelone/updateCutter'
		var body = {
			cutterId: cutter.cutterId,
			status: cutter.status
		}
		$scope.dontShowLoading = true

		$http.post(url, body).success(function() {

		}).error(function(err) {
			cutter.status = originalValue
			toastr.error('There was a problem trying to set visibility status.')
		}).finally(function() {
			$scope.dontShowLoading = false
		})
	}
	$scope.addCutterMode = false
	$scope.toggleAddCutter = function() {
		$scope.addCutterMode = !$scope.addCutterMode
	}
	$scope.saveAddCutter = function() {
		var url = Global.baseUrl + 'dullgradelevelone/saveCutter'


		var cutterPocket = _.find($scope.cutterPockets, function(cp) {
			return cp.cutterPocketSizeId === $scope.activePocketSize.cutterPocketSizeId && cp.cutterPocketTypeId === $scope.activePocketType.cutterPocketTypeId
		})
		if (typeof cutterPocket !== 'undefined') {
			$scope.currentAddCutter.cutterPocketId = cutterPocket.cutterPocketId
		} else {
			$scope.currentAddCutter.cutterPocketSizeId = $scope.activePocketSize.cutterPocketSizeId
			$scope.currentAddCutter.cutterPocketTypeId = $scope.activePocketType.cutterPocketTypeId
		}

		if($scope.activeCutter && !$scope.currentAddCutter.cutterPocketSizeId && !$scope.currentAddCutter.cutterPocketTypeId) {
			$scope.currentAddCutter.cutterPocketSizeId = $scope.activeCutter.cutterPocketSizeId
			$scope.currentAddCutter.cutterPocketTypeId = $scope.activeCutter.cutterPocketTypeId
		}


		$scope.currentAddCutter.cutterName = !$scope.currentAddCutter.cutterName ? $scope.currentAddCutter.partNumber : $scope.currentAddCutter.cutterName

		var body = $scope.currentAddCutter



		$http.post(url, body).success(function(cutter) {
			// use data
			$scope.getCutters(function() {
				$scope.getCutterPockets(function() {
					var cutterPocket = _.find($scope.cutterPockets, function(cp) {
						return cp.cutterPocketId === cutter.cutterPocketId
					})
					$scope.showCompatibleCutters(null, cutterPocket, true, 'modalSelectActiveCutter')
				})
			})

		}).error(function(err) {
			toastr.error('There was a problem trying to save the cutter.')
		})
	}

	$scope.openModalSelectNextFlow = function(toolRepair) {
		$scope.selectedTools[0] = toolRepair
		getFlowRelationshipForCurrentFlow(toolRepair.flowId)
	}

	$scope.activeCutter = {}
	$scope.currentLot = {}
	$scope.openModalAndSetLotNumbers = function(cutterName, idOfModal) {
		debugger
		if($scope.selectedTools[0].toolFamilyId === 4) {
			// for bit
			$scope.currentLot = {}
			// deep copy of cutter, do not want to change a reference of a cutter
			$scope.activeCutter = JSON.parse(JSON.stringify(cutterName))
			updateTotalPulled($scope.activeCutter)

			$scope.currentLot.qty = $scope.activeCutter.count - $scope.activeCutter.totalPulled
			$('#' + idOfModal).modal('show')
			setTimeout(function() {
				$('#lotNumberField').focus()
			}, 500)
		 } else {
			// for non bit
			debugger;
		 	$scope.currentLot = {}
		 	let lotQty = 0
		 	for (let lot of cutterName.lots) {
				lotQty += lot.qty
		 	}
			$scope.currentLot.qty = cutterName.newQty - lotQty
		 	$scope.activeCutter = JSON.parse(JSON.stringify(cutterName))
			updateTotalPulled($scope.activeCutter)
		 	$('#' + idOfModal).modal('show')
		 	setTimeout(function() {
		 		$('#lotNumberField').focus()
		 	}, 500)
		 }
	}
	function getNextCutterDullGradesForLot(cutterId, groups, qty) {
		var found = 0
		var nextCutterDullgrades = []
		_.each(groups, function(g) {
			if(found < qty) {
				_.each(g.blades, function(b) {
					if(found < qty) {
						_.each(b.rows, function(r) {
							if(found < qty) {
								_.each(r.pockets, function(p) {
									if(found < qty) {
										if(!p.lotNumber && p.cutterId === cutterId && p.cutterActionId === 1) {
											nextCutterDullgrades.push(p)
											found++
										}
									}
								})
							}
						})
					}
				})
			}
		})
		return JSON.parse(JSON.stringify(nextCutterDullgrades))
	}
	$scope.addLot = function(currentLot, activeCutter, cb) {

		if(currentLot && currentLot.lotNumber && currentLot.qty) {
			activeCutter.totalPulled = typeof activeCutter.totalPulled !== 'undefined' ? activeCutter.totalPulled : 0
			if(activeCutter.totalPulled + currentLot.qty <= activeCutter.count) {
				if(!activeCutter.lots) activeCutter.lots = []
				var alreadyLot = _.find(activeCutter.lots, function(lot) {
					return lot.lotNumber === currentLot.lotNumber
				})

				if(alreadyLot) {
					var cutterDullGrades = getNextCutterDullGradesForLot(activeCutter.cutterId, $scope.groups, currentLot.qty)
					if(cutterDullGrades && cutterDullGrades.length > 0) {
						_.each(cutterDullGrades, function(cdg) {
							cdg.lotNumber = currentLot.lotNumber
							alreadyLot.items.push(cdg)
						})
					}
				}
				else {
					var cutterDullGrades = getNextCutterDullGradesForLot(activeCutter.cutterId, $scope.groups, currentLot.qty)
					if(cutterDullGrades && cutterDullGrades.length > 0) {
						var lot = {
							lotNumber: currentLot.lotNumber,
							items: []
						}
						_.each(cutterDullGrades, function(cdg) {
							cdg.lotNumber = currentLot.lotNumber
							lot.items.push(cdg)
						})

						activeCutter.lots.push(lot)
					}
				}

				updateTotalPulled(activeCutter)
				if(cb) cb()
			}
			$scope.currentLot = {}
		} else {
			if(cb) cb()
		}
	}

	$scope.removeLot = function(currentLot, activeCutter) {
		debugger
		if($scope.selectedTools[0].toolFamilyId === 4) {
			// for bit
			var url = Global.baseUrl + 'dullgradelevelone/removeLot'
			var body = currentLot.items
			$scope.loading = true
			$scope.dontShowLoading = true
			$http.post(url, body).success(function(data) {
				$scope.$broadcast('reloadCutterDullGradeAndCutterNames', {dullgrades: $scope.dullgrades})
				$('#modalSetLotNumbers').modal('hide')
			}).error(function(err) {

			}).finally(function() {
				$scope.loading = false
				$scope.dontShowLoading = false
			})
		} else {
			// for non bit
			let cuttersWithThisLot = activeCutter.cutters.filter(cutter => cutter.lotNumber === currentLot.lotNumber)

			var url = Global.baseUrl + 'dullgradelevelone/removeLot'
			var body = cuttersWithThisLot

			$scope.loading = true
			$scope.dontShowLoading = true
			$http.post(url, body).success(function(data) {
				$scope.getDullGradeStructureForCurrentRepair(function() {
					$scope.setColorClasses($scope.groups, true)
				})
				$('#modalSetLotNumbers').modal('hide')
			}).error(function(err) {

			}).finally(function() {
				$scope.loading = false
				$scope.dontShowLoading = false
			})
		}
	}
	function removeLotNumberFromDullGradeCutters(dullGradeCutters, groups) {

		_.each(groups, function(g) {
			_.each(g.blades, function(b) {
				_.each(b.rows, function(r) {
					_.each(r.pockets, function(p) {
						_.each(dullGradeCutters, function(dgc) {
							console.log('p vs dgc')
							console.log(p.cutterDullGradeId + ' | ' + dgc.cutterDullGradeId)
							if(p.cutterDullGradeId === dgc.cutterDullGradeId && p.lotNumber) {

								p.lotNumber = undefined
							}
						})
					})
				})
			})
		})
	}
	function updateTotalPulled(activeCutter) {
		debugger;
		var pulled = 0
		_.each(activeCutter.lots, function(lot) {
			if(lot.items) pulled += lot.items.length
		})
		activeCutter.totalPulled = pulled
	}

	$scope.addSaveLot = function(activeCutter, lot) {
		debugger
		if(lot.qty && lot.qty > 0 && lot.lotNumber) {

			// check that there is a hashId assigned to this tool and/or repair
			if(($scope.selectedTools[0].toolFamilyId !== 4 && $scope.selectedTools[0].hashId) || $scope.selectedTools[0].toolFamilyId === 4) {
				var url = Global.baseUrl + 'dullgradelevelone/addSaveLot'
				var body = {
					cutter: activeCutter,
					dullGrade: $scope.dullGrade,
					lotNumber: lot.lotNumber,
					qty: lot.qty,
					isReclaim: lot.isReclaim
				}
				$scope.loading = true
				$scope.dontShowLoading = true
				$('#modalSetLotNumbers').modal('hide')
				$http.post(url, body).success(function(data) {
					debugger
					 if($scope.selectedTools[0].toolFamilyId === 4) {
						$scope.$broadcast('reloadCutterDullGradeAndCutterNames')
					} else {
						$scope.getDullGradeStructureForCurrentRepair(function() {
				 		$scope.setColorClasses($scope.groups, true)
					})
				 }

					$('#modalSetLotNumbers').modal('hide')
				}).error(function(err) {

				}).finally(function() {
					$scope.loading = false
					$scope.dontShowLoading = false
				})
			} else {
				if($scope.selectedTools[0].toolFamilyId !== 4 && !$scope.selectedTools[0].hashId) {
					toastr.warning('You must save the cutter structure before you can assign any lot numbers.')
					setTimeout(function() {
						toastr.warning('Assign each pocket with a cutter and click "SAVE/CONFIRM"')
					}, 4000)
				}
			}
		}
	}

	$scope.saveLotsForCutterAndRepair = function(activeCutter) {

		$scope.loading = true
		$scope.dontShowLoading = true
		$scope.addLot($scope.currentLot, activeCutter, function() {

			var url = Global.baseUrl + 'dullgradelevelone/saveLotsForCutterAndRepair'
			var body = {
				repair: $scope.selectedTools[0],
				cutter: activeCutter
			}

			$http.post(url, body).success(function(data) {
				$scope.$broadcast('reloadCutterDullGradeAndCutterNames')
			}).finally(function() {
				$scope.loading = false
				$scope.dontShowLoading = false
			})
		})
	}

	function getNextCutterDullGradesForCutterReplacement(activeCutter, newReplacementCutter, groups, qty) {
		var found = 0
		var cutterDullGrades = []
		_.each(groups, function(g) {
			if(found < qty) {
				_.each(g.blades, function(b) {
					if(found < qty) {
						_.each(b.rows, function(r) {
							if(found < qty) {
								_.each(r.pockets, function(p) {
									if(found < qty) {
										if(activeCutter.replacedCutterId) {
											if(p.cutterId === activeCutter.replacedCutterId && p.replaceWithCutterId === activeCutter.cutterId && !p.lotNumber && p.cutterActionId === 1) {
												cutterDullGrades.push(p)
												found++
											}
										} else {
											if(p.cutterId === activeCutter.cutterId && !p.lotNumber && p.cutterActionId === 1 && !p.replaceWithCutterId) {
												cutterDullGrades.push(p)
												found++
											}
										}
									}
								})
							}
						})
					}
				})
			}
		})
		return cutterDullGrades
	}
	function getMaxQtyReplacementCutters(activeCutter) {

		var lotItemQty = 0
		_.each(activeCutter.lots, function(lot) {
			_.each(lot.items, function(item) {
				lotItemQty++
			})
		})
		return activeCutter.count - lotItemQty
	}
	$scope.addAsCutterReplacement = function(newReplacementCutter, qty) {
		debugger;
		var currentCutter = JSON.parse(JSON.stringify($scope.activeCutter))

		var cutterDullGradesToReplace = [];
		// if qty is not defined, then just add active cutter
		if(currentCutter && typeof qty === 'undefined') {
			cutterDullGradesToReplace.push(currentCutter);
		} else {
			cutterDullGradesToReplace = getNextCutterDullGradesForCutterReplacement(currentCutter, newReplacementCutter, $scope.groups, qty)
		}

		console.log(cutterDullGradesToReplace.length)

		if(cutterDullGradesToReplace.length === 0) {
			toastr.error('Cutter(s) may already have lot numbers');
		} else {
			if(newReplacementCutter && currentCutter) {
				var url = Global.baseUrl + 'dullgradelevelone/addAsCutterReplacement'
				var body = {
					currentCutter: currentCutter,
					replacementCutter: newReplacementCutter,
					cutterDullGradesToReplace: cutterDullGradesToReplace,
					dullGrade: $scope.dullGrade
				}
				if($scope.reasonForChange){
					body.reasonForChange = $scope.reasonForChange
				}

				$scope.loading = true
				$scope.dontShowLoading = true

				$http.post(url, body).success(function(data) {
					$scope.$broadcast('reloadCutterDullGradeAndCutterNames')
					$('#modalSelectActiveCutter').modal('hide')
				}).error(function(err) {
					if(err.message) {
						toastr.error(err.message);
					}
				}).finally(function() {
					$scope.loading = false
					$scope.dontShowLoading = false
				})
			}
		}
	}

	$scope.replaceOnePocketCutter = function() {
		debugger;
	};

	$scope.revertToOriginalCutter = function() {

	};

	$scope.removeRepairFromKiln = function(kiln) {
    	if($scope.confirmedRemoveFromKiln) {
    		var url = Global.baseUrl + 'dullgradelevelone/removeRepairFromKiln'
    		var body = {
    			kiln: kiln,
				laborTimeStamps: $scope.laborTimeStampsForKiln
    		}
			debugger;
    		$http.post(url, body).success(function(data) {
    			$scope.showConfirmRemoveRepairFromKiln = false
    			$scope.laborTimeStampsForKiln = []
    			$scope.confirmedRemoveFromKiln = false
    			$('#modalKilnCutterDullGrades').modal('hide')
    			$scope.getFlowRecords()
    			$scope.getFlowsWithToolRepairs([21, 22])
    			$scope.removeAllUsers()
    			$scope.$parent.finishedAddingTools = false
    		}).error(function(err) {
    			$scope.showConfirmRemoveRepairFromKiln = false
    			toastr.error('There was a problem pulling back the repair from the kiln.')
    		})
    	} else {
    		// show UI that tells user that there is a labor timestamp that exists and if they want to proceed?
    	}
	}

	$scope.prepareRemoveRepairFromKiln = function(kiln) {
    	$scope.checkIfTimeStampsForWorkStation(kiln, function(err, data) {
    		if(!err) {
    			$scope.showConfirmRemoveRepairFromKiln = true
    			if(data.length === 0) $scope.confirmedRemoveFromKiln = true
    			else $scope.currentKiln.hasLaborTimeStamps = true
    			$scope.laborTimeStampsForKiln = data
    		} else {
    			toastr.error('There was a problem fetching any existing timestamp information. Better wait to remove this repair from the kiln until we are certain.')
    		}
    	})
	}

	$scope.removeOverlay = function() {
    	$scope.showConfirmRemoveRepairFromKiln = false
	}

	$scope.checkIfTimeStampsForWorkStation = function(kiln, cb) {
    	if(kiln.toolRepairs[0]) {
    		var url = Global.baseUrl + 'dullgradelevelone/getTimeStampsForWorkStation'
    		var body = {
    			repair: kiln.toolRepairs[0]
    		}

    		$http.post(url, body).success(function(data) {
    			if(cb) cb(null, data)
    		}).error(function(err) {
    			if(cb) cb(err, null)
    		})
    	} else if(cb) cb({error: 'There are not tools in this workstation.'}, null)
	}

	$scope.$watch('currentAuthenticatedUsers', function() {
	}, true)

	$scope.isThereAnyCuttersThatNeedLotNumbers = function(ifCheckingToBeStopped, cb) {
		debugger;
		_.each($scope.dullGrade.groups[0].blades, function(b) {
			_.each(b.rows, function(r) {
				_.each(r.pockets, function(p) {
					if(p.cutterActionId === 1) {
						if(p.lotNumber === null) {
							debugger;
							_.each($scope.dullGrade.cutterNames, function(cn) {
								if(!p.replaceWithCutterId){
									if(cn.cutterId === p.cutterId && p.isReclaim === cn.isReclaim) {
										if(!cn.lots || cn.count !== cn.lots.length){
											cn.markedForLotNumber = true;
											p.markedForLotNumber = true;
										} else {
											cn.markedForLotNumber = false;
											p.markedForLotNumber = false;
										}
									}
								} else {
									if(cn.cutterId === p.replaceWithCutterId && cn.replacedCutterId === p.cutterId && p.isReclaim === cn.isReclaim) {
										if(!cn.lots || cn.count !== cn.lots.length){
											cn.markedForLotNumber = true;
											p.markedForLotNumber = true;
										} else {
											cn.markedForLotNumber = false;
											p.markedForLotNumber = false;
										}
									}
								}
							});
						}
					}
				});
			});
		});
		if(ifCheckingToBeStopped){
			$scope.checkIfBitCanBeStopped();
		}
		if(cb) cb()
	};

	$scope.checkIfBitCanBeStopped = function () {
		// var chronoList = _.pluck($scope.workStationFilter, 'chrono');
		// var lowestChrono = _.min(chronoList);
		// debugger;
		// if($scope.selectedTools[0].chrono === lowestChrono){
		// 	$scope.showStop = true;
		// } else {
		var needToAddLot = false;
		_.each($scope.dullGrade.groups[0].blades, function(b) {
			_.each(b.rows, function(r) {
				_.each(r.pockets, function(p) {
					if(p.cutterActionId === 1) {
						if(p.lotNumber === null) {
							_.each($scope.dullGrade.cutterNames, function(cn) {
								if(p.markedForLotNumber === true){
									needToAddLot = true;
								}
							});
						}
					}
				});
			});
		});
		if(needToAddLot){
			toastr.warning("You need to add a lot number to an added cutter before being able to stop!");
		} else {
			$scope.showStop = true;
		}
		// }
	};

	$scope.goBackToDullGradeHome = () => {
		setTimeout(function() {
			$location.path('/dullgrade-home')
			$scope.$apply()
		}, 1000)
	}

	$scope.goToToolLocator = (password) => {
		if (!password) toastr.warning('You must provide a password to logout.')
		else {
			$scope.currentUrl = $location.path()
			var url = Global.baseUrl + 'User/checkManagerCredentials'
			var body = {
				password: password
			}

			$scope.managerPassord = null

			$http.post(url, body).success(function(data) {
				$scope.currentAuthenticatedUsers.push(data);
				debugger;
				$('#modalManagerToolLocatorAccess').modal('hide')
				if($scope.workStation.facilityId === 1){
					setTimeout(function () {
						$location.path('/bitlocator')
						$scope.$apply()
					}, 1000)
				} else {
					setTimeout(function () {
						$location.path('/dnrlocator')
						$scope.$apply()
					}, 1000)
				}
				
			}).error(function() {
				toastr.error('You did not provide the correct password.')
			})
		}
	}

	$scope.returnFromBitLocatorToLastUrl = () => {
		$scope.currentAuthenticatedUsers = [];
		$scope.selectedTools = [];
		setTimeout(function() {
			$location.path($scope.currentUrl)
			$scope.$apply()
		}, 500)
	}

	$scope.goToReplaceCutters = function() {
		$scope.currentUrl = $location.path()
		setTimeout(function() {
			$location.path('/replacecutters')
			$scope.$apply()
		}, 1000)
	};

	// *********************************
	// Third Party Inspection
	// *********************************
	$scope.baseUrl = Global.baseUrl;

	$scope.createImageRecord = function(imgRecord, cb) {
		var url = Global.baseUrl + 'dullgradelevelone/createImageRecord';
		if(imgRecord.length){
			var data = {
				imgRecord: imgRecord
			}
		} else {
			var data = imgRecord
		}
		gPost(url, data, function (err, response) {
			if (err) toastr.error("There was a problem creating image record.");
			else {
				if (cb) cb(response)
			}
		});
	};

	$scope.getPicturesForThisRepair = function(repairId) {
		if (repairId) {
			var url = Global.baseUrl + 'dullgradelevelone/getPicturesForThisRepair';
			var data = {
				repairId: repairId
			};
			gPost(url, data,function(err,res) {
				if (err) toastr.error('There was a problem trying to get the pictures.');
				else {
					if (res) {
						var pics = res;
						var counter = 0;
						_.each($scope.pictures, function (picture) {
							if (pics.length > 0) {
								var match = false;
								_.each(pics, function (pic) {
									var arr = pic.name.split('|');
									var nameToCompare = arr[1].trim();
									if (nameToCompare === picture.name) {
										match = true;
										counter++
										var dateAdded = moment(pic.dateUploaded).format('ll');
										picture.name = nameToCompare;
										picture.dateAdded = dateAdded;
										picture.url = '/home/ubuntusdpi/sdpidocuments/thirdpartyinspectiondocuments/' + pic.documentId + '.' + pic.fileExtension;

										picture.picTaken = true;
										picture.encryptedName = pic.encryptedName;
										picture.documentId = pic.documentId;
										picture.fileExtension = pic.fileExtension;
									}
								});
								if (!match) {
									picture.url = '';
									picture.picTaken = false;
									delete picture.dateAdded;
									delete picture.encryptedName;
									delete picture.documentId;
									delete picture.fileExtension;
								}
							} else {
								picture.url = '';
								picture.picTaken = false;
								delete picture.dateAdded;
								delete picture.encryptedName;
								delete picture.documentId;
								delete picture.fileExtension;
							}

						});
						_.each(pics, function (pic) {
							var arr = pic.name.split('|');
							var nameToCompare = arr[1].trim();
							if ($scope.thirdPartyInspectionImage) {
								if (nameToCompare === $scope.thirdPartyInspectionImage.name) {
									match = true;
									var dateAdded = moment(pic.dateUploaded).format('ll');
									$scope.thirdPartyInspectionImage.name = nameToCompare;
									$scope.thirdPartyInspectionImage.dateAdded = dateAdded;
									$scope.thirdPartyInspectionImage.url = '/home/ubuntusdpi/sdpidocuments/thirdpartyinspectiondocuments/' + pic.documentId + '.' + pic.fileExtension;
									$scope.thirdPartyInspectionImage.picTaken = true;
									$scope.thirdPartyInspectionImage.encryptedName = pic.encryptedName;
									$scope.thirdPartyInspectionImage.documentId = pic.documentId;
									$scope.thirdPartyInspectionImage.fileExtension = pic.fileExtension;
								//}  else if (nameToCompare === $scope.billOfLadingImage.name) {
								// 	match = true;
								// 	var dateAdded = moment(pic.dateUploaded).format('ll');
								// 	$scope.billOfLadingImage.name = nameToCompare;
								// 	$scope.billOfLadingImage.dateAdded = dateAdded;
								// 	$scope.billOfLadingImage.url = '/home/ubuntusdpi/sdpidocuments/billofladingreceiveddocuments/' + pic.documentId + '.' + pic.fileExtension;
								// 	$scope.billOfLadingImage.picTaken = true;
								// 	$scope.billOfLadingImage.encryptedName = pic.encryptedName;
								// 	$scope.billOfLadingImage.documentId = pic.documentId;
								// 	$scope.billOfLadingImage.fileExtension = pic.fileExtension;
								 } else if (nameToCompare === $scope.thirdPartyInspectionAcknowledgement.name) {
									match = true;
									var dateAdded = moment(pic.dateUploaded).format('ll');
									$scope.thirdPartyInspectionAcknowledgement.name = nameToCompare;
									$scope.thirdPartyInspectionAcknowledgement.dateAdded = dateAdded;
									$scope.thirdPartyInspectionAcknowledgement.url = '/home/ubuntusdpi/sdpidocuments/thirdpartyinspectiondocuments/' + pic.documentId + '.' + pic.fileExtension;
									$scope.thirdPartyInspectionAcknowledgement.picTaken = true;
									$scope.thirdPartyInspectionAcknowledgement.encryptedName = pic.encryptedName;
									$scope.thirdPartyInspectionAcknowledgement.documentId = pic.documentId;
									$scope.thirdPartyInspectionAcknowledgement.fileExtension = pic.fileExtension;
								}
							} else if ($scope.thirdPartyInspectionImageTwo) {
								if (nameToCompare === $scope.thirdPartyInspectionImageTwo.name) {
									match = true;
									var dateAdded = moment(pic.dateUploaded).format('ll');
									$scope.thirdPartyInspectionImageTwo.name = nameToCompare;
									$scope.thirdPartyInspectionImageTwo.dateAdded = dateAdded;
									$scope.thirdPartyInspectionImageTwo.url = '/home/ubuntusdpi/sdpidocuments/finalQualityImage/' + pic.documentId + '.' + pic.fileExtension;
									$scope.thirdPartyInspectionImageTwo.picTaken = true;
									$scope.thirdPartyInspectionImageTwo.encryptedName = pic.encryptedName;
									$scope.thirdPartyInspectionImageTwo.documentId = pic.documentId;
									$scope.thirdPartyInspectionImageTwo.fileExtension = pic.fileExtension;
								}
								if (nameToCompare === $scope.paintedBoxEndImage.name) {
									match = true;
									var dateAdded = moment(pic.dateUploaded).format('ll');
									$scope.paintedBoxEndImage.name = nameToCompare;
									$scope.paintedBoxEndImage.dateAdded = dateAdded;
									$scope.paintedBoxEndImage.url = '/home/ubuntusdpi/sdpidocuments/finalQualityImage/' + pic.documentId + '.' + pic.fileExtension;
									$scope.paintedBoxEndImage.picTaken = true;
									$scope.paintedBoxEndImage.encryptedName = pic.encryptedName;
									$scope.paintedBoxEndImage.documentId = pic.documentId;
									$scope.paintedBoxEndImage.fileExtension = pic.fileExtension;
								}
								if (nameToCompare === $scope.paintedPinEndImage.name) {
									match = true;
									var dateAdded = moment(pic.dateUploaded).format('ll');
									$scope.paintedPinEndImage.name = nameToCompare;
									$scope.paintedPinEndImage.dateAdded = dateAdded;
									$scope.paintedPinEndImage.url = '/home/ubuntusdpi/sdpidocuments/finalQualityImage/' + pic.documentId + '.' + pic.fileExtension;
									$scope.paintedPinEndImage.picTaken = true;
									$scope.paintedPinEndImage.encryptedName = pic.encryptedName;
									$scope.paintedPinEndImage.documentId = pic.documentId;
									$scope.paintedPinEndImage.fileExtension = pic.fileExtension;
								}
							} else if ($scope.inspectionSheetImage) {
								if (nameToCompare === $scope.inspectionSheetImage.name) {
									match = true;
									var dateAdded = moment(pic.dateUploaded).format('ll');
									$scope.inspectionSheetImage.name = nameToCompare;
									$scope.inspectionSheetImage.dateAdded = dateAdded;
									$scope.inspectionSheetImage.url = '/home/ubuntusdpi/sdpidocuments/initialdullgradedocuments/' + pic.documentId + '.' + pic.fileExtension;
									$scope.inspectionSheetImage.picTaken = true;
									$scope.inspectionSheetImage.encryptedName = pic.encryptedName;
									$scope.inspectionSheetImage.documentId = pic.documentId;
									$scope.inspectionSheetImage.fileExtension = pic.fileExtension;
								}
							} 
						});
						$scope.$broadcast('getPicturesForRepairRan')
						if (pics) $scope.pics = pics;
						else $scope.pics = [];
						var qtyLeft = $scope.pictures.length - counter;
						if (qtyLeft < 0) qtyLeft = 0;
						$scope.minPicturesLeftToTake = qtyLeft;
						// url = Global.baseUrl + 'dullgradelevelone/getQtyPicturesRequiredObject';
						// $http.get(url).success(function (proVar) {
						// 	if (proVar) {
						// 		var qtyLeft = parseInt(proVar.varValue) - $scope.pics.length;
						// 		if (qtyLeft < 0) qtyLeft = 0;
						// 		$scope.minPicturesLeftToTake = qtyLeft;
						// 	}
						// }).error(function (err) {
						// 	toastr.error('There was a problem getting the qty of pictures required in order to send this tool to Dull Grade. Check with IT.');
						// });
					}
				}
			});
		}
	};

	$scope.get32Inches = function(cb) {
		var url = Global.baseUrl + 'dullgradelevelone/get32Inches'
		$http.get(url).success(function(data) {
			if (cb) cb(data)
		}).error(function(err) {
			toastr.error("There was a problem getting 32Inches from database.");
		});
	}

	$scope.goToDnrKilnHome = function (ws, cb) {
		var url = Global.baseUrl + 'User/setfacilityandworkstation';
		var data = {
			workstation: ws.workStationId,
			facility: $scope.workStation.facilityId
		};
		$http.post(url, data).success(function(success) {
			var goToUrl = '/dnrKilnHome';
			if(cb) cb();
			setTimeout(function() {
				$location.path(goToUrl);
				$scope.getStationUsers();
				$scope.currentAuthenticatedUsers = [];
				$scope.$apply();
			}, 500);
		}).error(function(err){
			toastr.error('There was a problem going to kiln.');
		});
	}

	$scope.openModalMarkAsNewTool = function() {
		$('#modalMarkAsNewTool').modal('show')
	}

	$scope.markAsNewTool = function() {
		let tempGroups = JSON.parse(JSON.stringify($scope.groups))
		for (let group of tempGroups) {
			for (let blade of group.blades) {
				for (let row of blade.rows) {
					for (let pocket of row.pockets) {
						pocket.cutterConditionId = undefined
						pocket.cutterCondition = undefined
						pocket.cutterActionId = 1
						pocket.cutterAction = '/N'
					}
				}
			}
		}
		$scope.groups = tempGroups
		$('#modalMarkAsNewTool').modal('hide')
		toastr.info('All pockets were marked as new!')
	}

	$scope.didAssignAllLotNumbers = function() {
		let totNew = 0
		let totLotsAssigned = 0
		if($scope.activeColorClasses && $scope.activeColorClasses.length > 0) {
			for (let acc of $scope.activeColorClasses) {
				totNew += acc.newQty
				for (let lot of acc.lots) {
					totLotsAssigned += lot.qty
				}
			}
			if(totNew - totLotsAssigned === 0) return true
			else return false
		} else return false
	}

	$scope.getDNRShipToStockPoints = () => {
		var url = Global.baseUrl + 'dullgradelevelone/getDNRShipToStockPoints';
		$http.get(url).success(function(stockPoints) {
			$scope.DNRShipStockPoints = stockPoints;
		}).error(function(err){
			toastr.error("There was a problem getting DNR ship to stock points.");
		})
	}
	$scope.getUserStockPoints = () => {
		var url = Global.baseUrl + 'dullgradelevelone/getUserStockPoints';
		$http.get(url).success(function (stockPoints) {
			$scope.userStockPoints = stockPoints;
		}).error(function (err) {
			toastr.error("There was a problem getting DNR ship to stock points.");
		})
	}
}])

var didTouchMovie = false

app.directive('threeDimensionalPanel', function() {
	return {
		replace: false,
		scope: false,
		link: function(scope, element, attr) {

			var groupIndex = typeof attr.gindex === 'undefined' ? 0 : parseInt(attr.gindex)

			var swipe = { sX: 0, sY: 0, eX: 0, eY: 0 }

			var minX = 30,
				maxX = 30,
				minY = 50,
				maxY = 60

			element[0].addEventListener('touchstart', function(e) {
				didTouchMove = false
				swipe.sX = e.touches[0].clientX
				swipe.sY = e.touches[0].clientY
			})

			element[0].addEventListener('touchmove', function(e) {
				if (!didTouchMove) {
					if (!swipe.sX || !swipe.sY) {
						return
					}
					swipe.eX = e.touches[0].clientX
		    swipe.eY = e.touches[0].clientY

					if (Math.abs(swipe.sY - swipe.eY) < 50 && Math.abs(swipe.sX - swipe.eX) > 80) {

						var currentDeg = $(e.target).data('deg')

						var transformProp = Modernizr.prefixed('transform')
						var deg = $(e.target).data('deg')
						var currentRotateYDegree = typeof groupIndex === 'undefined' || typeof scope.groups[groupIndex].currentRotateYDegree === 'undefined' ? 0 : scope.groups[groupIndex].currentRotateYDegree

						if (swipe.eX > swipe.sX) {
							// swiped right
							currentRotateYDegree = currentRotateYDegree + scope.degreeIncrement
						} else {
							// swiped left
							currentRotateYDegree = currentRotateYDegree - scope.degreeIncrement
						}

						var carousel = $('.carousel').get(groupIndex)

						console.log(currentDeg + ' | ' + currentRotateYDegree)
						carousel.style = 'transform: translateZ( ' + scope.translateZValue + 'px ) rotateY(' + currentRotateYDegree + 'deg)'
						carousel.dataset.deg = currentRotateYDegree

						scope.groups[groupIndex].currentRotateYDegree = currentRotateYDegree
						didTouchMove = true
					}
				}
			})
		}
	}
})

app.directive('printClearout', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: true,
		link: function postLink(scope, iElement, iAttrs) {
			jQuery('clearout-print').on('click', function(e) {
				alert('hello')
			})
		}
	}
})

app.directive('keypress', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: true,
		link: function postLink(scope, iElement, iAttrs) {
			jQuery(document).on('keypress', function(e) {
				scope.$apply(scope.keyPressed(e))
			})
		}
	}
})
app.directive('rotatebit', function () {
	return {
		restrict: "A",
		link: function (scope, elem, attrs) {
			//On click
			var e = {};
			$(elem).click(function (e) {
				e.charCode = $(this).data('code');
				scope.$apply(scope.keyPressed(e))
			});
		}
	}
})

app.directive('groupIndexAuto', function() {
	return {
		scope: false,
		link: function(scope, element, attrs) {
			(function($) {
				function setOnScroll() {
					setTimeout(function() {
						let containerTops = []
						$('.group-container').each(function() {
							containerTops.push($(this).offset().top)
						})

						$(window).scroll(function() {
							let counter = 0
							for (let top of containerTops) {
								if($(window).scrollTop() + (($(window)[0].innerHeight / 2) * .9) > top) {
									scope.$parent.groupIndex = counter
									for (let group of scope.$parent.groups) {
										group.blur = true
									}
									scope.groups[counter].blur = false

									scope.$apply();
								}
								counter++
							}
						})
					}, 1000)
				}
				$(document).ready(function() {
					scope.$on('getDullGradeStructureForCurrentRepair:executed', function() {
						setOnScroll()
					})
					scope.$on('getCutterOrHashStructure:executed', function() {
						setOnScroll()
					})
				})
			})(jQuery)
		}
	}
})

app.filter('reverse', function() {
	return function(items) {
		if (typeof items !== 'undefined' && angular.isArray(items)) {
			return items.slice().reverse()
		}
	}
})

app.filter('newfilter', function() {
	return function(items) {
		if(items && items.length > 0) {
			return items.filter(function (item) {
				return item.newQty && item.newQty > 0
			})
		} else return items
	}
})

app.filter('floor', function() {
	return function(input) {
		return Math.floor(input)
	}
})

app.config(function(IdleProvider, KeepaliveProvider) {
	IdleProvider.idle(1795)
	IdleProvider.timeout(5)
	KeepaliveProvider.interval(10)
})
