app.controller('NgBondoPaintController', ['$http', '$scope', '$location', 'Global', '$filter', function ($http, $scope, $location, Global, $filter) {
	function init() {
		// configuration
		$scope.$parent.maxUsers = 1;
		$scope.$parent.finishedAddingTools = true;
		$scope.$parent.needDullGradeRecords = false;
		$scope.$parent.needCutterStructure = false;
		$scope.showBackButtonOnStop = true;
		$scope.bondoPaintUsers = [];
		$scope.$parent.showBitLocator = true;

		// initiate arrays and objects
		$scope.$parent.selectedTools = [];
		$scope.getFlowRecords();
		$scope.$on('getFlowRecordsRan', function() {
			debugger;
			if ($scope.bondoPaintUsers.length === 0) {
				$scope.bondoPaintUsers = $scope.$parent.allowedUsers;
			}
			if ($scope.$parent.allowedUsers.length !== $scope.bondoPaintUsers.length) {
				$scope.$parent.allowedUsers = $scope.bondoPaintUsers;
			}
			$scope.bondoStationRecords = _.filter($scope.stationRecords, function(sr) {
				return sr.flowGroupId === 1;
			});
			$scope.paintStationRecords = _.filter($scope.stationRecords, function(sr) {
				return sr.flowGroupId === 2;
			});

			angular.forEach($scope.bondoStationRecords, function(bsr) {
				var running = _.filter($scope.currentRunningLaborTimeStamps, function(crlts) {
					if(crlts.repairId === bsr.repairId){
						_.filter($scope.currentAuthenticatedUsers, function(cau){
							if(crlts.userId === cau.userId){
								bsr.currentUser = cau;
								bsr.runningTimeStamp = crlts;
								bsr.running = true;
								return crlts.repairId === bsr.repairId;
							}
						});
					}
				});
			});
			angular.forEach($scope.paintStationRecords, function(psr) {
				var running = _.filter($scope.currentRunningLaborTimeStamps, function(crlts) {
					if(crlts.repairId === psr.repairId){
						_.filter($scope.currentAuthenticatedUsers, function(cau){
							if(crlts.userId === cau.userId){
								psr.currentUser = cau;
								psr.runningTimeStamp = crlts;
								psr.running = true;
								return crlts.repairId === psr.repairId;
							}
						});
					}
				});
			});

			$scope.$parent.currentAuthenticatedUsers = [];
			$scope.$parent.currentRunningLaborTimeStamps = [];
			$scope.$parent.selectedTools = [];
			$scope.$parent.finishedAddingTools = true;
		});
	}

	init();

	$scope.userLogout = function() {
		$scope.$parent.currentAuthenticatedUsers = [];
		$scope.getStationUsers();
	};

	$scope.showStopScreen = function(selected, show) {
		if(show === false) {
			$scope.$parent.showStop = false;
			$scope.$parent.currentAuthenticatedUsers = [];
			$scope.$parent.currentRunningLaborTimeStamps = [];
			$scope.$parent.selectedTools = [];
		} else {
			$scope.$parent.openTimeStampStartTime = selected.runningTimeStamp.startTime;
			$scope.$parent.currentAuthenticatedUsers.push(selected.currentUser);
			$scope.$parent.currentRunningLaborTimeStamps.push(selected.runningTimeStamp);
			$scope.$parent.selectedTools.push(selected);
			$scope.$parent.showStop = true;
		}

	};
}]);
